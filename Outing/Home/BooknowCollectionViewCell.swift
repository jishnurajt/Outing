//
//  BooknowCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 08/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class BooknowCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var imageviewcars: UIImageView!
    
    @IBOutlet weak var lblname: UILabel!
    
    @IBOutlet weak var lblnumber: UILabel!
    
    @IBOutlet weak var btnbooknow: UIButton!
    
}
