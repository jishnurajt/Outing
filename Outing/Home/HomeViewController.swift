//
//  HomeViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 08/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher


class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var collectionviewbrandheight: NSLayoutConstraint!
    @IBOutlet weak var imagebrandsheight: NSLayoutConstraint!
    @IBOutlet weak var collectionviewcarimageheight: NSLayoutConstraint!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var collectionviewcarimages: UICollectionView!
    @IBOutlet weak var collectionviewbrands: UICollectionView!
    var screenSize  = UIScreen.main.bounds
    var screenWidth = UIScreen.main.bounds.width
    var carimage=[String]()
    var carprices=[String]()
    var carnumber=[Int]()
    
    var homemodel=Homeviewmodel()
    var carcount=Int()
    var yatchcount=Int()
    var helicoptercount=Int()
    var carbrands:[Brands]?=[]
    var imageurl=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var carstatus=Int()
    var helistatus=Int()
    var yatchstatus=Int()
    var packagestatus=Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        homemodel.brands{ (model) in
            self.brandsdata(data:model)
        }
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            imagebrandsheight.constant=140
           // collectionviewbrandheight.constant=70
        }else{
            if screenSize.height>800{
                imagebrandsheight.constant=110
              //  collectionviewbrandheight.constant=50
            }else{
                imagebrandsheight.constant=90
               // collectionviewbrandheight.constant=40
            }
        }
        
        collectionviewbrands.layer.cornerRadius = collectionviewbrands.frame.height/2
        collectionviewbrands.clipsToBounds = true
        
    }
    //    func numberOfSections(in collectionView: UICollectionView) -> Int{
    //
    //           return 1
    //       }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewcarimages{
            return carnumber.count ?? 0
        }else{
            return carbrands?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView==collectionviewcarimages{
            //collectionviewcarimageheight.constant=screenSize.height-20
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "BooknowCollectionViewCell",
                                                           for: indexPath) as! BooknowCollectionViewCell
            cell1.lblname.text=carprices[indexPath.row]
            
            if indexPath.item==2{
                cell1.lblnumber.text=""
            }else if indexPath.item==1{
                cell1.lblnumber.text=String(carnumber[indexPath.row])+" Yachts"
            }else if indexPath.item==0{
                cell1.lblnumber.text=String(carnumber[indexPath.row])+" Cars"
            }else{
                cell1.lblnumber.text=""
            }
            cell1.imageviewcars.image=UIImage(named:carimage[indexPath.row])
            
//            if UIDevice.current.userInterfaceIdiom == .pad{
//                cell1.imageviewcars.contentMode = .scaleToFill
//            }else{
//                if screenSize.height>800{
//                    cell1.imageviewcars.contentMode = .scaleToFill
//                }else{
//                    cell1.imageviewcars.contentMode = .scaleToFill
//                }
//            }
            
            
            return cell1
        }else{
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "CarbrandsCollectionViewCell",
                                                           for: indexPath) as! CarbrandsCollectionViewCell
            // let totalimage=imageurl+brands?[indexPath.row].brand_image ?? ""
            // cell2.imageviewbrands.image=brands[indexPath.row]
            let url = URL(string:imageurl+(carbrands?[indexPath.row].brand_image ?? ""))
            
            cell2.imageviewbrands.kf.indicatorType = .activity
            cell2.imageviewbrands.kf.setImage(with: url)
            if UIDevice.current.userInterfaceIdiom == .pad{
                cell2.imageviewbrands.contentMode = .scaleAspectFit
            }else{
                if screenSize.height>800{
                     cell2.imageviewbrands.contentMode = .scaleAspectFit
                }else{
                    cell2.imageviewbrands.contentMode = .scaleAspectFit
                }
            }
           
            return cell2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView==collectionviewcarimages{
            if indexPath.item==0{
                if carstatus==1{
                let car = self.storyboard?.instantiateViewController (withIdentifier: "CarViewController") as! CarViewController
                if cart?.count ?? 0>0{
                 car.cart=cart
                 car.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(car, animated: true)
                }else{
                    let comingsoon = self.storyboard?.instantiateViewController (withIdentifier: "ComingsoonViewController") as! ComingsoonViewController

                    self.navigationController?.pushViewController(comingsoon, animated: true)
                }
            }else if indexPath.item==1{
//                 if yatchstatus==1{
                let yatch = self.storyboard?.instantiateViewController (withIdentifier: "YatchViewController") as! YatchViewController
                if cart?.count ?? 0>0{
                 yatch.cart=cart
                 yatch.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(yatch, animated: true)
//                 }else{
//                    let comingsoon = self.storyboard?.instantiateViewController (withIdentifier: "ComingsoonViewController") as! ComingsoonViewController
//                                      
//                        self.navigationController?.pushViewController(comingsoon, animated: true)
//                }
                
            }else if indexPath.item==2{
                if helistatus==1{
                let helicopter = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterViewController") as! HelicopterViewController
                if cart?.count ?? 0>0{
                 helicopter.cart=cart
                 helicopter.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(helicopter, animated: true)
                }else{
                    let comingsoon = self.storyboard?.instantiateViewController (withIdentifier: "ComingsoonViewController") as! ComingsoonViewController
                                  
                    self.navigationController?.pushViewController(comingsoon, animated: true)
                }
//            }else if indexPath.item==3{
//                if packagestatus==1{
//                let package = self.storyboard?.instantiateViewController (withIdentifier: "PackageViewController") as! PackageViewController
//                if cart?.count ?? 0>0{
//                 package.cart=cart
//                 package.imageurlgift=imageurlgift
//                }
//                self.navigationController?.pushViewController(package, animated: true)
//                }else{
//                    let comingsoon = self.storyboard?.instantiateViewController (withIdentifier: "ComingsoonViewController") as! ComingsoonViewController
//
//                                       self.navigationController?.pushViewController(comingsoon, animated: true)
//                }
            }else{
                let addon = self.storyboard?.instantiateViewController (withIdentifier: "AddonViewController") as! AddonViewController
                if cart?.count ?? 0>0{
                addon.cart=cart
                addon.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(addon, animated: true)
            }
        }else{
            let car = self.storyboard?.instantiateViewController (withIdentifier: "CarViewController") as! CarViewController
            car.brandidfromhome=carbrands?[indexPath.row].brand_id ?? ""
            car.carbrandflag=1
            if cart?.count ?? 0>0{
             car.cart=cart
             car.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(car, animated: true)
        }
    }
    
    //    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //    {
    //
    //
    //
    //    }
    func brandsdata(data: Homeclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.carcount=data.data?.car_count ?? 0
                self.yatchcount=data.data?.yacht_count ?? 0
                self.helicoptercount=data.data?.helicopter_count ?? 0
                self.carnumber.append(self.carcount)
                self.carnumber.append(self.yatchcount)
                self.carnumber.append(self.helicoptercount)
                self.carnumber.append(0)
                //self.carnumber.append(0)
                self.carbrands=data.data?.brands
                self.imageurl=data.data?.image_url ?? ""
               // self.carimage=["bg","bg-2","bg-3","bg-pckage","bg-addon"]
               // self.carprices=["Cars","Yachts","Helicopter","Packages","Addons"]
                 self.carimage=["bg","bg-2","bg-3","bg-addon"]
                 self.carprices=["Cars","Yachts","Helicopter","Gifts"]
                self.carstatus=data.data?.car_status ?? 0
              
                 self.helistatus=data.data?.helicopter_status ?? 0
                 self.yatchstatus=data.data?.yacht_status ?? 0
//                 self.yatchstatus=1
                 self.packagestatus = 0 //  data.data?.package_status ?? 0
                self.collectionviewbrands.reloadData()
                self.collectionviewcarimages.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    @IBAction func actionSideMenu(_ sender: Any) {
//        if let sideMenu = self.sideMenuController {
//            sideMenu.showLeftViewAnimated()
//        }
        
//        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
//          let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
//        vc?.modalPresentationStyle = .fullScreen
//         vc?.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc!, animated: false,completion: nil)
        
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
         navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
         vc?.cart=cart
         vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
                   
        
    }
    
    @IBAction func btnsearchaction(_ sender: Any) {
        let search = self.storyboard?.instantiateViewController (withIdentifier: "SearchViewController") as! SearchViewController
        if cart?.count ?? 0>0{
         search.cart=cart
         search.imageurlgift=imageurlgift
        }
        self.navigationController?.pushViewController(search, animated: true)
    }
    
}


extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView==collectionviewcarimages{
            return CGSize(width: collectionView.frame.width*0.65, height: collectionView.frame.height-4)
        }else{
                return CGSize(width:collectionView.frame.width/3 - 20, height: collectionView.frame.height)

        }
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            //if collectionView==collectionviewcarimages{
                return 20

        }
}
