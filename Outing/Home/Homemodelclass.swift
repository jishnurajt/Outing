//
//  Homemodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 14/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
struct Homeclass : Codable {
    let status : Bool?
    let message : String?
    let data : HomeData?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(HomeData.self, forKey: .data)
    }
    
}
struct Brands : Codable {
    let brand_id : String?
    let brand_name : String?
    let category_id : String?
    let brand_image : String?
    let description : String?
    let created_at_date : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case brand_id = "brand_id"
        case brand_name = "brand_name"
        case category_id = "category_id"
        case brand_image = "brand_image"
        case description = "description"
        case created_at_date = "created_at_date"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        brand_id = try values.decodeIfPresent(String.self, forKey: .brand_id)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        brand_image = try values.decodeIfPresent(String.self, forKey: .brand_image)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}
struct HomeData : Codable {
    let brands : [Brands]?
    let car_count : Int?
    let helicopter_count : Int?
    let yacht_count : Int?
    let image_url : String?
    let car_status:Int?
    let package_status:Int?
    let helicopter_status:Int?
    let yacht_status:Int?
    enum CodingKeys: String, CodingKey {
        
        case brands = "brands"
        case car_count = "car_count"
        case helicopter_count = "helicopter_count"
        case yacht_count = "yacht_count"
        case image_url = "image_url"
        case car_status = "car_status"
        case package_status = "package_status"
        case helicopter_status = "helicopter_status"
        case yacht_status = "yacht_status"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        brands = try values.decodeIfPresent([Brands].self, forKey: .brands)
        car_count = try values.decodeIfPresent(Int.self, forKey: .car_count)
        helicopter_count = try values.decodeIfPresent(Int.self, forKey: .helicopter_count)
        yacht_count = try values.decodeIfPresent(Int.self, forKey: .yacht_count)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        car_status = try values.decodeIfPresent(Int.self, forKey: .car_status)
        package_status = try values.decodeIfPresent(Int.self, forKey: .package_status)
        helicopter_status = try values.decodeIfPresent(Int.self, forKey: .helicopter_status)
        yacht_status = try values.decodeIfPresent(Int.self, forKey: .yacht_status)
    }
    
}

