//
//  ForgotpasswordViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit


class ForgotpasswordViewController: UIViewController {

    @IBOutlet weak var btnsendotp: UIButton!
    @IBOutlet weak var txtfldotp: UITextField!
    @IBOutlet weak var txtfldpassword: UITextField!
    @IBOutlet weak var viewpasswordedit: UIView!
    @IBOutlet weak var txtfldemail: UITextField!
    var loginmodel=Loginviewmodel()
    var sendotpfalg=0
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    override func viewDidLoad() {
        super.viewDidLoad()

        btnsendotp.layer.cornerRadius=15
        txtfldemail.placeholeder(name: "Email")
        //txtfldemail.setLeftPaddingPoints(30)
        txtfldpassword.placeholeder(name: "Password")
      //  txtfldpassword.setLeftPaddingPoints(30)
        txtfldotp.placeholeder(name: "OTP")
     //   txtfldotp.setLeftPaddingPoints(30)
        
        
             txtfldemail.setcornerradius1()
             txtfldpassword.setcornerradius1()
        txtfldotp.setcornerradius1()
        
        
           hideKeyboardWhenTappedAround()
           
    }
    

    @IBAction func btnsendotpaction(_ sender: Any) {
        if sendotpfalg==1||btnsendotp.currentTitle=="Confirm"{
            
            if (txtfldpassword.text == ""){
                
                self.showToast(message: "Please enter your password", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                
                
            }else if (txtfldotp.text == ""){
            
            self.showToast(message: "Please enter your Otp", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
            
            }else if (txtfldemail.text == ""){
            
            self.showToast(message: "Please enter your email", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
            
        }else if !isValidEmail(testStr: txtfldemail.text!){
                
                self.showToast(message: "Please enter valid email id", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
            
            
            
        }else{
         loginmodel.otpforpassword=txtfldotp.text ?? ""
            loginmodel.passwordforcreatepassword=txtfldpassword.text ?? ""
                loginmodel.emailforpassword=txtfldemail.text ?? ""
                    
                    loginmodel.create_new_password{ (model) in
                        self.create_new_passworddata(data:model)
                    }
                }
            
        }else{
            
            
            if (txtfldemail.text == ""){
                
                self.showToast(message: "Please enter your email", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                
                
            }else if !isValidEmail(testStr: txtfldemail.text!){
                    
                    self.showToast(message: "Please enter valid email id", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                
                
                
                
            }else{
             
                
                    loginmodel.emailforpassword=txtfldemail.text ?? ""
                        
                        loginmodel.forgot_password{ (model) in
                            self.forgot_passworddata(data:model)
                        }
                    }
            
            
        }
                
        
    }
                func isValidEmail(testStr:String) -> Bool {
                    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
                    
                    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
                    return emailTest.evaluate(with: testStr)
                }
        
        
        
        func forgot_passworddata(data: Forgotpasswordclass) {
            print("data",data)
            
            let status=data.status
            if status==true{
                DispatchQueue.main.async{
                     self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                    self.viewpasswordedit.isHidden=false
                    self.btnsendotp.setTitle("Confirm", for: .normal)
                    self.sendotpfalg=1
                }
            }else{
                DispatchQueue.main.async{
                    self.viewpasswordedit.isHidden=true
                    self.btnsendotp.setTitle("Send OTP", for: .normal)
                    self.sendotpfalg=0
                    self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                   
                }
            }
        }
            
        
        func create_new_passworddata(data: Createnewpasswrdclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                 self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
                let userid=data.data?.user_id
                UserDefaults.standard.set(userid, forKey: "userid")
                if self.cart?.count ?? 0>0{
                    home.cart=self.cart
                    home.imageurlgift=self.imageurlgift
                }
                       self.navigationController?.pushViewController(home, animated: true)
              
            }
        }else{
            DispatchQueue.main.async{
               
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
               
            }
        }
    }
        
        
    

     @IBAction func btnloginnow(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
        if cart?.count ?? 0>0{
        login.cart=cart
        login.imageurlgift=imageurlgift
        }
         self.navigationController?.pushViewController(login, animated: true)
     }
    
    @IBAction func btnregisteraction(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController (withIdentifier: "RegisterViewController") as! RegisterViewController
        if cart?.count ?? 0>0{
        login.cart=cart
        login.imageurlgift=imageurlgift
        }
        
         self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    @IBAction func btncontinueasguestaction(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
        if cart?.count ?? 0>0{
        login.cart=cart
        login.imageurlgift=imageurlgift
        }
        
         self.navigationController?.pushViewController(login, animated: true)
    }
    

}
