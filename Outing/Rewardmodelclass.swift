//
//  Rewardmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 29/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct Point_history : Codable {
    let point_history_id : String?
    let user_id : String?
    let point : String?
    let type : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case point_history_id = "point_history_id"
        case user_id = "user_id"
        case point = "point"
        case type = "type"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        point_history_id = try values.decodeIfPresent(String.self, forKey: .point_history_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        point = try values.decodeIfPresent(String.self, forKey: .point)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct Point : Codable {
    let user_point_id : String?
    let user_id : String?
    let point : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_point_id = "user_point_id"
        case user_id = "user_id"
        case point = "point"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_point_id = try values.decodeIfPresent(String.self, forKey: .user_point_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        point = try values.decodeIfPresent(String.self, forKey: .point)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct Rewardclass : Codable {
    let status : Bool?
    let message : String?
    let data : RewardData?
    let percentage: String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
         case percentage = "percentage"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(RewardData.self, forKey: .data)
        percentage = try values.decodeIfPresent(String.self, forKey: .percentage)
    }

}

struct RewardData : Codable {
    let point : [Point]?
    let point_history : [Point_history]?

    enum CodingKeys: String, CodingKey {

        case point = "point"
        case point_history = "point_history"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        point = try values.decodeIfPresent([Point].self, forKey: .point)
        point_history = try values.decodeIfPresent([Point_history].self, forKey: .point_history)
    }

}
