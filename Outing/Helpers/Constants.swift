//
//  Constants.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

typealias WebServiceCompletionHandler = (_ status:Bool, _ message:String?, _ responseObject:AnyObject?, _ error:NSError?) -> Void
