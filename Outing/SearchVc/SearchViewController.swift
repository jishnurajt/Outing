//
//  SearchViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 04/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{

    @IBOutlet weak var lblnoitemfound: UILabel!
    @IBOutlet weak var tableviewsearch: UITableView!
    @IBOutlet weak var txtfldsearch: UITextField!
    var packagemodel=Packageviewmodel()
    var list:[Listsearch]?=[]
    var image:[Images]?=[]
    var carimage=String()
    var heliimage=String()
    var yatchimage=String()
    var giftimage=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfldsearch.layer.cornerRadius=20
        txtfldsearch.placeholeder(name: "Search here")
        txtfldsearch.setLeftPaddingPoints(43)
        txtfldsearch.delegate=self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return list?.count ?? 0
           
       }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
          let cell = (tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as? SearchTableViewCell)!
        var url:URL!
        cell.lblname.text=list?[indexPath.row].brand_model
        if list?[indexPath.row].type=="1"{
            url = URL(string:carimage+(list?[indexPath.row].images?[0].car_image ?? ""))
    }else if list?[indexPath.row].type=="2"{
              url = URL(string:heliimage+(list?[indexPath.row].images?[0].helicopter_image ?? ""))
        }else if list?[indexPath.row].type=="3"{
             url = URL(string:yatchimage+(list?[indexPath.row].images?[0].yacht_image ?? ""))
        }else if list?[indexPath.row].type=="4"{
             url = URL(string:giftimage+(list?[indexPath.row].images?[0].gift_image ?? ""))
        }
    
        
          cell.imageviewsearchitem.kf.indicatorType = .activity
          cell.imageviewsearchitem.kf.setImage(with: url)
          cell.imageviewsearchitem.contentMode = .scaleAspectFit
           return cell
        }
        
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
               
                   return 140
              
           }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if list?[indexPath.row].type=="1"{
                let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
                cardetail.carid = list?[indexPath.row].car_id ?? ""
               
                if cart?.count ?? 0>0{
                               cardetail.cart=cart
                               cardetail.imageurlgift=imageurlgift
                               }
                self.navigationController?.pushViewController(cardetail, animated: true)
            }else if list?[indexPath.row].type=="2"{
            let helicopter = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterViewController") as! HelicopterViewController
                if cart?.count ?? 0>0{
                helicopter.cart=cart
                helicopter.imageurlgift=imageurlgift
                }
                           self.navigationController?.pushViewController(helicopter, animated: true)
            
            }else if list?[indexPath.row].type=="3"{
                let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
                cardetail.yatchid = list?[indexPath.row].car_id ?? ""
                cardetail.yatchflag==1
                       
                if cart?.count ?? 0>0{
                cardetail.cart=cart
                cardetail.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(cardetail, animated: true)
                
            }else if list?[indexPath.row].type=="4"{
                
              let addon = self.storyboard?.instantiateViewController (withIdentifier: "AddonViewController") as! AddonViewController
                 addon.giftid = list?[indexPath.row].car_id ?? ""
                 addon.searchflag=1
                       
                if cart?.count ?? 0>0{
                addon.cart=cart
                addon.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(addon, animated: true)
                
                
            }
    }
    func textFieldDidBeginEditing(_ textField: UITextField){
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        packagemodel.keyword=string
        
         packagemodel.search{ (model) in
             self.searchdata(data:model)
         }
        
        return true
        
    }
    
    func searchdata(data: Searchclass) {
        print("data",data)
        var baseprice=Int()
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
              //  self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.list=data.data?.list
                self.carimage=data.data?.car_image_url ?? ""
                self.yatchimage=data.data?.car_image_url ?? ""
                self.heliimage=data.data?.helicopter_image_url ?? ""
                self.giftimage=data.data?.gift_image_url ?? ""
                self.lblnoitemfound.isHidden=true
                self.tableviewsearch.reloadData()
            }
        }else{
            DispatchQueue.main.async{
               // self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.lblnoitemfound.isHidden=false
                
            }
        }
    }
    
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
           
}
