//
//  SearchTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 04/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var imageviewsearchitem: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageviewsearchitem.layer.cornerRadius=10
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
