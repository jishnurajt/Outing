//
//  CartTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 23/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var btnremove: UIButton!
    @IBOutlet weak var imageviewcar: UIImageView!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblreturndate: UILabel!
    @IBOutlet weak var lblreceivedate: UILabel!
    @IBOutlet weak var lblcarname: UILabel!
    @IBOutlet weak var viewcart: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageviewcar.layer.cornerRadius=10
        viewcart.layer.cornerRadius=10
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
