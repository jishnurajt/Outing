//
//  PaynowTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class PaynowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnswitch: UISwitch!
    @IBOutlet weak var btnclose: UIButton!
    @IBOutlet weak var lblofferappliedtobill: UILabel!
    @IBOutlet weak var lbloffername: UILabel!
    @IBOutlet weak var lblofferapplied: UILabel!
    @IBOutlet weak var imageviewtag: UIImageView!
    @IBOutlet weak var imageviewclose: UIImageView!
    @IBOutlet weak var lblsubtotal: UILabel!

    @IBOutlet weak var lblavailablerewardpoints: UILabel!
    @IBOutlet weak var btnapplycouponheight: NSLayoutConstraint!
    @IBOutlet weak var btnwantoaddmoreheight: NSLayoutConstraint!
    @IBOutlet weak var btnpaynow: UIButton!
    @IBOutlet weak var lblcarttotalprice: UILabel!
    @IBOutlet weak var btnwanttoaddmore: UIButton!
    
    @IBOutlet weak var lblpayusingrewardpoint: UILabel!
    @IBOutlet weak var lblyouwillsaverewardpoints: UILabel!
    @IBOutlet weak var btnrewardponts: UIButton!
    @IBOutlet weak var btnapplycoupon: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnpaynow.layer.cornerRadius=20
        btnapplycoupon.layer.cornerRadius=15
         btnrewardponts.layer.cornerRadius=15
        btnrewardponts.layer.borderWidth=0.5
        btnrewardponts.layer.borderColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0).cgColor
        if UIDevice.current.userInterfaceIdiom == .pad{
            btnwantoaddmoreheight.constant=65
            btnapplycouponheight.constant=60
        }else{
             btnwantoaddmoreheight.constant=54
            btnapplycouponheight.constant=54
        }
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
