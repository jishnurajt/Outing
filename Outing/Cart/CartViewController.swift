//
//  CartViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 23/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import  CoreData



class CartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var cartname_array=[String]()
    var cartdate_array=[String]()
    var carprice_array=[String]()
    var cartime_array=[String]()
    
    @IBOutlet weak var btnsidemenu: UIButton!
    
    var carimage_array=[String]()
    var totalamount=0
    var cartdetail=[Cartdetail]()
    var userid=String()
    var productlistarray=[[String:Any]]()
    var gift=[[String:String]]()
    var dict_products=[String:Any]()
    
    var packageflag=Int()
    var offertotal=Int()
    var deleted=Int()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var totalamountaddon=0
    var plusminusflag=0
    var offer_id="0"
    var offertotalafteroffer=Int()
    var offerflag=Int()
    var offername=String()
    var rewardmodel=Rewardviewmodel()
    var percentage=Int()
    var point=0
    var pointafterreward=Int()
    var rewardamountafterreward=0
    var totalamountafterreward=Int()
    
    var rewardamount=Int()
    var switchflag=0
    
    let numberFormatter = NumberFormatter()
    
    @IBOutlet weak var tableviewcart: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberFormatter.numberStyle = .decimal
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        btnsidemenu.layer.masksToBounds=true
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            userid=UserDefaults.standard.value(forKey: "userid") as! String
        }
        //        rewardmodel.user_point{ (model) in
        //            self.userpointdata(data:model)
        //        }
        fetchdata()
        
        
        
    }
    
    
    func userpointdata(data: Rewardclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            
            
            DispatchQueue.main.async{
                if data.data?.point?.count ?? 0 > 0{
                    self.point=(data.data?.point?[0].point as! NSString).integerValue
                }
                //        self.point=600
                self.percentage=(data.percentage as! NSString).integerValue
                print("percentage",self.percentage)
                let div=self.totalamount*self.percentage
                print("totalamount",self.totalamount)
                print("div",div)
                self.rewardamount=div/100
                print("rewardamount",self.rewardamount)
                self.tableviewcart.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // return 2
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if section==0{
        //            return cartname_array.count
        //        }else{
        //            return 1
        //        }
        if section==0{
            return cartdetail.count
        }else if section==1{
            return cart!.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            if cartdetail[indexPath.row].flag == "4"{
                let cell = (tableView.dequeueReusableCell(withIdentifier: "Addoncart1TableViewCell", for: indexPath) as? Addoncart1TableViewCell)!
                cell.lbladdon.text=cartdetail[indexPath.row].carname
                
                    let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(carprice_array[indexPath.row]) ?? 0)) ?? ""
                    cell.lblprice.text="QAR "+formattedNumber
                
                cell.lblcount.text = String(cartdetail[indexPath.row].quantity ?? "0")
                let url = URL(string:cartdetail[indexPath.row].carimage ?? "")
                cell.imageviewcar.kf.indicatorType = .activity
                cell.imageviewcar.kf.setImage(with: url)
                cell.imageviewcar.contentMode = .scaleAspectFit
                cell.btnremovegift.tag=indexPath.row
                cell.btnremovegift.addTarget(self, action: #selector(btnremoveaction(sender:)), for: .touchUpInside)
                cell.btnplusgift.tag=indexPath.row
                cell.btnplusgift.addTarget(self, action: #selector(plusPressedA(sender:)), for: .touchUpInside)
                cell.btnminusgift.tag=indexPath.row
                cell.btnminusgift.addTarget(self, action: #selector(minusPressedA(sender:)), for: .touchUpInside)
                return cell
            }else{
                let cell = (tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as? CartTableViewCell)!
                cell.lblcarname.text=cartname_array[indexPath.row]
                cell.lblreceivedate.text=cartdate_array[indexPath.row]
                cell.lblreturndate.text="Time: "+cartime_array[indexPath.row]
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(carprice_array[indexPath.row]) ?? 0)) ?? ""
                cell.lblprice.text="QAR "+formattedNumber
                let url = URL(string:carimage_array[indexPath.row])
                cell.imageviewcar.kf.indicatorType = .activity
                cell.imageviewcar.kf.setImage(with: url)
                cell.imageviewcar.contentMode = .scaleAspectFit
                cell.btnremove.tag=indexPath.row
                cell.btnremove.addTarget(self, action: #selector(btnremoveaction), for: .touchUpInside)
                
                return cell
                
            }
        }else  if indexPath.section==1{
            let cell = (tableView.dequeueReusableCell(withIdentifier: "Addoncart1TableViewCell", for: indexPath) as? Addoncart1TableViewCell)!
            cell.lbladdon.text=cart![indexPath.row].title
            if cart?[indexPath.row].added_price==0{
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(cart![indexPath.row].base_price!) ?? 0)) ?? ""
                cell.lblprice.text="QAR "+formattedNumber
            }else{
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(cart![indexPath.row].added_price ))) ?? ""
                cell.lblprice.text="QAR "+formattedNumber
            }
            cell.lblcount.text = String(cart![indexPath.row].count)
            let url = URL(string:imageurlgift+(cart![indexPath.row].gift_images?[0].gift_image ?? ""))
            cell.imageviewcar.kf.indicatorType = .activity
            cell.imageviewcar.kf.setImage(with: url)
            cell.imageviewcar.contentMode = .scaleAspectFit
            cell.btnremovegift.tag=indexPath.row
            cell.btnremovegift.addTarget(self, action: #selector(removePressed(sender:)), for: .touchUpInside)
            cell.btnplusgift.tag=indexPath.row
            cell.btnplusgift.addTarget(self, action: #selector(plusPressed(sender:)), for: .touchUpInside)
            cell.btnminusgift.tag=indexPath.row
            cell.btnminusgift.addTarget(self, action: #selector(minusPressed(sender:)), for: .touchUpInside)
            return cell
            
        }else{
            
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "PaynowTableViewCell", for: indexPath) as? PaynowTableViewCell)!
            
            if packageflag==1{
                totalamount=totalamount-(totalamount*5/100)
                offertotal=totalamount*5/100
            }else{
                
            }
            if carprice_array.count==0&&cart?.count==0{
                
                cell1.lblcarttotalprice.isHidden=true
                cell1.lblsubtotal.isHidden=true
                cell1.btnpaynow.isHidden=true
                cell1.btnapplycoupon.isHidden=true
                cell1.imageviewtag.isHidden=true
                
                cell1.imageviewclose.isHidden=true
                cell1.lbloffername.isHidden=true
                cell1.lblofferappliedtobill.isHidden=true
                cell1.lblofferapplied.isHidden=true
                cell1.btnclose.isHidden=true
                cell1.btnrewardponts.isHidden=true
                cell1.lblavailablerewardpoints.isHidden=true
                cell1.lblyouwillsaverewardpoints.isHidden=true
                cell1.btnswitch.isHidden=true
                cell1.lblpayusingrewardpoint.isHidden=true
                
            }else{
                cell1.lblcarttotalprice.isHidden=false
                cell1.lblsubtotal.isHidden=false
                cell1.btnpaynow.isHidden=false
                cell1.btnapplycoupon.isHidden=false
                cell1.imageviewtag.isHidden=false
                
                cell1.imageviewclose.isHidden=false
                //   cell1.btnrewardponts.isHidden=false
                //  cell1.lblavailablerewardpoints.isHidden=false
                //   cell1.lblyouwillsaverewardpoints.isHidden=false
                //   cell1.btnswitch.isHidden=false
                //  cell1.lblpayusingrewardpoint.isHidden=false
                
                
                if offerflag==1{
                    cell1.btnapplycoupon.setTitle("", for: .normal)
                    cell1.lbloffername.isHidden=false
                    cell1.lblofferappliedtobill.isHidden=false
                    cell1.lblofferapplied.isHidden=false
                    cell1.lbloffername.text=offername
                    cell1.lblofferapplied.text="Offer applied"
                    cell1.imageviewclose.isHidden=true
                    cell1.btnclose.isHidden=false
                    cell1.btnclose.addTarget(self, action: #selector(btncloseaction), for: .touchUpInside)
                    totalamount=totalamount-offertotalafteroffer
                }else{
                    cell1.btnapplycoupon.setTitle("APPLY COUPON", for: .normal)
                    cell1.lblofferappliedtobill.isHidden=true
                    cell1.lbloffername.isHidden=true
                    cell1.lblofferapplied.isHidden=true
                    cell1.imageviewclose.isHidden=false
                    cell1.btnclose.isHidden=true
                }
            }
            
            if switchflag==1{
                //                cell1.lblavailablerewardpoints.text="Available Reward Point:"+String(pointafterreward)
                //
                //         let formattedNumber = numberFormatter.string(from: NSNumber(value: rewardamountafterreward)) ?? ""
                let formattedNumber2 = numberFormatter.string(from: NSNumber(value: totalamountafterreward)) ?? ""
                //                cell1.lblyouwillsaverewardpoints.text="*You will save \(formattedNumber) QAR using \(rewardamountafterreward) reward points"
                //
                // cell1.lblcarttotalprice.text="Total: QAR. "+String(formattedNumber2)+" + \(rewardamountafterreward) reward points"
                cell1.lblsubtotal.text="Sub Total : QAR."+String(formattedNumber2)
            }else{
                //  let formattedNumber = numberFormatter.string(from: NSNumber(value: rewardamount)) ?? ""
                let formattedNumber2 = numberFormatter.string(from: NSNumber(value: totalamount)) ?? ""
                //            cell1.lblavailablerewardpoints.text="Available Reward Point:"+String(point)
                //                cell1.lblyouwillsaverewardpoints.text="*You will save \(formattedNumber) QAR using \(formattedNumber) reward points"
                cell1.lblcarttotalprice.text="Total: QAR. "+String(formattedNumber2)
                cell1.lblsubtotal.text="Sub Total : QAR."+String(formattedNumber2)
            }
            
            
            cell1.btnwanttoaddmore.addTarget(self, action: #selector(wantoaddmore), for: .touchUpInside)
            cell1.btnpaynow.addTarget(self, action: #selector(btnpaynowaction), for: .touchUpInside)
            cell1.btnapplycoupon.addTarget(self, action: #selector(btnapplycouponaction), for: .touchUpInside)
            cell1.btnswitch.addTarget(self, action: #selector(btnswitchaction(sender:)), for: .valueChanged)
            
            return cell1
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 250
        }else if indexPath.section==1{
            return 240
        }else{
            return 500
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section==1{
            if cart?[indexPath.row].status == "" {
                let productVC = UIStoryboard.init(name: "ProductScreen", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsVC_id") as? ProductDetailsVC
                productVC?.gift = cart?[indexPath.row]
                productVC?.imageurlgift = imageurlgift
                self.navigationController?.pushViewController(productVC!, animated: true)
            }
        }
    }
    @objc func btncloseaction(){
        offerflag=0
        
        totalamount=totalamount+offertotalafteroffer
        offertotalafteroffer=0
        tableviewcart.reloadData()
        
        
        
    }
    @objc func btnswitchaction(sender:UISwitch){
        
        if sender.isOn==true{
            switchflag=1
            let div=self.totalamount*self.percentage
            print("totalamount",self.totalamount)
            
            rewardamountafterreward=div/100
            print("rewardamount",rewardamount)
            if Int(point) ?? 0>=rewardamountafterreward{
                totalamountafterreward=totalamount-rewardamountafterreward
                pointafterreward=point-rewardamountafterreward
            }else{
                self.showToast(message: "Sorry you don't have enough points to redeem", font: .boldSystemFont(ofSize: 13), duration: 2)
            }
        }else{
            switchflag=0
            rewardmodel.user_point{ (model) in
                self.userpointdata(data:model)
            }
        }
        
        // }
        tableviewcart.reloadData()
        
    }
    
    
    @objc func btnpaynowaction(){
        //self.performSegue(withIdentifier: "showPayNow", sender: nil)
        //        let addoncart = self.storyboard?.instantiateViewController (withIdentifier: "AddonPaymentViewController") as! AddonPaymentViewController
        //         addoncart.totalamount=totalamount
        //         addoncart.dict_main=dict_products
        //         addoncart.offer_total=offertotal
        //        self.navigationController?.pushViewController(addoncart, animated: true)
        let vc = self.storyboard?.instantiateViewController (withIdentifier: "AddonPaymentViewController") as! AddonPaymentViewController
        print(offertotal," ",offertotalafteroffer," ",totalamountafterreward, " " ,rewardamountafterreward," ",totalamount)
        
        vc.offer_total = offertotal+offertotalafteroffer
        if switchflag==1{
            vc.totalamount = totalamountafterreward
            vc.point=String(rewardamountafterreward)
        }else{
            vc.totalamount = totalamount
        }
        //            vc.totalamount = 1
        vc.dict_main=productlistarray
        vc.userid = userid
        vc.offerid=offer_id
        
        let isAddon = !(cartdetail.map({$0.flag == "4"}).contains(false))
        vc.typePayemnt = isAddon ? 0:1
        if isAddon{
            self.present(vc, animated: true, completion: nil)
        }else{
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @objc func payNowPressed(sender:UIButton){
        
        
    }
    func updateCart()  {
        totalamount=0
        if carprice_array.count>1{
            for  i in 0...carprice_array.count-1{
                print("cari",carprice_array[i])
                let amount=Int(carprice_array[i]) ?? 0
                totalamount+=amount
            }
        }else if carprice_array.count==1{
            totalamount=Int(carprice_array[0]) ?? 0
        }else{
            totalamount=0
        }
        print("totalamount",totalamount)
        
        if cart!.count > 0 {
            totalamountaddon = 0
            for (index,item) in cart!.enumerated() {
                totalamountaddon = totalamountaddon + (item.count * Int(item.base_price!)!)
                totalamount=totalamount+totalamountaddon
            }
            print("totalamountaddon",totalamountaddon)
            print("totalamount",totalamount)
            
            
        }
        
        
        if switchflag==1{
            let div=self.totalamount*self.percentage
            print("totalamount",self.totalamount)
            
            rewardamountafterreward=div/100
            print("rewardamount",rewardamount)
            if Int(point) ?? 0>=rewardamountafterreward{
                totalamountafterreward=totalamount-rewardamountafterreward
                pointafterreward=point-rewardamountafterreward
            }else{
                self.showToast(message: "Sorry you don't have enough points to redeem", font: .boldSystemFont(ofSize: 13), duration: 2)
            }
            
            
        }else{
            
            //            rewardmodel.user_point{ (model) in
            //                self.userpointdata(data:model)
            //            }
            //
        }
        
        
        
        tableviewcart.reloadData()
        //        } else {
        //            self.navigationController?.popViewController(animated: true)
        //        }
    }
    @objc func removePressed(sender:UIButton){
        cart?.remove(at: sender.tag)
        tableviewcart.reloadData()
        updateCart()
    }
    @objc func minusPressed(sender:UIButton){
        let num = (cart?[sender.tag].count)!
        cart?[sender.tag].count = num - 1
        let price=Int(cart?[sender.tag].base_price ?? "")
        let actualprice=price! * (cart?[sender.tag].count)!
        cart?[sender.tag].added_price = actualprice
        if cart?[sender.tag].count == 0 {
            cart?.remove(at: sender.tag)
        }
        tableviewcart.reloadData()
        updateCart()
        
    }
    @objc func plusPressed(sender:UIButton){
        
        let num = (cart?[sender.tag].count)!
        print("num",num)
        cart?[sender.tag].count = num + 1
        let price=Int(cart?[sender.tag].base_price ?? "")
        let actualprice=price! * (cart?[sender.tag].count)!
        cart?[sender.tag].added_price = actualprice
        print("cart",cart)
        tableviewcart.reloadData()
        updateCart()
        
    }
    
    @objc func removePressedA(sender:UIButton){
        cartdetail.remove(at: sender.tag)
        carprice_array.remove(at: sender.tag)
        tableviewcart.reloadData()
        updateCart()
    }
    @objc func minusPressedA(sender:UIButton){
        let num = Int(cartdetail[sender.tag].quantity ?? "0")! - 1
        cartdetail[sender.tag].quantity = String(num)
        let price=Int(cartdetail[sender.tag].carprice ?? "")
        let actualprice=price! * num
        carprice_array[sender.tag] = String(actualprice)
        if num < 1 {
            cartdetail.remove(at: sender.tag)
        }
        tableviewcart.reloadData()
        updateCart()
        
    }
    @objc func plusPressedA(sender:UIButton){
        
        let num = Int(cartdetail[sender.tag].quantity ?? "0")! + 1
        print("num",num)
        //cart?[sender.tag].count = num
        cartdetail[sender.tag].quantity = String(num)
        let price=Int(cartdetail[sender.tag].carprice ?? "")! * num
        carprice_array[sender.tag] = String(price)
        print("cart",cart)
        tableviewcart.reloadData()
        updateCart()
        
    }
    @objc func addMorePressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func btnapplycouponaction(){
        //  let coupon = self.storyboard?.instantiateViewController (withIdentifier: "CouponViewController") as! CouponViewController
        let coupon = UIStoryboard(name: "ProductScreen", bundle: nil).instantiateViewController (withIdentifier: "EnterCouponViewController") as! EnterCouponViewController
        coupon.totalamount=totalamount
        coupon.applied = { [weak self] total,afterOffer,id,name,flag in
            self?.totalamount = total
            self?.offertotalafteroffer = afterOffer
            self?.offer_id = id
            self?.offername = name
            self?.offerflag = flag
            self?.fetchdata()
            self?.updateCart()
            
        }
        self.present(coupon, animated: true, completion: nil)
        
    }
    @objc func btnremoveaction(sender:UIButton){
        
        deleted=sender.tag
        print("deleted",deleted)
        deletedata()
        
    }
    // MARK: - Navigation
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if (segue.identifier == "showPayNow") {
    //            let vc = segue.destination as! AddonPaymentViewController
    //            print(offertotal," ",offertotalafteroffer," ",totalamountafterreward, " " ,rewardamountafterreward," ",totalamount)
    //
    //            vc.offer_total = offertotal+offertotalafteroffer
    //            if switchflag==1{
    //            vc.totalamount = totalamountafterreward
    //            vc.point=String(rewardamountafterreward)
    //            }else{
    //            vc.totalamount = totalamount
    //            }
    //            //            vc.totalamount = 1
    //            vc.dict_main=productlistarray
    //            vc.userid = userid
    //            vc.offerid=offer_id
    //
    //            let isAddon = !(cartdetail.map({$0.flag == "4"}).contains(false))
    //            vc.typePayemnt = isAddon ? 0:1
    //
    //        }
    //    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        if carprice_array.count>0||cart?.count ?? 0>0{
            self.navigationController?.popViewController(animated: true)
        }else{
            let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
            if cart?.count ?? 0>0{
                home.cart=cart
                home.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
    @objc func wantoaddmore(){
        let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
        if cart?.count ?? 0>0{
            home.cart=cart
            home.imageurlgift=imageurlgift
        }
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    
    
    func deletedata(){
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Cartdetail")
        do {
            cartdetail = try managedContext.fetch(fetchRequest) as! [Cartdetail]
            print("deleted",deleted)
            let objectdelete=cartdetail[deleted]
            managedContext.delete(objectdelete)
            do{
                try managedContext.save()
                fetchdata()
                
            }catch let error as NSError {
                
                print("could not save . \(error), \(error.userInfo)")
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    
    
    
    func fetchdata(){
        
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Cartdetail")
        
        //        //3
        do {
            cartdetail = try managedContext.fetch(fetchRequest) as! [Cartdetail]
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        
        
        if cartdetail.count>0{
            cartname_array.removeAll()
            carprice_array.removeAll()
            cartdate_array.removeAll()
            carimage_array.removeAll()
            cartime_array.removeAll()
            totalamount=0
            for i in 0...cartdetail.count-1{
              //  if userid==cartdetail[i].userid{
                    cartname_array.append(cartdetail[i].carname ?? "")
                    carprice_array.append(cartdetail[i].carprice ?? "")
                    cartdate_array.append(cartdetail[i].cardate ?? "")
                    carimage_array.append(cartdetail[i].carimage ?? "")
                    cartime_array.append((cartdetail[i].pick_time ?? " ")+" "+(cartdetail[i].drop_time ?? ""))
               // }
                
            }
            
            if carprice_array.count>1{
                for  i in 0...carprice_array.count-1{
                    print("cari",carprice_array[i])
                    let amount=Int(carprice_array[i]) ?? 0
                    totalamount+=amount
                }
            }else if carprice_array.count==1{
                totalamount=Int(carprice_array[0]) ?? 0
            }else{
                
            }
            
            
            
            var item=[String:Any]()
            var item1=[String:Any]()
            
            for i in 0...cartdetail.count-1{
               // if userid==cartdetail[i].userid{
                    
                    if cartdetail[i].package_id==""{
                        
                    }else{
                        // packageflag=1
                    }
                    //  if cartdetail[i].flag=="1" {
                    item.updateValue(cartdetail[i].car_id ?? "", forKey: "car_id")
                    
                    //  }else if cartdetail[i].flag=="2"{
                    //  item.updateValue(cartdetail[i].car_id ?? "", forKey: "helicopter_id")
                    //   }else if cartdetail[i].flag=="3"{
                    //   item.updateValue(cartdetail[i].car_id ?? "", forKey: "yacht_id")
                    //   }
                    
                    item.updateValue(cartdetail[i].pick_time ?? "", forKey: "pick_time")
                    item.updateValue(cartdetail[i].drop_time ?? "", forKey: "drop_time")
                    item.updateValue(cartdetail[i].total_days ?? "1", forKey: "total_days")
                    item.updateValue(cartdetail[i].type ?? "", forKey: "type")
                    item.updateValue(cartdetail[i].receive_loc ?? "", forKey: "receive_loc")
                    item.updateValue(cartdetail[i].return_loc ?? "", forKey: "return_loc")
                    item.updateValue(cartdetail[i].package_id ?? "0", forKey: "package_id")
                    item.updateValue(cartdetail[i].flag ?? "", forKey: "flag")
                    item.updateValue(cartdetail[i].people ?? "", forKey: "people")
                    item1.updateValue(cartdetail[i].addon_cat_id ?? "", forKey: "addon_cat_id")
                    item1.updateValue(cartdetail[i].addon_brand_id ?? "", forKey: "addon_brand_id")
                    item1.updateValue([cartdetail[i].gift_id ?? ""], forKey: "gift_id")
                    
                    print("item1",item1)
                    item.updateValue(item1, forKey: "gift")
                    print("item",item)
                    //                    let dateFormatterGet = DateFormatter()
                    //                    dateFormatterGet.dateFormat = "yyyy-MM-dd"
                    //                    let dateFormatterPrint = DateFormatter()
                    //                    dateFormatterPrint.dateFormat = "d-M-yyyy"
                    // if let date = dateFormatterGet.date(from: cartdetail[i].pick_date ?? "") {
                    item.updateValue(cartdetail[i].pick_date ?? "", forKey: "pick_date")
                    //  }
                    //  if let date = dateFormatterGet.date(from: cartdetail[i].drop_date ?? "") {
                    item.updateValue(cartdetail[i].drop_date ?? "", forKey: "drop_date")
                    //  }
               // }
                productlistarray.append(item)
                
                
                print("productlistarray",productlistarray)
                print("cartname_array",cartname_array)
            }
            
            //  dict_products.updateValue(productlistarray, forKey: "product_list")
            
            //  dict_products.updateValue(userid, forKey: "user_id")
            // dict_products.updateValue(offertotal, forKey: "offer_tot")
            // dict_main.updateValue(dict_products, forKey: "data")
            
        }else{
            
            cartname_array.removeAll()
            cartdate_array.removeAll()
            carprice_array.removeAll()
            carimage_array.removeAll()
            cartime_array.removeAll()
            
        }
        
        updateCart()
        
        
        
    }
    
}
