//
//  EnterCouponViewController.swift
//  Outing
//
//  Created by Jishnu Raj T on 11/06/21.
//  Copyright © 2021 Arun Vijayan. All rights reserved.
//

import UIKit

class EnterCouponViewController: UIViewController {

    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var couponCodeTF: UITextField!
    var couponmodel=Couponviewmodel()
    
    var totalamount=0
    var offertotalafteroffer = 0
    var applied:((_ totalamount:Int,
                  _ offertotalafteroffer:Int,
                  _ offer_id:String,
                  _ offername:String,
                  _ offerflag:Int)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func applyCouponAPI(){
        self.couponCodeTF.resignFirstResponder()
        guard couponCodeTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            self.showToast(message: "Please enter a valid coupon code", font: .boldSystemFont(ofSize: 13), duration: 1)
            return
        }
        applyBtn.setTitle("Loading...", for: .normal)
        self.couponmodel.offerApply(code: couponCodeTF.text!, total: String(totalamount), completion: { model in
            DispatchQueue.main.async{
            self.applyBtn.setTitle("Apply", for: .normal)
            if model.status == true{
                self.offertotalafteroffer=(Int(model.data?.discount ?? "0") ?? 0)
                let id = model.data?.id ?? ""
                self.applied?(self.totalamount, self.offertotalafteroffer, id, "Offer", 1)
                self.dismiss(animated: true, completion: nil)
            }else{
                self.showToast(message: model.message ?? "Error", font: .boldSystemFont(ofSize: 13), duration: 1)
            }
            }
        })
    }
    
    
    
    @IBAction func applyAction(_ sender: UIButton) {
        applyCouponAPI()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
