//
//  IntroCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 19/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class IntroCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btncontinue: UIButton!
    @IBOutlet weak var lblluxury: UILabel!
    @IBOutlet weak var pagecontrol: UIPageControl!
    @IBOutlet weak var btnskip: UIButton!
    @IBOutlet weak var imageviewintro: UIImageView!
}
