//
//  SpashscreenvideoViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 19/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import NotificationCenter

class SpashscreenvideoViewController: UIViewController {
  let playerController = AVPlayerViewController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           playVideo()
       // self.dismiss(animated: true, completion: nil)
                  
       }
   override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       NotificationCenter.default.addObserver(self, selector: #selector(self.catchIt), name: NSNotification.Name(rawValue: "myNotif"), object: nil)
   }
    
    
    @objc func catchIt(_ userInfo: Notification){

        let prefs: UserDefaults = UserDefaults.standard
        prefs.removeObject(forKey: "startUpNotif")

       // if userInfo.userInfo?["userInfo"] != nil{
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: RedirectAppInactiveVC = storyboard.instantiateViewController(withIdentifier: "RedirectAppInactiveVC") as! RedirectAppInactiveVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: RedirectAppActiveVC = storyboard.instantiateViewController(withIdentifier: "RedirectAppActiveVC") as! RedirectAppActiveVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        if userInfo.userInfo!["type"] as! String=="0"{
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
            let vc: SpashscreenvideoViewController = storyboard!.instantiateViewController(withIdentifier: "SpashscreenvideoViewController") as! SpashscreenvideoViewController
            self.navigationController?.pushViewController(vc, animated: true)
             //navigation.navigationBar.isHidden=true
         }else if userInfo.userInfo!["type"] as! String=="1"{
             let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let vc: CardetailsViewController = storyboard!.instantiateViewController(withIdentifier: "CardetailsViewController") as! CardetailsViewController
             vc.carid="8"
                       self.navigationController?.pushViewController(vc, animated: true)
             
            
             }else if userInfo.userInfo!["type"] as! String=="2"{
             let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let vc: HelicopterViewController = storyboard!.instantiateViewController(withIdentifier: "HelicopterViewController") as! HelicopterViewController
           
                      self.navigationController?.pushViewController(vc, animated: true)
            
             
             
         }else if userInfo.userInfo!["type"] as! String=="3"{
             let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc: CardetailsViewController = storyboard!.instantiateViewController(withIdentifier: "CardetailsViewController") as! CardetailsViewController
            vc.yatchid="6"
                       self.navigationController?.pushViewController(vc, animated: true)
                        
                        
         }else if userInfo.userInfo!["type"] as! String=="4"{
             
             let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let vc: PackageViewController = storyboard!.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
            
                       self.navigationController?.pushViewController(vc, animated: true)
                        
             
            
             
         }else if userInfo.userInfo!["type"] as! String=="5"{
             let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc: AddonViewController = storyboard!.instantiateViewController(withIdentifier: "AddonViewController") as! AddonViewController
                       
                                  self.navigationController?.pushViewController(vc, animated: true)
                                   
                        
                      
         }
         
        
       // }
    }
    

        func playVideo() {
           guard let path = Bundle.main.path(forResource: "Sequence 01_1", ofType:"mp4") else {
               debugPrint("video.m4v not found")
               return
           }
           let player = AVPlayer(url: URL(fileURLWithPath: path))
         
           playerController.player = player
           playerController.showsPlaybackControls=false
            present(playerController, animated: false) {
               player.play()
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying),
                                                       name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
               
        }
     
       }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        playerController.dismiss(animated: false, completion: nil)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        goto()
    }
    
    func goto(){
       
       
       
       let intro = self.storyboard?.instantiateViewController (withIdentifier: "IntroViewController") as! IntroViewController
       self.navigationController?.pushViewController(intro, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
