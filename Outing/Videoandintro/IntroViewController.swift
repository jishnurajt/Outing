//
//  IntroViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 19/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit



class IntroViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionviewintro: UICollectionView!
     var images=["Screen1","Screen_02"]
    var labels=["WELCOME TO OUTING","EXPERIENCE LUXURY"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let statusBarFrame = UIApplication.shared.statusBarFrame
//        let statusBarView = UIView(frame: statusBarFrame)
//        self.view.addSubview(statusBarView)
//
//        statusBarView.backgroundColor = UIColor(red: 44 / 255, green: 44 / 255, blue: 44 / 255, alpha: 1.0)
//        if let user_id=UserDefaults.standard.value(forKey: "userid"){
//            let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
//            self.navigationController?.pushViewController(home, animated: true)
//                      
//        }else{
//            let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
//            self.navigationController?.pushViewController(login, animated: true)
//        }
        if let userid=UserDefaults.standard.value(forKey: "userid"){
               let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController

                          self.navigationController?.pushViewController(home!, animated: true)
               }else{

               }
        
    }
   

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell",
                                                          for: indexPath) as! IntroCollectionViewCell
        cell1.pagecontrol.currentPage=indexPath.item
        if indexPath.item==0{

            cell1.btncontinue.isHidden=true
            cell1.btnskip.isHidden=false
        }else{
              cell1.btncontinue.isHidden=false
            cell1.btnskip.isHidden=true
        }
        cell1.imageviewintro.image=UIImage(named:images[indexPath.item])
        cell1.lblluxury.text=labels[indexPath.item]
        cell1.btncontinue.addTarget(self, action: #selector(goto), for: .touchUpInside)
        cell1.btnskip.addTarget(self, action: #selector(goto), for: .touchUpInside)
        return cell1
                  
        
    }
    @objc func goto(){
        let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
              self.navigationController?.pushViewController(login, animated: true)
    }

}
extension IntroViewController: UICollectionViewDelegateFlowLayout {
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let screenSize = UIScreen.main.bounds.height
    let screenWidth =  UIScreen.main.bounds.width
   
    return CGSize(width: screenWidth, height: screenSize)
   
}
}
