//
//  RewardsTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 29/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class RewardsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblreddempoints: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
