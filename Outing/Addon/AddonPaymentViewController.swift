//
//  AddonPaymentViewController.swift
//  Outing
//
//  Created by Aleena on 10/3/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import WebKit
import Foundation
class AddonPaymentViewController: UIViewController, WKNavigationDelegate{
    var cart:[Giftbook]?=[]
    var totalamount=Int()
    var userid=String()
    var webView = WKWebView()
    var dict_main=[[String:Any]]()
    var offer_total=Int()
    var offerid=String()
    var point=String()
    var bookingid = ""
    var typePayemnt = 0 // 0 - addon only // 1 - vechile  // 2 - from booking 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        webView = WKWebView(frame: view.frame)
        webView.navigationDelegate = self
      
        if typePayemnt == 0{
            let link = URL(string: "http://outing.qa/site/product_pay/\(userid)/\(totalamount)")!
            print("link",link)
            let request = URLRequest(url: link)
            webView.load(request)
            self.view.addSubview(webView)
        } else if typePayemnt == 2{
            let link = URL(string: "http://outing.qa/site/pay_request/" + bookingid)!
            print("link",link)
            let request = URLRequest(url: link)
            webView.load(request)
            self.view.addSubview(webView)
        }else{
            submit()
        }
        // Do any additional setup after loading the view.
    }
    
    //    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    //        let dict = message.body as! [String:AnyObject]
    //        print("dict",dict)
    ////        let username    = dict["username"] as! String
    ////        let secretToken = dict["secretToken"] as! String
    //
    //        // now use the name and token as you see fit!
    //    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        var dict=[String:Any]()
        print("redirect didFinish")
        print(webView)
        
        
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                   
                                   completionHandler: { (html: Any?, error: Error?) in
                                    print(html as Any)
                                    var htmlString = html as! String
                                   
                                    
                                    if htmlString.contains("InvoiceNo") {
                                        let startIndexJSON  = htmlString.range(of: "{")?.lowerBound
                                        let endIndexJSON  = htmlString.range(of: "}}")?.upperBound
                                        htmlString = String(htmlString.prefix(upTo: endIndexJSON!))
                                        htmlString = String(htmlString.suffix(from: startIndexJSON!))
                                        dict = self.convertToDictionary(text: htmlString)!
                                        webView.removeFromSuperview()
                                        if self.typePayemnt == 0{
                                        self.submit(data: dict)
                                        }
                                    }
                                    else{
                                        if htmlString.contains("status"){
                                            let startIndexJSON  = htmlString.range(of: "{")?.lowerBound
                                            let endIndexJSON  = htmlString.range(of: "}")?.upperBound
                                            htmlString = String(htmlString.prefix(upTo: endIndexJSON!))
                                            htmlString = String(htmlString.suffix(from: startIndexJSON!))
                                            dict = self.convertToDictionary(text: htmlString) ?? [:]
                                            if dict["status"] as? Bool == false{
                                                //
                                                let ordersubmitted = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrdersubmittedViewController") as? OrdersubmittedViewController
                                                
                                                ordersubmitted!.failedflag=1
                                                self.navigationController?.pushViewController(ordersubmitted!, animated: true)
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    //  }
                                    //                                    }
        })
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func submit(data:[String: Any]?=nil){
      let dataObject = data!["data"] as! [String:String]
        
        var itemList = [[String:String]]()
        for (_,item) in cart!.enumerated() {
            
            let jsonObject = ["gift_id" : String(item.gift_id!),
                              "qty" : String(item.count),
                              "price" : String(item.base_price!),
                              "variance_id" : String(item.gift_variance![item.selectedVarienceIndex].gift_variance_id!)]
            itemList.append(jsonObject)
        }
        let json = [
            "data" : [
                "user_id" : userid,
                "offer_id" : offerid,
                "offer_tot" : String(offer_total),
                "point":point,
                "reference" : dataObject["reference"]!,
                "InvoiceNo" : dataObject["InvoiceNo"]!,
                "product_list" : dict_main,
                "item_list" : itemList,
                "note":""
            ]
        ]
        print("json",json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://www.outing.qa/api/Data/submit_order_new_product")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                DispatchQueue.main.sync {
                    print("status",responseJSON["status"])
                   
                        let ordersubmitted = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrdersubmittedViewController") as? OrdersubmittedViewController
                        let response=responseJSON["Response"] as! [String:Any]
                        let bookingid=response["booking_unique_id"] as! String
                        print("bookingid",bookingid)
                        ordersubmitted!.bookingid=bookingid
                    ordersubmitted!.failedflag = responseJSON["status"] as! Bool==true ? 0 : 1
                        self.navigationController?.pushViewController(ordersubmitted!, animated: true)
                  
                    
                    //                    for controller in self.navigationController!.viewControllers as Array {
                    //                        if controller.isKind(of: HomeViewController.self) {
                    //                            self.navigationController!.popToViewController(controller, animated: true)
                    //                            break
                    //                        }
                    //                    }
                }
                
            }
        }
        
        task.resume()
    }
    
    func submit(){
      //  let dataObject = data!["data"] as! [String:String]
        
        var itemList = [[String:String]]()
        for (_,item) in cart!.enumerated() {
            
            let jsonObject = ["gift_id" : String(item.gift_id!),
                              "qty" : String(item.count),
                              "price" : String(item.base_price!),
                              "variance_id" : String(item.gift_variance![item.selectedVarienceIndex].gift_variance_id!)]
            itemList.append(jsonObject)
        }
        let json = [
            "data" : [
                "user_id" : userid,
                "offer_id" : offerid,
                "offer_tot" : String(offer_total),
                "point":point,
                //"reference" : dataObject["reference"]!,
               // "InvoiceNo" : dataObject["InvoiceNo"]!,
                "product_list" : dict_main,
                "item_list" : itemList
            ]
        ]
        print("json",json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://www.outing.qa/api/Data/submit_order_new")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                DispatchQueue.main.sync {
                    print("status",responseJSON["status"])
                   
                        let ordersubmitted = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrdersubmittedViewController") as? OrdersubmittedViewController
                        let response=responseJSON["Response"] as! [String:Any]
                        let bookingid=response["booking_unique_id"] as! String
                        print("bookingid",bookingid)
                        ordersubmitted!.bookingid=bookingid
                    ordersubmitted!.failedflag = responseJSON["status"] as! Bool==true ? 0 : 1
                        self.navigationController?.pushViewController(ordersubmitted!, animated: true)
                  
                    
                    //                    for controller in self.navigationController!.viewControllers as Array {
                    //                        if controller.isKind(of: HomeViewController.self) {
                    //                            self.navigationController!.popToViewController(controller, animated: true)
                    //                            break
                    //                        }
                    //                    }
                }
                
            }
        }
        
        task.resume()
    }
    
}


