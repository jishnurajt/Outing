//
//  AddonPaynowTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class AddonPaynowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnpaynow: UIButton!
    @IBOutlet weak var lblcarttotalprice: UILabel!
    @IBOutlet weak var btnwanttoaddmore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnpaynow.layer.cornerRadius=20
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
