//
//  GiftCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
protocol passbackdatadelegate{
    func passback(varientsname:String,row:Int,selectedvarience:Int)
    
    
}


class GiftBookCollectionViewCell: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbldescrptn: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imageviewgift: UIImageView!
    
    @IBOutlet weak var collectionviewvarients: UICollectionView!
    @IBOutlet weak var btnvar1 : UIButton!
    @IBOutlet weak var btnvar2: UIButton!
    @IBOutlet weak var btnvar3: UIButton!
    @IBOutlet weak var btnvar4: UIButton!
    @IBOutlet weak var btnvar5: UIButton!
    var varience:[Gift_book_variance]?=[]
    var imagevarience=String()
    var delegate:passbackdatadelegate!
    var rowforoutercollectionview=Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionviewvarients.delegate=self
        collectionviewvarients.dataSource=self
    
    }
    
    func loadCollectionview(varientsdisplayed:[Gift_book_variance],imageurl:String,row:Int) {
       self.varience = varientsdisplayed
        self.imagevarience=imageurl
        rowforoutercollectionview=row
        print("varientsdisplayed",varientsdisplayed)
       
       self.collectionviewvarients.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return varience?.count ?? 0
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell4 = collectionView.dequeueReusableCell(withReuseIdentifier: "VarientsCollectionViewCell",
        for: indexPath) as! VarientsCollectionViewCell
        let url=URL(string:imagevarience+(varience?[indexPath.row].gift_variance_image ?? ""))
         cell4.imageviewvarients?.kf.indicatorType = .activity
        cell4.layer.cornerRadius=10
        cell4.imageviewvarients.kf.setImage(with: url)
        return cell4
    }
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
        return CGSize(width: 100, height: CGFloat(100))
    }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        delegate.passback(varientsname: varience?[indexPath.row].gift_variance_name ?? "",row: rowforoutercollectionview,selectedvarience:indexPath.row)
    }
}
