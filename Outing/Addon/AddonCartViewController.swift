
//
//  AddonCartViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 23/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import  CoreData



class AddonCartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var totalamount=0
    var userid=String()
     var totalamountaddon=0
    
    @IBOutlet weak var btnbackaction: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var cartTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // btnbackaction.layer.cornerRadius=25
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            userid=UserDefaults.standard.value(forKey: "userid") as! String
        }
        
        
        btnsidemenu.layer.masksToBounds=true
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
             btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        
        updateCart()
    }
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0{
            return cart!.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            let cell = (tableView.dequeueReusableCell(withIdentifier: "AddonCartTableViewCell", for: indexPath) as? AddonCartTableViewCell)!
            cell.lbladdon.text=cart![indexPath.row].title
            cell.lblprice.text="QAR "+cart![indexPath.row].base_price!
            cell.lblcount.text = String(cart![indexPath.row].count)
            let url = URL(string:imageurlgift+(cart![indexPath.row].gift_images?[0].gift_image ?? ""))
            cell.imageviewcar.kf.indicatorType = .activity
            cell.imageviewcar.kf.setImage(with: url)
            cell.imageviewcar.contentMode = .scaleAspectFit
            cell.btnremovegift.tag=indexPath.row
            cell.btnremovegift.addTarget(self, action: #selector(removePressed(sender:)), for: .touchUpInside)
            cell.btnplusgift.tag=indexPath.row + 500
            cell.btnplusgift.addTarget(self, action: #selector(plusPressed(sender:)), for: .touchUpInside)
            cell.btnminusgift.tag=indexPath.row + 700
            cell.btnminusgift.addTarget(self, action: #selector(minusPressed(sender:)), for: .touchUpInside)
            return cell
        }else{
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "AddonPaynowTableViewCell", for: indexPath) as? AddonPaynowTableViewCell)!
            cell1.lblcarttotalprice.text="Total: QAR. "+String(totalamount)
            cell1.btnwanttoaddmore.addTarget(self, action: #selector(addMorePressed(sender:)), for: .touchUpInside)
            cell1.btnpaynow.addTarget(self, action: #selector(payNowPressed(sender:)), for: .touchUpInside)
            return cell1
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 190
        }else{
            return 230
        }
    }
    
    
    @IBAction func btnsidemnuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
         let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
         
         let navController = UINavigationController(rootViewController: vc!)
         navController.modalPresentationStyle = .fullScreen
          navController.modalPresentationStyle = .overCurrentContext
         navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
         vc?.cart=cart
         vc?.imageurlgift=imageurlgift
        }
         self.present(navController, animated:false, completion: nil)
    }
    
    func updateCart()  {
        if cart!.count > 0 {
            totalamount = 0
            for (index,item) in cart!.enumerated() {
                totalamountaddon = totalamountaddon + (item.count * Int(item.base_price!)!)
            }
            cartTableView.reloadData()
            
        } else {
            //self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func removePressed(sender:UIButton){
        cart?.remove(at: sender.tag)
        cartTableView.reloadData()
        updateCart()
    }
    @objc func minusPressed(sender:UIButton){
        let num = (cart?[sender.tag - 700].count)!
        cart?[sender.tag - 700].count = num - 1
        if cart?[sender.tag - 700].count == 0 {
            cart?.remove(at: sender.tag - 700)
        }
        cartTableView.reloadData()
        updateCart()
        
    }
    @objc func plusPressed(sender:UIButton){
        let num = (cart?[sender.tag - 500].count)!
        cart?[sender.tag - 500].count = num + 1
        
        cartTableView.reloadData()
        updateCart()
        
    }
    @objc func addMorePressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    @objc func payNowPressed(sender:UIButton){
        
        self.performSegue(withIdentifier: "showPayNow", sender: nil)
    }
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showPayNow") {
            let vc = segue.destination as! AddonPaymentViewController
            vc.cart = cart
            vc.totalamount = totalamount
            vc.userid = userid
        }
    }
}
