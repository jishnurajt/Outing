//
//  Addonbookclass.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct Addonbookclass : Codable {
    let status : Bool?
    let message : String?
    let data : AddonbookData?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddonbookData.self, forKey: .data)
    }
    
}
struct AddonbookData : Codable {
    let addon_category : [Addon_book_category]?
    let image_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case addon_category = "addon_category"
        case image_url = "image_url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_category = try values.decodeIfPresent([Addon_book_category].self, forKey: .addon_category)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }
    
}

struct Addon_book_category : Codable {
    let addon_cat_id : String?
    let addon_cat_name : String?
    let addon_cat_image : String?
    let created_at_date : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case addon_cat_id = "addon_cat_id"
        case addon_cat_name = "addon_cat_name"
        case addon_cat_image = "addon_cat_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_cat_name = try values.decodeIfPresent(String.self, forKey: .addon_cat_name)
        addon_cat_image = try values.decodeIfPresent(String.self, forKey: .addon_cat_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}
//addondrand

struct Addonbookbrandclass : Codable {
    let status : Bool?
    let message : String?
    let data : AddonbookbrandData?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddonbookbrandData.self, forKey: .data)
    }
    
}
struct AddonbookbrandData : Codable {
    let addon_brands : [Addon_book_brands]?
    let image_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case addon_brands = "addon_brands"
        case image_url = "image_url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_brands = try values.decodeIfPresent([Addon_book_brands].self, forKey: .addon_brands)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }
    
}
struct Addon_book_brands : Codable {
    let addon_brand_id : String?
    let addon_brand_name : String?
    let addon_cat_id : String?
    let addon_brand_image : String?
    let created_at_date : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case addon_brand_id = "addon_brand_id"
        case addon_brand_name = "addon_brand_name"
        case addon_cat_id = "addon_cat_id"
        case addon_brand_image = "addon_brand_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
        addon_brand_name = try values.decodeIfPresent(String.self, forKey: .addon_brand_name)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_brand_image = try values.decodeIfPresent(String.self, forKey: .addon_brand_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}

//datagift
struct Giftbookclass : Codable {
    let status : Bool?
    let message : String?
    let data : GiftbookData?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(GiftbookData.self, forKey: .data)
    }
    
}
struct Giftbook : Codable {
    let gift_id : String?
    let provider_id : String?
    let addon_cat_id : String?
    let addon_brand_id : String?
    let title : String?
    let description : String?
    var base_price : String?
    let latitude : String?
    let longitude : String?
    let created_at_date : String?
    let status : String?
    let gift_images : [Gift_book_images]?
    let gift_variance : [Gift_book_variance]?
    var count : Int = 0
    var selectedVarienceIndex : Int = 0
    var added_price : Int = 0
    enum CodingKeys: String, CodingKey {
        
        case gift_id = "gift_id"
        case provider_id = "provider_id"
        case addon_cat_id = "addon_cat_id"
        case addon_brand_id = "addon_brand_id"
        case title = "title"
        case description = "description"
        case base_price = "base_price"
        case latitude = "latitude"
        case longitude = "longitude"
        case created_at_date = "created_at_date"
        case status = "status"
        case gift_images = "gift_images"
        case gift_variance = "variance"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        gift_images = try values.decodeIfPresent([Gift_book_images].self, forKey: .gift_images)
        gift_variance = try values.decodeIfPresent([Gift_book_variance].self, forKey: .gift_variance)
    }
    
}
struct Gift_book_variance : Codable {
    let gift_variance_id : String?
    let gift_id : String?
    let gift_variance_image : String?
    let gift_variance_name : String?
    let gift_variance_color : String?
    let status : String?
    let created_at_date : String?
    let selected : Bool
    enum CodingKeys: String, CodingKey {
        
        case gift_variance_id = "variance_id"
        case gift_id = "gift_id"
        case gift_variance_image = "color_image"
        case gift_variance_name = "name"
        case gift_variance_color = "color"
        case status = "created_at_date"
        case created_at_date = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_variance_id = try values.decodeIfPresent(String.self, forKey: .gift_variance_id)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        gift_variance_image = try values.decodeIfPresent(String.self, forKey: .gift_variance_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        gift_variance_name = try values.decodeIfPresent(String.self, forKey: .gift_variance_name)
        gift_variance_color = try values.decodeIfPresent(String.self, forKey: .gift_variance_color)
        selected = false
    }
    
}
struct Gift_book_images : Codable {
    let gift_image_id : String?
    let gift_id : String?
    let gift_image : String?
    let created_at_date : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case gift_image_id = "gift_image_id"
        case gift_id = "gift_id"
        case gift_image = "gift_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_image_id = try values.decodeIfPresent(String.self, forKey: .gift_image_id)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        gift_image = try values.decodeIfPresent(String.self, forKey: .gift_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}
struct GiftbookData : Codable {
    let gift : [Giftbook]?
    let image_url : String?
    let color_image_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case gift = "gift"
        case image_url = "image_url"
        case color_image_url = "color_image_url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift = try values.decodeIfPresent([Giftbook].self, forKey: .gift)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        color_image_url = try values.decodeIfPresent(String.self, forKey: .color_image_url)
    }
    
}

//giftdetails

//struct giftdetailclass : Codable {
//    let status : Bool?
//    let message : String?
//    let data : Data?
//
//    enum CodingKeys: String, CodingKey {
//
//        case status = "status"
//        case message = "message"
//        case data = "data"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        status = try values.decodeIfPresent(Bool.self, forKey: .status)
//        message = try values.decodeIfPresent(String.self, forKey: .message)
//        data = try values.decodeIfPresent(Data.self, forKey: .data)
//    }
//
//}
//
//
//struct Variance : Codable {
//    let variance_id : String?
//    let name : String?
//    let color : String?
//    let color_image : String?
//    let gift_id : String?
//    let created_at_date : String?
//    let status : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case variance_id = "variance_id"
//        case name = "name"
//        case color = "color"
//        case color_image = "color_image"
//        case gift_id = "gift_id"
//        case created_at_date = "created_at_date"
//        case status = "status"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        variance_id = try values.decodeIfPresent(String.self, forKey: .variance_id)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        color = try values.decodeIfPresent(String.self, forKey: .color)
//        color_image = try values.decodeIfPresent(String.self, forKey: .color_image)
//        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
//        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//    }
//
//}
//
//
//
//struct Gift : Codable {
//    let gift_id : String?
//    let provider_id : String?
//    let addon_cat_id : String?
//    let addon_brand_id : String?
//    let title : String?
//    let description : String?
//    let base_price : String?
//    let bprice : String?
//    let latitude : String?
//    let longitude : String?
//    let created_at_date : String?
//    let status : String?
//    let excel_status : String?
//    let gift_images : [Gift_images]?
//    let variance : [Variance]?
//
//    enum CodingKeys: String, CodingKey {
//
//        case gift_id = "gift_id"
//        case provider_id = "provider_id"
//        case addon_cat_id = "addon_cat_id"
//        case addon_brand_id = "addon_brand_id"
//        case title = "title"
//        case description = "description"
//        case base_price = "base_price"
//        case bprice = "bprice"
//        case latitude = "latitude"
//        case longitude = "longitude"
//        case created_at_date = "created_at_date"
//        case status = "status"
//        case excel_status = "excel_status"
//        case gift_images = "gift_images"
//        case variance = "variance"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
//        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
//        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
//        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
//        bprice = try values.decodeIfPresent(String.self, forKey: .bprice)
//        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
//        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
//        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        excel_status = try values.decodeIfPresent(String.self, forKey: .excel_status)
//        gift_images = try values.decodeIfPresent([Gift_images].self, forKey: .gift_images)
//        variance = try values.decodeIfPresent([Variance].self, forKey: .variance)
//    }
//
//}
//
//struct Gift_images : Codable {
//    let gift_image_id : String?
//    let gift_id : String?
//    let gift_image : String?
//    let created_at_date : String?
//    let status : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case gift_image_id = "gift_image_id"
//        case gift_id = "gift_id"
//        case gift_image = "gift_image"
//        case created_at_date = "created_at_date"
//        case status = "status"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        gift_image_id = try values.decodeIfPresent(String.self, forKey: .gift_image_id)
//        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
//        gift_image = try values.decodeIfPresent(String.self, forKey: .gift_image)
//        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//    }
//
//}
//
//struct Data : Codable {
//    let gift : [Gift]?
//    let image_url : String?
//    let color_image_url : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case gift = "gift"
//        case image_url = "image_url"
//        case color_image_url = "color_image_url"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        gift = try values.decodeIfPresent([Gift].self, forKey: .gift)
//        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
//        color_image_url = try values.decodeIfPresent(String.self, forKey: .color_image_url)
//    }
//
//}
