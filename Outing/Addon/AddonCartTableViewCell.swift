//
//  CartTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 23/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class AddonCartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageviewcar: UIImageView!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbladdon: UILabel!
    @IBOutlet weak var viewcart: UIView!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnplusgift: UIButton!
    @IBOutlet weak var btnminusgift: UIButton!
    @IBOutlet weak var btnremovegift: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageviewcar.layer.cornerRadius=10
        viewcart.layer.cornerRadius=10
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
