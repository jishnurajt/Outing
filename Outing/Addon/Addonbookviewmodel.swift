//
//  Addonbookviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

public class Addonbookviewmodel{
    var addon_cat_id=String()
    var addonbrandid=String()
    var gift_id=String()
    
    func addon_category(completion : @escaping (Addonbookclass) -> ())  {
        
        
        let poststring="security_token=out564bkgtsernsko"
        
        var request = NSMutableURLRequest(url: APIEndPoint.addon_category.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
                
                let decoder = JSONDecoder()
                let model = try decoder.decode(Addonbookclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
    func addon_brand(completion : @escaping (Addonbookbrandclass) -> ())  {
        
        
        let poststring="addon_cat_id=\(addon_cat_id)&security_token=out564bkgtsernsko"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.addon_brand.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
                
                let decoder = JSONDecoder()
                let model = try decoder.decode(Addonbookbrandclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
    
    func gift(completion : @escaping (Giftbookclass) -> ())  {
        
        
        let poststring="addon_brand_id=\(addonbrandid)&addon_cat_id=\(addon_cat_id)&security_token=out564bkgtsernsko"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.gift.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
                
                let decoder = JSONDecoder()
                let model = try decoder.decode(Giftbookclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    



func gift_details(completion : @escaping (Giftbookclass) -> ())  {
    
    
  let poststring="gift_id=\(gift_id)&security_token=out564bkgtsernsko"
    print("poststring",poststring)
    var request = NSMutableURLRequest(url: APIEndPoint.gift_details.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
            
            let decoder = JSONDecoder()
            let model = try decoder.decode(Giftbookclass.self, from:
                data!) //Decode JSON Response Data
            print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
}
