//
//  AddonViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 01/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import  CoreData
import MobileCoreServices
import RxSwift


class AddonViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,passbackdatadelegate{
    
    @IBOutlet weak var lblnoproductsavailable: UILabel!
    
    @IBOutlet weak var collectionviewaddons: UICollectionView!
    @IBOutlet weak var collectionviewgift: UICollectionView!
    @IBOutlet weak var collectionviewbrands: UICollectionView!
    @IBOutlet weak var viewCartView: UIView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var btnzoom: UIButton!
    var addonbookmodel=Addonbookviewmodel()
    var addoncategory:[Addon_book_category]?=[]
    var addonbrands:[Addon_book_brands]?=[]
    var gift:[Giftbook]?=[]
    @IBOutlet weak var imageviewwidth: NSLayoutConstraint!
    @IBOutlet weak var imageviewheight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionviewvarients: UICollectionView!
    var  varients:[Gift_book_variance]? = []
    
    var cart:[Giftbook]?=[]
    var imageurl=String()
    var selectedindex=1000
    var selectedindexaddonbrand=1000
    var selectedgiftindex=1000
    var imageurladdonbrand=String()
    var imageurlgift=String()
    var coloImageUrl=String()
    var nameofjwellery=""
    var nameofbrand=""
    var selectectedrow=0
    var varientname=String()
    var searchflag=Int()
    var giftid=String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageview.isHidden=true
        btnzoom.isHidden=true
        imageview.layer.cornerRadius=10
        if UIDevice.current.userInterfaceIdiom == .pad{
            imageviewwidth.constant=500
            imageviewheight.constant=500
        }else{
            imageviewwidth.constant=300
            imageviewheight.constant=300
        }
        
     //   btnback.layer.cornerRadius=25
        if searchflag==0{
        addonbookmodel.addon_category{ (model) in
            self.addoncategorydata(data:model)
        }
        }else{
            addonbookmodel.gift_id=giftid
              addonbookmodel.gift_details{ (model) in
                self.giftdata(data:model)
            }
        }
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnbackaction(_ sender: Any) {
        if let type=UserDefaults.standard.value(forKey: "type"){
            let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            
            
            self.navigationController?.pushViewController(home!, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func animateZoomforCell(zoomCell : UIImageView)
    {
        UIImageView.animate(
            withDuration: 3.0,
            delay: 0,
            options: UIImageView.AnimationOptions.curveEaseOut,
            animations: {
                zoomCell.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
        },
            completion: nil)
    }
    func animateZoomforCellremove(zoomCell : UIImageView)
    {
        UIImageView.animate(
            withDuration:3.0,
            delay: 0,
            options: UIImageView.AnimationOptions.curveEaseOut,
            animations: {
                zoomCell.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
        },
            completion: nil)
        
    }
    
    @IBAction func btnzoomaction(_ sender: UIButton) {
        
        if sender.isSelected==false{
            animateZoomforCell(zoomCell: imageview)
            sender.isSelected=true
        }else{
            animateZoomforCellremove(zoomCell: imageview)
            sender.isSelected=false
            
        }
        
    }
    
    
    @IBAction func btnsearchaction(_ sender: Any) {
    }
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        
        self.present(navController, animated:false, completion: nil)
    }
    
    @IBAction func viewCartPressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "showCartaddon", sender: nil)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewaddons{
            return addoncategory?.count ?? 0
        }else if collectionView==collectionviewbrands {
            return addonbrands?.count ?? 0
        }else{
            return gift?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView==collectionviewaddons{
            var lastelement=0
            if searchflag==0{
            if addoncategory?.count ?? 0>0{
                lastelement=(addoncategory?.count ?? 0)-1
                if indexPath.row==lastelement{
                    addonbookmodel.addon_category{ (model) in
                        self.addoncategorydata(data:model)
                    }
                }
                
            }
            }
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AddonsBookCollectionViewCell",
                                                           for: indexPath) as! AddonsBookCollectionViewCell
            cell1.lbladdons.text=addoncategory?[indexPath.row].addon_cat_name
            
            
            if indexPath.row==selectedindex{
                // cell1.viewaddons.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell1.viewaddons.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell1.lbladdons.textColor = .darkGray
            }else{
                cell1.viewaddons.backgroundColor = .darkGray
                cell1.lbladdons.textColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
            }
            
            return cell1
        }else if collectionView==collectionviewbrands{
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandsBookCollectionViewCell",
                                                           for: indexPath) as! BrandsBookCollectionViewCell
            cell2.lblbrands.text=addonbrands?[indexPath.row].addon_brand_name
            
            if indexPath.row==selectedindexaddonbrand{
                cell2.lblbrands.textColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
            }else{
                cell2.lblbrands.textColor = .darkGray
            }
            
            return cell2
        }else{
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftBookCollectionViewCell",
                                                           for: indexPath) as! GiftBookCollectionViewCell
            cell3.layer.cornerRadius=10
            cell3.lbltitle.text=gift?[indexPath.row].title
            cell3.lbldescrptn.text=gift?[indexPath.row].description
            cell3.lblprice.text="QAR "+(gift?[indexPath.row].base_price ?? "")
           // cell3.layer.cornerRadius=cell3.frame.height/2
            var url:URL!
            if gift?[indexPath.row].gift_images?.count ?? 0>0{
            url = URL(string:imageurlgift+(gift?[indexPath.row].gift_images?[0].gift_image ?? ""))
            }
            
            cell3.imageviewgift.kf.indicatorType = .activity
            cell3.imageviewgift.kf.setImage(with: url)
            cell3.imageviewgift.layer.cornerRadius=20
            
            //imageview.kf.setImage(with: url)
            cell3.loadCollectionview(varientsdisplayed:(gift?[indexPath.row].gift_variance!)!,imageurl:coloImageUrl,row:indexPath.row)
            cell3.delegate=self
            if  gift?[indexPath.row].gift_variance?.count ?? 0>0{
            varients = gift?[indexPath.row].gift_variance  ?? []
            cell3.lbltitle.text = (gift?[indexPath.row].title!)! + "("+(varients?[(gift?[indexPath.row].selectedVarienceIndex)!].gift_variance_name! ?? "") + ")"
            }
           
            // cell3.lbltitle.text = (gift?[indexPath.row].title!)! + " (" + varients[(gift?[indexPath.row].selectedVarienceIndex)!].gift_variance_name! + ")"
            return cell3
        }
        
    }
    
    //            var  varients : [Gift_book_variance] = []
    //            if (gift?[indexPath.row].gift_variance!.count)! > 0 {
    //                varients = gift?[indexPath.row].gift_variance  ?? []
    //                cell3.lbltitle.text = (gift?[indexPath.row].title!)! + " (" + varients[(gift?[indexPath.row].selectedVarienceIndex)!].gift_variance_name! + ")"
    //                for(index, varient) in varients.enumerated() {
    //                    let imageView = cell3.viewWithTag(index + 101) as? UIImageView
    //                    let url = URL(string:coloImageUrl+(varient.gift_variance_image ?? ""))
    //                    if index == 0 {
    //                        cell3.btnvar1.tag = indexPath.row + 1000
    //                        cell3.btnvar1.addTarget(self, action: #selector(var1Pressed(sender:)), for: .touchUpInside)
    //                    } else if index == 1 {
    //                        cell3.btnvar2.tag = indexPath.row + 1100
    //                        cell3.btnvar2.addTarget(self, action: #selector(var2Pressed(sender:)), for: .touchUpInside)
    //                    } else if index == 2 {
    //                        cell3.btnvar3.tag = indexPath.row + 1200
    //                        cell3.btnvar3.addTarget(self, action: #selector(var3Pressed(sender:)), for: .touchUpInside)
    //                    } else if index == 3 {
    //                        cell3.btnvar4.tag = indexPath.row + 1300
    //                        cell3.btnvar4.addTarget(self, action: #selector(var4Pressed(sender:)), for: .touchUpInside)
    //                    } else if index == 4 {
    //                        cell3.btnvar5.tag = indexPath.row + 1400
    //                        cell3.btnvar5.addTarget(self, action: #selector(var5Pressed(sender:)), for: .touchUpInside)
    //                    }
    //                    imageView?.kf.indicatorType = .activity
    //                    imageView?.kf.setImage(with: url)
    //                    imageView?.contentMode = .scaleToFill
    //                    //imageView?.layer.cornerRadius=7
    //                    if gift?[indexPath.row].selectedVarienceIndex == index {
    //                        //imageView?.layer.borderWidth = 1
    //                       // imageView?.layer.borderColor = UIColor.darkGray.cgColor
    //                    } else {
    //                        imageView?.layer.borderWidth=0
    //                    }
    //                }
    //            } else {
    //                cell3.lbltitle.text=gift?[indexPath.row].title
    //                (cell3.viewWithTag(101) as? UIImageView)?.image = UIImage()
    //                (cell3.viewWithTag(102) as? UIImageView)?.image = UIImage()
    //                (cell3.viewWithTag(103) as? UIImageView)?.image = UIImage()
    //                (cell3.viewWithTag(104) as? UIImageView)?.image = UIImage()
    //                (cell3.viewWithTag(105) as? UIImageView)?.image = UIImage()
    //
    //                (cell3.viewWithTag(101) as? UIImageView)?.layer.borderWidth = 0
    //                (cell3.viewWithTag(102) as? UIImageView)?.layer.borderWidth = 0
    //                (cell3.viewWithTag(103) as? UIImageView)?.layer.borderWidth = 0
    //                (cell3.viewWithTag(104) as? UIImageView)?.layer.borderWidth = 0
    //                (cell3.viewWithTag(105) as? UIImageView)?.layer.borderWidth = 0
    //
    //
    //                cell3.btnvar1.isHidden = true
    //                cell3.btnvar2.isHidden = true
    //                cell3.btnvar3.isHidden = true
    //                cell3.btnvar4.isHidden = true
    //                cell3.btnvar5.isHidden = true
    //            }
    //
    //            if indexPath.row==selectedgiftindex{
    //                cell3.lbltitle.textColor=UIColor.darkGray
    //                cell3.lblprice.textColor=UIColor.darkGray
    //                //cell3.btnbookgift.setBackgroundImage(<#T##image: UIImage?##UIImage?#>, for: <#T##UIControl.State#>)
    //            }else{
    //                cell3.lbltitle.textColor = .darkGray
    //                cell3.lblprice.textColor = .darkGray
    //                //cell3.btnbookgift.setBackgroundImage(<#T##image: UIImage?##UIImage?#>, for: <#T##UIControl.State#>)
    //            }
    //            return cell3
    //        }
    
    
    @objc func btnshowimagepressed(sender:UIButton){
        imageview.isHidden=false
        btnzoom.isHidden=false
        let url = URL(string:imageurlgift+(gift?[sender.tag].gift_images?[0].gift_image ?? ""))
        imageview.kf.setImage(with: url)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        if collectionView==collectionviewaddons{
            let category = addoncategory?[indexPath.row].addon_cat_name
            
            let attributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 22)]
            
            return CGSize(width: category!.size(withAttributes: attributes).width + 20, height: 36)
        }else if collectionView==collectionviewbrands {
            
            let category = addonbrands?[indexPath.row].addon_brand_name
            
            let attributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 22)]
            
            return CGSize(width: category!.size(withAttributes: attributes).width + 20, height: 36)
        }else{
            return CGSize(width: collectionView.frame.width/2 - 30, height: CGFloat(250))
        }
    }
    
    func passback(varientsname: String,row:Int,selectedvarience:Int) {
        
      
        varientname=varientsname
        selectectedrow=row
        gift?[row].selectedVarienceIndex=selectedvarience
        collectionviewgift.reloadData()
         updateCartByView()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView==collectionviewaddons{
            selectedindex=indexPath.row
            addonbookmodel.addon_cat_id=self.addoncategory?[indexPath.row].addon_cat_id ?? ""
            //            viewaddonheight.constant=480
            //            self.viewheightrorscrolling.constant=1300+795+480
            //            lblbrands.isHidden=false
            addonbookmodel.addon_brand{ (model) in
                self.addonbranddata(data:model)
            }
            nameofjwellery=addoncategory?[indexPath.row].addon_cat_name ?? ""
            self.collectionviewaddons.reloadData()
        }else if collectionView==collectionviewbrands {
            selectedindexaddonbrand=indexPath.row
            if addonbrands?.count ?? 0>0{
                addonbookmodel.addon_cat_id=self.addonbrands?[indexPath.row].addon_cat_id ?? ""
                addonbookmodel.addonbrandid=self.addonbrands?[indexPath.row].addon_brand_id ?? ""
                //                viewaddonheight.constant=684
                //                self.viewheightrorscrolling.constant=1300+795+684
                
                addonbookmodel.gift{ (model) in
                    self.giftdata(data:model)
                }
                nameofbrand=addonbrands?[indexPath.row].addon_brand_name ?? ""
                
                self.collectionviewbrands.reloadData()
            }
            
        }else{
            selectedgiftindex=indexPath.row
            //            priceofgiftselected=self.gift?[indexPath.row].base_price ?? ""
            
            //            addonviewheight.constant=114
            //            self.viewheightrorscrolling.constant=1300+795+684+114
            //            lblpriceofgift.text=priceofgiftselected
            //            lbljwelleryselected.text=nameofjwellery+" > "+nameofbrand
            //            lblgiftselected.text=gift?[indexPath.row].title
            //            carprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
            //            lbltotalprice.text="QAR "+String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
            self.collectionviewgift.reloadData()
            if gift != nil{
                let productVC = UIStoryboard.init(name: "ProductScreen", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsVC_id") as? ProductDetailsVC
                    productVC?.gift = gift?[indexPath.row]
                productVC?.imageurlgift = imageurlgift
                self.navigationController?.pushViewController(productVC!, animated: true)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionviewbrands {
            let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
    @objc func bookgiftaction(sender:UIButton){
        
        gift?[sender.tag].count = 1
        collectionviewgift.reloadData()
        let item = (gift?[sender.tag])!
        cart?.append(item)
        updateCart()
    }
    @objc func minusPressed(sender:UIButton){
        let num = (gift?[sender.tag - 700].count)!
        gift?[sender.tag - 700].count = num - 1
        let mainItem = (gift?[sender.tag - 700])!
        for (index,item) in cart!.enumerated() {
            if mainItem.gift_id == item.gift_id {
                if mainItem.count > 0 {
                    cart![index] = mainItem
                } else {
                    cart?.remove(at: index)
                }
            }
        }
        collectionviewgift.reloadData()
        updateCart()
        
    }
    @objc func plusPressed(sender:UIButton){
        let num = (gift?[sender.tag - 500].count)!
        gift?[sender.tag - 500].count = num + 1
        let mainItem = (gift?[sender.tag - 500])!
        for (index,item) in cart!.enumerated() {
            if mainItem.gift_id == item.gift_id {
                cart![index] = mainItem
            }
        }
        
        collectionviewgift.reloadData()
        updateCart()
        
    }
    @objc func var1Pressed(sender:UIButton){
        let num = sender.tag - 1000
        gift?[num].selectedVarienceIndex = 0
        collectionviewgift.reloadData()
        updateCartByView()
        
    }
    @objc func var2Pressed(sender:UIButton){
        let num = sender.tag - 1100
        gift?[num].selectedVarienceIndex = 1
        collectionviewgift.reloadData()
        updateCartByView()
    }
    @objc func var3Pressed(sender:UIButton){
        let num = sender.tag - 1200
        gift?[num].selectedVarienceIndex = 2
        collectionviewgift.reloadData()
        updateCartByView()
    }
    @objc func var4Pressed(sender:UIButton){
        let num = sender.tag - 1300
        gift?[num].selectedVarienceIndex = 3
        collectionviewgift.reloadData()
        updateCartByView()
    }
    @objc func var5Pressed(sender:UIButton){
        let num = sender.tag - 1400
        gift?[num].selectedVarienceIndex = 4
        collectionviewgift.reloadData()
        updateCartByView()
    }
    func updateCart()  {
        if cart!.count > 0 {
            viewCartView.isHidden = false
            var price = 0
            for (index,item) in cart!.enumerated() {
                price = price + (item.count * Int(item.base_price!)!)
            }
            totalPriceLabel.text = String(price)
        } else {
            viewCartView.isHidden = true
        }
    }
    func updateViewByCart()  {
        if cart!.count > 0 && gift!.count > 0 {
            for (i,mainItem) in gift!.enumerated() {
                for (j,cartItem) in cart!.enumerated() {
                    if mainItem.gift_id == cartItem.gift_id {
                        gift![i] = cartItem
                    }
                }
            }
        }
    }
    func updateCartByView()  {
        if cart!.count > 0 && gift!.count > 0 {
            for (i,mainItem) in gift!.enumerated() {
                for (j,cartItem) in cart!.enumerated() {
                    if mainItem.gift_id == cartItem.gift_id {
                        cart![j] = mainItem
                    }
                }
            }
        }
    }
    
    func addoncategorydata(data: Addonbookclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                
                if self.addoncategory?.count ?? 0==0{
                    self.addoncategory=data.data?.addon_category
                }else{
                    self.addoncategory!+=(data.data?.addon_category)!
                }
                self.imageurl=data.data?.image_url ?? ""
                self.selectedindex=0
                self.addonbookmodel.addon_cat_id=self.addoncategory?[0].addon_cat_id ?? ""
                self.addonbookmodel.addon_brand{ (model) in
                    self.addonbranddata(data:model)
                }
               self.lblnoproductsavailable.isHidden=true
                self.collectionviewaddons.reloadData()
                self.collectionviewbrands.reloadData()
                self.collectionviewgift.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.lblnoproductsavailable.isHidden=false
                self.collectionviewaddons.reloadData()
                self.collectionviewbrands.reloadData()
                //self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewgift.reloadData()
            }
        }
    }
    
    
    func addonbranddata(data: Addonbookbrandclass) {
        print("data",data)
        addonbrands=[]
         gift=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.addonbrands=data.data?.addon_brands
                self.imageurladdonbrand=data.data?.image_url ?? ""
                self.selectedindexaddonbrand=0
                self.collectionviewbrands.reloadData()
                self.collectionviewaddons.reloadData()
                self.addonbookmodel.addon_cat_id = self.addonbrands?[0].addon_cat_id ?? ""
                self.addonbookmodel.addonbrandid = self.addonbrands?[0].addon_brand_id ?? ""
                
                self.addonbookmodel.gift{ (model) in
                    self.giftdata(data:model)
                }
                
                self.lblnoproductsavailable.isHidden=true
            }
        }else{
            
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewbrands.reloadData()
                self.collectionviewgift.reloadData()
                self.lblnoproductsavailable.isHidden=false
            }
        }
    }
    func giftdata(data: Giftbookclass) {
        print("data",data)
        //        gift=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.gift=data.data?.gift
                print("gift",self.gift)
                self.imageurlgift=data.data?.image_url ?? ""
                
                print("imageurlgift",self.imageurlgift)
                self.coloImageUrl=data.data?.color_image_url ?? ""
                self.collectionviewgift.reloadData()
                self.updateViewByCart()
            }
        }else{
            DispatchQueue.main.async{
                //                   self.viewaddonheight.constant=480
                //                   self.viewheightrorscrolling.constant=1300+795+480
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewgift.reloadData()
            }
        }
    }
    
    
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showCartaddon") {
            print("cart",cart)
            let vc = segue.destination as! CartViewController
            vc.cart = cart
            vc.imageurlgift = imageurlgift
        }
    }
    
    
}
