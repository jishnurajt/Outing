//
//  BrandsCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class BrandsBookCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewbrands: UIView!
    @IBOutlet weak var lblbrands: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
