//
//  VarientsCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 12/11/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class VarientsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageviewvarients: UIImageView!
    
    @IBOutlet weak var btnvarients: UIButton!
}
