//
//  ContactusViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 01/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class ContactusViewController: UIViewController {

    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var txtfldmeassage: UITextField!
    @IBOutlet weak var txtfleemail: UITextField!
    @IBOutlet weak var txtfldphone: UITextField!
    @IBOutlet weak var txtfldname: UITextField!
    @IBOutlet weak var btnsubmit: UIButton!
    var loginmodel=Loginviewmodel()
    var fblink=String()
    var instagaramlink=String()
    var whatsappnumber=String()
    var contactnumber=String()
     var address=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginmodel.social{ (model) in
                   self.socialdata(data:model)
               }

        btnsubmit.layer.cornerRadius=15
        
        btnsidemenu.layer.masksToBounds=true
                       
                       btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
             btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
    }
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
       let storyboard = UIStoryboard(name: "Menu", bundle: nil)
         let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
         
         let navController = UINavigationController(rootViewController: vc!)
         navController.modalPresentationStyle = .fullScreen
          navController.modalPresentationStyle = .overCurrentContext
         navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
                vc?.cart=cart
                vc?.imageurlgift=imageurlgift
                       }
         self.present(navController, animated:false, completion: nil)
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsubmitaction(_ sender: Any) {
        if (txtfldname.text == ""){
                    
                    self.showToast(message: "Please enter your name", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                    
                }else if (txtfldphone.text == ""){
                    
                    self.showToast(message: "Please enter phonenumber", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                    
                }else if txtfleemail.text == ""{
        
                    self.showToast(message: "Plaese enter email", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
        
                
                
        //        else if(txtfldpassword.text!.count < 8){
        //
        //            self.showToast(message: "Please enter password with atleast 8 characters", font: UIFont.boldSystemFont(ofSize: 13), duration: 2)
        //        }
                    
                    
                } else if !isValidEmail(testStr: txtfleemail.text!){
                    
                    self.showToast(message: "Please enter valid email id", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                    
                }else{
                    loginmodel.emailcontact=txtfleemail.text ?? ""
                    loginmodel.phone=txtfldphone.text ?? ""
            loginmodel.name=txtfldname.text ?? ""
            loginmodel.message=txtfldmeassage.text ?? ""
                    loginmodel.contact{ (model) in
                        self.contactdata(data:model)
                    }
                }
            
            }
                
                func isValidEmail(testStr:String) -> Bool {
                    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
                    
                    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
                    return emailTest.evaluate(with: testStr)
                }
    
    
    
    func contactdata(data: Contactusclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
               
                self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.txtfldmeassage.text=""
                self.txtfleemail.text=""
                self.txtfldphone.text=""
                self.txtfldname.text=""
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
               
            }
        }
    }
    
    func socialdata(data: Socialclass) {
               print("data",data)
               
               let status=data.status
               if status==true{
                   DispatchQueue.main.async{
                       self.fblink=data.data?.details?[0].fb ?? ""
                       self.instagaramlink=data.data?.details?[0].instagram ?? ""
                       self.whatsappnumber=data.data?.details?[0].whatsapp ?? ""
                       self.contactnumber=data.data?.details?[0].contact_number ?? ""
                       self.address=data.data?.details?[0].address ?? ""
                    self.lbladdress.text=self.address
                    print("self.fblink", self.fblink)
                      
                   }
               }else{
                   DispatchQueue.main.async{
                       self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                      
                   }
               }
           }
           
      
    @IBAction func btnfbaction(_ sender: Any) {
       
        if fblink==""||fblink.isEmpty==true{
        
        }else{
             let document = self.storyboard?.instantiateViewController(withIdentifier: "DocumentviewViewController") as! DocumentviewViewController
        document.myurl=URL(string:fblink)
            if cart?.count ?? 0>0{
                           document.cart=cart
                           document.imageurlgift=imageurlgift
                           }
             self.navigationController?.pushViewController(document, animated: true)
        }

       
        
    }
    
    
    @IBAction func btninstagaramaction(_ sender: Any) {
       
        if instagaramlink==""||instagaramlink.count==0{
            
        }else{
             let document = self.storyboard?.instantiateViewController(withIdentifier: "DocumentviewViewController") as! DocumentviewViewController
            if cart?.count ?? 0>0{
            document.cart=cart
            document.imageurlgift=imageurlgift
            }
             self.navigationController?.pushViewController(document, animated: true)
        
        document.myurl=URL(string:instagaramlink)
        }


       
        
    }
    
        
    @IBAction func btnwhatsappaction(_ sender: Any) {
        openWhatsapp()
        
        
    }
    func openWhatsapp(){
        let urlWhats = "whatsapp://send?phone=\(whatsappnumber)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    print("Install Whatsapp")
                }
            }
        }
    }
}
    
    
   

