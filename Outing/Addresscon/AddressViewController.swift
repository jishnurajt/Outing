//
//  AddressViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 01/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import RxSwift

class AddressViewController: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var txtfldother: UITextField!
    @IBOutlet weak var txtfldcity: UITextField!
    @IBOutlet weak var txtfldzone: UITextField!
    @IBOutlet weak var txtfldstreet: UITextField!
    @IBOutlet weak var txtfldbuilding: UITextField!
    @IBOutlet weak var btnsidemenu: UIButton!
    
    
    @IBOutlet weak var btnsubmitt: UIButton!
    @IBOutlet weak var viewadress: UIView!
    @IBOutlet weak var btnaddnew: UIButton!
    @IBOutlet weak var lblcurrentadress: UILabel!
    
    @IBOutlet weak var viewadressheight: NSLayoutConstraint!
    var loginmodel=Loginviewmodel()
    var model = ProfileViewModel()
       let bag = DisposeBag()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDetails()
         btnaddnew.layer.cornerRadius=20
        btnsubmitt.layer.cornerRadius=20
        btnsidemenu.layer.masksToBounds=true
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
       if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
             btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
    }
    

    @IBAction func btnsidemenuaction(_ sender: Any) {
       let storyboard = UIStoryboard(name: "Menu", bundle: nil)
         let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
         
         let navController = UINavigationController(rootViewController: vc!)
         navController.modalPresentationStyle = .fullScreen
          navController.modalPresentationStyle = .overCurrentContext
         navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
                       }
         self.present(navController, animated:false, completion: nil)
    }
    
     @IBAction func btnbackaction(_ sender: Any) {
      
        self.navigationController?.popViewController(animated: true)
     }
     
   func getUserDetails() {
   model.getUserDetails().subscribe(
       onNext:{ data in
           
        if let details = data.details{
            let address = details.address
            if address==""{
                self.viewadressheight.constant=640
                self.lblcurrentadress.isHidden=true
                self.btnaddnew.isHidden=true
                self.scrollview.isScrollEnabled=true
            }else{
                self.lblcurrentadress.text=address
                self.viewadressheight.constant=0
                 self.viewadress.isHidden=true
                self.lblcurrentadress.isHidden=false
                self.btnaddnew.isHidden=false
                 self.scrollview.isScrollEnabled=false
           }
        }
        
   },onError:{ error in
                      print("error")
                  }).disposed(by: bag)
              }
    
    
    @IBAction func btnsubmitaction(_ sender: Any) {
        let adress_edited=(txtfldstreet.text ?? "") + "\n" + (txtfldcity.text ?? "")
         loginmodel.addressedited=adress_edited
        loginmodel.profile_edit{ (model) in
                self.profile_editdata(data:model)
               }
        
    }
    
    
    @IBAction func btnaddnewaction(_ sender: Any) {
         self.viewadressheight.constant=640
         self.btnaddnew.isHidden=true
         self.scrollview.isScrollEnabled=true
         self.viewadress.isHidden=false
    }
    
    func profile_editdata(data: Profile_editclass) {
                  print("data",data)
                  
                  let status=data.status
                  if status==true{
                   DispatchQueue.main.async{
                       
                    self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                   
                       self.getUserDetails()
                    
                      }
                  }else{
                      DispatchQueue.main.async{
                          self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                         
                      }
                  }
              }
        
        
    }


