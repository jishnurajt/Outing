//
//  LeftMenuTableViewCell.swift
//  Outing
//
//  Created by Personal on 27/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
       @IBOutlet weak var labelName: UILabel!
       @IBOutlet weak var imageViewGradient: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(item: MenuItemModel) {
        if let name = item.name {
            self.labelName.text = name
        }
    if let image = item.image {
                   self.imageViewIcon.image = UIImage(named: image)
               }
           }
}
