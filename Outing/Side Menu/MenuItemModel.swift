//
//  MenuItemModel.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

class MenuItemModel {
    var name: String?
    var image: String?
}
