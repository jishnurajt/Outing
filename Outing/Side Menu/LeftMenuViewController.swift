//
//  LeftMenuViewController.swift
//  Outing
//
//  Created by Personal on 27/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {
    
    @IBOutlet weak var btnsdemenu: UIButton!
    var arrayMenuData = [MenuItemModel]()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMenuData()
        
        print("cart",cart)
        btnsdemenu.layer.masksToBounds=true
        
        btnsdemenu.layer.cornerRadius=btnsdemenu.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsdemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsdemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        
    }
    
    func showProfile() {
        if let userid=UserDefaults.standard.value(forKey: "userid"){
            let home = UIStoryboard.init(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            if cart?.count ?? 0>0{
                home?.cart=cart
                home?.imageurlgift=imageurlgift
            }
            
            self.navigationController?.pushViewController(home!, animated: true)
        }else{
            self.showToast(message: "Please login to continue", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
        }
    }
    
    @IBAction func btndismiss(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func btndismissaction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}




//MARK:- Table view data source methods
extension LeftMenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath) as! LeftMenuTableViewCell
        cell.configure(item: arrayMenuData[indexPath.row])
        return cell
    }
    
}

//MARK:- Table view delegate methods
extension LeftMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let userid=UserDefaults.standard.value(forKey: "userid"){
            if indexPath.row == 4{
                self.showProfile()
                
                
            }
            else if indexPath.row == 3 {
                
                
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RewardpointsViewController") as? RewardpointsViewController
                //                if cart?.count ?? 0>0{
                //                    home?.cart=cart
                //                    home?.imageurlgift=imageurlgift
                //                }
                
                
                self.navigationController?.pushViewController(home!, animated: true)
            }
            else if indexPath.row == 5{
                
                //            let VC1 = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
                //            let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                //            navController.navigationBar.isHidden=true
                //            self.present(navController, animated:false, completion: nil)
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressViewController") as? AddressViewController
                if cart?.count ?? 0>0{
                    home?.cart=cart
                    home?.imageurlgift=imageurlgift
                }
                
                self.navigationController?.pushViewController(home!, animated: true)
            }
            else if indexPath.row == 8{
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactusViewController") as? ContactusViewController
                if cart?.count ?? 0>0{
                    home?.cart=cart
                    home?.imageurlgift=imageurlgift
                }
                
                self.navigationController?.pushViewController(home!, animated: true)
            }
            else if indexPath.row == 1{
                let home = UIStoryboard.init(name: "Menu", bundle:nil).instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
                //                home.paymentflag=1
                //                if cart?.count ?? 0>0{
                //                    home.cart=cart
                //                    home.imageurlgift=imageurlgift
                //                }
                self.navigationController?.pushViewController(home, animated: true)
                
                
            }
            else if indexPath.row == 2{
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "paymentViewController") as! paymentViewController
                home.paymentflag=2
                if cart?.count ?? 0>0{
                    home.cart=cart
                    home.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(home, animated: true)
                
                
            }
            else if indexPath.row == 7{
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "paymentViewController") as! paymentViewController
                home.paymentflag=3
                if cart?.count ?? 0>0{
                    home.cart=cart
                    home.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(home, animated: true)
            }
            else if indexPath.row == 6{
                
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
                if cart?.count ?? 0>0{
                    home.cart=cart
                    home.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(home, animated: true)
                
                
            }else{
                
                
            }
            
        }else{
            self.showToast(message: "Please login to continue", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
        }
        
        
        if indexPath.row == 9{
            if let userid=UserDefaults.standard.value(forKey: "userid"){
                UserDefaults.standard.removeObject(forKey: "userid")
                UserDefaults.standard.removeObject(forKey: "userimage")
                self.showToast(message: "Successfully logged out", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
                
                self.arrayMenuData[indexPath.row].name="Login"
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                if cart?.count ?? 0>0{
                    home.cart=cart
                    home.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(home, animated: true)
                
                
                tableView.reloadData()
                
            }else{
                
                self.arrayMenuData[indexPath.row].name="Login"
                tableView.reloadData()
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                if cart?.count ?? 0>0{
                    home.cart=cart
                    home.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(home, animated: true)

            }
            
            
        }  else if indexPath.row == 0 {
            
            
            let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            if cart?.count ?? 0>0{
                home?.cart=cart
                home?.imageurlgift=imageurlgift
            }
            
            
            self.navigationController?.pushViewController(home!, animated: true)
        }
        
    }
}

//MARK:- To setup Side menu data
extension LeftMenuViewController {
    
    func setMenuData() {
        
        let itemOne = MenuItemModel()
        itemOne.name = "Home"
        itemOne.image = "ic-home"
        
        let itemTwo = MenuItemModel()
        itemTwo.name = "Bookings"
        itemTwo.image = "ic-booking"
        
        let itemThree = MenuItemModel()
        itemThree.name = "Payments"
        itemThree.image = "ic-payments"
        
        let itemFour = MenuItemModel()
        itemFour.name = "Reward Points"
        itemFour.image = "ic_reward_icon"
        
        let itemFive = MenuItemModel()
        itemFive.name = "Profile"
        itemFive.image = "ic-profile"
        
        let itemSix = MenuItemModel()
        itemSix.name = "Address"
        itemSix.image = "ic-address"
        
        let itemSeven = MenuItemModel()
        itemSeven.name = "My Cart"
        itemSeven.image = "ic-card"
        
        let itemEight = MenuItemModel()
        itemEight.name = "Reviews"
        itemEight.image = "ic-review"
        
        let itemNine = MenuItemModel()
        itemNine.name = "Contact us"
        itemNine.image = "ic-chat"
        
        let itemTen = MenuItemModel()
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            itemTen.name = "Logout"
        }else{
            itemTen.name = "Login"
        }
        itemTen.image = "ic-logout"
        
        self.arrayMenuData.append(itemOne)
        self.arrayMenuData.append(itemTwo)
        self.arrayMenuData.append(itemThree)
        self.arrayMenuData.append(itemFour)
        self.arrayMenuData.append(itemFive)
        self.arrayMenuData.append(itemSix)
        self.arrayMenuData.append(itemSeven)
        self.arrayMenuData.append(itemEight)
        self.arrayMenuData.append(itemNine)
        self.arrayMenuData.append(itemTen)
        
    }
}
