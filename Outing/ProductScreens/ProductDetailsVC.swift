//
//  ProductDetailsVC.swift
//  Outing
//
//  Created by Sooraj on 29/05/21.
//  Copyright © 2021 Arun Vijayan. All rights reserved.
//

import UIKit
import CoreData
class ProductDetailsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDetails: UILabel!
    @IBOutlet weak var productAmount: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    @IBOutlet weak var plusBGView: UIView!
    @IBOutlet weak var minusBGView: UIView!
    @IBOutlet weak var cartBGView: UIView!
    @IBOutlet weak var productImagesCV: UICollectionView!
    @IBOutlet weak var viewCartView: UIView!
    @IBOutlet weak var productImagePageControl: UIPageControl!
    
    var gift:Giftbook?
    var images:[Gift_book_images] = []
    var imageurlgift = ""
    var bannerTimer:Timer?
    var currentBanner = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        productQuantity.text = "0"
        updateview()
        productImagesCV.delegate = self
        productImagesCV.dataSource = self
        //backButton.layer.cornerRadius = 25
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if let giftData = gift{
            setDetails(data: giftData)
        }
    }
    
    func updateview(){
        let current = Int(productQuantity.text ?? "0") ?? 0
        plusBGView.isHidden = current == 0
        minusBGView.isHidden = current == 0
        //cartBGView.isHidden = current != 0
        productQuantity.isHidden = current == 0
        
        viewCartView.isHidden = current == 0
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bannerTimer?.invalidate()
    }
    @IBAction func cartAction(_ sender: UIButton) {
        productQuantity.text = "1"
        updateview()
        updateProductCount(isBack: false)
        
    }
    
    @IBAction func plusAction(_ sender: UIButton) {
        let current = Int(productQuantity.text ?? "0") ?? 0
        productQuantity.text = "\(current + 1)"
        updateview()
        updateProductCount(isBack: false)
    }
    @IBAction func minusAction(_ sender: UIButton) {
        let current = Int(productQuantity.text ?? "0") ?? 0
        productQuantity.text = "\(current - 1)"
        updateview()
        updateProductCount(isBack: false)
    }
    func setDetails(data:Giftbook){
        productTitle.text = data.title
        productDetails.text = data.description
        productAmount.text = "QAR " + (data.base_price ?? "")
        images = data.gift_images ?? []
        productImagePageControl.numberOfPages = images.count
        productImagesCV.reloadData()
        startBannerSlide()
        fetchdata()
    }
    
    func startBannerSlide(){
        if self.images.count > 1{
            bannerTimer?.invalidate()
            bannerTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
                if self.currentBanner == self.images.count - 1 {
                    self.currentBanner = 0
                    self.productImagePageControl.currentPage = self.currentBanner
                } else {
                    self.currentBanner += 1
                    self.productImagePageControl.currentPage = self.currentBanner
                }
                self.productImagesCV.scrollToItem(at: IndexPath(row: self.currentBanner, section: 0), at: .right, animated: true)
                print("current slide \(self.currentBanner)")
            }
        }
    }
    
    func addCartData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let manageContent = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Cartdetail", in: manageContent)!
        
        let users = NSManagedObject(entity: userEntity, insertInto: manageContent)
        guard let data = gift else {return}
        users.setValue(data.title, forKeyPath: "carname")
        users.setValue(data.base_price, forKeyPath: "carprice")
        users.setValue(data.gift_images?.count ?? 0 > 0 ? (imageurlgift + (data.gift_images?[0].gift_image ?? "")) : "", forKeyPath: "carimage")
        users.setValue(productQuantity.text ?? "0", forKeyPath: "quantity")
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            users.setValue(user_id, forKeyPath: "userid")
        }
        users.setValue(data.gift_id, forKey: "car_id")
        users.setValue("4", forKey: "flag")
        do{
            try manageContent.save()
        }catch let error as NSError {
            
            print("could not save . \(error), \(error.userInfo)")
        }
        //self.navigationController?.popViewController(animated: true)
    }
    
    func fetchdata(){
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Cartdetail")
        var cartdetail:[Cartdetail] = []
        do {
            guard let data = gift else {return}
            cartdetail = try managedContext.fetch(fetchRequest) as! [Cartdetail]
            for (index,item) in cartdetail.enumerated(){
                if item.car_id == data.gift_id{
                    productQuantity.text = cartdetail[index].quantity
                    updateview()
                    break
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func updateProductCount(isBack:Bool = true){
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context =
            appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cartdetail")
        guard let data = gift else {return}
        let id = data.gift_id ?? ""
        request.predicate = NSPredicate(format: "car_id == %@",id)
        do{
            let data = try context.fetch(request) as? [Cartdetail] ?? []
            if data.count == 0{
                addCartData()
            }else{
                if productQuantity.text ?? "0" == "0"{
                    context.delete(data[0])
                }else{
                    data[0].quantity = productQuantity.text ?? "0"
                }
                try context.save()
                if isBack{
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }catch{
            print("data not found")
        }
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        updateProductCount()
    }
    
    @IBAction func viewCart(_ sender: UIButton) {
        
        if let userid=UserDefaults.standard.value(forKey: "userid"){
            let cart1 = UIStoryboard(name: "Main", bundle: nil) .instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            cart1.imageurlgift=self.imageurlgift
            self.navigationController?.pushViewController(cart1, animated: true)
        }else{
            self.showToast(message: "Please login to continue", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
            login.cartflag=1
            login.imageurlgift=imageurlgift
            
            self.navigationController?.pushViewController(login, animated: true)
        }
        
        
    }
    
    //MARK:- CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: productImagesCV.frame.width, height: (productImagesCV.frame.height))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCVC_id", for: indexPath) as! ProductDetailsCVC
        let data = images[indexPath.item]
        cell.productImage.kf.indicatorType = .activity
        cell.productImage.contentMode = .scaleAspectFit
        cell.productImage.kf.setImage(with: URL(string: imageurlgift + (data.gift_image ?? "")))
        return cell
    }
    
    
}


class curvedButtonView: UIView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
    }
    
}

class curvedView: UIView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.clipsToBounds = true
        self.layer.cornerRadius = 20
    }
    
}

class roundedView: UIView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.height/2
    }
    
}

class ChangeToTintColor: UIImageView {
    override func awakeFromNib() {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = self.tintColor
    }
}
