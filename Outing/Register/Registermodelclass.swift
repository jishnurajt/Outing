//
//  Registermodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 12/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
//send_user
struct Registerclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
//user_add
struct Useraddclass : Codable {
    let status : Bool?
    let message : String?
    let data : UserData?
    let user_image_path : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
        case user_image_path = "user_image_path"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(UserData.self, forKey: .data)
        user_image_path = try values.decodeIfPresent(String.self, forKey: .user_image_path)
    }

}
struct UserData : Codable {
    let user_id : String?
    let username : String?
    let email_id : String?
    let mobile : String?
    let dob : String?
    let password : String?
    let last_login : String?
    let user_image : String?
    let device_id : String?
    let device_token : String?
    let latitude : String?
    let longitude : String?
    let source : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case username = "username"
        case email_id = "email_id"
        case mobile = "mobile"
        case dob = "dob"
        case password = "password"
        case last_login = "last_login"
        case user_image = "user_image"
        case device_id = "device_id"
        case device_token = "device_token"
        case latitude = "latitude"
        case longitude = "longitude"
        case source = "source"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        last_login = try values.decodeIfPresent(String.self, forKey: .last_login)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
