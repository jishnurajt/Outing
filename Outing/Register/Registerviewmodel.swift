//
//  Registerviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 12/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class RegisterViewModel {
   
   var email=String()
    var mobile=String()
    var name=String()
    var password=String()
    var deviceid=1
    var devicetoken=1
    var source=2
    var latitude=String()
    var longitude=String()
    var otp=String()
    var dob=String()
    
    
func send_user(completion : @escaping (Registerclass) -> ())  {
     

     let poststring="email_id=\(email)&mobile=\(mobile)&security_token=out564bkgtsernsko"
     
     var request = NSMutableURLRequest(url: APIEndPoint.send_user.url as URL)
     request.httpMethod = "POST"
     request.httpBody = poststring.data(using: String.Encoding.utf8)
     let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
          print("poststring",poststring)
         //self.showIndicator(isHidden: true)
         if error != nil{
             print("error",error)
             return
         }
         let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
         
         print("respnsedate,\(responsestring)")
         do {
             
            
                 let decoder = JSONDecoder()
                 let model = try decoder.decode(Registerclass.self, from:
                     data!) //Decode JSON Response Data
                 print(model)
             completion(model)
             
             
         } catch let error as NSError {
         }
     }
     task.resume()
 }
    
    func user_add(completion : @escaping (Useraddclass) -> ())  {
         

         let poststring="email_id=\(email)&mobile=\(mobile)&security_token=out564bkgtsernsko&name=\(name)&latitude=\(latitude)&longitude=\(longitude)&device_id=1&device_token=1&source=2&dob=\(dob)&password=\(password)"
         
         var request = NSMutableURLRequest(url: APIEndPoint.user_add.url as URL)
         request.httpMethod = "POST"
         request.httpBody = poststring.data(using: String.Encoding.utf8)
         let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
             //self.showIndicator(isHidden: true)
             if error != nil{
                 print("error",error)
                 return
             }
             let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
             
             print("respnsedate,\(responsestring)")
             do {
                 
                
                     let decoder = JSONDecoder()
                     let model = try decoder.decode(Useraddclass.self, from:
                         data!) //Decode JSON Response Data
                     print(model)
                 completion(model)
                 
                 
             } catch let error as NSError {
             }
         }
         task.resume()
     }

    }



