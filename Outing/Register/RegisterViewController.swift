//
//  RegisterViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 07/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation
import GoogleMaps
import GooglePlaces


class RegisterViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnregister: UIButton!
    @IBOutlet weak var btncontinue: UIButton!
    @IBOutlet weak var txtfldotp6: UITextField!
    @IBOutlet weak var txtfldotp5: UITextField!
    @IBOutlet weak var txtfldotp4: UITextField!
    @IBOutlet weak var txtfldotp3: UITextField!
    @IBOutlet weak var txtfldotp2: UITextField!
    @IBOutlet weak var txtfldotp1: UITextField!
    @IBOutlet weak var lblotpemailid: UILabel!
    @IBOutlet weak var viewotp: UIView!
    @IBOutlet weak var txtfldpassword: UITextField!
    @IBOutlet weak var txtfldmobileniumber: UITextField!
    @IBOutlet weak var txtflddob: UITextField!
    @IBOutlet weak var txtfldemail: UITextField!
    @IBOutlet weak var txtfldfullname: UITextField!
    
    @IBOutlet weak var btnchooselicense: UIButton!
    @IBOutlet weak var lbllicense: UILabel!
    @IBOutlet weak var viewdoc3: UIView!
    @IBOutlet weak var viewdoc2: UIView!
    @IBOutlet weak var btnchooseqtrid: UIButton!
    @IBOutlet weak var lblqtrid: UILabel!
    @IBOutlet weak var btnchoosepassport: UIButton!
    @IBOutlet weak var lblpassport: UILabel!
    @IBOutlet weak var viewdoc1: UIView!
    @IBOutlet weak var viewuploading: UIView!
    @IBOutlet weak var mapview: GMSMapView!
    
    var registrationmodel=RegisterViewModel()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
     var cartflag=Int()
    var doclist:[Doc_listdocument]?=[]
    var typedoc_array=[String]()
    let bag=DisposeBag()
    var imageToUpload = UIImage()
    var model=ProfileViewModel()
     var loginmodel=Loginviewmodel()
    let locationManager = CLLocationManager()
       var latitude=CLLocationDegrees()
       var longitude=CLLocationDegrees()
   
    var userlocation=CLLocation()
    var placeaddress=String()
    
        private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
    
            let geocoder = GMSGeocoder()
            print("coordinate",coordinate)
            latitude=coordinate.latitude
            longitude=coordinate.longitude
            print("latitude",latitude)
             print("longitude",longitude)
            
            geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
                guard let address = response?.firstResult(), let lines = address.lines else {
                    return
                }
    
                // 3
                print("......",lines.joined(separator: "\n"))
    
    
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
    
        }
        
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewotp.isHidden=true
        txtfldfullname.placeholeder(name: "Full name")
        txtfldfullname.setLeftPaddingPoints(50)
        txtfldemail.placeholeder(name: "Email Address")
        txtfldemail.setLeftPaddingPoints(50)
        txtflddob.placeholeder(name: "Date of Birth")
        txtflddob.setLeftPaddingPoints(50)
        txtfldmobileniumber.placeholeder(name: "Mobile Number")
        txtfldmobileniumber.setLeftPaddingPoints(50)
        txtfldpassword.placeholeder(name: "Password")
        txtfldpassword.setLeftPaddingPoints(50)
        txtfldfullname.setcornerradius1()
        txtfldemail.setcornerradius1()
        txtflddob.setcornerradius1()
        txtfldmobileniumber.setcornerradius1()
        txtfldpassword.setcornerradius1()
        txtfldotp1.setbordercolour()
         txtfldotp2.setbordercolour()
         txtfldotp3.setbordercolour()
         txtfldotp4.setbordercolour()
         txtfldotp5.setbordercolour()
         txtfldotp6.setbordercolour()
        
        btncontinue.layer.cornerRadius=20
        btnregister.layer.cornerRadius=25
        
        
        txtfldotp1.delegate=self
        txtfldotp2.delegate=self
        txtfldotp3.delegate=self
        txtfldotp4.delegate=self
        txtfldotp5.delegate=self
        //txtfldotp1.becomeFirstResponder()
        
        txtfldotp1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtfldotp2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtfldotp3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtfldotp4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtfldotp5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
               
        txtfldotp6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
               
        viewdoc1.layer.cornerRadius=10
        viewdoc2.layer.cornerRadius=10
        viewdoc3.layer.cornerRadius=10
        btnchooseqtrid.layer.cornerRadius=10
        btnchoosepassport.layer.cornerRadius=10
        btnchooselicense.layer.cornerRadius=10
        locationupdate()
        
        hideKeyboardWhenTappedAround()
        
    }
    
        func locationupdate(){
    
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
    
            mapview.delegate=self
            mapview.isHidden=true
    
            //        mapview.camera=GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 9.582912, longitude: 76.547326), zoom: 15, bearing: 0, viewingAngle: 0)
    
            mapview.delegate = self
            mapview.isMyLocationEnabled = true
            mapview.settings.zoomGestures   = true
            
    
    
    
        }
    //
        
        
    @IBAction func dobAction(_ sender: Any) {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionStyles.showTime(false)
        selector.optionCalendarFontColorPastDates = .black
        selector.optionCalendarFontColorFutureDates = .lightGray
    //    let old = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
    //    selector.optionRangeOfEnabledDates.setStartDate(old)
        //selector.optionCurrentDateRange.setEndDate(Date())
     //   let old = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
      //  selector.optionCurrentDateRange .setStartDate(old)
        
        present(selector, animated: true, completion: nil)
    }
    
    
    @IBAction func btnregisteraction(_ sender: Any) {
        
        if(txtfldfullname.text == ""){
            
            self.showToast(message: "Please enter your name", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if (txtfldemail.text == ""){
            
            self.showToast(message: "Please enter your email", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if (txtflddob.text == ""){
            
            self.showToast(message: "Please enter your DOB", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if (txtfldmobileniumber.text == ""){
            
            self.showToast(message: "Please enter your phone number", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if (txtfldpassword.text == ""){
            
            self.showToast(message: "Please enter password", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if !isPasswordValid(txtfldpassword.text!){
            
            self.showToast(message: "Invalid Password", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }
        else if(txtfldpassword.text!.count < 8){
            
            self.showToast(message: "Please enter password with atleast 8 characters", font: UIFont.boldSystemFont(ofSize: 13), duration: 2)
        }
            
            
        else if !isValidEmail(testStr: txtfldemail.text!){
            
            self.showToast(message: "Please enter valid email id", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }else{
            registrationmodel.email=txtfldemail.text ?? ""
            registrationmodel.mobile=txtfldmobileniumber.text ?? ""
            registrationmodel.send_user{ (model) in
                self.registerdata(data:model)
            }
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^[A-Za-z\\d$@$#!%*?&]{1,}")
        return passwordTest.evaluate(with: password)
    }
    
    
    
    func registerdata(data: Registerclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.lblotpemailid.text="We send OTP to \(self.txtfldemail.text!)"
                self.viewotp.isHidden=false
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewotp.isHidden=true
            }
        }
    }
    
    @IBAction func btnloginaction(_ sender: Any) {
//        let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
//       if cart?.count ?? 0>0{
//                      login.cart=cart
//                      login.imageurlgift=imageurlgift
//                      }
//        self.navigationController?.pushViewController(login, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnchangeemail(_ sender: Any) {
        self.viewotp.isHidden=true
    }
    
    @objc func textFieldDidChange(textField: UITextField){

        let text = textField.text

        if text?.utf16.count==1{
            switch textField{
            case txtfldotp1:
                txtfldotp2.becomeFirstResponder()
            case txtfldotp2:
                txtfldotp3.becomeFirstResponder()
            case txtfldotp3:
                txtfldotp4.becomeFirstResponder()
            case txtfldotp4:
                txtfldotp5.becomeFirstResponder()
            case txtfldotp5:
                txtfldotp6.becomeFirstResponder()
            case txtfldotp6:
                txtfldotp6.resignFirstResponder()
            default:
                break
            }
        }else{

        }
    }
    
    
    @IBAction func btncontinueaction(_ sender: Any) {
        if(txtfldotp1.text == "")||(txtfldotp2.text == "")||(txtfldotp3.text == "")||(txtfldotp4.text == "")||(txtfldotp5.text == "")||(txtfldotp6.text == ""){

                   self.showToast(message: "Please enter OTP to continue", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)

        }else{
            let text1=(txtfldotp1.text ?? "")+(txtfldotp2.text ?? "")+(txtfldotp3.text ?? "")
            let text2=(txtfldotp4.text ?? "")+(txtfldotp5.text ?? "")+(txtfldotp6.text ?? "")
            registrationmodel.name=txtfldfullname.text ?? ""
            registrationmodel.password=txtfldpassword.text ?? ""
            registrationmodel.otp=text1+text2
            registrationmodel.dob=txtflddob.text ?? ""
            registrationmodel.latitude=String(latitude)
            registrationmodel.longitude=String(longitude)
        registrationmodel.user_add{ (model) in
            self.useradddata(data:model)
        }
        }
        
    }
    
    func useradddata(data: Useraddclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.viewotp.isHidden=true
               
                let userid=data.data?.user_id
                UserDefaults.standard.set(userid, forKey: "userid")
                
                let image=(data.user_image_path ?? "")+(data.data?.user_image ?? "")
                UserDefaults.standard.set("https://www.outing.qa/user_image/user.png", forKey: "notimage")
                UserDefaults.standard.set(image, forKey: "userimage")
                
                
                
//                if self.cartflag==1{
//                self.loginmodel.user_id=data.data?.user_id ?? ""
//                 self.loginmodel.document_list{ (model) in
//                    self.document_listdata(data:model)
//                    }
//                }else{
                 let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
                
                if self.cart?.count ?? 0>0{
                    home.cart=self.cart
                    home.imageurlgift=self.imageurlgift
               }
                       self.navigationController?.pushViewController(home, animated: true)
                
           // }
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewotp.isHidden=false
            }
        }
    }
    
    func document_listdata(data: Doclistclass) {
     
     let status=data.status
     if status==true{
         
       doclist=data.data?.doc_list
    
      if doclist?.count ?? 0 > 1{
              for i in 0...(doclist?.count ?? 0)-1{
                  typedoc_array.append(doclist?[i].type ?? "")
              }
                 }else{
                     typedoc_array.append(doclist?[0].type ?? "")
                 }
        
      DispatchQueue.main.async{
      
          if self.typedoc_array.contains("2"){
              self.viewdoc2.isHidden=true
      }
          if self.typedoc_array.contains("3"){
              self.viewdoc3.isHidden=true
      }
          if self.typedoc_array.contains("1"){
              self.viewdoc1.isHidden=true
      }
      }
      if typedoc_array.contains("1") && typedoc_array.contains("2")  && typedoc_array.contains("3"){
           DispatchQueue.main.async{
        self.viewuploading.isHidden=true
            
            
            let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            
             if self.cart?.count ?? 0>0{
                 cart1.cart=self.cart
                 cart1.imageurlgift=self.imageurlgift
             }
             self.navigationController?.pushViewController(cart1, animated: true)
        }
      }else{
             self.viewuploading.isHidden=false
        }
    
        
     }else{
         DispatchQueue.main.async{
            self.viewuploading.isHidden=false
        }
        
        }
    }
    func uploadUserData(documentType: String) {
        model.updateUserData(image: imageToUpload, type: documentType).subscribe(
            onNext:{ data in
                print("API success")
                self.loginmodel.document_list{ (model) in
                    self.document_listdata(data:model)
                }
        },onError:{ error in
            self.showToast(message: "Error in uploading please try again later", font: .boldSystemFont(ofSize: 13), duration: 2)
            print("error")
        }).disposed(by: bag)
    }
    
    
    
    func pickImage(type: DocumentType) {
        ImagePickerManager().pickImage(self) { image in
            // self.imageViewUser.image = image
            self.imageToUpload = image
            
            switch type {
            case .passport:
                self.uploadUserData(documentType: "1")
            case .license:
                self.uploadUserData(documentType: "3")
            case .qatarId:
                self.uploadUserData(documentType: "2")
            }
        }
    }
    
    
    @IBAction func btnpasspotcation(_ sender: Any) {
        self.pickImage(type: .passport)
    }
    @IBAction func btnqtridaction(_ sender: Any) {
        self.pickImage(type: .qatarId)
    }
    
    @IBAction func btnlicensecation(_ sender: Any) {
         self.pickImage(type: .license)
    }
    
    @IBAction func btncloseviewaction(_ sender: Any) {
        self.viewuploading.isHidden=true
    }
    
    
    
    
    @IBAction func btnresendotpaction(_ sender: Any) {
        self.viewotp.isHidden=false
        registrationmodel.email=txtfldemail.text ?? ""
                   registrationmodel.mobile=txtfldmobileniumber.text ?? ""
                   registrationmodel.send_user{ (model) in
                       self.registerdata(data:model)
                   }
        
    }
    
}
extension RegisterViewController: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()

        //5
        mapview.isMyLocationEnabled = true
        mapview.settings.myLocationButton = true
    }

    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        userlocation=locations.last!
       // let camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        // 7
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
        longitude: location.coordinate.longitude,
        zoom: 15)
        mapview.camera = camera
        latitude=location.coordinate.latitude
        longitude=location.coordinate.longitude
        // 8
        
        print("latitude",latitude)
        print("longitude",longitude)
        showMarker(position: camera.target)
        locationManager.stopUpdatingLocation()
        

    }

    func showMarker(position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
       
        // marker.title = "\(currentaddress)"
        print("title",marker.title)
        // marker.snippet = "San Francisco"
        marker.map = mapview
    }
}

extension RegisterViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)

    }


}
extension RegisterViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        placeaddress=place.formattedAddress!
        print("placeaddress",placeaddress)
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        latitude=place.coordinate.latitude
        longitude=place.coordinate.longitude
        print("lat",latitude)
        print("long",latitude)
        
       
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}





extension RegisterViewController: WWCalendarTimeSelectorProtocol{
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
       
        let order = NSCalendar.current.compare(Date(), to: date, toGranularity: .day)
               return order == .orderedDescending
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected Date- \(date)")
        
            
            let format = DateFormatter()
            format.dateFormat = "dd/MM/yyyy"
            format.timeZone = .current
            
            let selected=format.string(from: date)
            
        txtflddob.text = selected
        
    }
    
    
    
}

