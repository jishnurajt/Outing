//
//  ProfileViewController.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import UIKit
import RxSwift
import Kingfisher

enum DocumentType {
    case passport
    case qatarId
    case license
}


class ProfileViewController: UIViewController {

   
    @IBOutlet weak var btnimagepicker: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var txtflddob: UITextField!
    @IBOutlet weak var txtfldname: UITextField!
    @IBOutlet weak var viewdoctypetoedit: UIView!
    @IBOutlet weak var btnremove: UIButton!
    @IBOutlet weak var lbldocumentstring: UILabel!
    @IBOutlet weak var lbldocumenttype: UILabel!
    @IBOutlet weak var viewforeditinganduploading: UIView!
    @IBOutlet weak var btnview: UIButton!
    @IBOutlet weak var btnedit: UIButton!
    @IBOutlet weak var viewedit: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonProfileIcon: UIButton!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserDob: UILabel!
    
    @IBOutlet weak var btnprofileedit: UIButton!
    var model = ProfileViewModel()
    let bag = DisposeBag()
    
    var imageToUpload = UIImage()
    var arrayProfileData = [UserDataModel]()
    var userDetails: UserDetails?
    var userDocument = DocumentType.passport
    var userdocumentdata:[User_documents]=[]
    var documenturl=String()
    var novalueflag=Int()
    var urlstring:URL!
    var documenttype=String()
    var documentstring=String()
    var loginmodel=Loginviewmodel()
    var docidpassed=String()
    var cart:[Giftbook]?=[]
       var imageurlgift=String()
    
    let activityView = UIActivityIndicatorView(style: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUserDetails()
        self.setupData()
        btnview.layer.cornerRadius=10
        btnedit.layer.cornerRadius=10
        btnremove.layer.cornerRadius=10
        viewdoctypetoedit.layer.cornerRadius=10
      //  btnback.layer.cornerRadius=25
        self.btnimagepicker.isHidden=true
        
        buttonProfileIcon.layer.masksToBounds=true
                       
        buttonProfileIcon.layer.cornerRadius=buttonProfileIcon.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            buttonProfileIcon.kf.setImage(with: url, for: .normal)
        }else{
             buttonProfileIcon.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        btnprofileedit.setBackgroundImage(UIImage(named:"Icon material-edit"), for: .normal)
        btnprofileedit.setTitle(nil, for: .normal)
       
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        
    }
   
    
    
    @IBAction func actionProfileIcon(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
         let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
         
         let navController = UINavigationController(rootViewController: vc!)
         navController.modalPresentationStyle = .fullScreen
          navController.modalPresentationStyle = .overCurrentContext
         navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
        vc?.cart=cart
        vc?.imageurlgift=imageurlgift
        }
         self.present(navController, animated:false, completion: nil)
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- function to upload image to server
    /// parameters
    /// image - Document image picked from gallery or camera
    /// type -  1-passport,2-qatar id,3-license
    
    func uploadUserData(documentType: String) {
        activityView.startAnimating()
        model.updateUserData(image: imageToUpload, type: documentType).subscribe(
            onNext:{ data in
                print("API success")
               self.getUserDetails()
                self.btnimagepicker.isHidden=true
                self.activityView.stopAnimating()
                self.tableView.reloadData()
        },onError:{ error in
            print("error")
            self.activityView.stopAnimating()
        }).disposed(by: bag)
    }
    
    
    func uploadUserDataedit(username:String,dob:String) {
        activityView.startAnimating()
        model.updateUserDataedit(image: imageToUpload, user: username,do_b:dob).subscribe(
            onNext:{ data in
                print("API success")
               self.getUserDetails()
                self.btnimagepicker.isHidden=true
                self.tableView.reloadData()
                self.activityView.stopAnimating()
        },onError:{ error in
            print("error")
            self.activityView.stopAnimating()
        }).disposed(by: bag)
    }
    
    //MARK:- function to get user details from server
    func getUserDetails() {
        model.getUserDetails().subscribe(
            onNext:{ data in
                
                var imageBaseUrl = ""
                if let image = data.user_image_url {
                    imageBaseUrl = image
                }
                if let details = data.details {
                    self.userDetails = details
                    if let name = details.username {
//                        self.labelUserName.text = name
                        self.txtfldname.text=name
                    }
                    if let dob = details.dob {
//                        self.labelUserDob.text = "DOB: \(dob)"
                       // self.labelUserDob.text = "DOB:"
                        self.txtflddob.text=dob
                    }
                    if let image = details.user_image {
                        let url = URL(string: imageBaseUrl + image)
                        UserDefaults.standard.set(imageBaseUrl + image, forKey: "userimage")
                        self.imageViewUser.kf.setImage(with: url)
                    }
                }
                if let document = data.user_documents {
                self.userdocumentdata=document
                }
                self.documenturl=data.doc_url ?? ""
                
                self.arrayProfileData.removeAll()
                self.setupData()
                self.tableView.reloadData()
        },onError:{ error in
            print("error")
        }).disposed(by: bag)
    }
    
    //MARK:- function to pick image from gallery or camera
    func pickImage(type: DocumentType) {
        ImagePickerManager().pickImage(self) { image in
           // self.imageViewUser.image = image
            self.imageToUpload = image
            
            switch type {
            case .passport:
                self.uploadUserData(documentType: "1")
            case .license:
                 self.uploadUserData(documentType: "3")
            case .qatarId:
                self.uploadUserData(documentType: "2")
            }
        }
    }
    func pickImage1(){
        ImagePickerManager().pickImage(self) { image in
           // self.imageViewUser.image = image
            self.imageToUpload = image
            self.uploadUserDataedit(username:self.txtfldname.text ?? "",dob: self.txtflddob.text ?? "")
            
        }
    }
    
    @IBAction func btnviewaction(_ sender: Any) {
        if novalueflag==1{
            viewforeditinganduploading.isHidden=false
            lbldocumenttype.text=documenttype
            lbldocumentstring.text=documentstring
            btnremove.setTitle("Choose file", for: .normal)
            
        }else{
         let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DocumentviewViewController") as? DocumentviewViewController
        home!.myurl=urlstring
            if cart?.count ?? 0>0{
                home?.cart=cart
                home?.imageurlgift=imageurlgift
            }
        self.navigationController?.pushViewController(home!, animated: true)
        }
        
    }
    
    @IBAction func btneditaction(_ sender: Any) {
        viewedit.isHidden=true
        viewforeditinganduploading.isHidden=false
        lbldocumenttype.text=documenttype
        lbldocumentstring.text=documentstring
        if novalueflag==1{
            btnremove.setTitle("Choose file", for: .normal)
        }else{
            btnremove.setTitle("Remove", for: .normal)
        }
        
    }
    
    @IBAction func btnremoveaction(_ sender: Any) {
        if btnremove.currentTitle=="Choose file"{
            viewedit.isHidden=true
            viewforeditinganduploading.isHidden=true
            if documenttype=="Passport"{
            self.pickImage(type: .passport)
            }else if documenttype=="Qatar ID"{
            self.pickImage(type: .qatarId)
            }else if documenttype=="License"{
            self.pickImage(type: .qatarId)
        }
        }else{
            loginmodel.docid=docidpassed
            loginmodel.delete_doc{ (model) in
            self.deletedocdata(data:model)
            }
    
    }
    }
    
    @IBAction func btnimagepicker(_ sender: Any) {
        
        pickImage1()
        
    }
    
    
    
    func deletedocdata(data: Deletedocclass) {
           print("data",data)
           
           let status=data.status
           if status==true{
            DispatchQueue.main.async{
                self.viewforeditinganduploading.isHidden=true
                   self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.getUserDetails()
               }
           }else{
               DispatchQueue.main.async{
                   self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                  
               }
           }
       }
    
    @IBAction func btnclosecation(_ sender: Any) {
        viewforeditinganduploading.isHidden=true
    }
    
    @IBAction func btnprofileeditaction(_ sender: Any) {
        if btnprofileedit.currentBackgroundImage==UIImage(named:"Icon material-edit"){
            btnprofileedit.setBackgroundImage(UIImage(named:" "), for: .normal)
        btnprofileedit.setTitle("Save", for: .normal)
        txtfldname.isUserInteractionEnabled=true
        txtflddob.isUserInteractionEnabled=true
        btnimagepicker.isHidden=false

       }else{
       
        txtfldname.isUserInteractionEnabled=false
        txtflddob.isUserInteractionEnabled=false
        let username=txtfldname.text ?? ""
        let dob=txtflddob.text ?? ""
         loginmodel.usernameedited=username
         loginmodel.dobedited=dob
         loginmodel.profile_edit{ (model) in
         self.profile_editdata(data:model)
        }
//        imageToUpload = UIImage(named: " ")!
//       // pickImage1()
//        }
        
    }
    }
     func profile_editdata(data: Profile_editclass) {
              print("data",data)
              
              let status=data.status
              if status==true{
               DispatchQueue.main.async{
                self.btnprofileedit.setTitle(nil, for: .normal)
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.btnprofileedit.setBackgroundImage(UIImage(named:"Icon material-edit"), for: .normal)
                   self.getUserDetails()
                
                  }
              }else{
                  DispatchQueue.main.async{
                      self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                     
                  }
              }
          }
    
    
}

//MARK:- Table view data source methods
extension ProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayProfileData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.configure(item: arrayProfileData[indexPath.row])
        return cell
    }
}

//MARK:- Table view delegate methods
extension ProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            print("handle mobile")
        case 1:
            print("handle Email")
        case 2:
            print("passport")
            viewedit.isHidden=false
            documenttype=arrayProfileData[indexPath.row].itemName ?? ""
            documentstring=arrayProfileData[indexPath.row].itemValue ?? ""
            docidpassed=arrayProfileData[indexPath.row].docid ?? ""
            if documentstring==""{
                novalueflag=1
                btnedit.isHidden=true
                btnview.setTitle("Upload now", for: .normal)
            }else{
                 novalueflag=0
                urlstring=URL(string:arrayProfileData[indexPath.row].itemValue ?? "")
                btnedit.isHidden=false
                btnview.setTitle("View", for: .normal)
            }
           // self.pickImage(type: .passport)
        case 3:
            print("qatar id")
            viewedit.isHidden=false
            documenttype=arrayProfileData[indexPath.row].itemName ?? ""
             documentstring=arrayProfileData[indexPath.row].itemValue ?? ""
             docidpassed=arrayProfileData[indexPath.row].docid ?? ""
            print("documentstring",documentstring)
            if documentstring==""{
                novalueflag=1
                btnedit.isHidden=true
                btnview.setTitle("Upload now", for: .normal)
            }else{
                 novalueflag=0
                urlstring=URL(string:arrayProfileData[indexPath.row].itemValue ?? "")
                btnedit.isHidden=false
                btnview.setTitle("View", for: .normal)
            }
           // self.pickImage(type: .qatarId)
        case 4:
            print("license")
            viewedit.isHidden=false
            documenttype=arrayProfileData[indexPath.row].itemName ?? ""
             documentstring=arrayProfileData[indexPath.row].itemValue ?? ""
             docidpassed=arrayProfileData[indexPath.row].docid ?? ""
            if documentstring==""{
                novalueflag=1
                btnedit.isHidden=true
                btnview.setTitle("Upload now", for: .normal)
            }else{
                 novalueflag=0
                urlstring=URL(string:arrayProfileData[indexPath.row].itemValue ?? "")
                btnedit.isHidden=false
                btnview.setTitle("View", for: .normal)
            }
          //  self.pickImage(type: .license)
        default:
            break
        }
}
    
    
}

extension ProfileViewController {
    
    func setupData() {
        let itemOne = UserDataModel()
        itemOne.itemName = "Mobile"
        if let details = self.userDetails {
            if let mobile = details.mobile {
                itemOne.itemValue = mobile
                itemOne.icon="Icon awesome-mobile-alt"
                itemOne.icon1="Group 269-profile"
            }
        }
        
        let itemTwo = UserDataModel()
        itemTwo.itemName = "Email"
        
        if let details = self.userDetails {
            if let email = details.email_id {
                itemTwo.itemValue = email
                itemTwo.icon="Icon zocial-email-1"
                itemTwo.icon1="Group 269-profile"
            }
        }
        
        
        let itemThree = UserDataModel()
         let itemFour = UserDataModel()
         let itemFive = UserDataModel()
        
        if userdocumentdata.count>1{
        for i in 0...userdocumentdata.count-1{
            if userdocumentdata[i].type=="1"{
                itemThree.itemValue = documenturl+(userdocumentdata[i].document ?? "")
                itemThree.docid=(userdocumentdata[i].d_id ?? "")
            }else if userdocumentdata[i].type=="2"{
                itemFour.itemValue = documenturl+(userdocumentdata[i].document ?? "")
                itemFour.docid=(userdocumentdata[i].d_id ?? "")
            }else if userdocumentdata[i].type=="3"{
                print("documentur",documenturl+(userdocumentdata[i].document ?? ""))
                itemFive.itemValue = documenturl+(userdocumentdata[i].document ?? "")
                 itemFive.docid=(userdocumentdata[i].d_id ?? "")
            }
        }
        }else if userdocumentdata.count==1{
          if userdocumentdata[0].type=="1"{
                itemThree.itemValue = documenturl+(userdocumentdata[0].document ?? "")
            itemThree.docid=(userdocumentdata[0].d_id ?? "")
            }else if userdocumentdata[0].type=="2"{
                itemFour.itemValue = documenturl+(userdocumentdata[0].document ?? "")
            itemFour.docid=(userdocumentdata[0].d_id ?? "")
            }else if userdocumentdata[0].type=="3"{
                itemFive.itemValue = documenturl+(userdocumentdata[0].document ?? "")
            itemFive.docid=(userdocumentdata[0].d_id ?? "")
            }
        }else{
            
        }
        
        
        itemThree.itemName = "Passport"
        itemThree.icon = "Icon awesome-passport"
        itemThree.icon1="Group 270"
            
        
       
        itemFour.itemName = "Qatar ID"
        itemFour.icon = "Icon awesome-id-card"
        itemFour.icon1="Group 270"
        
       
        itemFive.itemName = "License"
        itemFive.icon = "Icon awesome-id-badge"
        itemFive.icon1="Group 270"
        
        self.arrayProfileData.append(itemOne)
        self.arrayProfileData.append(itemTwo)
        self.arrayProfileData.append(itemThree)
        self.arrayProfileData.append(itemFour)
        self.arrayProfileData.append(itemFive)
    }
}
