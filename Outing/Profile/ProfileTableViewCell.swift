//
//  ProfileTableViewCell.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var imageViewTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        cornerView.roundCorners(corners: [.topRight,.bottomRight], radius: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(item: UserDataModel) {
        
        self.labelContent.text = ""
        self.labelName.text = ""
        
        if let title = item.itemName {
            self.labelName.text = title
        }
        
        if let titleData = item.itemValue {
            self.labelContent.text = titleData
        }
        if let imageData = item.icon {
            self.imageViewIcon.image =  UIImage(named:imageData)
            if let imageData1 = item.icon1 {
                self.imageViewTick.image =  UIImage(named:imageData1)
            }
        }
    }

}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
