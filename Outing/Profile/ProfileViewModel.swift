//
//  ProfileViewModel.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
import RxSwift

class ProfileViewModel {
    var userid=String()
    
    
    func updateUserData(image: UIImage, type: String) -> Observable<ProfileData> {
        return Observable.create { observer in
            if let user_id=UserDefaults.standard.value(forKey: "userid"){
                self.userid=UserDefaults.standard.value(forKey: "userid") as! String
            }
            //Parameter
            var parameter = [String: Any]()
            
            parameter["user_id"] = self.userid
            parameter["type"] = type
            parameter["security_token"] = "out564bkgtsernsko"
            
            let dataImage = image.jpegData(compressionQuality: 0.5)
            
            // Request
            WebserviceRequest.shared.updateProfileMultiPart(imgData: dataImage!, parameter: parameter) {
                (success, message, responseObject, error) in
                if success {
                    
                    if let response = responseObject as? ProfileResponseModel {
                        if let status = response.status {
                            if status {
                                if let data =  response.data {
                                    observer.onNext(data)
                                }
                            }
                        }
                    }
                    
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Failed to update user data"])
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getUserDetails() -> Observable<UserDetailData> {
        return Observable.create { observer in
            if let user_id=UserDefaults.standard.value(forKey: "userid"){
                self.userid=UserDefaults.standard.value(forKey: "userid") as! String
            }
            //Parameter
            var parameter = [String: Any]()
            
            parameter["user_id"] = self.userid
             
            parameter["security_token"] = "out564bkgtsernsko"
            
            
            // Request
            WebserviceRequest.shared.UserProfile(parameter: parameter) {
                (success, message, responseObject, error) in
                if success {
                    
                    if let response = responseObject as? UserDetailsResponseModel {
                        if let status = response.status {
                            if status {
                                if let data = response.data {
                                    observer.onNext(data)
                                }
                            }
                        }
                    }
                    
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Failed to get user data"])
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func updateUserDataedit(image: UIImage,user:String,do_b:String) -> Observable<ProfileeditData> {
        return Observable.create { observer in
            if let user_id=UserDefaults.standard.value(forKey: "userid"){
                self.userid=UserDefaults.standard.value(forKey: "userid") as! String
            }
            //Parameter
            var parameter = [String: Any]()

            parameter["user_id"] = self.userid
            parameter["username"]=user
            parameter["dob"]=do_b
            parameter["security_token"] = "out564bkgtsernsko"

            let dataImage = image.jpegData(compressionQuality: 0.5)

            // Request
                WebserviceRequest.shared.updateProfileeditMultiPart(imgData: dataImage!, parameter: parameter) {
                (success, message, responseObject, error) in
                if success {

                    if let response = responseObject as? Profileeditclass {
                        if let status = response.status {
                            if status {
                                if let data =  response.data {
                                    observer.onNext(data)
                                }
                            }
                        }
                    }

                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Failed to update user data"])
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }

}
