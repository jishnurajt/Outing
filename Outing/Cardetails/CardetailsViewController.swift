//
//  CardetailsViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 10/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Lightbox


class CardetailsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var carImageHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbltermsconditions: UILabel!
    @IBOutlet weak var lblenginecapacity1: UILabel!
    @IBOutlet weak var lblfuelcapacity1: UILabel!
    @IBOutlet weak var lblmaxspeed1: UILabel!
    @IBOutlet weak var lblcapacity1: UILabel!
    @IBOutlet weak var lblmodel1: UILabel!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var lbldet: UILabel!
    @IBOutlet weak var lblnoofphotos: UILabel!
    @IBOutlet weak var btnproceed: UIButton!
    @IBOutlet weak var btniagree: UIButton!
    @IBOutlet weak var btncancel: UIButton!
    @IBOutlet weak var tableviewheightconstraint: NSLayoutConstraint!
    @IBOutlet weak var tableviewattributes: UITableView!
    @IBOutlet weak var collectionviewadditionalpackage: UICollectionView!
    @IBOutlet weak var collectionviewcarimages: UICollectionView!
    
    @IBOutlet weak var lblcarname: UILabel!
    
    @IBOutlet weak var lblcarprice: UILabel!
    
    @IBOutlet weak var imageviewcar: UIImageView!
    
    @IBOutlet weak var lblmodalyear: UILabel!
    
    @IBOutlet weak var lblenginecapacity: UILabel!
    @IBOutlet weak var lblfuelcapacity: UILabel!
    @IBOutlet weak var lblmaxspeed: UILabel!
    @IBOutlet weak var lblcapacity: UILabel!
    @IBOutlet weak var spec3: UIView!
    
    @IBOutlet weak var attributeLbl: UILabel!
    @IBOutlet weak var specStack: UIStackView!
    @IBOutlet weak var spec4: UIView!
    @IBOutlet weak var spec2: UIView!
    @IBOutlet weak var spec1: UIView!
    @IBOutlet weak var spec5: UIView!
    @IBOutlet weak var specEmpty: UIView!
    @IBOutlet weak var time2: UIView!
    @IBOutlet weak var time3: UIView!
    @IBOutlet weak var time4: UIView!
    @IBOutlet weak var time5: UIView!
    @IBOutlet weak var price2: UILabel!
    @IBOutlet weak var price3: UILabel!
    @IBOutlet weak var price4: UILabel!
    @IBOutlet weak var price5: UILabel!
    
    @IBOutlet weak var lbldescription: UILabel!
    @IBOutlet weak var mainAttributeLbl: UILabel!
    var additionalpackage=[String]()
    var carid=String()
    var cardetailmodel=Cardetailviewmodel()
    var gallery:[Gallery]?=[]
    
    var imageurl=String()
    var attributeurl=String()
    var disable:[Disable]?=[]
    var imageadditional=["photo-6","bg-1","photo-6","bg-1"]
    var additionaltour=["Desert Tour","Desert Tour","Desert Tour","Desert Tour"]
    var additionalprice=["Add QAR 500 only","Add QAR 500 only","Add QAR 500 only","Add QAR 500 only"]
    var yatchflag=0
    var yatchid=String()
    var yatchmodel=Yatchviewmodel()
    var cardetail:[Details]?=[]
    var attributes:[FeaturesNew]?=[]
    var features:[Features]?=[]
    var carimage:[Car_imagesdetail]?=[]
    var yatchdetail:[Yacht_listdetails]?=[]
    var yatchattributes:[YatchAttribute]?=[]
    var yatchfeatures:[yatchFeatures]?=[]
    var yatchimage:[Yacht_imagedetails]?=[]
    var yatchimageurl=String()
    var yatchattributeurl=String()
    var yatchdisable:[Disable_y]?=[]
    var idarray=[String]()
    var typearray=[String]()
    var packageid=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  btnback.layer.cornerRadius=25
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.height
        carImageHeight.constant = screenWidth - 350
        if yatchflag==1{
            mainAttributeLbl.text = nil
            yatchmodel.yatchid=yatchid
            yatchmodel.yacht_details{(model) in
                self.yachtdetaildata(data:model)
            }
            attributeLbl.text = "ADDITIONAL FEATURES"
        }else{
            spec4.isHidden = true
            cardetailmodel.carid=carid
            cardetailmodel.car_details{(model) in
                self.cardetaildata(data:model)
            }
            attributeLbl.text = "MAIN ATTRIBUTES"
        }
        btnproceed.layer.cornerRadius=20
        btniagree.layer.cornerRadius=20
        btniagree.setBackgroundImage(UIImage(named:"icons8-unchecked-checkbox-30"), for: .normal)
        btnsidemenu.layer.masksToBounds=true
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if yatchflag==1{
            if section==0{
                return yatchattributes?.count ?? 0
            }else if section==1{  if yatchflag==1{
                return 1
            }else{
                return 0
            }
            
            }else{
                return yatchfeatures?.count ?? 0
            }
        }else{
            if section==0{
                return attributes?.count ?? 0
            }else if section==1{
                return 1
            }else{
                return features?.count ?? 0
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            let cell = (tableView.dequeueReusableCell(withIdentifier: "AttributesTableViewCell", for: indexPath) as? AttributesTableViewCell)!
            if yatchflag==1{
                cell.lblattributes.text=yatchattributes?[indexPath.row].a_name
                let url = URL(string:yatchattributeurl+(yatchattributes?[indexPath.row].a_image ?? ""))
                
                cell.imageviewattributes.kf.indicatorType = .activity
                cell.imageviewattributes.kf.setImage(with: url)
            }else{
                cell.lblattributes.text=attributes?[indexPath.row].f_name
                let url = URL(string:attributeurl+(attributes?[indexPath.row].f_image ?? ""))
                
                cell.imageviewattributes.kf.indicatorType = .activity
                cell.imageviewattributes.kf.setImage(with: url)
            }
            return cell
        }else  if indexPath.section==1{
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "AdditionalfeaturesTableViewCell", for: indexPath) as? AdditionalfeaturesTableViewCell)!
            
            return cell1
        }else{
            let cell2 = (tableView.dequeueReusableCell(withIdentifier: "FeatureslistTableViewCell", for: indexPath) as? FeatureslistTableViewCell)!
            if yatchflag==1{
                cell2.lblfeatures.text=yatchfeatures?[indexPath.row].f_name
            }else{
                cell2.lblfeatures.text=features?[indexPath.row].f_name
            }
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 50
        }else if indexPath.section==1{
          //  if yatchflag==1{
          //      return 50
           // }else{
                return .leastNormalMagnitude
          //  }
        }else{
            
            return 30
            
            
        }
        
    }
    
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewcarimages{
            if yatchflag==1{
                return yatchimage?.count ?? 0
            }else{
                return gallery?.count ?? 0
            }
        }else{
            if yatchflag==1{
                return 4
            }else{
                return 4
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView==collectionviewcarimages{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "CardetailimageCollectionViewCell",
                                                           for: indexPath) as! CardetailimageCollectionViewCell
            // cell1.layer.cornerRadius=20
            if yatchflag==1{
                let url = URL(string:yatchimageurl+(yatchimage?[indexPath.row].yacht_image ?? ""))
                cell1.imageviewcardetails.layer.cornerRadius=10
                cell1.imageviewcardetails.kf.indicatorType = .activity
                cell1.imageviewcardetails.kf.setImage(with: url)
            }else{
                let url = URL(string:imageurl+(gallery?[indexPath.row].car_image ?? ""))
                cell1.imageviewcardetails.layer.cornerRadius=10
                cell1.imageviewcardetails.kf.indicatorType = .activity
                cell1.imageviewcardetails.kf.setImage(with: url)
            }
            cell1.imageviewcardetails.contentMode = .scaleAspectFill
            // cell1.imageviewcardetails.image=UIImage(named:carimage[indexPath.row])
            //
            return cell1
        }else{
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "AdditionalpackagesCollectionViewCell",
                                                           for: indexPath) as! AdditionalpackagesCollectionViewCell
            //            cell2.imageviewadditional.image=UIImage(named: imageadditional[indexPath.item])
            //            cell2.lbltour.text=additionaltour[indexPath.item]
            //            cell2.lblprice.text=additionalprice[indexPath.item]
            if indexPath.row % 3 == 0{
                cell2.viewadditional.backgroundColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell2.lbltour.textColor=UIColor.white
                cell2.lblprice.textColor=UIColor.lightGray
            }else{
                cell2.viewadditional.backgroundColor=UIColor.darkGray
                cell2.lbltour.textColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell2.lblprice.textColor=UIColor.lightGray
            }
            cell2.layer.cornerRadius=20
            //            cell2.imageviewbrands.image=UIImage(named:carbrands[indexPath.row])
            return cell2
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        if collectionView==collectionviewcarimages{
            if indexPath.row % 3 == 0{
                if UIDevice.current.userInterfaceIdiom == .pad{
                    // return CGSize(width:150, height: CGFloat(270))
                    return CGSize(width:(collectionviewcarimages.frame.width/2)-200, height: collectionviewcarimages.frame.height)
                }else{
                    return CGSize(width:(collectionviewcarimages.frame.width/2)-5, height: collectionviewcarimages.frame.height)
                }
            }else{
                if UIDevice.current.userInterfaceIdiom == .pad{
                    return CGSize(width:(collectionviewcarimages.frame.width/2)-200, height: (collectionviewcarimages.frame.height/2)-5)
                }else{
                    return CGSize(width: (collectionviewcarimages.frame.width/2)-5, height: (collectionviewcarimages.frame.height/2)-5)
                }
            }
        }else{
            return CGSize(width: 200, height: CGFloat(225))
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionviewcarimages{
            if yatchflag==1, let imageUrls = self.yatchimage?.map({ LightboxImage(imageURL: URL(string:self.yatchimageurl+($0.yacht_image ?? ""))!)}){
                openImagesGallery(images: imageUrls, pos: indexPath.row)
            }else if let imageUrls = self.carimage?.map({ LightboxImage(imageURL: URL(string:self.imageurl+($0.car_image ?? ""))!)}){
                openImagesGallery(images: imageUrls.reversed(), pos: indexPath.row)
            }
        }
    }
    
    
    func cardetaildata(data: Cardetailclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.attributes=data.data?.featuresNew
                self.features=data.data?.features
                self.imageurl=data.data?.image_url ?? ""
                self.attributeurl=data.data?.features_image_url ?? ""
                self.cardetail=data.data?.details
                self.carimage=self.cardetail?[0].car_images
                self.gallery=data.data?.gallery
                self.disable=data.data?.disable
                
                self.lbldescription.lineBreakMode = .byWordWrapping
                self.lbldescription.numberOfLines = 0
                self.lbldescription.sizeToFit()
                self.lbldescription.text=self.cardetail?[0].description
                let url = URL(string:self.cardetail?[0].car_image_new ?? "")
                
                self.imageviewcar.kf.indicatorType = .activity
                self.imageviewcar.kf.setImage(with: url)
                self.imageviewcar.contentMode = .scaleAspectFit
                self.lblcarname.text=(self.cardetail?[0].brand_name ?? "")+" "+String(self.cardetail?[0].brand_model ?? "")
                if let price = self.cardetail?[0].base_price ,let number = Int(price){
                    let numberFormatter = NumberFormatter()
                    numberFormatter.numberStyle = .decimal
                    let formattedNumber = numberFormatter.string(from: NSNumber(value: number))
                    self.lblcarprice.text="QAR "+(formattedNumber ??  "")+" per day"
                }
                self.lblmodalyear.text=self.cardetail?[0].brand_model
                self.lblnoofphotos.text = String(self.carimage?.count ?? 0)+" shots"
                //self.lblenginecapacity.text=self.cardetail?[0].engine_capacity
                //self.lblfuelcapacity.text=self.cardetail?[0].fuel
                self.lblmaxspeed.text=self.cardetail?[0].man_year
                self.lblcapacity.text=self.cardetail?[0].seat
                self.lbltermsconditions.setHTMLFromString(text: self.cardetail?[0].terms ?? "")
                
                let attributeheignt=50*(self.attributes?.count ?? 1)
                let featureheight=(self.features?.count ?? 1)*50
                let totalheight=attributeheignt+featureheight
                self.tableviewheightconstraint.constant=CGFloat(totalheight)
                self.tableviewattributes.reloadData()
                self.collectionviewcarimages.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    func openImagesGallery(images:[LightboxImage],pos:Int){
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images,startIndex: pos)
        // Set delegates.
        //  controller.pageDelegate = self
        //  controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
    }
    
    func yachtdetaildata(data: Yatchdetailclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.yatchdetail=data.data?.yacht_list
                self.yatchattributes=data.data?.attribute
                self.yatchfeatures=data.data?.features
                self.yatchimageurl=data.data?.image_url ?? ""
                self.yatchattributeurl=data.data?.attribute_image_url ?? ""
                // self.cardetail=data.data?.details
                self.yatchimage=self.yatchdetail?[0].yacht_image
                self.yatchdisable=data.data?.disable_y
                self.lbldescription.lineBreakMode = .byWordWrapping
                self.lbldescription.numberOfLines = 0
                self.lbldescription.sizeToFit()
                self.lbldescription.text=self.yatchdetail?[0].description
                
                let url = URL(string:self.yatchdetail?[0].yacht_image_new ?? "")
                
                self.imageviewcar.kf.indicatorType = .activity
                self.imageviewcar.kf.setImage(with: url)
                self.imageviewcar.contentMode = .scaleAspectFit
                self.lblcarname.setHTMLFromString(text:self.yatchdetail?[0].yacht_name ?? "",alignment: .center)
                
                //self.lblmodalyear.text=self.yatchdetail?[0].year
                self.lblnoofphotos.text = String(self.yatchimage?.count ?? 0)+" shots"
                self.lblenginecapacity.text=self.yatchdetail?[0].seat
                self.lblfuelcapacity.setHTMLFromString(text:self.yatchdetail?[0].yacht_name ?? "")
                //self.lblmaxspeed.text=self.yatchdetail?[0].bathroom
                //  self.lblcapacity.text=self.yatchdetail?[0].sleep
                //  self.lblmodel1.text="Year"
                self.lblcapacity1.text="Seating Capacity"
                //  self.lblmaxspeed1.text="Length"
                //  self.lblfuelcapacity1.text="Bathrooms"
                //   self.lblenginecapacity1.text="Sleeps"
                self.lbltermsconditions.setHTMLFromString(text: self.yatchdetail?[0].terms ?? "")
                let attributeheignt=50*(self.yatchattributes?.count ?? 1)
                let featureheight=(self.yatchfeatures?.count ?? 1)*30
                let totalheight=attributeheignt+featureheight
                self.tableviewheightconstraint.constant=CGFloat(totalheight)
                self.tableviewattributes.reloadData()
                self.collectionviewcarimages.reloadData()
                
                self.spec1.isHidden = true
                self.spec3.isHidden = true
                self.spec4.isHidden = false
                
                self.specEmpty.isHidden = false
                
                
                
                
                for (i,item) in (data.data?.yacht_price ?? []).enumerated() {
                    if  let number = item.price{
                        let numberFormatter = NumberFormatter()
                        numberFormatter.numberStyle = .decimal
                        let formattedNumber = numberFormatter.string(from: NSNumber(value: number))
                        switch i {
                        case 0:
                            self.lblcarprice.text="QAR "+(formattedNumber ?? "")+" (2h)"
                            self.price2.text = "QAR "+(formattedNumber ?? "")
                            self.time2.isHidden = false
                        case 1:
                            
                            self.price3.text = "QAR "+(formattedNumber ?? "")
                            self.time3.isHidden = false
                        case 2:
                            
                            self.price4.text = "QAR "+(formattedNumber ?? "")
                            self.time4.isHidden = false
                        case 3:
                            
                            self.price5.text = "QAR "+(formattedNumber ?? "")
                            self.time5.isHidden = false
                        default:
                            break
                        }
                        
                    }
                }
                self.specStack.sizeToFit()
                self.specStack.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        if let type=UserDefaults.standard.value(forKey: "type"){
            let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            
            
            self.navigationController?.pushViewController(home!, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnsearchaction(_ sender: Any) {
    }
    
    @IBAction func btnproceedaction(_ sender: UIButton) {
        if btniagree.currentBackgroundImage==UIImage(named:"icons8-unchecked-checkbox-30"){
            showToast(message: "Please agree to terms and condition.",font: .boldSystemFont(ofSize: 13), duration: 2)
        }else{
            
            let carbook = self.storyboard?.instantiateViewController(withIdentifier: "CarbookViewController") as! CarbookViewController
            if yatchflag==1{
                //if yatchdetail?.count ?? 0>0 && yatchimage?.count ?? 0>0 && yatchdisable?.count ?? 0>0{
                
                carbook.carpricefromdetail=self.yatchdetail?[0].base_price ?? ""
                
                
                carbook.carname=self.yatchdetail?[0].yacht_name ?? ""
                
                carbook.carimage=self.yatchdetail?[0].yacht_image_new ?? ""
                
                carbook.yatchdisablefromcardetails=yatchdisable
                carbook.yatchid=self.yatchdetail?[0].yacht_id ?? ""
                carbook.yatchflag=1
                carbook.idarray=idarray
                carbook.typearray=typearray
                carbook.packageid=packageid
                // }
                
            }else{
                
                // if cardetail?.count ?? 0>0 && carimage?.count ?? 0>0 && disable?.count ?? 0>0{
                
                carbook.carpricefromdetail=self.cardetail?[0].base_price ?? ""
                
                
                carbook.carname=(self.cardetail?[0].brand_name)!+" "+String(self.cardetail?[0].brand_model ?? "")
                
                carbook.carimage=self.cardetail?[0].car_image_new ?? ""
                carbook.carid=self.cardetail?[0].car_id ?? ""
                carbook.disablefromcardetails=disable
                carbook.typearray=typearray
                carbook.idarray=idarray
                carbook.packageid=packageid
                
            }
            //}
            
            if cart?.count ?? 0>0{
                carbook.cart=cart
                carbook.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(carbook, animated: true)
        }
    }
    
    @IBAction func btniagreeaction(_ sender: Any) {
        if btniagree.currentBackgroundImage==UIImage(named:"icons8-unchecked-checkbox-30"){
            btniagree.setBackgroundImage(UIImage(named:"icons8-tick-box-26"), for: .normal)
            //            btniagree.setBackgroundImage(UIImage(named:"checkmark.rectangle"), for: .normal)
        }else{
            btniagree.setBackgroundImage(UIImage(named:"icons8-unchecked-checkbox-30"), for: .normal)
        }
    }
    
    @IBAction func btncancelaction(_ sender: Any) {
    }
}
