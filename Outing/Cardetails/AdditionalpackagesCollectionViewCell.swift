//
//  AdditionalpackagesCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 10/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class AdditionalpackagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbltour: UILabel!
    @IBOutlet weak var viewadditional: UIView!
    @IBOutlet weak var imageviewadditional: UIImageView!
}
