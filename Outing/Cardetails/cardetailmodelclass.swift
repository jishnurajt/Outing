//
//  cardetailmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 14/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
struct Cardetailclass : Codable {
    let status : Bool?
    let message : String?
    let data : CardetailData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CardetailData.self, forKey: .data)
    }

}
struct Car_imagesdetail : Codable {
    let car_image_id : String?
    let car_image : String?
    let car_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case car_image_id = "car_image_id"
        case car_image = "car_image"
        case car_id = "car_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_image_id = try values.decodeIfPresent(String.self, forKey: .car_image_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
struct YachtPrice :Codable{
    let price:Double?
    enum CodingKeys: String, CodingKey {
        case price
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        price = try values.decodeIfPresent(Double.self, forKey: .price)
        
    }
}

struct Details : Codable {
    let car_id : String?
    let brand_model : String?
    let brand_id : String?
    let provider_id : String?
    let engine_number : String?
    let chasis_number : String?
    let man_year : String?
    let seat : String?
    let max_speed : String?
    let fuel : String?
    let engine_capacity : String?
    let description : String?
    let latitude : String?
    let longitude : String?
    let base_price : String?
    let rent_price : String?
    let passenger : String?
    let features : String?
    let attribute_id : String?
    let baggage : String?
    let door : String?
    let gear : String?
    let slider : String?
    let status : String?
    let created_at_date : String?
    let brand_name : String?
    let category_id : String?
    let brand_image : String?
    let car_images : [Car_imagesdetail]?
    let terms : String?
    let car_image_new:String?

    enum CodingKeys: String, CodingKey {

        case car_id = "car_id"
        case brand_model = "brand_model"
        case brand_id = "brand_id"
        case provider_id = "provider_id"
        case engine_number = "engine_number"
        case chasis_number = "chasis_number"
        case man_year = "man_year"
        case seat = "seat"
        case max_speed = "max_speed"
        case fuel = "fuel"
        case engine_capacity = "engine_capacity"
        case description = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case base_price = "base_price"
        case rent_price = "rent_price"
        case passenger = "passenger"
        case features = "features"
        case attribute_id = "attribute_id"
        case baggage = "baggage"
        case door = "door"
        case gear = "gear"
        case slider = "slider"
        case status = "status"
        case created_at_date = "created_at_date"
        case brand_name = "brand_name"
        case category_id = "category_id"
        case brand_image = "brand_image"
        case car_images = "car_images"
        case terms = "terms"
        case car_image_new
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        brand_model = try values.decodeIfPresent(String.self, forKey: .brand_model)
        brand_id = try values.decodeIfPresent(String.self, forKey: .brand_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        engine_number = try values.decodeIfPresent(String.self, forKey: .engine_number)
        chasis_number = try values.decodeIfPresent(String.self, forKey: .chasis_number)
        man_year = try values.decodeIfPresent(String.self, forKey: .man_year)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        max_speed = try values.decodeIfPresent(String.self, forKey: .max_speed)
        fuel = try values.decodeIfPresent(String.self, forKey: .fuel)
        engine_capacity = try values.decodeIfPresent(String.self, forKey: .engine_capacity)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        door = try values.decodeIfPresent(String.self, forKey: .door)
        gear = try values.decodeIfPresent(String.self, forKey: .gear)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        brand_image = try values.decodeIfPresent(String.self, forKey: .brand_image)
        car_images = try values.decodeIfPresent([Car_imagesdetail].self, forKey: .car_images)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        car_image_new = try values.decodeIfPresent(String.self, forKey: .car_image_new)
    }

}

struct Attribute : Codable {
    let attribute_id : String?
    let a_name : String?
    let type : String?
    let a_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case attribute_id = "attribute_id"
        case a_name = "a_name"
        case type = "type"
        case a_image = "a_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        a_name = try values.decodeIfPresent(String.self, forKey: .a_name)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        a_image = try values.decodeIfPresent(String.self, forKey: .a_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct CardetailData : Codable {
    let details : [Details]?
    let disable : [Disable]?
    let features : [Features]?
    let featuresNew : [FeaturesNew]?
    let attribute : [Attribute]?
    let gallery : [Gallery]?
    let image_url : String?
    let attribute_image_url : String?
    let features_image_url : String?

    enum CodingKeys: String, CodingKey {

        case details = "details"
        case disable = "disable"
        case features = "features"
        case featuresNew = "features_new"
        case attribute = "attribute"
        case gallery = "gallery"
        case image_url = "image_url"
        case attribute_image_url = "attribute_image_url"
        case features_image_url = "feature_image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        details = try values.decodeIfPresent([Details].self, forKey: .details)
        disable = try values.decodeIfPresent([Disable].self, forKey: .disable)
        features = try values.decodeIfPresent([Features].self, forKey: .features)
        featuresNew = try values.decodeIfPresent([FeaturesNew].self, forKey: .featuresNew)
        attribute = try values.decodeIfPresent([Attribute].self, forKey: .attribute)
         gallery = try values.decodeIfPresent([Gallery].self, forKey: .gallery)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        attribute_image_url = try values.decodeIfPresent(String.self, forKey: .attribute_image_url)
        features_image_url = try values.decodeIfPresent(String.self, forKey: .features_image_url)
    }

}

struct Disable : Codable {
    let pick_up_date : String?
    let drop_date : String?

    enum CodingKeys: String, CodingKey {

        case pick_up_date = "pick_up_date"
        case drop_date = "drop_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pick_up_date = try values.decodeIfPresent(String.self, forKey: .pick_up_date)
        drop_date = try values.decodeIfPresent(String.self, forKey: .drop_date)
    }

}

struct Features : Codable {
    let features_id : String?
    let f_name : String?
    let cat_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case features_id = "features_id"
        case f_name = "f_name"
        case cat_id = "cat_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        features_id = try values.decodeIfPresent(String.self, forKey: .features_id)
        f_name = try values.decodeIfPresent(String.self, forKey: .f_name)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
struct FeaturesNew : Codable {
    let features_id : String?
    let f_name : String?
    let f_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case features_id = "cf_id"
        case f_name = "f_name"
        case f_image = "f_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        features_id = try values.decodeIfPresent(String.self, forKey: .features_id)
        f_name = try values.decodeIfPresent(String.self, forKey: .f_name)
        f_image = try values.decodeIfPresent(String.self, forKey: .f_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct Gallery : Codable {
    let gallery_id : String?
    let car_id : String?
    let car_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case gallery_id = "gallery_id"
        case car_id = "car_id"
        case car_image = "car_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gallery_id = try values.decodeIfPresent(String.self, forKey: .gallery_id)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
