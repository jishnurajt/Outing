//
//  FeatureslistTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class FeatureslistTableViewCell: UITableViewCell {
     @IBOutlet weak var lblfeatures: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
