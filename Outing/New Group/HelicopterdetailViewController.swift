//
//  HelicopterdetailViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import  CoreData
import MobileCoreServices
import RxSwift



class HelicopterdetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    
    
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var btniagree: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnchoosefilelicense: UIButton!
    @IBOutlet weak var btnchoosefileqtrid: UIButton!
    @IBOutlet weak var btnchoosefilepsprt: UIButton!
    @IBOutlet weak var lbldoclicense: UILabel!
    @IBOutlet weak var lbldocqtrid: UILabel!
    @IBOutlet weak var lbldocpassport: UILabel!
    @IBOutlet weak var viewdoc3: UIView!
    @IBOutlet weak var viewdoc2: UIView!
    @IBOutlet weak var viewdoc1: UIView!
    @IBOutlet weak var viewdocumentupload: UIView!
    @IBOutlet weak var btnaddtocart: UIButton!
    @IBOutlet weak var lblbookingtotalprice: UILabel!
    @IBOutlet weak var viewbookingaddon: UIView!
    
    @IBOutlet weak var viewbookingaddonheight: NSLayoutConstraint!
    
    @IBOutlet weak var lblgiftprice: UILabel!
    @IBOutlet weak var lblgiftname: UILabel!
    @IBOutlet weak var lbljwelleryselected: UILabel!
    
    @IBOutlet weak var lblbookingprice: UILabel!
    @IBOutlet weak var lblbookingdetaildate: UILabel!
    @IBOutlet weak var viewaddonsheliheight: NSLayoutConstraint!
    @IBOutlet weak var viewaddons: UIView!
    @IBOutlet weak var collectionviewgift: UICollectionView!
    @IBOutlet weak var collectionviewbrandshelicopter: UICollectionView!
    @IBOutlet weak var lblbrands: UILabel!
    @IBOutlet weak var collectionviewhelicopteraddons: UICollectionView!
    @IBOutlet weak var lbltotalprice: UILabel!
    @IBOutlet weak var lblfeecancelation: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblnooftravelers: UILabel!
    @IBOutlet weak var lblcityyourtime: UILabel!
    @IBOutlet weak var lbldateselected: UILabel!
    
    @IBOutlet weak var btnterms: UIButton!
    
    
    var dateselected=String()
    var timeselected=String()
    var traveller=1
    var carbookmodel=Carbookviewmodel()
    var addoncategory:[Addon_category]?=[]
    var addonbrands:[Addon_brands]?=[]
    var gift:[Gift]?=[]
    var imageurl=String()
    var selectedindex = -1
    var selectedindexaddonbrand = -1
    var selectedgiftindex = -1
    var imageurladdonbrand=String()
    var imageurlgift1=String()
    var nameofjwellery=""
    var nameofbrand=""
    var priceofcarafterdateselected=""
    var priceofgiftselected=""
    
    var helicoptermodel=Helicopterviewmodel()
    var helicopterlist:[Helicopter_list]?=[]
    var idarray=[String]()
    var typearray=[String]()
    var userid=String()
    var loginmodel=Loginviewmodel()
    var helicopterimage=String()
    var cartdetail=[Cartdetail]()
    let bag=DisposeBag()
    var imageToUpload = UIImage()
    var model = ProfileViewModel()
    var doclist:[Doc_listdocument]?=[]
    var typedoc_array=[String]()
    var helicopterprice=String()
    var helicoptername=String()
    var helicopterid=String()
    var packageid="0"
    var endtimeselected=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var pricecalculated=Int()
    
    @IBOutlet weak var viewscrollingheight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name:"Poppins", size: 13.0),
            .foregroundColor: UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        //.double.rawValue, .thick.rawValue
        
        
        
        let attributeString = NSMutableAttributedString(string: "Terms & Conditions",
                                                        attributes: yourAttributes)
        btnterms.setAttributedTitle(attributeString, for: .normal)
        
        
        viewscrollingheight.constant=1750
        
        // btnback.layer.cornerRadius=25
        
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        btniagree.setBackgroundImage(UIImage(named:"icons8-unchecked-checkbox-30"), for: .normal)
        
        
        viewdoc1.layer.cornerRadius=10
        viewdoc2.layer.cornerRadius=10
        viewdoc3.layer.cornerRadius=10
        viewdocumentupload.layer.cornerRadius=10
        btnchoosefilepsprt.layer.cornerRadius=10
        btnchoosefileqtrid.layer.cornerRadius=10
        btnchoosefilelicense.layer.cornerRadius=10
        viewaddonsheliheight.constant=240
        lblbrands.isHidden=true
        viewbookingaddonheight.constant=0
        lblbookingdetaildate.text=dateselected
        lbldateselected.text=dateselected
        btnaddtocart.layer.cornerRadius=20
        // lblcityyourtime.text="Doha City Tour "+timeselected
        lblnooftravelers.text=String(traveller)+" Passenger"
        lblfeecancelation.text = nil //"Free cancellation before \(dateselected)"
        helicoptermodel.helicopter_id=helicopterid
        helicoptermodel.helicopter_details{ (model) in
            self.helicopterdetailsdata(data:model)
        }
        carbookmodel.addon_category{ (model) in
            self.addoncategorydata(data:model)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewhelicopteraddons{
            return addoncategory?.count ?? 0
        }else if collectionView==collectionviewbrandshelicopter {
            return addonbrands?.count ?? 0
        }else{
            return gift?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView==collectionviewhelicopteraddons{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AddonheliCollectionViewCell",
                                                           for: indexPath) as! AddonheliCollectionViewCell
            cell1.lbladdons.text=addoncategory?[indexPath.row].addon_cat_name
            let url = URL(string:imageurl+(addoncategory?[indexPath.row].addon_cat_image ?? ""))
            
            cell1.imageviewaddons.kf.indicatorType = .activity
            cell1.imageviewaddons.kf.setImage(with: url)
            cell1.imageviewaddons.contentMode = .scaleAspectFill
            if indexPath.row==selectedindex{
                cell1.viewddoncollection.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell1.lbladdons.textColor = .white
            }else{
                cell1.viewddoncollection.backgroundColor = .darkGray
                cell1.lbladdons.textColor = .white
            }
            
            return cell1
        }else if collectionView==collectionviewbrandshelicopter{
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandheliCollectionViewCell",
                                                           for: indexPath) as! BrandheliCollectionViewCell
            //cell2.lblbrands.text=addonbrands?[indexPath.row].addon_brand_name
            let url = URL(string:imageurladdonbrand+(addonbrands?[indexPath.row].addon_brand_image ?? ""))
            
            cell2.imageviewbrands.kf.indicatorType = .activity
            cell2.imageviewbrands.kf.setImage(with: url)
            // cell2.imageviewbrands.contentMode = .scaleToFill
            if indexPath.row==selectedindexaddonbrand{
                cell2.viewbrands.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                //cell2.lblbrands.textColor = .white
            }else{
                cell2.viewbrands.backgroundColor = .darkGray
                //cell2.lblbrands.textColor = .black
            }
            
            return cell2
        }else{
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftheliCollectionViewCell",
                                                           for: indexPath) as! GiftheliCollectionViewCell
            
            cell3.lblfiftname.text=gift?[indexPath.row].title
            //cell3.lbldescrptn.text=gift?[indexPath.row].description
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:Int(gift?[indexPath.row].base_price ?? "0") ?? 0)) ?? ""
            cell3.lblgiftprice.text="QAR "+(formattedNumber)
            let url = URL(string:imageurlgift1+(gift?[indexPath.row].gift_images?[0].gift_image ?? ""))
            
            cell3.imageviewgift.kf.indicatorType = .activity
            cell3.imageviewgift.kf.setImage(with: url)
            
            
            cell3.btngiftselected.tag=indexPath.row
            cell3.btngiftselected.addTarget(self, action: #selector(bookgiftaction(sender:)), for: .touchUpInside)
            cell3.btngiftselected.isUserInteractionEnabled = false
            if indexPath.row==selectedgiftindex{
                cell3.lblfiftname.textColor=UIColor.white
                cell3.lblgiftprice.textColor=UIColor.white
                cell3.btngiftselected.setImage(UIImage(named:"tick"), for: .normal)
            }else{
                cell3.lblfiftname.textColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell3.lblgiftprice.textColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell3.btngiftselected.setImage(UIImage(named:"plus"), for: .normal)
            }
            return cell3
        }
    }
    
    
    @IBAction func btnbckaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btniagreeaction(_ sender: Any) {
        if btniagree.currentBackgroundImage==UIImage(named:"icons8-unchecked-checkbox-30"){
            btniagree.setBackgroundImage(UIImage(named:"icons8-tick-box-26"), for: .normal)
            //            btniagree.setBackgroundImage(UIImage(named:"checkmark.rectangle"), for: .normal)
        }else{
            btniagree.setBackgroundImage(UIImage(named:"icons8-unchecked-checkbox-30"), for: .normal)
        }
        
        
    }
    
    
    @IBAction func btntermasandconditions(_ sender: Any) {
        let document = self.storyboard?.instantiateViewController(withIdentifier: "DocumentviewViewController") as! DocumentviewViewController
        document.myurl=URL(string:"https://www.outing.qa/helicopter/terms")
        if cart?.count ?? 0>0{
            document.cart=cart
            document.imageurlgift=imageurlgift
        }
        
        self.navigationController?.pushViewController(document, animated: true)
        
        
    }
    
    
    
    @objc func bookgiftaction(sender:UIButton){
        
        // sender.setBackgroundImage("s", for: .normal)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        
        if collectionView==collectionviewhelicopteraddons{
            return CGSize(width: 160, height: CGFloat(120))
        }else if collectionView==collectionviewbrandshelicopter {
            return CGSize(width: 150, height: CGFloat(120))
        }else{
            return CGSize(width: 170, height: CGFloat(200))
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView==collectionviewhelicopteraddons{
            selectedindex=indexPath.row
            carbookmodel.addon_cat_id=self.addoncategory?[indexPath.row].addon_cat_id ?? ""
            
            carbookmodel.addon_brand{ (model) in
                self.addonbranddata(data:model)
            }
            selectedindexaddonbrand = -1
            nameofjwellery=addoncategory?[indexPath.row].addon_cat_name ?? ""
            self.collectionviewhelicopteraddons.reloadData()
        }else if collectionView==collectionviewbrandshelicopter {
            selectedindexaddonbrand=indexPath.row
            if addonbrands?.count ?? 0>0{
                carbookmodel.addon_cat_id=self.addonbrands?[indexPath.row].addon_cat_id ?? ""
                carbookmodel.addonbrandid=self.addonbrands?[indexPath.row].addon_brand_id ?? ""
                
                carbookmodel.gift{ (model) in
                    self.giftdata(data:model)
                }
                nameofbrand=addonbrands?[indexPath.row].addon_brand_name ?? ""
                
                self.collectionviewbrandshelicopter.reloadData()
            }
            
        }else{
            if selectedgiftindex != indexPath.row{
                selectedgiftindex=indexPath.row
                priceofgiftselected=self.gift?[indexPath.row].base_price ?? ""
                
                viewbookingaddonheight.constant=114
                lblgiftprice.text=priceofgiftselected
                lbljwelleryselected.text=nameofjwellery+" > "+nameofbrand
                lblgiftname.text=gift?[indexPath.row].title
                //helicopterprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                //helicopterprice=String(Int(priceofgiftselected)!+Int(helicopterlist?[0].base_price ?? "")!)
                helicopterprice=String(Int(priceofgiftselected)!+pricecalculated)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(priceofgiftselected)!+pricecalculated)) ?? ""
                lblbookingtotalprice.text="QAR "+formattedNumber
                
            }else{
                priceofgiftselected = "0"
                selectedgiftindex = -1
                viewbookingaddonheight.constant=0
                lblgiftprice.text=nil
                lbljwelleryselected.text=nil
                lblgiftname.text=nil
                //helicopterprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                //helicopterprice=String(Int(priceofgiftselected)!+Int(helicopterlist?[0].base_price ?? "")!)
                helicopterprice=String(Int(priceofgiftselected)!+pricecalculated)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(priceofgiftselected)!+pricecalculated)) ?? ""
                lblbookingtotalprice.text="QAR "+formattedNumber
            }
            self.collectionviewgift.reloadData()
            
        }
        
    }
    
    
    
    
    
    func addoncategorydata(data: Addonclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.addoncategory=data.data?.addon_category
                self.imageurl=data.data?.image_url ?? ""
                self.carbookmodel.addon_cat_id=self.addoncategory?[0].addon_cat_id ?? ""
                //                self.carbookmodel.addon_brand{ (model) in
                //                    self.addonbranddata(data:model)
                //                }
                self.collectionviewhelicopteraddons.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    func addonbranddata(data: Addonbrandclass) {
        print("data",data)
        addonbrands=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.addonbrands=data.data?.addon_brands
                self.imageurladdonbrand=data.data?.image_url ?? ""
                self.viewaddonsheliheight.constant=460
                self.viewscrollingheight.constant=1750+460
                self.lblbrands.isHidden=false
                self.collectionviewgift.isHidden=true
                self.collectionviewbrandshelicopter.reloadData()
                self.collectionviewhelicopteraddons.reloadData()
                self.carbookmodel.addon_cat_id = self.addonbrands?[0].addon_cat_id ?? ""
                self.carbookmodel.addonbrandid = self.addonbrands?[0].addon_brand_id ?? ""
                self.priceofgiftselected = "0"
                self.selectedgiftindex = -1
                self.viewbookingaddonheight.constant=0
                self.lblgiftprice.text=nil
                self.lbljwelleryselected.text=nil
                self.lblgiftname.text=nil
                //helicopterprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                //helicopterprice=String(Int(priceofgiftselected)!+Int(helicopterlist?[0].base_price ?? "")!)
                self.helicopterprice=String(Int( self.priceofgiftselected)! + self.pricecalculated)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: Int( self.priceofgiftselected)! + self.pricecalculated)) ?? ""
                self.lblbookingtotalprice.text="QAR "+formattedNumber
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewaddonsheliheight.constant=240
                self.lblbrands.isHidden=true
                self.viewscrollingheight.constant=1750+240
                self.collectionviewgift.isHidden=true
                self.collectionviewbrandshelicopter.reloadData()
            }
        }
    }
    func giftdata(data: Giftclass) {
        print("data",data)
        gift=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.gift=data.data?.gift
                self.imageurlgift1=data.data?.image_url ?? ""
                self.viewaddonsheliheight.constant=684
                self.viewscrollingheight.constant=1750+684
                self.collectionviewgift.isHidden=false
                self.collectionviewgift.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.viewaddonsheliheight.constant=460
                self.viewscrollingheight.constant=1750+460
                self.collectionviewgift.isHidden=false
                //  self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewgift.reloadData()
            }
        }
    }
    func helicopterdetailsdata(data: Helicopterclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            
            helicopterlist=data.data?.helicopter_list
            helicoptername=helicopterlist?[0].helicopter_name ?? ""
            helicopterid=helicopterlist?[0].helicopter_id ?? ""
            DispatchQueue.main.async{
                self.lblcityyourtime.text=(self.helicoptername ?? "")+" "+self.timeselected
                // self.lblprice.text="QAR "+(self.helicopterlist?[0].base_price ?? "")
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value:self.pricecalculated)) ?? ""
                
                self.lblprice.text="QAR "+formattedNumber
                //self.lbltotalprice.text="QAR "+(self.helicopterlist?[0].base_price ?? "")
                self.lbltotalprice.text="QAR "+formattedNumber
                self.lblbookingprice.text="QAR "+formattedNumber
                //self.lblbookingprice.text="QAR "+(self.helicopterlist?[0].base_price ?? "")
                //self.lblbookingtotalprice.text="QAR "+(self.helicopterlist?[0].base_price ?? "")
                self.lblbookingtotalprice.text="QAR "+formattedNumber
                //self.helicopterprice=self.helicopterlist?[0].base_price ?? ""
                self.helicopterprice=String(self.pricecalculated)
                self.helicopterimage=(data.data?.image_url ?? "")+(data.data?.helicopter_list?[0].helicopter_images?[0].helicopter_image ?? "")
            }
            //                   DispatchQueue.main.async{
            //                       self.tableviewhelicopter.reloadData()
            //                       
            //                   }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    @IBAction func btnslidemenuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
        
    }
    
    @IBAction func btnaddtocartaction(_ sender: Any) {
        if btniagree.currentBackgroundImage==UIImage(named:"icons8-unchecked-checkbox-30"){
            showToast(message: "Please agree to terms and condition.",font: .boldSystemFont(ofSize: 13), duration: 2)
        }else{
            if let user_id=UserDefaults.standard.value(forKey: "userid"){
                userid=UserDefaults.standard.value(forKey: "userid") as! String
                //            loginmodel.document_list{ (model) in
                //                self.document_listdata(data:model)
                //            }
                
                
                
                saveData()
                let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
                if self.cart?.count ?? 0>0{
                    cart1.cart=self.cart
                    cart1.imageurlgift=self.imageurlgift
                }
                self.navigationController?.pushViewController(cart1, animated: true)
                
                //          let cart = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
                ////                  cart.cartname_array=cartcarnamearray
                ////                  cart.cartdate_array=cartdatearry
                ////                  cart.carprice_array=cartcarprice
                ////                  cart.carimage_array=cartcarimagearry
                //                 self.navigationController?.pushViewController(cart, animated: true)
                
            }else{
                let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
                login.cartflag=1
                if cart?.count ?? 0>0{
                    login.cart=cart
                    login.imageurlgift=imageurlgift
                }
                self.navigationController?.pushViewController(login, animated: true)
                
            }
        }
    }
    
    func saveData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let manageContent = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Cartdetail", in: manageContent)!
        
        let users = NSManagedObject(entity: userEntity, insertInto: manageContent)
        
        users.setValue(helicoptername, forKeyPath: "carname")
        users.setValue(helicopterprice, forKeyPath: "carprice")
        users.setValue(helicopterimage, forKeyPath: "carimage")
        users.setValue(dateselected, forKeyPath: "cardate")
        users.setValue(userid, forKeyPath: "userid")
        users.setValue(String(traveller), forKey: "people")
        
        
        
        users.setValue(userid, forKeyPath: "userid")
        users.setValue(helicopterid, forKey: "car_id")
        users.setValue(dateselected, forKey: "pick_date")
        users.setValue(dateselected, forKey: "drop_date")
        users.setValue(packageid, forKey: "package_id")
        //               if timeselectedreceive==""{
        users.setValue(timeselected, forKey: "pick_time")
        //               }else{
        //                  users.setValue(timeselectedreceive, forKey: "pick_time")
        //               }
        //               if timeselectedreturn==""{
        users.setValue(endtimeselected, forKey: "drop_time")
        //               }else{
        //                  users.setValue(timeselectedreturn, forKey: "drop_time")
        //               }
        
        //                users.setValue(numberofdaysbooked, forKey: "total_days")
        //               
        users.setValue("0", forKey: "type")
        users.setValue("", forKey: "receive_loc")
        users.setValue("", forKey: "return_loc")
        //              
        //                                     users.setValue(lblreturnlocationselected.text, forKey: "return_loc")
        //              
        users.setValue("2", forKey: "flag")
        
        if priceofgiftselected==""{
            users.setValue("", forKey: "addon_cat_id")
            users.setValue("", forKey: "addon_brand_id")
            users.setValue("", forKey: "gift_id")
        }else if selectedgiftindex != -1{
            users.setValue(self.addoncategory?[selectedindex].addon_cat_id, forKey: "addon_cat_id")
            users.setValue(self.addonbrands?[selectedindexaddonbrand].addon_brand_id, forKey: "addon_brand_id")
            users.setValue(self.gift?[selectedgiftindex].gift_id, forKey: "gift_id")
        }
        
        
        
        
        
        do{
            try manageContent.save()
            cartdetail.append(users as! Cartdetail)
        }catch let error as NSError {
            
            print("could not save . \(error), \(error.userInfo)")
        }
        
        
        
    }
    
    
    
    func document_listdata(data: Doclistclass) {
        
        let status=data.status
        if status==true{
            
            doclist=data.data?.doc_list
            
            if doclist?.count ?? 0 > 1{
                for i in 0...(doclist?.count ?? 0)-1{
                    typedoc_array.append(doclist?[i].type ?? "")
                }
            }else{
                typedoc_array.append(doclist?[0].type ?? "")
            }
            DispatchQueue.main.async{
                
                if self.typedoc_array.contains("2"){
                    self.viewdoc2.isHidden=true
                }
                if self.typedoc_array.contains("3"){
                    self.viewdoc3.isHidden=true
                }
                if self.typedoc_array.contains("1"){
                    self.viewdoc1.isHidden=true
                }
            }
            if typedoc_array.contains("1") && typedoc_array.contains("2")  && typedoc_array.contains("3"){
                DispatchQueue.main.async{
                    self.viewdocumentupload.isHidden=true
                }
                DispatchQueue.main.async{
                    if self.idarray.count>0{
                        if self.typearray[0]=="1"{
                            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CardetailsViewController
                            
                            cardetail.carid=self.idarray[0]
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            cardetail.idarray=self.idarray
                            cardetail.typearray=self.typearray
                            
                            if self.cart?.count ?? 0>0{
                                cardetail.cart=self.cart
                                cardetail.imageurlgift=self.imageurlgift
                            }
                            
                            self.navigationController?.pushViewController(cardetail, animated: true)
                        }
                        else if self.typearray[0]=="2"{
                            let helicopter = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterViewController") as! HelicopterViewController
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            helicopter.idarray=self.idarray
                            helicopter.typearray=self.typearray
                            
                            if self.cart?.count ?? 0>0{
                                helicopter.cart=self.cart
                                helicopter.imageurlgift=self.imageurlgift
                            }
                            self.navigationController?.pushViewController(helicopter, animated: true)
                        }else{
                            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CardetailsViewController
                            cardetail.yatchid=self.idarray[0]
                            cardetail.yatchflag=1
                            
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            cardetail.idarray=self.idarray
                            cardetail.typearray=self.typearray
                            if self.cart?.count ?? 0>0{
                                cardetail.cart=self.cart
                                cardetail.imageurlgift=self.imageurlgift
                            }
                            
                            
                            self.navigationController?.pushViewController(cardetail, animated: true)
                        }
                    }else{
                        let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
                        if self.cart?.count ?? 0>0{
                            cart1.cart=self.cart
                            cart1.imageurlgift=self.imageurlgift
                        }
                        self.navigationController?.pushViewController(cart1, animated: true)
                    }
                    
                }
                
                
            }else{
                DispatchQueue.main.async{
                    self.viewdocumentupload.isHidden=false
                }
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
                self.viewdocumentupload.isHidden=false
            }
        }
    }
    
    
    @IBAction func btndoccloseaction(_ sender: Any) {
        self.viewdocumentupload.isHidden=true
    }
    
    @IBAction func btnpsportuploadcation(_
                                            sender: Any) {
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //               importMenu.delegate = self
        //               importMenu.modalPresentationStyle = .formSheet
        //               importMenu.addOption(withTitle: "Image", image: nil, order: .first, handler: {
        self.pickImage(type: .passport)
        //                   print("New Doc Requested") })
        //               self.present(importMenu, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnqtridaction(_ sender: Any) {
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //               importMenu.delegate = self
        //               importMenu.modalPresentationStyle = .formSheet
        //               importMenu.addOption(withTitle: "Image", image: nil, order: .first, handler: {
        self.pickImage(type: .qatarId)
        //                   print("New Doc Requested") })
        //               self.present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnlicenseaction(_ sender: Any) {
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //               importMenu.delegate = self
        //               importMenu.modalPresentationStyle = .formSheet
        //               importMenu.addOption(withTitle: "Image", image: nil, order: .first, handler: {
        self.pickImage(type: .license)
        //                   print("New Doc Requested") })
        //               self.present(importMenu, animated: true, completion: nil)
    }
    
    func uploadUserData(documentType: String) {
        model.updateUserData(image: imageToUpload, type: documentType).subscribe(
            onNext:{ data in
                print("API success")
                self.loginmodel.document_list{ (model) in
                    self.document_listdata(data:model)
                }
            },onError:{ error in
                self.showToast(message: "Error in uploading please try again later", font: .boldSystemFont(ofSize: 13), duration: 2)
                print("error")
            }).disposed(by: bag)
    }
    
    
    
    func pickImage(type: DocumentType) {
        ImagePickerManager().pickImage(self) { image in
            // self.imageViewUser.image = image
            self.imageToUpload = image
            
            switch type {
            case .passport:
                self.uploadUserData(documentType: "1")
            case .license:
                self.uploadUserData(documentType: "3")
            case .qatarId:
                self.uploadUserData(documentType: "2")
            }
        }
    }
    
    
}
//extension HelicopterdetailViewController:UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate{
//    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
//        documentPicker.delegate = self
//        let documentTypes = [kUTTypePDF as String, "com.microsoft.word.doc", "com.microsoft.excel.xls", kUTTypePNG as String, kUTTypeJPEG as String]
//        let documentPicker = UIDocumentPickerViewController(documentTypes: documentTypes, in: .import)
//        present(documentPicker, animated: true, completion: nil)
//    }
//    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard let myURL = urls.first else {
//            return
//        }
//        print("import result : \(myURL)")
//    }
//    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        print("view was cancelled")
//        dismiss(animated: true, completion: nil)
//    }
//
//
//}
