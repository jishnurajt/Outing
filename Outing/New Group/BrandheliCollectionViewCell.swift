//
//  BrandheliCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class BrandheliCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewbrands: UIView!
    @IBOutlet weak var imageviewbrands: UIImageView!
    override func awakeFromNib() {
        viewbrands.layer.cornerRadius=10
       super.awakeFromNib()
    }
}
