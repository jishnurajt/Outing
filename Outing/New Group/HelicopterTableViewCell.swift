//
//  HelicopterTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher


protocol helicopterdetails{
    func helicopter(date:String,time:String,endtime:String)
    
    
}
protocol imagesliderdetails{
    func imageslider(counter:Int,url:[URL])
    
    
}
class HelicopterTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,DateScrollPickerDelegate,DateScrollPickerDataSource,UICollectionViewDelegateFlowLayout{

    
    @IBOutlet weak var zoominoutimageheight: NSLayoutConstraint!
    @IBOutlet weak var collectionviewhelimageheight: NSLayoutConstraint!
    @IBOutlet weak var collectionviewheliimages: UICollectionView!
    @IBOutlet weak var btntrvlrselectionheight: NSLayoutConstraint!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btncheckavailability: UIButton!
    @IBOutlet weak var collectionviewtimeheight: NSLayoutConstraint!
    @IBOutlet weak var imagetobezoomed: UIImageView!
      @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewtrvler: UIView!
    
    @IBOutlet weak var seatLabel: UILabel!
    @IBOutlet weak var collectionviewtime: UICollectionView!
    @IBOutlet weak var datescrollpicker: DateScrollPicker!
    @IBOutlet weak var lblcurrentmonth: UILabel!
    @IBOutlet weak var viewavailability: UIView!
    @IBOutlet weak var btntrvlr1: UIButton!
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var lblbaseprice: UILabel!
    @IBOutlet weak var btnbook: UIButton!
    @IBOutlet weak var btnviewtrvlrdismiss: UIButton!
    @IBOutlet weak var btntrvlraction: UIButton!
    @IBOutlet weak var btntrvlr4: UIButton!
    @IBOutlet weak var btntrvlr3: UIButton!
    @IBOutlet weak var btntrvlr2: UIButton!
     var format = DateScrollPickerFormat()
    var helicoptermodel=Helicopterviewmodel()
    var morning:[Morning]?=[]
    var evening:[Evening]?=[]
    var timearray=[String]()
    var delegate:helicopterdetails!
    var delegate1:imagesliderdetails!
     var images=[URL]()
    var dateselected=String()
    var endtimearry=[String]()
   
    var imageurl=String()
    var helicopterimages:[Helicopter_images]?=[]
    //var timer=Timer()
    //var counter=0
   // var cell3=HelicopterimageCollectionViewCell()
    var helicopterid=String()
    var seletedTimeInde = -1
    
    @IBOutlet weak var datescrollpickerheight: NSLayoutConstraint!
    @IBOutlet weak var btnsidemenu: UIButton!
    var screnWidth=UIScreen.main.bounds.width
    var screenHeight=UIScreen.main.bounds.height
    var centerFlowLayout = SJCenterFlowLayout()
       var scrollToEdgeEnabled: Bool = true
    @IBOutlet weak var btnshowscrollview: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.selectionStyle = .none
        viewavailability.layer.cornerRadius=15
        btnbook.layer.cornerRadius=20
        btncheckavailability.layer.cornerRadius=20
        collectionviewtime.delegate=self
        collectionviewtime.dataSource=self
        datescrollpicker.delegate=self
        datescrollpicker.dataSource=self
       // datescrollpickerheight.constant=62
        collectionviewheliimages.delegate = self
        collectionviewheliimages.dataSource=self
//        if UIDevice.current.userInterfaceIdiom == .pad{
//            btntrvlrselectionheight.constant=68
//            collectionviewhelimageheight.constant=300
//            zoominoutimageheight.constant=333
//        }else{
//            if screenHeight>800{
//             btntrvlrselectionheight.constant=54
//            collectionviewhelimageheight.constant=250
//             zoominoutimageheight.constant=250
//            }else{
//                btntrvlrselectionheight.constant=54
//                collectionviewhelimageheight.constant=250
//                 zoominoutimageheight.constant=260
//            }
//        }
        helicoptermodel.helicopter_id="1"
        helicoptermodel.helicopter_details{ (model) in
            self.helicopterdetailsdata(data:model)
        }
        
        btnsidemenu.layer.masksToBounds=true
                
        
        self.scrollview.minimumZoomScale=0.5

              self.scrollview.maximumZoomScale=6.0

               //self.scrollview.contentSize=CGSize(width: 1280, height: 960)
              // self.scrollview.contentSize=self.imagetobezoomed.frame.size
              //self.scrollview.delegate=self
               
        
                btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
     
         
        
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
             btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
   
        centerFlowLayout.itemSize = CGSize(width: collectionviewheliimages.frame.width*0.7, height: collectionviewheliimages.frame.height)
          
          centerFlowLayout.animationMode = SJCenterFlowLayoutAnimation.scale(sideItemScale: 0.6, sideItemAlpha: 0.6, sideItemShift: 0.0)
        
        centerFlowLayout.scrollDirection = .horizontal
        collectionviewheliimages.collectionViewLayout = centerFlowLayout
        
        
        
        /// Number of days
        format.days = 5

        /// Top label date format
        format.topDateFormat = "MMM"

        /// Top label font
       // format.topFont = UIFont.systemFont(ofSize: 15, weight: .regular)

        /// Top label text color
        format.topTextColor = UIColor.lightGray

        /// Top label selected text color
        format.topTextSelectedColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)

        /// Medium label date format
        format.mediumDateFormat = "dd"

        /// Medium label font
        //format.mediumFont = UIFont.systemFont(ofSize: 15, weight: .regular)

        /// Medium label text color
        format.mediumTextColor = UIColor.black

        /// Medium label selected text color
        format.mediumTextSelectedColor = UIColor.black
   
        /// Bottom label date format
        format.bottomDateFormat = "EEE"

        /// Bottom label font
      //  format.bottomFont = UIFont.systemFont(ofSize: 15, weight: .regular)

        /// Bottom label text color
        format.bottomTextColor = UIColor.lightGray

        /// Bottom label selected text color
        format.bottomTextSelectedColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)

        /// Day radius
       // format.dayRadius = 20

        /// Day background color
        format.dayBackgroundColor = UIColor.lightGray
       // format.dayBackgroundColor = .clear
        /// Day background selected color
       // format.dayBackgroundSelectedColor = UIColor.darkGray
        format.dayBackgroundSelectedColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)


        /// Selection animation
        format.animatedSelection = true

        /// Separator enabled
        format.separatorEnabled = false

        /// Separator top label date format
        format.separatorTopDateFormat = "MMM"

        /// Separator top label font
       // format.separatorTopFont = UIFont.systemFont(ofSize: 20, weight: .bold)

        /// Separator top label text color
        //format.separatorTopTextColor = UIColor.black

        /// Separator bottom label date format
       // format.separatorBottomDateFormat = "yyyy"

        /// Separator bottom label font
       // format.separatorBottomFont = UIFont.systemFont(ofSize: 20, weight: .regular)

        /// Separator bottom label text color
     //   format.separatorBottomTextColor = UIColor.black

        /// Separator background color
        format.separatorBackgroundColor = UIColor.lightGray.withAlphaComponent(0.2)

        /// Fade enabled
        format.fadeEnabled = true

        /// Animation scale factor
        format.animationScaleFactor = 1.1

        /// Animation scale factor
   //     format.dayPadding = 15

        /// Top margin data label
 //       format.topMarginData = 10

        /// Dot view size
      //  format.dotWidth = 10
        
        datescrollpicker.format = format
        datescrollpicker.setMinMaxDates(minDate: Date().addDays(2), maxDate: nil)
        
//        datescrollpicker.selectToday(animated: true)
       // collectionviewtimeheight.constant=0
        
    }
    
  
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imagetobezoomed
    }
    
    func zoomRect(for scrollView: UIScrollView?, withScale scale: Float, withCenter center: CGPoint) -> CGRect {

        var zoomRect: CGRect!

        // The zoom rect is in the content view's coordinates.
        // At a zoom scale of 1.0, it would be the size of the
        // imageScrollView's bounds.
        // As the zoom scale decreases, so more content is visible,
        // the size of the rect grows.
        zoomRect.size.height = (scrollview?.frame.size.height ?? 0.0) / CGFloat(scale)
        zoomRect.size.width = (scrollview?.frame.size.width ?? 0.0) / CGFloat(scale)

        // choose an origin so as to get the right center.
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)

        return zoomRect
    }
       
    func transformlarge(cell: UICollectionViewCell){
        UIView.animate(withDuration:3.0){
            cell.transform=CGAffineTransform(scaleX: 1.06, y: 1.06)
        }
        let gemnerator=UIImpactFeedbackGenerator(style: .light)
        gemnerator.impactOccurred()
    }
    func transformtostandard(cell: UICollectionViewCell){
        UIView.animate(withDuration:3.0){
            cell.transform=CGAffineTransform.identity
        
    }
    }
     func animateZoomforCell(zoomCell : UICollectionViewCell)
    {
        UIView.animate(
            withDuration: 3.0,
            delay: 0,
            options: UIView.AnimationOptions.curveEaseOut,
            animations: {
                            zoomCell.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
                },
        completion: nil)
    }
    func animateZoomforCellremove(zoomCell : UICollectionViewCell)
    {
        UIView.animate(
            withDuration:3.0,
            delay: 0,
            options: UIView.AnimationOptions.curveEaseOut,
            animations: {
                zoomCell.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
        },
            completion: nil)

    }
    
    func helicopterdetailsdata(data: Helicopterclass) {
                   print("data",data)
                  
                   let status=data.status
                   if status==true{
                     DispatchQueue.main.async{
                        self.imageurl=data.data?.image_url ?? ""
                    //self.lblbaseprice.text=data.data?.helicopter_list?[0].base_price ?? ""
                        self.helicopterid=data.data?.helicopter_list?[0].helicopter_id ?? ""
                        self.helicopterimages=data.data?.helicopter_list?[0].helicopter_images
                        if self.helicopterimages?.count==0{
                            self.collectionviewhelimageheight.constant=0
                        }else{
                            self.collectionviewhelimageheight.constant=270
                        }
                        
                        self.collectionviewheliimages.reloadData()
                        for i in 0...(self.helicopterimages?.count ?? 0)-1{
                            let totalimage=self.imageurl+(self.helicopterimages?[i].helicopter_image ?? "")
                            self.images.append(URL(string:totalimage)!)
                        }
                    }
                   
                   }else{
                       DispatchQueue.main.async{
                          
                           
                       }
                   }
               }

  
    func selectToday(animated: Bool?){
        
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
    
    }
    
    
    func dateScrollPicker(_ dateScrollPicker: DateScrollPicker, didSelectDate date: Date){
        print("date",date)
        let format = DateFormatter()
        format.dateFormat = "dd-MM-yyyy"
        helicoptermodel.helicopter_id=helicopterid
        seletedTimeInde = -1
        helicoptermodel.choosendate=format.string(from: date)
        helicoptermodel.get_time_slot{ (model) in
                   self.gettimeslotdata(data:model)
               }
        format.dateFormat = "MMM dd, yyyy"
         dateselected=format.string(from: date)
        btnSelectDate.setTitle(dateselected, for: .normal)
        self.delegate.helicopter(date:dateselected,time:"",endtime:"")
    }
    
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView==collectionviewheliimages{
        return helicopterimages?.count ?? 0
    }else{
    
    return timearray.count
   }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView==collectionviewheliimages{
           if self.scrollToEdgeEnabled, let cIndexPath = centerFlowLayout.currentCenteredIndexPath,
               cIndexPath != indexPath {
               centerFlowLayout.scrollToPage(atIndex: indexPath.row)
           }
        }else{
            seletedTimeInde = indexPath.row
            collectionView.reloadData()
            timeselected(sender: seletedTimeInde)
        }
       }
  
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          if collectionView==collectionviewheliimages{
           let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "HelicopterimageCollectionViewCell",for: indexPath) as! HelicopterimageCollectionViewCell
           
            let url = URL(string:imageurl+(helicopterimages?[indexPath.row].helicopter_image ?? ""))
//             let index=IndexPath.init(item: counter-1, section: 0)
//            if indexPath==index{
//                transformlarge(cell: cell3)
//
//            }else{
//                transformtostandard(cell: cell3)
//            }
            cell3.imagehelicopter.kf.indicatorType = .activity
            cell3.imagehelicopter.kf.setImage(with: url)
          return cell3
            
          }else{
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell",
                                                              for: indexPath) as! TimeCollectionViewCell
        cell1.btntime.setTitle(timearray[indexPath.row], for: .normal)
        
            cell1.btntime.backgroundColor = indexPath.row == seletedTimeInde ? UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0) : .lightGray
            
        return cell1
        }
        }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView==collectionviewheliimages{
            
            return CGSize(width: collectionView.frame.width*0.7, height: collectionView.frame.height)
        }else{
            return CGSize(width: 110, height: CGFloat(70))
        }
        
    }
    
    
    
    @objc func timeselected(sender:Int){
        self.delegate.helicopter(date:dateselected,time:timearray[sender],endtime:endtimearry[sender])
        
    }
    func gettimeslotdata(data: Timeslotclass) {
        print("data",data)
        DispatchQueue.main.async{
            self.timearray.removeAll()
            self.endtimearry.removeAll()
        self.collectionviewtime.reloadData()
        }
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.morning=data.data?.morning
                self.evening=data.data?.evening
                if self.morning?.count ?? 0 > 0{
                    for i in 0...(self.morning?.count ?? 0)-1{
                        let formatter = DateFormatter()
                                               formatter.dateFormat = "HH:mm:ss"
                                               let time = formatter.date(from:self.morning?[i].start_time ?? "")
                                             let endtime=formatter.date(from:self.morning?[i].end_time ?? "")
                                               formatter.dateFormat = "h:mm a"
                                               let actualtime=formatter.string(from: time!)
                                               let actualendtime=formatter.string(from: endtime!)
                                               print("time",time)
                                                print("actualtime",actualtime)
                       
                         self.timearray.append(actualtime)
                        self.endtimearry.append(actualendtime)
                        
                       
                }
                }
                if self.evening?.count ?? 0>0{
                    for i in 0...(self.evening?.count ?? 0)-1{
                                      
                        let formatter = DateFormatter()
                        formatter.dateFormat = "HH:mm:ss"
                        let time = formatter.date(from: self.evening?[i].start_time ?? "")
                         let endtime=formatter.date(from:self.evening?[i].end_time ?? "")
                        formatter.dateFormat = "h:mm a"
                        let actualtime=formatter.string(from: time!)
                        let actualendtime=formatter.string(from: endtime!)
                        print("time",time)
                         print("actualtime",actualtime)
                    self.timearray.append(actualtime)
                    self.endtimearry.append(actualendtime)
                }
           //     self.collectionviewtimeheight.constant=80
                self.collectionviewtime.reloadData()
                    if self.timearray.count == 0{
                        self.btnbook.alpha = 0.5
                    }else{
                        self.btnbook.alpha = 1
                    }
                
            }
            }
        }else{
            DispatchQueue.main.async{
              //   self.collectionviewtimeheight.constant=0
               // self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
}

