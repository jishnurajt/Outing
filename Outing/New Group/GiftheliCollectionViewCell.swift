//
//  GiftheliCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class GiftheliCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblgiftprice: UILabel!
    @IBOutlet weak var lblfiftname: UILabel!
    @IBOutlet weak var btngiftselected: UIButton!
    @IBOutlet weak var imageviewgift: UIImageView!
}
