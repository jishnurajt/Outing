//
//  HelicopterViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit




class HelicopterViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,helicopterdetails,imagesliderdetails{
    
    
  
    
    @IBOutlet weak var btnsidemenu: UIButton!
    
    @IBOutlet weak var btnback: UIButton!
    
    
    var quesnswrarray=[String]()
    var answerarray=[String]()
    var selectedindex=1000
    var helicoptermodel=Helicopterviewmodel()
    var cell=HelicopterTableViewCell()
    var datepassed=String()
    var timepassed=String()
    var idarray=[String]()
     var typearray=[String]()
    var packageid=String()
    var endtimepassed=String()
    var screnWidth=UIScreen.main.bounds.width
     var screenHeight=UIScreen.main.bounds.height
    var cart:[Giftbook]?=[]
       var imageurlgift=String()
    var helicopterlist:[Helicopter_listactual]?
    var baseprice=Int()
    var helicopter_id=String()
    var seat = 4
    var traveller = 1
    
    @IBOutlet weak var tableviewhelicopter: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
       // btnback.layer.cornerRadius=25
        
      
        
        tableviewhelicopter.rowHeight=UITableView.automaticDimension
        tableviewhelicopter.estimatedRowHeight=UITableView.automaticDimension
        
        
        helicoptermodel.helicopter_list{ (model) in
                   self.helicopterlistclassdata(data:model)
               }
       
        quesnswrarray=["Q. How many people can fly in your helicopters?","Q. What are your hours of operation?","Q. Do I need a reservation?","Q. Do you ever have delays or interruptions to scheduled flights?","Q. How long are your tours?","Q. Is parking available at the heliports?","Q. Can we take photos? If so, where can we take them?","Q. What happens if I buy a tour and my flight is cancelled due to weather?","Q. Should I call to check my reservation status on the day of my flight if the weather looks bad?","Q. Must I bring my photo identification with me?"]
        
        
        
        answerarray=["A. Our helicopters seat up to 4 Outingpassengers plus one pilot and one security representative. Due to payload restrictions, the number of passengers to be carried on board may be subject to change, especially during higher temperatures", "A. During weekends (Friday to Saturday) from 1500 hours to 1800 hours.","A. Yes. Reservations are required for all tours","A. Delays may occur in case of extreme weather situations or aircraft malfunctions. We will endeavour to inform all passengers as soon as such a delay is expected","A. OutingHelicopter Tours last approximately 30 minutes.","A. Yes, we have a designated parking zone available","A. Taking photos at the heliport is not permitted by regulation. However during your flight you can carry your camera on-board to capture the views.Please be advised that high-resolution cameras may be restricted by security.","A. We will do our best to reschedule your tour for another suitable date and time. In case a mutually agreeable time cannot be determined, we can offer a refund.","A. Yes. It is always a good idea to check prior to your flight if the weather conditions are optimal.","A. Yes. All passengers are required to bring a valid photo ID."]
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0{
            return 1
        }else{
            return quesnswrarray.count
        }
    }
    
    
    func imageslider(counter: Int, url: [URL]) {
       // let agrume = Agrume(urls: url, startIndex: counter, background: .colored(.clear))
//        agrume.didScroll = { [unowned self] index in
//            self.cell.collectionviewheliimages.scrollToItem(at: IndexPath(item: index, section: 0), at: [], animated: false)
//        }
      //  agrume.show(from: self)
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        
       if let type=UserDefaults.standard.value(forKey: "type"){
         let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
         
         
         self.navigationController?.pushViewController(home!, animated: true)
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            cell = (tableView.dequeueReusableCell(withIdentifier: "HelicopterTableViewCell", for: indexPath) as? HelicopterTableViewCell)!
            cell.delegate=self
           // cell.delegate1=self
            cell.btntrvlr1.addTarget(self, action: #selector(btntrvlr1action(sender:)), for: .touchUpInside)
            cell.btntrvlr2.addTarget(self, action: #selector(btntrvlr2action(sender:)), for: .touchUpInside)
            cell.btntrvlr3.addTarget(self, action: #selector(btntrvlr3action(sender:)), for: .touchUpInside)
            cell.btntrvlr4.addTarget(self, action: #selector(btntrvlr4action(sender:)), for: .touchUpInside)
            cell.btntrvlraction.addTarget(self, action: #selector(btntrvlraction(sender:)), for: .touchUpInside)
            cell.btnviewtrvlrdismiss.addTarget(self, action: #selector(btnviewtrvlrdismissaction(sender:)), for: .touchUpInside)
             cell.btncheckavailability.addTarget(self, action: #selector(btncheckavailabilityaction(sender:)), for: .touchUpInside)
            cell.btnbook.addTarget(self, action: #selector(btnbookaction(sender:)), for: .touchUpInside)
            cell.btnsidemenu.addTarget(self, action: #selector(btnsidemenuaction), for: .touchUpInside)
            cell.btnshowscrollview.addTarget(self, action: #selector(btnshowscrollviewaction), for: .touchUpInside)
            let today_date = Date()

                   print("today_date",today_date)
                                  let format = DateFormatter()
                                  format.dateFormat = "MMMM yyy"
                   let mydate=format.string(from: today_date)
                    print("mydate",mydate)
            cell.lblcurrentmonth.text=mydate
            
            return cell
        }else{
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "FrequentlyaskedTableViewCell", for: indexPath) as? FrequentlyaskedTableViewCell)!
            cell1.btnqustn.setTitle(quesnswrarray[indexPath.row], for: .normal)
            if selectedindex==indexPath.row{
                cell1.lblanswer.lineBreakMode = .byWordWrapping
                cell1.lblanswer.numberOfLines = 0
                cell1.lblanswer.sizeToFit()
                cell1.lblanswer.text=answerarray[indexPath.row]
                
            }else{
                cell1.lblanswer.text=""
            }
            cell1.btnqustn.tag=indexPath.row
            cell1.btnqustn.addTarget(self, action: #selector(btnqustnaction(sender:)), for: .touchUpInside)
            return cell1
        }
    }
    
     @objc func btnshowscrollviewaction(sender:UIButton){
        cell.scrollview.isHidden=false
        tableviewhelicopter.reloadData()
        
        
    }
    
    
    
    
    
    @objc func btnbookaction(sender:UIButton){
        if timepassed==""||timepassed==nil{
           self.showToast(message: "Please choose time slot", font:  .boldSystemFont(ofSize: 13), duration: 2)
        }else{
            cell.viewavailability.isHidden=true
        }
        
    }
    func helicopter(date: String, time: String, endtime:String) {
           datepassed=date
        timepassed=time
        endtimepassed=endtime
        
       }
    @objc func btnsidemenuaction(){
     let storyboard = UIStoryboard(name: "Menu", bundle: nil)
      let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
      
      let navController = UINavigationController(rootViewController: vc!)
      navController.modalPresentationStyle = .fullScreen
       navController.modalPresentationStyle = .overCurrentContext
      navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
        vc?.cart=cart
        vc?.imageurlgift=imageurlgift
        }
      self.present(navController, animated:false, completion: nil)
    }
    @objc func btncheckavailabilityaction(sender:UIButton){
     
        if timepassed==""||timepassed==nil{
           self.showToast(message: "Please choose time slot", font:  .boldSystemFont(ofSize: 13), duration: 2)
        }else{
        let helicpoterdetail = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterdetailViewController") as! HelicopterdetailViewController
        helicpoterdetail.dateselected=datepassed
        helicpoterdetail.timeselected=timepassed
        helicpoterdetail.traveller=traveller
            let calculatedbaseprice=(self.baseprice/self.seat)*(cell.btntrvlraction.currentTitle! as NSString).integerValue
        print("calculatedbaseprice",calculatedbaseprice)
            helicpoterdetail.pricecalculated=Int(calculatedbaseprice)
        helicpoterdetail.helicopterid=helicopter_id
        helicpoterdetail.idarray=idarray
        helicpoterdetail.typearray=typearray
        helicpoterdetail.packageid=packageid
        helicpoterdetail.endtimeselected=endtimepassed
       if cart?.count ?? 0>0{
        helicpoterdetail.cart=cart
       helicpoterdetail.imageurlgift=imageurlgift
       }
            self.navigationController?.pushViewController(helicpoterdetail, animated: true)
        }
    }
    
    @objc func btnqustnaction(sender:UIButton){
        selectedindex=sender.tag
        tableviewhelicopter.reloadData()
        
    }
    @objc func btntrvlr1action(sender:UIButton){
        cell.btntrvlraction.setTitle("1 Passenger", for: .normal)
        cell.viewtrvler.isHidden=true
        tableviewhelicopter.reloadData()
        traveller = 1
    }
    @objc func btntrvlr2action(sender:UIButton){
        cell.btntrvlraction.setTitle("2 Passengers", for: .normal)
        cell.viewtrvler.isHidden=true
        tableviewhelicopter.reloadData()
        traveller = 2
    }
    @objc func btntrvlr3action(sender:UIButton){
        cell.btntrvlraction.setTitle("3 Passengers", for: .normal)
        cell.viewtrvler.isHidden=true
        tableviewhelicopter.reloadData()
        traveller = 3
    }
    @objc func btntrvlr4action(sender:UIButton){
        cell.btntrvlraction.setTitle("4 Passengers", for: .normal)
        cell.viewtrvler.isHidden=true
        tableviewhelicopter.reloadData()
        traveller = 4
    }
    @objc func btntrvlraction(sender:UIButton){
        cell.viewtrvler.isHidden=false
        tableviewhelicopter.reloadData()
    }
    
    @objc func btnviewtrvlrdismissaction(sender:UIButton){
        cell.viewtrvler.isHidden=true
        cell.viewavailability.isHidden=true
        tableviewhelicopter.reloadData()
    }
    
    
    @IBAction func selectDate(_ sender: Any) {
        cell.viewavailability.isHidden=false
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return UITableView.automaticDimension
//            if UIDevice.current.userInterfaceIdiom == .pad{
//            return 3050
//            }else{
//                if screenHeight>800{
//               return 3180
//                }else{
//                     return 3270
//                }
//            }
        }else{
            if selectedindex==indexPath.row{
                return UITableView.automaticDimension
            }else{
                return 75
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section==0{
            if UIDevice.current.userInterfaceIdiom == .pad{
                       return 3050
                       }else{
                         if screenHeight>800{
                          return 3180
                           }else{
                                return 3170
                           }
                       }
        }else{
            if selectedindex==indexPath.row{
                return UITableView.automaticDimension
            }else{
                return 75
            }
        }
        
        
    }
    func helicopterlistclassdata(data: Helicopterlistclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.helicopterlist=data.data?.helicopter_list
                
                self.baseprice=Int(data.data?.helicopter_list?[0].base_price ?? "0") ?? 0
                self.seat = Int(data.data?.helicopter_list?[0].seat ?? "4") ?? 4
                let number = Double(self.baseprice/self.seat)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                self.cell.lblbaseprice.text="QAR "+formattedNumber+" per seat"

                
                
                self.cell.seatLabel.text = "Maximum \(self.seat) per flight"
                self.helicopter_id=data.data?.helicopter_list?[0].helicopter_id ?? ""
                self.helicoptermodel.helicopter_id=data.data?.helicopter_list?[0].helicopter_id ?? ""
                self.helicoptermodel.helicopter_details{ (model) in
                           self.helicopterdetailsdata(data:model)
                       }
               self.tableviewhelicopter.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    func helicopterdetailsdata(data: Helicopterclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
               
                self.tableviewhelicopter.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
}
