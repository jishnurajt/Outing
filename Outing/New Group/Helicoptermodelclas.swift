
//
//  Helicoptermodelclas.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct HelicopterFeatures : Codable {
    let features_id : String?
    let f_name : String?
    let cat_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case features_id = "features_id"
        case f_name = "f_name"
        case cat_id = "cat_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        features_id = try values.decodeIfPresent(String.self, forKey: .features_id)
        f_name = try values.decodeIfPresent(String.self, forKey: .f_name)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct HelicopterData : Codable {
    let helicopter_list : [Helicopter_list]?
    let disable_h : [Disable_h]?
    let features : [HelicopterFeatures]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case helicopter_list = "helicopter_list"
        case disable_h = "disable_h"
        case features = "features"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_list = try values.decodeIfPresent([Helicopter_list].self, forKey: .helicopter_list)
        disable_h = try values.decodeIfPresent([Disable_h].self, forKey: .disable_h)
        features = try values.decodeIfPresent([HelicopterFeatures].self, forKey: .features)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}


struct Helicopter_list : Codable {
    let helicopter_id : String?
    let helicopter_name : String?
    let description : String?
    let base_price : String?
    let rent_price : String?
    let provider_id : String?
    let features : String?
    let seat : String?
    let latitude : String?
    let longitude : String?
    let passenger : String?
    let baggage : String?
    let slider : String?
    let terms : String?
    let created_at_date : String?
    let status : String?
    let helicopter_images : [Helicopter_images]?

    enum CodingKeys: String, CodingKey {

        case helicopter_id = "helicopter_id"
        case helicopter_name = "helicopter_name"
        case description = "description"
        case base_price = "base_price"
        case rent_price = "rent_price"
        case provider_id = "provider_id"
        case features = "features"
        case seat = "seat"
        case latitude = "latitude"
        case longitude = "longitude"
        case passenger = "passenger"
        case baggage = "baggage"
        case slider = "slider"
        case terms = "terms"
        case created_at_date = "created_at_date"
        case status = "status"
        case helicopter_images = "helicopter_images"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_id = try values.decodeIfPresent(String.self, forKey: .helicopter_id)
        helicopter_name = try values.decodeIfPresent(String.self, forKey: .helicopter_name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        helicopter_images = try values.decodeIfPresent([Helicopter_images].self, forKey: .helicopter_images)
    }

}

struct Helicopter_images : Codable {
    let helicopter_image_id : String?
    let helicopter_id : String?
    let helicopter_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case helicopter_image_id = "helicopter_image_id"
        case helicopter_id = "helicopter_id"
        case helicopter_image = "helicopter_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_image_id = try values.decodeIfPresent(String.self, forKey: .helicopter_image_id)
        helicopter_id = try values.decodeIfPresent(String.self, forKey: .helicopter_id)
        helicopter_image = try values.decodeIfPresent(String.self, forKey: .helicopter_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct Helicopterclass : Codable {
    let status : Bool?
    let message : String?
    let data : HelicopterData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(HelicopterData.self, forKey: .data)
    }

}

struct Disable_h : Codable {
    let pick_up_date : String?
    let pick_up_time : String?

    enum CodingKeys: String, CodingKey {

        case pick_up_date = "pick_up_date"
        case pick_up_time = "pick_up_time"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pick_up_date = try values.decodeIfPresent(String.self, forKey: .pick_up_date)
        pick_up_time = try values.decodeIfPresent(String.self, forKey: .pick_up_time)
    }

}
//timeslot
struct Evening : Codable {
    let time_slote_id : String?
    let days : String?
    let start_time : String?
    let end_time : String?
    let created_at_date : String?
    let status : String?
    let section : String?

    enum CodingKeys: String, CodingKey {

        case time_slote_id = "time_slote_id"
        case days = "days"
        case start_time = "start_time"
        case end_time = "end_time"
        case created_at_date = "created_at_date"
        case status = "status"
        case section = "section"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        time_slote_id = try values.decodeIfPresent(String.self, forKey: .time_slote_id)
        days = try values.decodeIfPresent(String.self, forKey: .days)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        section = try values.decodeIfPresent(String.self, forKey: .section)
    }

}

struct Morning : Codable {
    let time_slote_id : String?
    let days : String?
    let start_time : String?
    let end_time : String?
    let created_at_date : String?
    let status : String?
    let section : String?

    enum CodingKeys: String, CodingKey {

        case time_slote_id = "time_slote_id"
        case days = "days"
        case start_time = "start_time"
        case end_time = "end_time"
        case created_at_date = "created_at_date"
        case status = "status"
        case section = "section"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        time_slote_id = try values.decodeIfPresent(String.self, forKey: .time_slote_id)
        days = try values.decodeIfPresent(String.self, forKey: .days)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        section = try values.decodeIfPresent(String.self, forKey: .section)
    }

}

struct TimeslotData : Codable {
    let status : Int?
    let morning : [Morning]?
    let evening : [Evening]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case morning = "morning"
        case evening = "evening"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        morning = try values.decodeIfPresent([Morning].self, forKey: .morning)
        evening = try values.decodeIfPresent([Evening].self, forKey: .evening)
    }

}

struct Timeslotclass : Codable {
    let status : Bool?
    let message : String?
    let data : TimeslotData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(TimeslotData.self, forKey: .data)
    }

}
//helicopter_list

struct HelicopterlistData : Codable {
    let helicopter_list : [Helicopter_listactual]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case helicopter_list = "helicopter_list"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_list = try values.decodeIfPresent([Helicopter_listactual].self, forKey: .helicopter_list)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}

struct Helicopterlist_images : Codable {
    let helicopter_image_id : String?
    let helicopter_id : String?
    let helicopter_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case helicopter_image_id = "helicopter_image_id"
        case helicopter_id = "helicopter_id"
        case helicopter_image = "helicopter_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_image_id = try values.decodeIfPresent(String.self, forKey: .helicopter_image_id)
        helicopter_id = try values.decodeIfPresent(String.self, forKey: .helicopter_id)
        helicopter_image = try values.decodeIfPresent(String.self, forKey: .helicopter_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}



struct Helicopter_listactual : Codable {
    let helicopter_id : String?
    let helicopter_name : String?
    let description : String?
    let base_price : String?
    let brand_name : String?
    let bprice : String?
    let provider_id : String?
    let features : String?
    let seat : String?
    let latitude : String?
    let longitude : String?
    let passenger : String?
    let baggage : String?
    let slider : String?
    let terms : String?
    let created_at_date : String?
    let status : String?
    let helicopter_images : [Helicopterlist_images]?

    enum CodingKeys: String, CodingKey {

        case helicopter_id = "helicopter_id"
        case helicopter_name = "helicopter_name"
        case description = "description"
        case base_price = "base_price"
        case brand_name = "brand_name"
        case bprice = "bprice"
        case provider_id = "provider_id"
        case features = "features"
        case seat = "seat"
        case latitude = "latitude"
        case longitude = "longitude"
        case passenger = "passenger"
        case baggage = "baggage"
        case slider = "slider"
        case terms = "terms"
        case created_at_date = "created_at_date"
        case status = "status"
        case helicopter_images = "helicopter_images"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        helicopter_id = try values.decodeIfPresent(String.self, forKey: .helicopter_id)
        helicopter_name = try values.decodeIfPresent(String.self, forKey: .helicopter_name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        bprice = try values.decodeIfPresent(String.self, forKey: .bprice)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        helicopter_images = try values.decodeIfPresent([Helicopterlist_images].self, forKey: .helicopter_images)
    }

}

struct Helicopterlistclass : Codable {
    let status : Bool?
    let message : String?
    let data : HelicopterlistData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(HelicopterlistData.self, forKey: .data)
    }

}
