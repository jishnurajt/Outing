//
//  AddonheliCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class AddonheliCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbladdons: UILabel!
    @IBOutlet weak var imageviewaddons: UIImageView!
    @IBOutlet weak var viewddoncollection: UIView!
    override func awakeFromNib() {
    super.awakeFromNib()
     viewddoncollection.layer.cornerRadius=10
        
    }
}
