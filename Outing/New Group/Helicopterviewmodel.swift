//
//  Helicopterviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 24/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class Helicopterviewmodel{
   var choosendate=String()
    var helicopter_id=String()
    
    func helicopter_list(completion : @escaping (Helicopterlistclass) -> ())  {
        

        let poststring="security_token=out564bkgtsernsko"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.helicopter_list.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Helicopterlistclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
    
    
    
    
    
    
func helicopter_details(completion : @escaping (Helicopterclass) -> ())  {
    

    let poststring="security_token=out564bkgtsernsko&helicopter_id=\(helicopter_id)"
    print("poststring",poststring)
    var request = NSMutableURLRequest(url: APIEndPoint.helicopter_details.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Helicopterclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    func get_time_slot(completion : @escaping (Timeslotclass) -> ())  {
        

        let poststring="security_token=out564bkgtsernsko&helicopter_id=\(helicopter_id)&chooseDate=\(choosendate)"
        print("poststring",poststring)
        
        var request = NSMutableURLRequest(url: APIEndPoint.get_time_slot.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Timeslotclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    

    
    
    
    
    
    
}
