//
//  HelicopterimageCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class HelicopterimageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagehelicopter: UIImageView!
    override func awakeFromNib() {
      super.awakeFromNib()
        imagehelicopter.layer.cornerRadius=20
    }
}
