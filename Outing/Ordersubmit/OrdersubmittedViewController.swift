//
//  OrdersubmittedViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 26/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import CoreData

class OrdersubmittedViewController: UIViewController {
    var cartdetail=[Cartdetail]()
    @IBOutlet weak var lblBOOKING: UILabel!
    @IBOutlet weak var lblbookingrequest: UILabel!
    @IBOutlet weak var lblthankyou: UILabel!
    @IBOutlet weak var lblbookingid: UILabel!
    var bookingid=String()
    var failedflag=Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        if failedflag==0{
        lblbookingid.text="#"+bookingid
        lblBOOKING.text="BOOKINGID:"
        lblbookingrequest.text="Your booking request has been sent successfully!"
        lblthankyou.text="Thank You"
    
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            
            let managedContext =
                appDelegate.persistentContainer.viewContext
            
            //2
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Cartdetail")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            //        //3
            do {
                 try managedContext.execute(deleteRequest)
                
                
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            
        }else{
            lblbookingid.text=""
                   lblBOOKING.text=""
                   lblbookingrequest.text="Your booking request failed! Please try again"
                   lblthankyou.text="Sorry!"
        }
       
    }
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        movetohome()
        
    }
    
    func movetohome(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            DispatchQueue.main.async{
                let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                
                //
                self.navigationController?.pushViewController(home!, animated: true)
            }
            
        }
        
    }
    
    
    
    
}
