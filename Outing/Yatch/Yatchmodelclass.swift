//
//  Yatchmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation


struct YatchlistData : Codable {
    let yacht_list : [Yacht_list]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case yacht_list = "yacht_list"
        case image_url = "image_url"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_list = try values.decodeIfPresent([Yacht_list].self, forKey: .yacht_list)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}


struct Yacht_image : Codable {
    let yacht_image_id : String?
    let yacht_image : String?
    let yacht_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case yacht_image_id = "yacht_image_id"
        case yacht_image = "yacht_image"
        case yacht_id = "yacht_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_image_id = try values.decodeIfPresent(String.self, forKey: .yacht_image_id)
        yacht_image = try values.decodeIfPresent(String.self, forKey: .yacht_image)
        yacht_id = try values.decodeIfPresent(String.self, forKey: .yacht_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}


struct Yacht_list : Codable {
    let yacht_id : String?
    let yacht_name : String?
    let seat : String?
    let description : String?
    let provider_id : String?
    let features : String?
    let base_price : String?
    let brand_name : String?
    let rent_price : String?
    let latitude : String?
    let longitude : String?
    let attribute_id : String?
    let created_at_date : String?
    let status : String?
    let year : String?
    let cabbin : String?
    let length : String?
    let bathroom : String?
    let sleep : String?
    let terms : String?
    let yacht_image : [Yacht_image]?

    enum CodingKeys: String, CodingKey {

        case yacht_id = "yacht_id"
        case yacht_name = "yacht_name"
        case seat = "seat"
        case description = "description"
        case provider_id = "provider_id"
        case features = "features"
        case base_price = "base_price"
        case brand_name = "brand_name"
        case rent_price = "rent_price"
        case latitude = "latitude"
        case longitude = "longitude"
        case attribute_id = "attribute_id"
        case created_at_date = "created_at_date"
        case status = "status"
        case year = "year"
        case cabbin = "cabbin"
        case length = "length"
        case bathroom = "bathroom"
        case sleep = "sleep"
        case terms = "terms"
        case yacht_image = "yacht_image"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_id = try values.decodeIfPresent(String.self, forKey: .yacht_id)
        yacht_name = try values.decodeIfPresent(String.self, forKey: .yacht_name)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        cabbin = try values.decodeIfPresent(String.self, forKey: .cabbin)
        length = try values.decodeIfPresent(String.self, forKey: .length)
        bathroom = try values.decodeIfPresent(String.self, forKey: .bathroom)
        sleep = try values.decodeIfPresent(String.self, forKey: .sleep)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        yacht_image = try values.decodeIfPresent([Yacht_image].self, forKey: .yacht_image)
    }

}

struct Yatchclass : Codable {
    let status : Bool?
    let message : String?
    let data : YatchlistData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(YatchlistData.self, forKey: .data)
    }

}
//ytch_details


struct yatchFeatures : Codable {
    let features_id : String?
    let f_name : String?
    let cat_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case features_id = "features_id"
        case f_name = "a_name"
        case cat_id = "cat_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        features_id = try values.decodeIfPresent(String.self, forKey: .features_id)
        f_name = try values.decodeIfPresent(String.self, forKey: .f_name)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}


struct YatchdetailData : Codable {
    let yacht_list : [Yacht_listdetails]?
    let disable_y : [Disable_y]?
    let features : [yatchFeatures]?
    let attribute : [YatchAttribute]?
    let image_url : String?
    let attribute_image_url : String?
    let yacht_price:[YachtPrice]?
    
    enum CodingKeys: String, CodingKey {

        case yacht_list = "yacht_list"
        case disable_y = "disable_y"
        case features = "attribute_new"
        case attribute = "attribute"
        case image_url = "image_url"
        case attribute_image_url = "attribute_image_url"
        case yacht_price
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_list = try values.decodeIfPresent([Yacht_listdetails].self, forKey: .yacht_list)
        disable_y = try values.decodeIfPresent([Disable_y].self, forKey: .disable_y)
        features = try values.decodeIfPresent([yatchFeatures].self, forKey: .features)
        attribute = try values.decodeIfPresent([YatchAttribute].self, forKey: .attribute)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        attribute_image_url = try values.decodeIfPresent(String.self, forKey: .attribute_image_url)
        yacht_price = try values.decodeIfPresent([YachtPrice].self, forKey: .yacht_price)
    }

}

struct YatchAttribute : Codable {
    let attribute_id : String?
    let a_name : String?
    let type : String?
    let a_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case attribute_id = "attribute_id"
        case a_name = "a_name"
        case type = "type"
        case a_image = "a_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        a_name = try values.decodeIfPresent(String.self, forKey: .a_name)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        a_image = try values.decodeIfPresent(String.self, forKey: .a_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}


struct Yacht_imagedetails : Codable {
    let yacht_image_id : String?
    let yacht_image : String?
    let yacht_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case yacht_image_id = "yacht_image_id"
        case yacht_image = "yacht_image"
        case yacht_id = "yacht_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_image_id = try values.decodeIfPresent(String.self, forKey: .yacht_image_id)
        yacht_image = try values.decodeIfPresent(String.self, forKey: .yacht_image)
        yacht_id = try values.decodeIfPresent(String.self, forKey: .yacht_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}


struct Disable_y : Codable {
    let pick_up_date : String?
    let drop_date : String?

    enum CodingKeys: String, CodingKey {

        case pick_up_date = "pick_up_date"
        case drop_date = "drop_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pick_up_date = try values.decodeIfPresent(String.self, forKey: .pick_up_date)
        drop_date = try values.decodeIfPresent(String.self, forKey: .drop_date)
    }

}

struct Yacht_listdetails : Codable {
    let yacht_id : String?
    let yacht_name : String?
    let seat : String?
    let description : String?
    let provider_id : String?
    let features : String?
    let base_price : String?
    let brand_name : String?
    let rent_price : String?
    let latitude : String?
    let longitude : String?
    let attribute_id : String?
    let created_at_date : String?
    let status : String?
    let year : String?
    let cabbin : String?
    let length : String?
    let bathroom : String?
    let sleep : String?
    let terms : String?
    let yacht_image : [Yacht_imagedetails]?
    let yacht_image_new:String?

    enum CodingKeys: String, CodingKey {

        case yacht_id = "yacht_id"
        case yacht_name = "yacht_name"
        case seat = "seat"
        case description = "description"
        case provider_id = "provider_id"
        case features = "features"
        case base_price = "base_price"
        case brand_name = "brand_name"
        case rent_price = "rent_price"
        case latitude = "latitude"
        case longitude = "longitude"
        case attribute_id = "attribute_id"
        case created_at_date = "created_at_date"
        case status = "status"
        case year = "year"
        case cabbin = "cabbin"
        case length = "length"
        case bathroom = "bathroom"
        case sleep = "sleep"
        case terms = "terms"
        case yacht_image = "yacht_image"
        case yacht_image_new
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yacht_id = try values.decodeIfPresent(String.self, forKey: .yacht_id)
        yacht_name = try values.decodeIfPresent(String.self, forKey: .yacht_name)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        cabbin = try values.decodeIfPresent(String.self, forKey: .cabbin)
        length = try values.decodeIfPresent(String.self, forKey: .length)
        bathroom = try values.decodeIfPresent(String.self, forKey: .bathroom)
        sleep = try values.decodeIfPresent(String.self, forKey: .sleep)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        yacht_image = try values.decodeIfPresent([Yacht_imagedetails].self, forKey: .yacht_image)
        yacht_image_new = try values.decodeIfPresent(String.self, forKey: .yacht_image_new)
      
    }

}
struct Yatchdetailclass : Codable {
    let status : Bool?
    let message : String?
    let data : YatchdetailData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(YatchdetailData.self, forKey: .data)
    }

}
//yatch_check


struct YatchcheckData : Codable {
    let days : Int?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case days = "days"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        days = try values.decodeIfPresent(Int.self, forKey: .days)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}


struct Yatchcheckclass : Codable {
    let status : Bool?
    let message : String?
    let data : YatchcheckData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(YatchcheckData.self, forKey: .data)
    }

}
