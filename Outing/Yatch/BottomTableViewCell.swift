//
//  BottomTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class BottomTableViewCell: UITableViewCell {

    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbltype: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageviewyatch.layer.cornerRadius=15
        self.selectionStyle = .none
    }
    @IBOutlet weak var imageviewyatch: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
