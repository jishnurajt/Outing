//
//  YatchViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher



class YatchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var tableviewyatch: UITableView!
    var namearry=[String]()
    var pricearray=[String]()
    var imagearray=[String]()
    var yatchmodel=Yatchviewmodel()
    var yatchlist:[Yacht_list]?=[]
    var imageurl=String()
    var yatchimage:[Yacht_image]?=[]
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var offset=0
    var pagination = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  btnback.layer.cornerRadius=25
        yatchmodel.offset=offset
        yatchmodel.yacht_list{ (model) in
            self.yatchlistdata(data:model)
        }
        namearry=["Luxury yatch 3","Yatch","Luxury Yatch"]
        pricearray=["QAR 1850/h","QAR 1500/h","QAR 2500/h"]
        imagearray=["form-2","form-3","form"]
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0{
            return 1
        }else{
            return yatchlist?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            let cell = (tableView.dequeueReusableCell(withIdentifier: "topTableViewCell", for: indexPath) as? topTableViewCell)!
            cell.btnsidemenu.addTarget(self, action: #selector(btnbackaction1), for: .touchUpInside)
            return cell
        }else{
            var lastelement=0
            if yatchlist?.count ?? 0 > 0,pagination{
                    lastelement=yatchlist!.count - 1
                    print("lastelement",lastelement)
                    if indexPath.row==lastelement{
                        
                        offset+=1
                        yatchmodel.offset=offset
                        yatchmodel.yacht_list{ (model) in
                            self.yatchlistdata(data:model)
                        }
                        }
                    }
                
                
            
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "BottomTableViewCell", for: indexPath) as? BottomTableViewCell)!
            cell1.lbltype.setHTMLFromString(text:yatchlist?[indexPath.row].yacht_name ?? "")
            if let price = yatchlist?[indexPath.row].base_price ,let number = Int(price){
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number*2))
                cell1.lblprice.text="QAR "+(formattedNumber ?? "")+"(2h)"
            }
            
            let url = URL(string: imageurl+(yatchlist?[indexPath.row].yacht_image?.first?.yacht_image ?? ""))
            
            cell1.imageviewyatch.kf.indicatorType = .activity
            cell1.imageviewyatch.kf.setImage(with: url)
            cell1.imageviewyatch.contentMode = .scaleAspectFill
            //        let url = URL(string: UIImage(named:imagearray[indexPath.row]))
            //         cell1.imageviewyatch.layer.cornerRadius=10
            //        cell1.imageviewyatch.kf.indicatorType = .activity
            //  cell1.imageviewyatch.image=UIImage(named:imagearray[indexPath.row])
            
            return cell1
            
        }
        
    }
    
    @objc func btnbackaction1(){
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad{
            if indexPath.section==0{
                return 661
            }else{
                return 360
            }
        }else{
            if indexPath.section==0{
                return 420
            }else{
                return 280
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
        cardetail.yatchflag=1
        cardetail.yatchid=yatchlist?[indexPath.row].yacht_id ?? ""
        if cart?.count ?? 0>0{
            cardetail.cart=cart
            cardetail.imageurlgift=imageurlgift
        }
        self.navigationController?.pushViewController(cardetail, animated: true)
        
    }
    
    func yatchlistdata(data: Yatchclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            if yatchlist?.count ?? 0 <= 0{
            yatchlist=data.data?.yacht_list
            }else{
                yatchlist!+=(data.data?.yacht_list)!
            }
            imageurl=data.data?.image_url ?? ""
            
            DispatchQueue.main.async{
                self.tableviewyatch.reloadData()
                
            }
        }else{
            pagination = false
            DispatchQueue.main.async{
              //  self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
}
