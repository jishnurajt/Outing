//
//  Yatchviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class Yatchviewmodel{
    var yatchid=String()
    var startdate=String()
    var enddate=String()
    var starttime=String()
    var endtime=String()
    var offset=Int()
    
func yacht_list(completion : @escaping (Yatchclass) -> ())  {
    

    let poststring="security_token=out564bkgtsernsko&offset=\(offset)"
    
    var request = NSMutableURLRequest(url: APIEndPoint.yacht_list.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Yatchclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    func yacht_details(completion : @escaping (Yatchdetailclass) -> ())  {
        

        let poststring="security_token=out564bkgtsernsko&yacht_id=\(yatchid)"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.yacht_details.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Yatchdetailclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    func check_yacht(completion : @escaping (Yatchcheckclass) -> ())  {
        

        let poststring="security_token=out564bkgtsernsko&yacht_id=\(yatchid)&startDate=\(startdate)&endDate=\(enddate)&startTime=\(starttime)&endTime=\(endtime)"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.check_yacht.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Yatchcheckclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
}
