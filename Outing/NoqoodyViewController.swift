//
//  NoqoodyViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import WebKit

class NoqoodyViewController: UIViewController,WKNavigationDelegate {
    var myurl:URL!
    var total=String()
    var userid=String()
    
    @IBOutlet weak var mywkwebview: WKWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            userid=UserDefaults.standard.value(forKey: "userid") as! String
        }
       // myurl=URL(string:"https://www.outing.qa/test/booking/pay/"+userid+"/"+total)
        myurl=URL(string:"https://www.outing.qa/booking/pay/"+userid+"/"+total)
        print("myurl",myurl)
        let mywkwebviewConfig = WKWebViewConfiguration()
        
       // mywkwebviewConfig.allowsInlineMediaPlayback = true
        mywkwebview = WKWebView(frame: self.view.frame, configuration: mywkwebviewConfig)
        let youtubeRequest = URLRequest(url: myurl!)
        mywkwebview.navigationDelegate = self
        mywkwebview?.load(youtubeRequest)
        guard let webView = mywkwebview else { return }
        self.view.addSubview(webView)
        //
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
