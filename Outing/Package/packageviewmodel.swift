//
//  packageviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class Packageviewmodel{
    var keyword=String()
func package_list(completion : @escaping (Packageclass) -> ())  {
    

    let poststring="security_token=out564bkgtsernsko"
    
    var request = NSMutableURLRequest(url: APIEndPoint.package_list.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Packageclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    
    
    
    
    func search(completion : @escaping (Searchclass) -> ())  {
        

        let poststring="keyword=\(keyword)"
        
        var request = NSMutableURLRequest(url: APIEndPoint.search.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Searchclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
    
    
    
    
    
    
    
    
}
