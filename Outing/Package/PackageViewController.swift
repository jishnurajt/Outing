//
//  PackageViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher



class PackageViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var btnbooknow: UIButton!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbloffer: UILabel!
    @IBOutlet weak var collectionviewpackages: UICollectionView!
    @IBOutlet weak var lblpackagename: UILabel!
    @IBOutlet weak var viewpckagedetails: UIView!
    
    @IBOutlet weak var viewdashedline: UIView!
    var packagemodel=Packageviewmodel()
    var list:[List]?=[]
    var details:[PackageDetails]?=[]
    var imageurl=String()
    var carimageurl=String()
    var helicopterimageurl=String()
    var yatchimageurl=String()
    var nameofvehicle=String()
    var idarray=[String]()
    var typearray=[String]()
    var packageid=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   btnback.layer.cornerRadius=25
        
        btnsidemenu.layer.masksToBounds=true
                       
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
             
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
             btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        packagemodel.package_list{ (model) in
            self.package_listdata(data:model)
        }
        viewpckagedetails.layer.cornerRadius=10
        btnbooknow.layer.cornerRadius=20
        
        
        

       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return details?.count ?? 0
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "PackageCollectionViewCell",
                                                       for: indexPath) as! PackageCollectionViewCell
        if  details?[indexPath.row].type=="1"{
            imageurl=carimageurl
            nameofvehicle=String(details?[0].vehicle_details?.brand_name ?? "")+" "+String(details?[0].vehicle_details?.brand_model ?? "")
        }else if details?[indexPath.row].type=="2"{
            imageurl=helicopterimageurl
            nameofvehicle=details?[1].vehicle_details?.vehicle_name ?? ""
        }else{
            imageurl=yatchimageurl
            //nameofvehicle=details?[2].vehicle_details?.vehicle_name ?? ""
        }
        let url = URL(string:imageurl+(details?[indexPath.row].vehicle_details?.vehicle_image ?? ""))
        cell1.layer.cornerRadius=15
        cell1.imageview.kf.indicatorType = .activity
        cell1.imageview.kf.setImage(with: url)
        cell1.imageview.contentMode = .scaleAspectFill
        
        cell1.lblname.text = nameofvehicle
        cell1.lblprice.text="QAR "+(details?[indexPath.row].vehicle_details?.base_price ?? "")
        return cell1
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        return CGSize(width: 200, height: CGFloat(200))
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
       if let type=UserDefaults.standard.value(forKey: "type"){
         let home = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
         
         
         self.navigationController?.pushViewController(home!, animated: true)
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    func package_listdata(data: Packageclass) {
        print("data",data)
        var baseprice=Int()
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.list=data.data?.list
                self.details=self.list?[0].details
                self.lblpackagename.text=self.list?[0].package_name
                self.packageid=self.list?[0].package_id ?? "0"
                let offer=(self.list?[0].offer as! NSString).integerValue
                for i in 0...(self.details?.count)! - 1{
                    let price=Int(self.details?[i].vehicle_details?.base_price ?? "")
                    baseprice+=price!
                    
                    self.idarray.append(self.details?[i].vehicle_details?.car_id ?? "")
                    self.typearray.append(self.details?[i].type ?? "")
                    
                }
                let offerprice=baseprice-((baseprice*offer)/100)
                self.lblprice.text="QAR "+String(offerprice)
                self.lbloffer.text=(self.list?[0].offer ?? "")+" % OFF"
                self.carimageurl=data.data?.car_image_url ?? ""
                self.helicopterimageurl=data.data?.helicopter_image_url ?? ""
                self.yatchimageurl=data.data?.yacht_image_url ?? ""
                print("baseprice",baseprice)
                self.collectionviewpackages.reloadData()
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    
    
    @IBAction func btnslidemenuaction(_ sender: Any) {
    let storyboard = UIStoryboard(name: "Menu", bundle: nil)
     let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
     
     let navController = UINavigationController(rootViewController: vc!)
     navController.modalPresentationStyle = .fullScreen
      navController.modalPresentationStyle = .overCurrentContext
     navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
        vc?.cart=cart
        vc?.imageurlgift=imageurlgift
        }
     self.present(navController, animated:false, completion: nil)
    
    
    }
    
    
    @IBAction func btnpackageaction(_ sender: Any) {
        
        if typearray[0]=="1"{
            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
            cardetail.carid=idarray[0]
            idarray.remove(at: 0)
            typearray.remove(at: 0)
             cardetail.idarray=idarray
              cardetail.typearray=typearray
            cardetail.packageid=packageid
            if cart?.count ?? 0>0{
                           cardetail.cart=cart
                           cardetail.imageurlgift=imageurlgift
                           }
                          
            self.navigationController?.pushViewController(cardetail, animated: true)
        }else if typearray[0]=="3"{
            
            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
            cardetail.yatchid=idarray[0]
            cardetail.yatchflag=1
            idarray.remove(at: 0)
            typearray.remove(at: 0)
             cardetail.idarray=idarray
             cardetail.typearray=typearray
             cardetail.packageid=packageid
            if cart?.count ?? 0>0{
            cardetail.cart=cart
            cardetail.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(cardetail, animated: true)
        }else{
            let helicopter = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterViewController") as! HelicopterViewController
            idarray.remove(at: 0)
            typearray.remove(at: 0)
             helicopter.idarray=idarray
             helicopter.typearray=typearray
             helicopter.packageid=packageid
            if cart?.count ?? 0>0{
            helicopter.cart=cart
            helicopter.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(helicopter, animated: true)
        }
        
    }
    
    
    @IBAction func btnbooknowcation(_ sender: Any) {
    }
}
