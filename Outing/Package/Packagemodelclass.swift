//
//  Packagemodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct PackageData : Codable {
    let list : [List]?
    let car_image_url : String?
    let helicopter_image_url : String?
    let yacht_image_url : String?

    enum CodingKeys: String, CodingKey {

        case list = "list"
        case car_image_url = "car_image_url"
        case helicopter_image_url = "helicopter_image_url"
        case yacht_image_url = "yacht_image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([List].self, forKey: .list)
        car_image_url = try values.decodeIfPresent(String.self, forKey: .car_image_url)
        helicopter_image_url = try values.decodeIfPresent(String.self, forKey: .helicopter_image_url)
        yacht_image_url = try values.decodeIfPresent(String.self, forKey: .yacht_image_url)
    }

}


struct PackageDetails : Codable {
    let package_details_id : String?
    let car_id : String?
    let type : String?
    let package_id : String?
    let created_at_date : String?
    let status : String?
    let vehicle_details : Vehicle_details?

    enum CodingKeys: String, CodingKey {

        case package_details_id = "package_details_id"
        case car_id = "car_id"
        case type = "type"
        case package_id = "package_id"
        case created_at_date = "created_at_date"
        case status = "status"
        case vehicle_details = "vehicle_details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        package_details_id = try values.decodeIfPresent(String.self, forKey: .package_details_id)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        package_id = try values.decodeIfPresent(String.self, forKey: .package_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        vehicle_details = try values.decodeIfPresent(Vehicle_details.self, forKey: .vehicle_details)
    }

}


struct List : Codable {
    let package_id : String?
    let package_name : String?
    let offer : String?
    let created_at_date : String?
    let status : String?
    let details : [PackageDetails]?

    enum CodingKeys: String, CodingKey {

        case package_id = "package_id"
        case package_name = "package_name"
        case offer = "offer"
        case created_at_date = "created_at_date"
        case status = "status"
        case details = "details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        package_id = try values.decodeIfPresent(String.self, forKey: .package_id)
        package_name = try values.decodeIfPresent(String.self, forKey: .package_name)
        offer = try values.decodeIfPresent(String.self, forKey: .offer)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        details = try values.decodeIfPresent([PackageDetails].self, forKey: .details)
    }

}


struct Vehicle_details : Codable {
    let car_id : String?
    let brand_model : String?
    let brand_id : String?
    let provider_id : String?
    let engine_number : String?
    let chasis_number : String?
    let man_year : String?
    let seat : String?
    let max_speed : String?
    let fuel : String?
    let engine_capacity : String?
    let description : String?
    let latitude : String?
    let longitude : String?
    let base_price : String?
    let rent_price : String?
    let passenger : String?
    let features : String?
    let attribute_id : String?
    let baggage : String?
    let door : String?
    let gear : String?
    let slider : String?
    let terms : String?
    let status : String?
    let created_at_date : String?
    let car_image_id : String?
    let car_image : String?
    let brand_name : String?
    let category_id : String?
    let brand_image : String?
    let vehicle_image : String?
    let vehicle_id : String?
    let vehicle_name : String?

    enum CodingKeys: String, CodingKey {

        case car_id = "car_id"
        case brand_model = "brand_model"
        case brand_id = "brand_id"
        case provider_id = "provider_id"
        case engine_number = "engine_number"
        case chasis_number = "chasis_number"
        case man_year = "man_year"
        case seat = "seat"
        case max_speed = "max_speed"
        case fuel = "fuel"
        case engine_capacity = "engine_capacity"
        case description = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case base_price = "base_price"
        case rent_price = "rent_price"
        case passenger = "passenger"
        case features = "features"
        case attribute_id = "attribute_id"
        case baggage = "baggage"
        case door = "door"
        case gear = "gear"
        case slider = "slider"
        case terms = "terms"
        case status = "status"
        case created_at_date = "created_at_date"
        case car_image_id = "car_image_id"
        case car_image = "car_image"
        case brand_name = "brand_name"
        case category_id = "category_id"
        case brand_image = "brand_image"
        case vehicle_image = "vehicle_image"
        case vehicle_id = "vehicle_id"
        case vehicle_name = "vehicle_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        brand_model = try values.decodeIfPresent(String.self, forKey: .brand_model)
        brand_id = try values.decodeIfPresent(String.self, forKey: .brand_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        engine_number = try values.decodeIfPresent(String.self, forKey: .engine_number)
        chasis_number = try values.decodeIfPresent(String.self, forKey: .chasis_number)
        man_year = try values.decodeIfPresent(String.self, forKey: .man_year)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        max_speed = try values.decodeIfPresent(String.self, forKey: .max_speed)
        fuel = try values.decodeIfPresent(String.self, forKey: .fuel)
        engine_capacity = try values.decodeIfPresent(String.self, forKey: .engine_capacity)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        door = try values.decodeIfPresent(String.self, forKey: .door)
        gear = try values.decodeIfPresent(String.self, forKey: .gear)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        car_image_id = try values.decodeIfPresent(String.self, forKey: .car_image_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        brand_image = try values.decodeIfPresent(String.self, forKey: .brand_image)
        vehicle_image = try values.decodeIfPresent(String.self, forKey: .vehicle_image)
        vehicle_id = try values.decodeIfPresent(String.self, forKey: .vehicle_id)
        vehicle_name = try values.decodeIfPresent(String.self, forKey: .vehicle_name)
    }

}

struct Packageclass : Codable {
    let status : Bool?
    let message : String?
    let data : PackageData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(PackageData.self, forKey: .data)
    }

}


//search

struct SearchData : Codable {
    let list : [Listsearch]?
    let car_image_url : String?
    let helicopter_image_url : String?
    let yacht_image_url : String?
    let gift_image_url : String?

    enum CodingKeys: String, CodingKey {

        case list = "list"
        case car_image_url = "car_image_url"
        case helicopter_image_url = "helicopter_image_url"
        case yacht_image_url = "yacht_image_url"
        case gift_image_url = "gift_image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([Listsearch].self, forKey: .list)
        car_image_url = try values.decodeIfPresent(String.self, forKey: .car_image_url)
        helicopter_image_url = try values.decodeIfPresent(String.self, forKey: .helicopter_image_url)
        yacht_image_url = try values.decodeIfPresent(String.self, forKey: .yacht_image_url)
        gift_image_url = try values.decodeIfPresent(String.self, forKey: .gift_image_url)
    }

}


struct Listsearch : Codable {
    let car_id : String?
    let brand_model : String?
    let type : String?
    let images : [Images]?

    enum CodingKeys: String, CodingKey {

        case car_id = "car_id"
        case brand_model = "brand_model"
        case type = "type"
        case images = "images"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        brand_model = try values.decodeIfPresent(String.self, forKey: .brand_model)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        images = try values.decodeIfPresent([Images].self, forKey: .images)
    }

}



struct Images : Codable {
    let car_view_image_id : String?
    let car_image : String?
    let car_id : String?
    let created_at_date : String?
    let status : String?
    let yacht_image_id : String?
    let yacht_image : String?
    let yacht_id : String?
    let helicopter_image_id : String?
    let helicopter_id : String?
    let helicopter_image : String?
    let gift_image_id : String?
    let gift_id : String?
    let gift_image : String?
   
    enum CodingKeys: String, CodingKey {

        case car_view_image_id = "car_view_image_id"
        case car_image = "car_image"
        case car_id = "car_id"
        case created_at_date = "created_at_date"
        case status = "status"
        case yacht_image_id = "yacht_image_id"
        case yacht_image = "yacht_image"
        case yacht_id = "yacht_id"
        case helicopter_image_id = "helicopter_image_id"
        case helicopter_id = "helicopter_id"
        case helicopter_image = "helicopter_image"
        case gift_image_id = "gift_image_id"
        case gift_id = "gift_id"
        case gift_image = "gift_image"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_view_image_id = try values.decodeIfPresent(String.self, forKey: .car_view_image_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        yacht_image_id = try values.decodeIfPresent(String.self, forKey: .yacht_image_id)
        yacht_image = try values.decodeIfPresent(String.self, forKey: .yacht_image)
        yacht_id = try values.decodeIfPresent(String.self, forKey: .yacht_id)
        helicopter_image_id = try values.decodeIfPresent(String.self, forKey: .helicopter_image_id)
        helicopter_id = try values.decodeIfPresent(String.self, forKey: .helicopter_id)
        helicopter_image = try values.decodeIfPresent(String.self, forKey: .helicopter_image)
        gift_image_id = try values.decodeIfPresent(String.self, forKey: .gift_image_id)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        gift_image = try values.decodeIfPresent(String.self, forKey: .gift_image)
    }

}


struct Searchclass : Codable {
    let status : Bool?
    let message : String?
    let data : SearchData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SearchData.self, forKey: .data)
    }

}
