//
//  PackageCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class PackageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblname: UILabel!
}
