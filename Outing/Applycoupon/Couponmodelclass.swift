//
//  Couponmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 27/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct Offers : Codable {
    let food_offer_id : String?
    let offer_name : String?
    let offer_code : String?
    let type : String?
    let amount : String?
    let min_amount : String?
    let start_date : String?
    let end_date : String?
    let activity : String?
    let description : String?
    let status : String?
    let created_at_date : String?

    enum CodingKeys: String, CodingKey {

        case food_offer_id = "food_offer_id"
        case offer_name = "offer_name"
        case offer_code = "offer_code"
        case type = "type"
        case amount = "amount"
        case min_amount = "min_amount"
        case start_date = "start_date"
        case end_date = "end_date"
        case activity = "activity"
        case description = "description"
        case status = "status"
        case created_at_date = "created_at_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        food_offer_id = try values.decodeIfPresent(String.self, forKey: .food_offer_id)
        offer_name = try values.decodeIfPresent(String.self, forKey: .offer_name)
        offer_code = try values.decodeIfPresent(String.self, forKey: .offer_code)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
        min_amount = try values.decodeIfPresent(String.self, forKey: .min_amount)
        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
        activity = try values.decodeIfPresent(String.self, forKey: .activity)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
    }

}


struct Couponclass : Codable {
    let status : Bool?
    let message : String?
    let data : CouponData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CouponData.self, forKey: .data)
    }

}

struct ApplyCouponclass : Codable {
    let status : Bool?
    let message : String?
    let data : ApplyCouponData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ApplyCouponData.self, forKey: .data)
    }

}
struct ApplyCouponData : Codable {
    let discount : String?
    let id:String?

    enum CodingKeys: String, CodingKey {

        case discount = "discount"
        case id = "offer_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        discount = try values.decodeIfPresent(String.self, forKey: .discount)
    }

}


struct CouponData : Codable {
    let offers : [Offers]?

    enum CodingKeys: String, CodingKey {

        case offers = "offers"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        offers = try values.decodeIfPresent([Offers].self, forKey: .offers)
    }

}

