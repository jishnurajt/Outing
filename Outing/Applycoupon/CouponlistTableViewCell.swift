//
//  CouponlistTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 27/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class CouponlistTableViewCell: UITableViewCell {

    @IBOutlet weak var btnapplycoupon: UIButton!
    @IBOutlet weak var viewcoupon: UIView!
    @IBOutlet weak var lbloffercode: UILabel!
    @IBOutlet weak var lblofferdescrption: UILabel!
    @IBOutlet weak var lbloffername: UILabel!
    @IBOutlet weak var btnoffercode: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewcoupon.layer.cornerRadius=20
        btnoffercode.layer.cornerRadius=20
        lbloffercode.layer.cornerRadius=20
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
