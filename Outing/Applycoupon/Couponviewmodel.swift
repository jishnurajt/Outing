//
//  Couponviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 27/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class Couponviewmodel{
   var userid=String()
    
func offer(completion : @escaping (Couponclass) -> ())  {
    if let user_id=UserDefaults.standard.value(forKey: "userid"){
        userid=UserDefaults.standard.value(forKey: "userid") as! String
    }

    
    let poststring="security_token=out564bkgtsernsko&user_id=\(userid)"
    
    var request = NSMutableURLRequest(url: APIEndPoint.offer.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Couponclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    
    
    func offerApply(code:String,total:String,completion : @escaping (ApplyCouponclass) -> ())  {
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            userid=UserDefaults.standard.value(forKey: "userid") as! String
        }

        
        let poststring="security_token=out564bkgtsernsko&user_id=\(userid)&offer_code=\(code)&total=\(total)"
        
        var request = NSMutableURLRequest(url: APIEndPoint.applyOffer.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ApplyCouponclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
}
