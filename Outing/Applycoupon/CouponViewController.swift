//
//  CouponViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 27/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class CouponViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var couponmodel=Couponviewmodel()
    var offers:[Offers]?=[]
    var totalamount=Int()
     var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    @IBOutlet weak var tableviewcoupon: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.couponmodel.offer{ (model) in
            self.offerdata(data:model)
        }
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0{
            return 1
        }else{
            return offers?.count ?? 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section==0{
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "ApllycouponTableViewCell", for: indexPath) as? ApllycouponTableViewCell)!
            return cell1
        }else{
            let cell2 = (tableView.dequeueReusableCell(withIdentifier: "CouponlistTableViewCell", for: indexPath) as? CouponlistTableViewCell)!
            cell2.lbloffername.text=offers?[indexPath.row].offer_name
            cell2.lblofferdescrption.text=offers?[indexPath.row].description
            
            cell2.lbloffercode.text=offers?[indexPath.row].offer_code
            cell2.btnapplycoupon.tag=indexPath.row
            cell2.btnapplycoupon.addTarget(self, action: #selector(btnapplycouponaction(sender:)), for: .touchUpInside)
            return cell2
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 100
        }else{
            return 200
        }
        
    }
    
    @objc func btnapplycouponaction(sender: UIButton){
        if (offers?[sender.tag].min_amount)==""{
            
            let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            cart1.totalamount=totalamount-(Int((offers?[sender.tag].amount)!) ?? 0)
            cart1.offertotalafteroffer=(Int((offers?[sender.tag].amount)!) ?? 0)
            cart1.offer_id=offers?[sender.tag].food_offer_id ?? ""
             cart1.offername=offers?[sender.tag].offer_name ?? ""
             cart1.offerflag=1
            cart1.cart=cart
            cart1.imageurlgift=imageurlgift
            
            self.navigationController?.pushViewController(cart1, animated: true)
        }else if totalamount < Int((offers?[sender.tag].min_amount)!) ?? 0{
            showToast(message: "Offers can be only applied to orders above" + " \(offers?[sender.tag].min_amount)",font:UIFont.boldSystemFont(ofSize: 12),duration: 2)
        }else{
            
            
            let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            cart1.totalamount=totalamount-(Int((offers?[sender.tag].amount)!) ?? 0)
            cart1.offertotalafteroffer=(Int((offers?[sender.tag].amount)!) ?? 0)
            cart1.offer_id=offers?[sender.tag].food_offer_id ?? ""
             cart1.offername=offers?[sender.tag].offer_name ?? ""
             cart1.offerflag=1
            cart1.cart=cart
            cart1.imageurlgift=imageurlgift
            self.navigationController?.pushViewController(cart1, animated: true)
        }
        
        
        //        if totalamount < Int((offers?[sender.tag].min_amount)!) ?? 0{
        //            showToast(message: "Totalamount should be greater than \(offers?[sender.tag].min_amount) to apply coupon", font:  .boldSystemFont(ofSize: 13), duration: 2)
        //        }else{
        //            totalamount=totalamount-(Int((offers?[sender.tag].amount)!) ?? 0)
        //        }
        
    }
    
    func offerdata(data: Couponclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.offers=data.data?.offers
                self.tableviewcoupon.reloadData()
                
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
}
