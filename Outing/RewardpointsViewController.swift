//
//  RewardpointsViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 28/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class RewardpointsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var rewardmodel=Rewardviewmodel()

    @IBOutlet weak var lblrewardpoints: UILabel!
    @IBOutlet weak var lblrewardactivity: UILabel!
    @IBOutlet weak var tableviewrewardpoints: UITableView!
    @IBOutlet weak var lblavialablerewardpoints: UILabel!
    var point=String()
    var pointhistory:[Point_history]?=[]
    override func viewDidLoad() {
        super.viewDidLoad()

       rewardmodel.user_point{ (model) in
            self.userpointdata(data:model)
        }
    }
    @IBOutlet weak var lblline: UILabel!
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pointhistory?.count ?? 0
       
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
              let cell = (tableView.dequeueReusableCell(withIdentifier: "RewardsTableViewCell", for: indexPath) as? RewardsTableViewCell)!
        var delimiter = " "
        cell.lbldate.text="Date:"+String(pointhistory?[indexPath.row].created_at_date?.components(separatedBy: delimiter).first ?? "")
        if pointhistory?[indexPath.row].type=="1"{
            cell.lblreddempoints.text="-"+(pointhistory?[indexPath.row].point ?? "")
        }else{
          cell.lblreddempoints.text="+"+(pointhistory?[indexPath.row].point ?? "")
        }
        return cell
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func userpointdata(data: Rewardclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            point=data.data?.point?.first?.point ?? ""
            pointhistory=data.data?.point_history
           
            DispatchQueue.main.async{
                self.lblavialablerewardpoints.text="Available Reward Points: "+self.point
               
                
                if self.pointhistory?.count ?? 0==0{
                    self.lblrewardactivity.isHidden=true
                    self.lblrewardpoints.isHidden=true
                    self.lblline.isHidden=true
                }else{
                    self.lblrewardactivity.isHidden=false
                    self.lblrewardpoints.isHidden=false
                    self.lblline.isHidden=false
                }
                self.tableviewrewardpoints.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
   
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
