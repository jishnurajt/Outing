//
//  APIEndpoints.swift
//  Outing
//
//  Created by Arun Vijayan on 12/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public enum APIEndPoint {
    
    case send_user
    case user_add
    case user_login
    case brands
    case car_list
    case car_list_brand
    case car_details
    case addon_category
    case addon_brand
    case gift
    case helicopter_details
    case get_time_slot
    case yacht_list
    case yacht_details
    case check_yacht
    case check_car
    case package_list
    case document_list
    case contact
    case delete_doc
    case forgot_password
    case create_new_password
    case booking_history
    case add_review
    case search
    case profile_edit
    case submit_enquiry
    case booking_history_product
    case update_user_profile
    case social
    case offer
    case applyOffer
    case user_point
    case submit_subscription_payment
    case gift_details
    case helicopter_list
    
    var url:URL{
        switch self {
        case .send_user:
            return Config.BASE_URL.appendingPathComponent("send_user")
        case .user_add:
            return Config.BASE_URL.appendingPathComponent("user_add")
        case .user_login:
            return Config.BASE_URL.appendingPathComponent("user_login")
        case .brands:
            return Config.BASE_URL.appendingPathComponent("brands")
        case .car_list:
            return Config.BASE_URL.appendingPathComponent("car_list")
        case .car_list_brand:
            return Config.BASE_URL.appendingPathComponent("car_list_brand")
        case .car_details:
            return Config.BASE_URL.appendingPathComponent("car_details")
        case .addon_category:
            return Config.BASE_URL.appendingPathComponent("addon_category")
        case .addon_brand:
            return Config.BASE_URL.appendingPathComponent("addon_brand")
        case .gift:
            return Config.BASE_URL.appendingPathComponent("gift")
        case .helicopter_details:
            return Config.BASE_URL.appendingPathComponent("helicopter_details")
        case .get_time_slot:
            return Config.BASE_URL.appendingPathComponent("get_time_slot")
        case .yacht_list:
            return Config.BASE_URL.appendingPathComponent("yacht_list")
        case .yacht_details:
            return Config.BASE_URL.appendingPathComponent("yacht_details")
        case .check_yacht:
            return Config.BASE_URL.appendingPathComponent("check_yacht")
        case .check_car:
            return Config.BASE_URL.appendingPathComponent("check_car")
        case .package_list:
            return Config.BASE_URL.appendingPathComponent("package_list")
        case .document_list:
            return Config.BASE_URL.appendingPathComponent("document_list")
        case .contact:
            return Config.BASE_URL.appendingPathComponent("contact")
        case .delete_doc:
            return Config.BASE_URL.appendingPathComponent("delete_doc")
        case .forgot_password:
            return Config.BASE_URL.appendingPathComponent("forgot_password")
        case .create_new_password:
            return Config.BASE_URL.appendingPathComponent("create_new_password")
        case .booking_history:
            return Config.BASE_URL.appendingPathComponent("booking_history")
        case .add_review:
            return Config.BASE_URL.appendingPathComponent("add_review")
        case .search:
            return Config.BASE_URL.appendingPathComponent("search")
        case .profile_edit:
            return Config.BASE_URL.appendingPathComponent("profile_edit")
        case .submit_enquiry:
            return Config.BASE_URL.appendingPathComponent("submit_enquiry")
        case .booking_history_product:
            return Config.BASE_URL.appendingPathComponent("booking_history_product")
        case .update_user_profile:
            return Config.BASE_URL.appendingPathComponent("update_user_profile")
        case .social:
            return Config.BASE_URL.appendingPathComponent("social")
        case .offer:
            return Config.BASE_URL.appendingPathComponent("offer")
        case .applyOffer:
            return Config.BASE_URL.appendingPathComponent("apply_offer")
        case .user_point:
            return Config.BASE_URL.appendingPathComponent("user_point")
        case .submit_subscription_payment:
            return Config.BASE_URL.appendingPathComponent("submit_subscription_payment")
        case .gift_details:
            return Config.BASE_URL.appendingPathComponent("gift_details")
        case .helicopter_list:
            return Config.BASE_URL.appendingPathComponent("helicopter_list")
            
        }
    }
}
