//
//  CarViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 08/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Kingfisher



class CarViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var imageviewcarleading: NSLayoutConstraint!
    @IBOutlet weak var imagebrandheight: NSLayoutConstraint!
    @IBOutlet weak var collectionviewbrandsheight: NSLayoutConstraint!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var lblnocaravailable: UILabel!
    @IBOutlet weak var collectionviewselectedcarbrand: UICollectionView!
    @IBOutlet weak var collectionviewselectedcarimage: UICollectionView!
    
    var screenSize  = UIScreen.main.bounds
    var screenWidth = UIScreen.main.bounds.width
    var carbrands:[Brands]?=[]
    var carmodel=Carviwmodel()
    var carlist:[Car_list]?=[]
    var imageurl=String()
    var imageurlbrand=String()
    var carimages:[Car_images]?=[]
    var homemodel=Homeviewmodel()
    var brandidfromhome=String()
    var carbrandflag=0
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var offset=0
    var offsetbrand=0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
     //   btnback.layer.cornerRadius=25
        if carbrandflag==0{
            carmodel.offset=offset
            carmodel.car_list{ (model) in
                self.carlistdata(data:model)
            }
        }else{
            carmodel.offsetbrand=offsetbrand
            carmodel.brandid=brandidfromhome
            carmodel.car_list_brand{ (model) in
                self.carlistbrandadata(data:model)
            }
        }
        homemodel.brands{ (model) in
            self.brandsdata(data:model)
        }
        
        
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            imagebrandheight.constant=140
            //collectionviewbrandsheight.constant=70
        }else{
            if screenSize.height>800{
                imagebrandheight.constant=110
                //  collectionviewbrandsheight.constant=50
            }else{
                imagebrandheight.constant=90
                //collectionviewbrandsheight.constant=40
            }
        }
        
        collectionviewselectedcarbrand.layer.cornerRadius = collectionviewselectedcarbrand.frame.height/2
        collectionviewselectedcarbrand.clipsToBounds = true
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewselectedcarimage{
            return carlist?.count ?? 0
        }else{
            return carbrands?.count ?? 0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView==collectionviewselectedcarimage{
            
            var lastelement=0
            if carbrandflag==0{
                if carlist?.count ?? 0 > 0 {
                    lastelement=carlist!.count - 1
                    print("lastelement",lastelement)
                    if indexPath.row==lastelement{
                        
                        offset+=1
                        carmodel.offset=offset
                        carmodel.car_list{ (model) in
                            self.carlistdata(data:model)
                        }
                    }
                }
            }else{
                
                if carlist?.count ?? 0 > 0{
                    lastelement=carlist!.count - 1
                    print("lastelement",lastelement)
                    if indexPath.row==lastelement{
                        
                        offsetbrand+=1
                        carmodel.offsetbrand=offsetbrand
                        carmodel.brandid=brandidfromhome
                        carmodel.car_list_brand{ (model) in
                            self.carlistbrandadata(data:model)
                            
                        }
                    }
                }
                
            }
            
            
            
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedcarimageCollectionViewCell",
                                                           for: indexPath) as! SelectedcarimageCollectionViewCell
            
            
            
//            if UIDevice.current.userInterfaceIdiom == .pad{
//                if indexPath.row==0{
//                    // cell1.imageviewcarleading.constant=170
//                    cell1.lblcarnameleading.constant=160
//                    cell1.lblcarpriceleading.constant=170
//                }else{
//                    //  cell1.imageviewcarleading.constant=0
//                    cell1.lblcarnameleading.constant=0
//                    cell1.lblcarpriceleading.constant=0
//                }
//            }else{
//                if screenSize.height>800{
//                    if indexPath.row==0{
//                        // cell1.imageviewcarleading.constant=60
//                        cell1.lblcarnameleading.constant=50
//                        cell1.lblcarpriceleading.constant=60
//                    }else{
//                        //  cell1.imageviewcarleading.constant = -10
//                        cell1.lblcarnameleading.constant = -10
//                        cell1.lblcarpriceleading.constant = -10
//                    }
//                    
//                }else{
//                    
//                    if indexPath.row==0{
//                        //  cell1.imageviewcarleading.constant=50
//                        cell1.lblcarnameleading.constant=50
//                        cell1.lblcarpriceleading.constant=60
//                    }else{
//                        // cell1.imageviewcarleading.constant = -20
//                        cell1.lblcarnameleading.constant = -10
//                        cell1.lblcarpriceleading.constant = -10
//                        
//                        
//                        
//                    }
//                }
//            }
//            
            cell1.lblcarname.text=String(carlist?[indexPath.row].brand_name ?? "")+" "+String(carlist?[indexPath.row].brand_model ?? "")
            if let price = carlist?[indexPath.row].base_price,let number = Int(price){
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number))
                cell1.lblcarprice.text="QAR "+(formattedNumber ??  "")+" per day"
            }
            
            let carimage=carlist?[indexPath.row].car_images?[0].car_image ?? ""
            let url = URL(string:imageurl+carimage)
            
            cell1.imageviewcar.kf.indicatorType = .activity
            cell1.imageviewcar.kf.setImage(with: url)
            
            //            if UIDevice.current.userInterfaceIdiom == .pad{
            //
            //                cell1.imageviewcar.contentMode = .scaleAspectFit
            //
            //            }else{
            //                if screenSize.height>800{
            //                    cell1.imageviewcar.contentMode = .scaleAspectFit
            //                }else{
            //                    cell1.imageviewcar.contentMode = .scaleAspectFit
            //                }
            //            }
            
            
            
            return cell1
        }else{
            
            
            //            var lastelement=0
            //
            //            if carbrands?.count ?? 0 >= 6{
            //                lastelement=carbrands!.count - 1
            //                print("lastelement",lastelement)
            //                if indexPath.row==lastelement{
            //
            //                    offsetbrand+=1
            //                    carmodel.offsetbrand=offsetbrand
            //                    carmodel.brandid=brandidfromhome
            //                    carmodel.car_list_brand{ (model) in
            //                        self.carlistbrandadata(data:model)
            //                    }
            //                }
            //            }
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedcarbrandCollectionViewCell",
                                                           for: indexPath) as! SelectedcarbrandCollectionViewCell
            let url = URL(string:imageurlbrand+(carbrands?[indexPath.row].brand_image ?? ""))
            
            cell2.imageviewcarbrand.kf.indicatorType = .activity
            cell2.imageviewcarbrand.kf.setImage(with: url)
            
            cell2.imageviewcarbrand.contentMode = .scaleAspectFit
            return cell2
        }
    }
    //    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //       {
    //
    //
    //
    //       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView==collectionviewselectedcarbrand{
            carmodel.brandid=carbrands?[indexPath.row].brand_id ?? ""
            carmodel.offsetbrand=0
            self.carbrandflag = 1
            self.offsetbrand = 0
            self.carlist?.removeAll()
            collectionView.reloadData()
            carmodel.car_list_brand{ (model) in
                self.carlistbrandadata(data:model)
            }
        }else{
            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CardetailsViewController") as! CardetailsViewController
            if cart?.count ?? 0>0{
                cardetail.cart=cart
                cardetail.imageurlgift=imageurlgift
            }
            cardetail.carid=carlist?[indexPath.row].car_id ?? ""
            self.navigationController?.pushViewController(cardetail, animated: true)
        }
        
    }
    
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
        
    }
    
    
    func carlistdata(data: Carlistclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                
                if self.carlist?.count ?? 0<=0{
                    self.carlist=data.data?.car_list
                }else{
                    self.carlist!+=(data.data?.car_list)!
                }
                
                self.imageurl=data.data?.image_url ?? ""
                self.collectionviewselectedcarimage.reloadData()
            }
        }else{
            DispatchQueue.main.async{
                //  self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    func brandsdata(data: Homeclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                //                if self.carbrands?.count ?? 0<0{
                //                    self.carbrands=data.data?.brands
                //                }
                self.carbrands = data.data?.brands
                
                self.imageurlbrand=data.data?.image_url ?? ""
                
                self.collectionviewselectedcarbrand.reloadData()
                
                
            }
        }else{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    func carlistbrandadata(data: Carlistclass) {
        print("data",data)
        
        let status=data.status
        
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                if self.carlist?.count ?? 0<=0{
                    self.carlist=data.data?.car_list
                }else{
                    self.carlist!+=(data.data?.car_list)!
                }
                self.imageurl=data.data?.image_url ?? ""
                self.collectionviewselectedcarimage.isHidden=false
                self.lblnocaravailable.isHidden=true
                self.collectionviewselectedcarimage.reloadData()
            }
        }else if carlist?.count == 0{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewselectedcarimage.isHidden=true
                self.lblnocaravailable.isHidden=false
                
            }
        }
    }
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnsearchaction(_ sender: Any) {
    }
}
extension CarViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView==collectionviewselectedcarimage{
            return collectionView.frame.size
        }else{
            return CGSize(width:collectionView.frame.width/3 - 20, height: collectionView.frame.height)
        }
    }
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView==collectionviewselectedcarbrand{
           return 20
            
        }else{
            return 0
        }
        
        
    }
  
}
