//
//  Carmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 14/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
//car_list
struct Carlistclass : Codable {
    let status : Bool?
    let message : String?
    let data : CarData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CarData.self, forKey: .data)
    }

}

struct Car_images : Codable {
    let car_image_id : String?
    let car_image : String?
    let car_id : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case car_image_id = "car_image_id"
        case car_image = "car_image"
        case car_id = "car_id"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_image_id = try values.decodeIfPresent(String.self, forKey: .car_image_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct Car_list : Codable {
    let car_id : String?
    let brand_model : String?
    let brand_id : String?
    let provider_id : String?
    let engine_number : String?
    let chasis_number : String?
    let man_year : String?
    let seat : String?
    let max_speed : String?
    let fuel : String?
    let engine_capacity : String?
    let description : String?
    let latitude : String?
    let longitude : String?
    let base_price : String?
    let rent_price : String?
    let passenger : String?
    let features : String?
    let attribute_id : String?
    let baggage : String?
    let door : String?
    let gear : String?
    let slider : String?
    let status : String?
    let created_at_date : String?
    let brand_name : String?
    let category_id : String?
    let brand_image : String?
    let car_images : [Car_images]?

    enum CodingKeys: String, CodingKey {

        case car_id = "car_id"
        case brand_model = "brand_model"
        case brand_id = "brand_id"
        case provider_id = "provider_id"
        case engine_number = "engine_number"
        case chasis_number = "chasis_number"
        case man_year = "man_year"
        case seat = "seat"
        case max_speed = "max_speed"
        case fuel = "fuel"
        case engine_capacity = "engine_capacity"
        case description = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case base_price = "base_price"
        case rent_price = "rent_price"
        case passenger = "passenger"
        case features = "features"
        case attribute_id = "attribute_id"
        case baggage = "baggage"
        case door = "door"
        case gear = "gear"
        case slider = "slider"
        case status = "status"
        case created_at_date = "created_at_date"
        case brand_name = "brand_name"
        case category_id = "category_id"
        case brand_image = "brand_image"
        case car_images = "car_images"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        brand_model = try values.decodeIfPresent(String.self, forKey: .brand_model)
        brand_id = try values.decodeIfPresent(String.self, forKey: .brand_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        engine_number = try values.decodeIfPresent(String.self, forKey: .engine_number)
        chasis_number = try values.decodeIfPresent(String.self, forKey: .chasis_number)
        man_year = try values.decodeIfPresent(String.self, forKey: .man_year)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        max_speed = try values.decodeIfPresent(String.self, forKey: .max_speed)
        fuel = try values.decodeIfPresent(String.self, forKey: .fuel)
        engine_capacity = try values.decodeIfPresent(String.self, forKey: .engine_capacity)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        door = try values.decodeIfPresent(String.self, forKey: .door)
        gear = try values.decodeIfPresent(String.self, forKey: .gear)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        brand_image = try values.decodeIfPresent(String.self, forKey: .brand_image)
        car_images = try values.decodeIfPresent([Car_images].self, forKey: .car_images)
    }

}
struct CarData : Codable {
    let car_list : [Car_list]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case car_list = "car_list"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_list = try values.decodeIfPresent([Car_list].self, forKey: .car_list)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}

