//
//  SelectedcarimageCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 08/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class SelectedcarimageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblcarnameleading: NSLayoutConstraint!
    @IBOutlet weak var lblcarpriceleading: NSLayoutConstraint!
    @IBOutlet weak var imageviewcarleading: NSLayoutConstraint!
    @IBOutlet weak var imageviewcar: UIImageView!
    
    @IBOutlet weak var lblcarname: UILabel!
    
    @IBOutlet weak var lblcarprice: UILabel!
}
