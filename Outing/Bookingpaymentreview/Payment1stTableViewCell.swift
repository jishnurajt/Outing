//
//  Payment1stTableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class Payment1stTableViewCell: UITableViewCell {

    @IBOutlet weak var segmentcontrol: UISegmentedControl!
    @IBOutlet weak var viewsegmentcontrol: UIView!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var lbltype: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        segmentcontrol.layer.cornerRadius = 20
               segmentcontrol.layer.masksToBounds = true
        viewsegmentcontrol.layer.cornerRadius = 20
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
