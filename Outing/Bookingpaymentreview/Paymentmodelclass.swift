//
//  Paymentmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct BookinghistoryData : Codable {
    let list : [List1]?
    let car_image_url : String?
    let helicopter_image_url : String?
    let yacht_image_url : String?

    enum CodingKeys: String, CodingKey {

        case list = "list"
        case car_image_url = "car_image_url"
        case helicopter_image_url = "helicopter_image_url"
        case yacht_image_url = "yacht_image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([List1].self, forKey: .list)
        car_image_url = try values.decodeIfPresent(String.self, forKey: .car_image_url)
        helicopter_image_url = try values.decodeIfPresent(String.self, forKey: .helicopter_image_url)
        yacht_image_url = try values.decodeIfPresent(String.self, forKey: .yacht_image_url)
    }

}


struct BookinghistoryDetails : Codable {
    let booking_detail_id : String?
    let booking_id : String?
    let provider_id : String?
    let car_id : String?
    let user_id : String?
    let pick_up_date : String?
    let pick_up_time : String?
    let drop_date : String?
    let drop_time : String?
    let base_price : String?
    let package_id : String?
    let total_days : String?
    let total_hour : String?
    let is_addon : String?
    let created_at_date : String?
    let status : String?
    let flag : String?
    let real : String?
    let vehicle_details : Vehicle_details1?
    let people:String?

    enum CodingKeys: String, CodingKey {

        case booking_detail_id = "booking_detail_id"
        case booking_id = "booking_id"
        case provider_id = "provider_id"
        case car_id = "car_id"
        case user_id = "user_id"
        case pick_up_date = "pick_up_date"
        case pick_up_time = "pick_up_time"
        case drop_date = "drop_date"
        case drop_time = "drop_time"
        case base_price = "base_price"
        case package_id = "package_id"
        case total_days = "total_days"
        case total_hour = "total_hour"
        case is_addon = "is_addon"
        case created_at_date = "created_at_date"
        case status = "status"
        case flag = "flag"
        case real = "real"
        case vehicle_details = "vehicle_details"
        case people
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        booking_detail_id = try values.decodeIfPresent(String.self, forKey: .booking_detail_id)
        booking_id = try values.decodeIfPresent(String.self, forKey: .booking_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        pick_up_date = try values.decodeIfPresent(String.self, forKey: .pick_up_date)
        pick_up_time = try values.decodeIfPresent(String.self, forKey: .pick_up_time)
        drop_date = try values.decodeIfPresent(String.self, forKey: .drop_date)
        drop_time = try values.decodeIfPresent(String.self, forKey: .drop_time)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        package_id = try values.decodeIfPresent(String.self, forKey: .package_id)
        total_days = try values.decodeIfPresent(String.self, forKey: .total_days)
        total_hour = try values.decodeIfPresent(String.self, forKey: .total_hour)
        is_addon = try values.decodeIfPresent(String.self, forKey: .is_addon)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        flag = try values.decodeIfPresent(String.self, forKey: .flag)
        real = try values.decodeIfPresent(String.self, forKey: .real)
        vehicle_details = try values.decodeIfPresent(Vehicle_details1.self, forKey: .vehicle_details)
        people = try values.decodeIfPresent(String.self, forKey: .people)
    }

}




struct List1 : Codable {
    let booking_id : String?
    let booking_unique_id : String?
    let user_id : String?
    let total_price : String?
    let offer_tot : String?
    let payment_type : String?
    let payment_status : String?
    let ref : String?
    let invoice : String?
    let is_finish : String?
    let created_at_date : String?
    let status : String?
    let details : [BookinghistoryDetails]?

    enum CodingKeys: String, CodingKey {

        case booking_id = "booking_id"
        case booking_unique_id = "booking_unique_id"
        case user_id = "user_id"
        case total_price = "total_price"
        case offer_tot = "offer_tot"
        case payment_type = "payment_type"
        case payment_status = "payment_status"
        case ref = "ref"
        case invoice = "invoice"
        case is_finish = "is_finish"
        case created_at_date = "created_at_date"
        case status = "status"
        case details = "details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        booking_id = try values.decodeIfPresent(String.self, forKey: .booking_id)
        booking_unique_id = try values.decodeIfPresent(String.self, forKey: .booking_unique_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
        offer_tot = try values.decodeIfPresent(String.self, forKey: .offer_tot)
        payment_type = try values.decodeIfPresent(String.self, forKey: .payment_type)
        payment_status = try values.decodeIfPresent(String.self, forKey: .payment_status)
        ref = try values.decodeIfPresent(String.self, forKey: .ref)
        invoice = try values.decodeIfPresent(String.self, forKey: .invoice)
        is_finish = try values.decodeIfPresent(String.self, forKey: .is_finish)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        details = try values.decodeIfPresent([BookinghistoryDetails].self, forKey: .details)
    }
    init() {
        booking_id=nil
        booking_unique_id=nil
        user_id=nil
        total_price=nil
        offer_tot=nil
        payment_type=nil
        payment_status=nil
        ref=nil
        invoice=nil
        is_finish=nil
        created_at_date=nil
        status=nil
        details=nil
    }

}

struct Vehicle_details1 : Codable {
    let car_id : String?
    let brand_model : String?
    let brand_id : String?
    let provider_id : String?
    let engine_number : String?
    let chasis_number : String?
    let man_year : String?
    let seat : String?
    let max_speed : String?
    let fuel : String?
    let engine_capacity : String?
    let description : String?
    let latitude : String?
    let longitude : String?
    let base_price : String?
    let rent_price : String?
    let passenger : String?
    let features : String?
    let attribute_id : String?
    let baggage : String?
    let door : String?
    let gear : String?
    let slider : String?
    let terms : String?
    let status : String?
    let created_at_date : String?
    let car_image_id : String?
    let car_image : String?
    let brand_name : String?
    let category_id : String?
    let brand_image : String?
    let vehicle_image : String?
    let vehicle_id : String?
    let vehicle_name : String?

    enum CodingKeys: String, CodingKey {

        case car_id = "car_id"
        case brand_model = "brand_model"
        case brand_id = "brand_id"
        case provider_id = "provider_id"
        case engine_number = "engine_number"
        case chasis_number = "chasis_number"
        case man_year = "man_year"
        case seat = "seat"
        case max_speed = "max_speed"
        case fuel = "fuel"
        case engine_capacity = "engine_capacity"
        case description = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case base_price = "base_price"
        case rent_price = "rent_price"
        case passenger = "passenger"
        case features = "features"
        case attribute_id = "attribute_id"
        case baggage = "baggage"
        case door = "door"
        case gear = "gear"
        case slider = "slider"
        case terms = "terms"
        case status = "status"
        case created_at_date = "created_at_date"
        case car_image_id = "car_image_id"
        case car_image = "car_image"
        case brand_name = "brand_name"
        case category_id = "category_id"
        case brand_image = "brand_image"
        case vehicle_image = "vehicle_image"
        case vehicle_id = "vehicle_id"
        case vehicle_name = "vehicle_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        car_id = try values.decodeIfPresent(String.self, forKey: .car_id)
        brand_model = try values.decodeIfPresent(String.self, forKey: .brand_model)
        brand_id = try values.decodeIfPresent(String.self, forKey: .brand_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        engine_number = try values.decodeIfPresent(String.self, forKey: .engine_number)
        chasis_number = try values.decodeIfPresent(String.self, forKey: .chasis_number)
        man_year = try values.decodeIfPresent(String.self, forKey: .man_year)
        seat = try values.decodeIfPresent(String.self, forKey: .seat)
        max_speed = try values.decodeIfPresent(String.self, forKey: .max_speed)
        fuel = try values.decodeIfPresent(String.self, forKey: .fuel)
        engine_capacity = try values.decodeIfPresent(String.self, forKey: .engine_capacity)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        rent_price = try values.decodeIfPresent(String.self, forKey: .rent_price)
        passenger = try values.decodeIfPresent(String.self, forKey: .passenger)
        features = try values.decodeIfPresent(String.self, forKey: .features)
        attribute_id = try values.decodeIfPresent(String.self, forKey: .attribute_id)
        baggage = try values.decodeIfPresent(String.self, forKey: .baggage)
        door = try values.decodeIfPresent(String.self, forKey: .door)
        gear = try values.decodeIfPresent(String.self, forKey: .gear)
        slider = try values.decodeIfPresent(String.self, forKey: .slider)
        terms = try values.decodeIfPresent(String.self, forKey: .terms)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        car_image_id = try values.decodeIfPresent(String.self, forKey: .car_image_id)
        car_image = try values.decodeIfPresent(String.self, forKey: .car_image)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        brand_image = try values.decodeIfPresent(String.self, forKey: .brand_image)
        vehicle_image = try values.decodeIfPresent(String.self, forKey: .vehicle_image)
        vehicle_id = try values.decodeIfPresent(String.self, forKey: .vehicle_id)
        vehicle_name = try values.decodeIfPresent(String.self, forKey: .vehicle_name)
    }

}

struct Bookinghistoryclass : Codable {
    let status : Bool?
    let message : String?
    let data : BookinghistoryData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(BookinghistoryData.self, forKey: .data)
    }

}
//bookinghistoryproduct



struct Gift_details : Codable {
    let gift_id : String?
    let provider_id : String?
    let addon_cat_id : String?
    let addon_brand_id : String?
    let title : String?
    let description : String?
    let base_price : String?
    let latitude : String?
    let longitude : String?
    let created_at_date : String?
    let status : String?
    let gift_image_id : String?
    let gift_image : String?
    let vehicle_image : String?

    enum CodingKeys: String, CodingKey {

        case gift_id = "gift_id"
        case provider_id = "provider_id"
        case addon_cat_id = "addon_cat_id"
        case addon_brand_id = "addon_brand_id"
        case title = "title"
        case description = "description"
        case base_price = "base_price"
        case latitude = "latitude"
        case longitude = "longitude"
        case created_at_date = "created_at_date"
        case status = "status"
        case gift_image_id = "gift_image_id"
        case gift_image = "gift_image"
        case vehicle_image = "vehicle_image"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        gift_image_id = try values.decodeIfPresent(String.self, forKey: .gift_image_id)
        gift_image = try values.decodeIfPresent(String.self, forKey: .gift_image)
        vehicle_image = try values.decodeIfPresent(String.self, forKey: .vehicle_image)
    }

}


struct bookinghistoryData : Codable {
    let list : [List2]?
    let gift_image_url : String?

    enum CodingKeys: String, CodingKey {

        case list = "list"
        case gift_image_url = "gift_image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([List2].self, forKey: .list)
        gift_image_url = try values.decodeIfPresent(String.self, forKey: .gift_image_url)
    }

}


struct Details_bookinghistory: Codable {
    let gift_booking_details_id : String?
    let gift_booking_id : String?
    let booking_id : String?
    let gift_id : String?
    let qty : String?
    let price : String?
    let user_id : String?
    let total : String?
    let variance_id : String?
    let created_at_date : String?
    let status : String?
    let gift_details : Gift_details?

    enum CodingKeys: String, CodingKey {

        case gift_booking_details_id = "gift_booking_details_id"
        case gift_booking_id = "gift_booking_id"
        case booking_id = "booking_id"
        case gift_id = "gift_id"
        case qty = "qty"
        case price = "price"
        case user_id = "user_id"
        case total = "total"
        case variance_id = "variance_id"
        case created_at_date = "created_at_date"
        case status = "status"
        case gift_details = "gift_details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_booking_details_id = try values.decodeIfPresent(String.self, forKey: .gift_booking_details_id)
        gift_booking_id = try values.decodeIfPresent(String.self, forKey: .gift_booking_id)
        booking_id = try values.decodeIfPresent(String.self, forKey: .booking_id)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        qty = try values.decodeIfPresent(String.self, forKey: .qty)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        total = try values.decodeIfPresent(String.self, forKey: .total)
        variance_id = try values.decodeIfPresent(String.self, forKey: .variance_id)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        gift_details = try values.decodeIfPresent(Gift_details.self, forKey: .gift_details)
    }

}

struct booking_history_class : Codable {
    let status : Bool?
    let message : String?
    let data : bookinghistoryData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(bookinghistoryData.self, forKey: .data)
    }

}

struct List2 : Codable {
    let gift_booking_id : String?
    let booking_id : String?
    let user_id : String?
    let total_price : String?
    let created_at_date : String?
    let status : String?
    let booking_unique_id : String?
    let invoice : String?
    let ref : String?
    let details : [Details_bookinghistory]?

    enum CodingKeys: String, CodingKey {

        case gift_booking_id = "gift_booking_id"
        case booking_id = "booking_id"
        case user_id = "user_id"
        case total_price = "total_price"
        case created_at_date = "created_at_date"
        case status = "status"
        case booking_unique_id = "booking_unique_id"
        case invoice = "invoice"
        case ref = "ref"
        case details = "details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_booking_id = try values.decodeIfPresent(String.self, forKey: .gift_booking_id)
        booking_id = try values.decodeIfPresent(String.self, forKey: .booking_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        booking_unique_id = try values.decodeIfPresent(String.self, forKey: .booking_unique_id)
        invoice = try values.decodeIfPresent(String.self, forKey: .invoice)
        ref = try values.decodeIfPresent(String.self, forKey: .ref)
        details = try values.decodeIfPresent([Details_bookinghistory].self, forKey: .details)
    }

    init() {
        gift_booking_id=nil
        booking_id=nil
        user_id=nil
        total_price=nil
        created_at_date=nil
        status=nil
        booking_unique_id=nil
        invoice=nil
        ref=nil
        details=nil
    }
}
