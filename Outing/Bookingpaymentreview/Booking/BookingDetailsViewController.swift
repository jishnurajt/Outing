//
//  BookingDetailsViewController.swift
//  Outing
//
//  Created by Jishnu Raj T on 08/06/21.
//  Copyright © 2021 Arun Vijayan. All rights reserved.
//

import UIKit

class BookingDetailsViewController: UIViewController, UITableViewDataSource ,UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    var list : List1!
    var proImage:URL!
    var giftUrl:String = ""
    var carUrl:String = ""
    var yatchUrl = ""
    var heliUrl = ""
    
    let numberFormatter = NumberFormatter()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            proImage = URL(string:image as! String)
        }else{
            proImage = URL(string:"https://www.outing.qa/user_image/user.png" )
        }
        numberFormatter.numberStyle = .decimal
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnsidemenuaction() {
       let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
         navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
//       if cart?.count ?? 0>0{
//       vc?.cart=cart
//       vc?.imageurlgift=imageurlgift
//       }
        self.present(navController, animated:false, completion: nil)
   }
    func payNow(){
           let addoncart = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "AddonPaymentViewController") as! AddonPaymentViewController
        addoncart.typePayemnt = 2
        addoncart.bookingid = list.booking_id ?? ""
        present(addoncart, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (list?.details?.count ?? 0) + 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier:"BookingDHeader") as! BookingDHeader
        cell.proImage.kf.setImage(with: self.proImage)
        cell.sidemenuAction = {
            self.btnsidemenuaction()
        }
        return cell
        
    }else if indexPath.row == 1{
        let cell = tableView.dequeueReusableCell(withIdentifier:"BookingTVC2") as! BookingTVC2
        var price = 0
        
         let   dateS = (list.created_at_date ?? "")
            cell.lbid.text = "Booking ID: "+(list.booking_unique_id ?? "")
            price  = Int(list.total_price ?? "") ?? 0
        
        let formattedNumber = numberFormatter.string(from: NSNumber(value: price)) ?? ""
        cell.lbprice.text = "QAR " + formattedNumber
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE MMM dd,yyyy"

        if let date = dateFormatterGet.date(from: dateS) {
            cell.lbdate.text = "Booking Date: "+dateFormatterPrint.string(from: date)
        } else {
           print("There was an error decoding the string")
        }

        return cell
    }else if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
        //last inde
        let cell = tableView.dequeueReusableCell(withIdentifier:"BookingTVC5") as! BookingTVC5
        if list.is_finish == "0"{
            cell.btn.isHidden = false
            cell.payBtn.isHidden = true
        }else if list.is_finish == "1"{
            cell.btn.isHidden = true
            cell.payBtn.isHidden = false
            cell.payNow = {
                self.payNow()
            }
        }else  if list.is_finish == "2"{
            cell.btn.isHidden = false
            cell.payBtn.isHidden = true
            cell.btn.setTitle("Sorry!, Admin cancelled your booking", for: .normal)
        }
        return cell
    }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTVC3") as! BookingTVC3
        var price = 0
        var pickTime = ""
        var pickDate = ""
        var dropTime = ""
        var dropDate = ""
        let details = list.details?[indexPath.row-2]
            cell.name.text = details?.vehicle_details?.vehicle_name
            pickDate = "\(details?.pick_up_date ?? "")"
            dropDate = "\(details?.drop_date ?? "")"
            pickTime = "\(details?.pick_up_time ?? "")"
            dropTime = "\(details?.drop_time ?? "")"
            price = Int(details?.base_price ?? "") ?? 0
            var url : URL!
        if  details?.flag == "1"{
            url = URL(string:carUrl+(details?.vehicle_details?.vehicle_image ?? ""))
            price = price * Int(details?.total_days ?? "1")!
        }else if details?.flag == "2"{
            url = URL(string:heliUrl+(details?.vehicle_details?.vehicle_image ?? ""))
            dropTime = ""
            dropDate = ""
            price = (price/(Int(details?.vehicle_details?.seat ?? "4")!)) * Int(details?.people ?? "1")!
        }else if details?.flag == "3"{
            price = price * Int(details?.total_hour ?? "1")!
            url = URL(string:yatchUrl+(details?.vehicle_details?.vehicle_image ?? ""))
            cell.name.setHTMLFromString(text: details?.vehicle_details?.vehicle_name ?? "")
        }
         
             
            cell.imageV.kf.indicatorType = .activity
            cell.imageV.kf.setImage(with: url)
        
       let  formattedNumber = numberFormatter.string(from: NSNumber(value: price)) ?? ""
        cell.price.text = "QAR " + formattedNumber
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE MMM dd,yyyy"
        cell.date.text = ""
        if let date = dateFormatterGet.date(from: pickDate) {
            cell.date.text = dateFormatterPrint.string(from: date)
        }
        if let date = dateFormatterGet.date(from: dropDate) {
            cell.date.text! += " "+dateFormatterPrint.string(from: date)
        }
      
        
       
        dateFormatterGet.dateFormat = "HH:mm:ss"
        dateFormatterPrint.dateFormat = "h:mm a"
        cell.time.text = ""
        if let date = dateFormatterGet.date(from: pickTime) {
            cell.time.text = "Time: "+dateFormatterPrint.string(from: date)
        }
        if let date = dateFormatterGet.date(from: dropTime) {
            cell.time.text! += " "+dateFormatterPrint.string(from: date)
        }
        
        return cell
    }
    }

}


class BookingTVC5:UITableViewCell{//55
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var payBtn: UIButton!
    
    @IBAction func payAction(_ sender: Any) {
        payNow?()
    }
    var payNow:(()->())?
}

class BookingDHeader:UITableViewCell{
    @IBOutlet weak var proImage: UIImageView!
    @IBAction func sideMenuAction(_ sender: Any) {
        sidemenuAction?()
    }
    var sidemenuAction:(()->())?
}
