//
//  BookingViewController.swift
//  Outing
//
//  Created by Jishnu Raj T on 07/06/21.
//  Copyright © 2021 Arun Vijayan. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {
    @IBOutlet weak var expandableTableView: ExpyTableView!
    
    var paymentmodel=Paymentviemodel()
    var list = [List1()]
    var list2=[List2()]
    
    var selectedSegment = 0
    var proImage:URL!
    var giftUrl:String = ""
    var carUrl:String = ""
    var yatchUrl = ""
    var heliUrl = ""
    
    let numberFormatter = NumberFormatter()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        numberFormatter.numberStyle = .decimal
        // Do any additional setup after loading the view.
        expandableTableView.dataSource = self
        expandableTableView.delegate = self
    
        expandableTableView.rowHeight = UITableView.automaticDimension
        expandableTableView.estimatedRowHeight = 44
    
        //Alter the animations as you want
        expandableTableView.expandingAnimation = .fade
        expandableTableView.collapsingAnimation = .fade
        
        
        self.paymentmodel.booking_history{ (model) in
             self.booking_historydata(data:model)
         }
        self.paymentmodel.booking_history_product{ (model) in
            self.booking_history_productdata(data:model)
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], for: .selected)
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            proImage = URL(string:image as! String)
        }else{
            proImage = URL(string:"https://www.outing.qa/user_image/user.png" )
        }
    }
    func booking_historydata(data: Bookinghistoryclass) {
           
           let status=data.status
           if status==true{
               DispatchQueue.main.async{
                self.carUrl = data.data?.car_image_url ?? ""
                self.heliUrl = data.data?.helicopter_image_url ?? ""
                self.yatchUrl = data.data?.yacht_image_url ?? ""
                self.list=data.data?.list ?? []
                self.expandableTableView.reloadData()
               }
           }else{
               DispatchQueue.main.async{
                   self.showToast(message: "No bookings available", font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.expandableTableView.reloadData()
                   
               }
           }
       }
    func booking_history_productdata(data: booking_history_class) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
             self.list2=data.data?.list ?? []
                self.giftUrl = data.data?.gift_image_url ?? ""
             self.expandableTableView.reloadData()
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message:  "No bookings available", font:  .boldSystemFont(ofSize: 13), duration: 2)

                self.expandableTableView.reloadData()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnsidemenuaction() {
       let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
         navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
//       if cart?.count ?? 0>0{
//       vc?.cart=cart
//       vc?.imageurlgift=imageurlgift
//       }
        self.present(navController, animated:false, completion: nil)
   }

}


//MARK: ExpyTableViewDataSourceMethods
extension BookingViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"BookingTVC2") as! BookingTVC2
        var price = 0
        var dateS = ""
        if  selectedSegment == 0 {
            dateS = (list[section].created_at_date ?? "")
            cell.lbid.text = "Booking ID: "+(list[section].booking_unique_id ?? "")
            price  = Int(list[section].total_price ?? "") ?? 0
        }else{
            dateS = (list2[section].created_at_date ?? "")
            cell.lbid.text = "Booking ID: "+(list2[section].booking_unique_id ?? "")
            price  = Int(list2[section].total_price ?? "") ?? 0
        }
        let formattedNumber = numberFormatter.string(from: NSNumber(value: price)) ?? ""
        cell.lbprice.text = "QAR " + formattedNumber
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE MMM dd,yyyy"

        if let date = dateFormatterGet.date(from: dateS) {
            cell.lbdate.text = "Booking Date: "+dateFormatterPrint.string(from: date)
        } else {
           print("There was an error decoding the string")
        }

        return cell
    }
}
//MARK: ExpyTableView delegate methods
extension BookingViewController: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
}


extension BookingViewController {
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return (section % 3 == 0) ? "iPhone Models" : nil
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"BookingTVC1") as! BookingTVC1
            cell.proImage.kf.setImage(with: self.proImage)
            cell.segmentCtrl.selectedSegmentIndex = selectedSegment
            cell.backButtonAction = {
                self.navigationController?.popViewController(animated: true)
            }
            cell.sidemenuAction = {
                self.btnsidemenuaction()
            }
            cell.segmantChange = { [weak self] ind in
                self?.selectedSegment = ind
                self?.expandableTableView.reloadData()
            }
            return cell
        }else{
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
           return 275
        }else{
            return CGFloat.leastNormalMagnitude
        }
    }
}
extension BookingViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //If you don't deselect the row here, seperator of the above cell of the selected cell disappears.
        //Check here for detail: https://stackoverflow.com/questions/18924589/uitableviewcell-separator-disappearing-in-ios7
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        //This solution obviously has side effects, you can implement your own solution from the given link.
        //This is not a bug of ExpyTableView hence, I think, you should solve it with the proper way for your implementation.
        //If you have a generic solution for this, please submit a pull request or open an issue.
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
        
        if selectedSegment == 0,indexPath.row != 0{
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
             let vc =   storyboard.instantiateViewController(withIdentifier: "BookingDetailsViewController") as! BookingDetailsViewController
            vc.list = list[indexPath.section]
                vc.proImage = proImage
                vc.giftUrl = giftUrl
                vc.carUrl = carUrl
                vc.yatchUrl = yatchUrl
                vc.heliUrl = heliUrl
           navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//MARK: UITableView Data Source Methods
extension BookingViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
         let count = selectedSegment == 0 ? list.count : list2.count
        return count == 0 ? 1:count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Please see https://github.com/okhanokbay/ExpyTableView/issues/12
        // The cell instance that you return from expandableCellForSection: data source method is actually the first row of belonged section. Thus, when you return 4 from numberOfRowsInSection data source method, first row refers to expandable cell and the other 3 rows refer to other rows in this section.
        // So, always return the total row count you want to see in that section
        
       // print("Row count for section \(section) is \(sampleData[section].count)")
        // +1 here is for BuyTableViewCell
        
         guard let count = selectedSegment == 0 ? list[section].details?.count : list2[section].details?.count else { return 0 }
        return count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier:"BookingTVC4") as! BookingTVC4
            cell.btn.isHidden = selectedSegment == 1
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTVC3") as! BookingTVC3
            var price = 0
            var pickTime = ""
            var pickDate = ""
            var dropTime = ""
            var dropDate = ""
            if  selectedSegment == 0 {
                let details = list[indexPath.section].details?[indexPath.row-1]
                cell.name.text = details?.vehicle_details?.vehicle_name
                pickDate = "\(details?.pick_up_date ?? "")"
                dropDate = "\(details?.drop_date ?? "")"
                pickTime = "\(details?.pick_up_time ?? "")"
                dropTime = "\(details?.drop_time ?? "")"
                price = Int(details?.base_price ?? "") ?? 0
                var url : URL!
                if  details?.flag == "1"{
                    url = URL(string:carUrl+(details?.vehicle_details?.vehicle_image ?? ""))
                    price = price * Int(details?.total_days ?? "1")!
                }else if details?.flag == "2"{
                    url = URL(string:heliUrl+(details?.vehicle_details?.vehicle_image ?? ""))
                    dropTime = ""
                    dropDate = ""
                    price = (price/(Int(details?.vehicle_details?.seat ?? "4")!)) * Int(details?.people ?? "1")!
                }else if details?.flag == "3"{
                    price = price * Int(details?.total_hour ?? "1")!
                    url = URL(string:yatchUrl+(details?.vehicle_details?.vehicle_image ?? ""))
                    cell.name.setHTMLFromString(text: details?.vehicle_details?.vehicle_name ?? "")
                }
                 
                cell.imageV.kf.indicatorType = .activity
                cell.imageV.kf.setImage(with: url)
            }else{
                let details = list2[indexPath.section].details?[indexPath.row-1]
                cell.name.text = details?.gift_details?.title
                cell.time.text = nil
                cell.date.text = "Quantity: \(details?.qty ?? "")"
                price = Int(details?.price ?? "") ?? 0
                let url = URL(string: giftUrl+( details?.gift_details?.gift_image ?? ""))
                cell.imageV.kf.indicatorType = .activity
                cell.imageV.kf.setImage(with: url)
            }
            let formattedNumber = numberFormatter.string(from: NSNumber(value: price)) ?? ""
            cell.price.text = "QAR " + formattedNumber
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEE MMM dd,yyyy"
            cell.date.text = ""
            if let date = dateFormatterGet.date(from: pickDate) {
                cell.date.text = dateFormatterPrint.string(from: date)
            }
            if let date = dateFormatterGet.date(from: dropDate) {
                cell.date.text! += " "+dateFormatterPrint.string(from: date)
            }
          
            
           
            dateFormatterGet.dateFormat = "HH:mm:ss"
            dateFormatterPrint.dateFormat = "h:mm a"
            cell.time.text = ""
            if let date = dateFormatterGet.date(from: pickTime) {
                cell.time.text = "Time: "+dateFormatterPrint.string(from: date)
            }
            if let date = dateFormatterGet.date(from: dropTime) {
                cell.time.text! += " "+dateFormatterPrint.string(from: date)
            }
            
            return cell
        }
    }
}











class BookingTVC1:UITableViewCell{ //275
    @IBOutlet weak var proImage: UIImageView!
    
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    @IBAction func sideMenuAction(_ sender: Any) {
        sidemenuAction?()
    }
    @IBAction func backActions(_ sender: Any) {
        backButtonAction?()
    }
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        segmantChange?(sender.selectedSegmentIndex)
    }
    
    var segmantChange:((Int)->())?
    var backButtonAction:(()->())?
    var sidemenuAction:(()->())?
}

class BookingTVC2:UITableViewCell,ExpyTableViewHeaderCell{
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
    }
    //70
    @IBOutlet weak var lbid: UILabel!
    @IBOutlet weak var lbdate: UILabel!
    @IBOutlet weak var lbprice: UILabel!
    
}

class BookingTVC3:UITableViewCell{//145
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var imageV: UIImageView!
}

class BookingTVC4:UITableViewCell{//55
    @IBOutlet weak var btn: UIButton!
    
    @IBAction func viewStatusAction(_ sender: Any) {
    }
}
