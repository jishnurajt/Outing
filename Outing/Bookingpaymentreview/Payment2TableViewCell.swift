//
//  Payment2TableViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class Payment2TableViewCell: UITableViewCell {

    @IBOutlet weak var btnwritereview: UIButton!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblid: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnwritereview.layer.cornerRadius=10
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
