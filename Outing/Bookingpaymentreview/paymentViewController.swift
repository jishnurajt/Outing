//
//  paymentViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 02/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import Cosmos



class paymentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var paymentmodel=Paymentviemodel()
    var list:[List1]?=[]
    var list2:[List2]?=[]
    var paymentflag=Int()
    
    
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var cosmicview: CosmosView!
    @IBOutlet weak var txtfldreview: UITextField!
    @IBOutlet weak var lblbookingid: UILabel!
    @IBOutlet weak var btnsubmitt: UIButton!
    @IBOutlet weak var viewreview: UIView!
    @IBOutlet weak var tableviewpayment: UITableView!
    var bookingid=String()
    var bookingidstring=String()
    var loginmodel=Loginviewmodel()
    var cell=Payment1stTableViewCell()
    var segmentflag=1
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    var selectedindex=Int()
    var selectedvehicledetails:Vehicle_details1?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewreview.layer.cornerRadius=10
        btnsubmitt.layer.cornerRadius=20
        self.paymentmodel.booking_history{ (model) in
            self.booking_historydata(data:model)
        }
        
        
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            
            
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], for: .selected)
        
    }
    
    @objc func segmentaction(){
        if cell.segmentcontrol.selectedSegmentIndex==0{
            segmentflag=1
            tableviewpayment.reloadData()
            self.paymentmodel.booking_history{ (model) in
                self.booking_historydata(data:model)
            }
            
        }else{
            segmentflag=2
            tableviewpayment.reloadData()
            self.paymentmodel.booking_history_product{ (model) in
                self.booking_history_productdata(data:model)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section==0{
            return 1
        }else{
            if  segmentflag==1{
                return list?.count ?? 0
            }else{
                return list2?.count ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section==0{
            cell = (tableView.dequeueReusableCell(withIdentifier: "Payment1stTableViewCell", for: indexPath) as? Payment1stTableViewCell)!
            if list?.count ?? 0>0||list2?.count ?? 0>0{
                cell.segmentcontrol.isHidden=false
            }else{
                cell.segmentcontrol.isHidden=true
            }
            if paymentflag==1{
                cell.lbltype.text="Bookings"
            }else if paymentflag==2{
                cell.lbltype.text="Payments"
            }else{
                cell.lbltype.text="Reviews"
            }
            cell.btnback.addTarget(self, action: #selector(btnbackaction), for: .touchUpInside)
            cell.segmentcontrol.addTarget(self, action: #selector(segmentaction), for: .valueChanged)
            
            return cell
            
        }else{
            let cell1 = (tableView.dequeueReusableCell(withIdentifier: "Payment2TableViewCell", for: indexPath) as? Payment2TableViewCell)!
            if  segmentflag==1{
                if paymentflag==2{
                    cell1.lblid.text="Booking Date : "+String(list?[indexPath.row].created_at_date ?? "")
                    cell1.lbldate.text="Payment Type : "+String(list?[indexPath.row].payment_type ?? "")
                    
                }else{
                    cell1.lblid.text="Booking ID : "+String(list?[indexPath.row].booking_unique_id ?? "")
                    cell1.lbldate.text="Booking Date : "+String(list?[indexPath.row].created_at_date ?? "")
                }
            }else{
                if paymentflag==2{
                    cell1.lblid.text="Booking Date : "+String(list2?[indexPath.row].created_at_date ?? "")
                    cell1.lbldate.text="Invoice : "+String(list2?[indexPath.row].invoice ?? "")
                }else{
                    cell1.lblid.text="Booking ID : "+String(list2?[indexPath.row].booking_unique_id ?? "")
                    cell1.lbldate.text="Booking Date : "+String(list2?[indexPath.row].created_at_date ?? "")
                }
            }
            if paymentflag==3{
                cell1.btnwritereview.isHidden=false
                cell1.lblprice.isHidden=true
                cell1.btnwritereview.tag=indexPath.row
                cell1.btnwritereview.addTarget(self, action: #selector(btnwritereviewaction(sender:)), for: .touchUpInside)
            }else{
                if  segmentflag==1{
                    
                    cell1.lblprice.text="QAR "+String(list?[indexPath.row].total_price ?? "")
                }else{
                    cell1.lblprice.text="QAR "+String(list2?[indexPath.row].total_price ?? "")
                }
                cell1.btnwritereview.isHidden=true
                cell1.lblprice.isHidden=false
            }
            return cell1
        }
    }
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedindex=indexPath.row
        selectedvehicledetails = self.list?[selectedindex].details?[0].vehicle_details
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0{
            return 370
        }else{
            return 100
        }
    }
    
    @objc func btnbackaction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnwritereviewaction(sender:UIButton){
        viewreview.isHidden=false
        bookingid=list?[sender.tag].booking_id ?? ""
        bookingidstring=list?[sender.tag].booking_unique_id ?? ""
        lblbookingid.text="Booking ID :"+bookingidstring
        
    }
    func booking_historydata(data: Bookinghistoryclass) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.list=data.data?.list
                self.tableviewpayment.reloadData()
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: "No bookings available", font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.tableviewpayment.reloadData()
                
            }
        }
    }
    
    
    @IBAction func btnsubmitaction(_ sender: Any) {
        if txtfldreview.text==""{
            self.showToast(message: "Please enter message", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }else if cosmicview.rating==0.0{
            self.showToast(message: "Please give a rate before submitt", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }else{
            loginmodel.bookingidforreview=bookingid
            loginmodel.messagereview=txtfldreview.text ?? ""
            
            loginmodel.add_review{ (model) in
                self.add_reviewdata(data:model)
            }
            
        }
    }
    func add_reviewdata(data: Writereviewclass) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.showToast(message: data.message ?? "", font:  .boldSystemFont(ofSize: 13), duration: 2)
                //self.tableviewpayment.reloadData()
                self.viewreview.isHidden=true
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message ?? "", font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    func booking_history_productdata(data: booking_history_class) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message ?? "", font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.list2=data.data?.list
                self.tableviewpayment.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                if self.paymentflag==1{
                    self.showToast(message:  "No bookings available", font:  .boldSystemFont(ofSize: 13), duration: 2)
                }else if self.paymentflag==2{
                    self.showToast(message:  "No Payments available", font:  .boldSystemFont(ofSize: 13), duration: 2)
                }else{
                    
                }
                self.tableviewpayment.reloadData()
            }
        }
    }
    
}
