//
//  Carbookmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
//addon_category

struct Addonclass : Codable {
    let status : Bool?
    let message : String?
    let data : AddonData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddonData.self, forKey: .data)
    }

}
struct AddonData : Codable {
    let addon_category : [Addon_category]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case addon_category = "addon_category"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_category = try values.decodeIfPresent([Addon_category].self, forKey: .addon_category)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}

struct Addon_category : Codable {
    let addon_cat_id : String?
    let addon_cat_name : String?
    let addon_cat_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case addon_cat_id = "addon_cat_id"
        case addon_cat_name = "addon_cat_name"
        case addon_cat_image = "addon_cat_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_cat_name = try values.decodeIfPresent(String.self, forKey: .addon_cat_name)
        addon_cat_image = try values.decodeIfPresent(String.self, forKey: .addon_cat_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
//addondrand

struct Addonbrandclass : Codable {
    let status : Bool?
    let message : String?
    let data : AddonbrandData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddonbrandData.self, forKey: .data)
    }

}
struct AddonbrandData : Codable {
    let addon_brands : [Addon_brands]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case addon_brands = "addon_brands"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_brands = try values.decodeIfPresent([Addon_brands].self, forKey: .addon_brands)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}
struct Addon_brands : Codable {
    let addon_brand_id : String?
    let addon_brand_name : String?
    let addon_cat_id : String?
    let addon_brand_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case addon_brand_id = "addon_brand_id"
        case addon_brand_name = "addon_brand_name"
        case addon_cat_id = "addon_cat_id"
        case addon_brand_image = "addon_brand_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
        addon_brand_name = try values.decodeIfPresent(String.self, forKey: .addon_brand_name)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_brand_image = try values.decodeIfPresent(String.self, forKey: .addon_brand_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

//datagift
struct Giftclass : Codable {
    let status : Bool?
    let message : String?
    let data : GiftData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(GiftData.self, forKey: .data)
    }

}
struct Gift : Codable {
    let gift_id : String?
    let provider_id : String?
    let addon_cat_id : String?
    let addon_brand_id : String?
    let title : String?
    let description : String?
    let base_price : String?
    let latitude : String?
    let longitude : String?
    let created_at_date : String?
    let status : String?
    let gift_images : [Gift_images]?

    enum CodingKeys: String, CodingKey {

        case gift_id = "gift_id"
        case provider_id = "provider_id"
        case addon_cat_id = "addon_cat_id"
        case addon_brand_id = "addon_brand_id"
        case title = "title"
        case description = "description"
        case base_price = "base_price"
        case latitude = "latitude"
        case longitude = "longitude"
        case created_at_date = "created_at_date"
        case status = "status"
        case gift_images = "gift_images"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        provider_id = try values.decodeIfPresent(String.self, forKey: .provider_id)
        addon_cat_id = try values.decodeIfPresent(String.self, forKey: .addon_cat_id)
        addon_brand_id = try values.decodeIfPresent(String.self, forKey: .addon_brand_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        base_price = try values.decodeIfPresent(String.self, forKey: .base_price)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        gift_images = try values.decodeIfPresent([Gift_images].self, forKey: .gift_images)
    }

}
struct Gift_images : Codable {
    let gift_image_id : String?
    let gift_id : String?
    let gift_image : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case gift_image_id = "gift_image_id"
        case gift_id = "gift_id"
        case gift_image = "gift_image"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift_image_id = try values.decodeIfPresent(String.self, forKey: .gift_image_id)
        gift_id = try values.decodeIfPresent(String.self, forKey: .gift_id)
        gift_image = try values.decodeIfPresent(String.self, forKey: .gift_image)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
struct GiftData : Codable {
    let gift : [Gift]?
    let image_url : String?

    enum CodingKeys: String, CodingKey {

        case gift = "gift"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gift = try values.decodeIfPresent([Gift].self, forKey: .gift)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}
//car_check



struct carcheckData : Codable {
    let days : Int?

    enum CodingKeys: String, CodingKey {

        case days = "days"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        days = try values.decodeIfPresent(Int.self, forKey: .days)
    }

}

struct Carcheckclass : Codable {
    let status : Bool?
    let message : String?
    let data : carcheckData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(carcheckData.self, forKey: .data)
    }

}
