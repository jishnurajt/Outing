//
//  AddonsCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class AddonsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewaddons: UIView!
    @IBOutlet weak var imageviewaddons: UIImageView!
    
    @IBOutlet weak var lbladdons: UILabel!
    override func awakeFromNib() {
       super.awakeFromNib()
        viewaddons.layer.cornerRadius=10
           
       }
       
    
    
}
