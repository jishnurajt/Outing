//
//  MapViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 22/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

protocol mapdelegate{
    func mapdatas(lat:CLLocationDegrees,long:CLLocationDegrees,mapaddress1:String,mapadress2:String,changelocationflag:Int)
    
    
}




class MapViewController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var lblreturnlocation: UILabel!
    @IBOutlet weak var lblreceivelocation: UILabel!
    @IBOutlet weak var viewcurrentlocation: UIView!
    @IBOutlet weak var btncontinue: UIButton!
    @IBOutlet weak var mapview: GMSMapView!
    var marker1=GMSMarker()
    var marker2=GMSMarker()
    
    @IBOutlet weak var btnreturnlocation: UIButton!
    @IBOutlet weak var btnrecivelocation: UIButton!
    let locationManager = CLLocationManager()
    var latitude=CLLocationDegrees()
    var longitude=CLLocationDegrees()
    var latitudereturn=CLLocationDegrees()
    var longitudereturn=CLLocationDegrees()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var lat=CLLocationDegrees()
    var long=CLLocationDegrees()
    var userlocation=CLLocation()
    var placeaddress=String()
    var currentAddress=String()
    var delegate:mapdelegate!
    var changeloactionflag=Int()
    var reciveflag=0
    var mapaddress1=String()
    var mapaddress2=String()
    var position=CLLocationCoordinate2D()
    var positionreturn=CLLocationCoordinate2D()
    var bounds = GMSCoordinateBounds()
   var markerDict: [String: GMSMarker] = [:]
    var custlatt:[CLLocationDegrees]=[]
    var custlong:[CLLocationDegrees]=[]
    
    
    
    
    
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        print("coordinate",coordinate)
        self.latitude=coordinate.latitude
        self.longitude=coordinate.longitude
        
        print("latitude",latitude)
        print("longitude",longitude)
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            // 3
            print("......",lines.joined(separator: "\n"))
            self.currentAddress=lines.joined(separator: "\n")
            DispatchQueue.main.async{
                if self.reciveflag==0{
                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                    if self.changeloactionflag==2{
                        self.lblreceivelocation.text="Outing, Burj Alfardan, lusail Marina, Doha"
                        
                        self.marker1.position=self.position
                        self.showMarker(position: self.position)
                    }else{
                        self.lblreceivelocation.text=self.currentAddress
                    }
                    
                    self.lblreturnlocation.text="Search your location here"
                    self.mapaddress1=self.currentAddress
                    self.mapaddress2=self.currentAddress
                    
                    
                   
                    self.custlatt.append(self.latitude)
                    self.custlong.append(self.longitude)
                    print("custlatt",self.custlatt)
                    print("custlong",self.custlong)
                   
                }else if self.reciveflag==1{
                    self.latitude=coordinate.latitude
                    self.longitude=coordinate.longitude
                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                    // self.showMarker(position: self.position)
                }else{
                    self.latitude=coordinate.latitude
                    self.longitude=coordinate.longitude
                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude:  self.longitude)
                    
                    
                    //                    if self.changeloactionflag==2{
                    //                                            self.lblreceivelocation.text="Outing"
                    //                                            self.lblreturnlocation.text=self.currentAddress
                    //                                            self.mapaddress1=self.currentAddress
                    //                                        }else{
                    //                                            self.lblreturnlocation.text=self.currentAddress
                    //                                            self.mapaddress2=self.currentAddress
                    //                                        }
                    //
                }
                
                
                //                }else if self.reciveflag==1{
                //
                //                    self.lblreceivelocation.text=self.currentAddress
                //                    self.mapaddress1=self.currentAddress
                ////                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                ////                    self.marker1.position=self.position
                //
                //
                //                }else {
                //                    if self.changeloactionflag==2{
                //                        self.lblreceivelocation.text="Outing"
                //                        self.lblreturnlocation.text=self.currentAddress
                //                        self.mapaddress1=self.currentAddress
                //                    }else{
                //                        self.lblreturnlocation.text=self.currentAddress
                //                        self.mapaddress2=self.currentAddress
                //                    }
                ////                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                ////                    self.marker2.position=self.position
                ////
                //                }
            }
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
        //          position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        //            marker1.position=position
        //               showMarker(position: position)
        
    }
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapview.mapType = .normal
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                self.mapview.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        btncontinue.layer.cornerRadius=20
        if changeloactionflag==2{
            btnrecivelocation.isUserInteractionEnabled=false
        }else{
            btnrecivelocation.isUserInteractionEnabled=true
        }
        locationupdate()
        viewcurrentlocation.layer.cornerRadius=10
        
        
       
    }
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btngpsaction(_ sender: Any) {
        
        //showMarker(position: position)
        searchController?.isActive = false
        viewcurrentlocation.isHidden=false
        searchController?.searchBar.isHidden=true
        
        guard let lat = mapview.myLocation?.coordinate.latitude,
            let lng = mapview.myLocation?.coordinate.longitude else { return  }

        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        mapview.animate(to: camera)

        return
    }
    
    func locationupdate(){
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapview.delegate=self
        mapview.isHidden=false
        mapview.mapType = GMSMapViewType(rawValue: 1)!
        //                mapview.camera=GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 9.582912, longitude: 76.547326), zoom: 15, bearing: 0, viewingAngle: 0)
        
        mapview.isMyLocationEnabled = true
        mapview.settings.zoomGestures   = true

    }
    
    
    func autosearchcompleted(){
        
       // DispatchQueue.main.async{
            if self.reciveflag==0{
                if self.changeloactionflag==2{
                    self.lblreceivelocation.text="Outing, Burj Alfardan, lusail Marina, Doha"
                }else{
                    self.lblreceivelocation.text=self.currentAddress
                }
                // self.lblreturnlocation.text=self.currentAddress
                self.lblreturnlocation.text="Search your location here"
                self.mapaddress1=self.currentAddress
                self.mapaddress2=self.currentAddress
                self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                self.marker1.position=self.position
                self.showMarker(position: self.position)
               
            }else if self.reciveflag==1{
                
                self.lblreceivelocation.text=self.currentAddress
                self.mapaddress1=self.currentAddress
                //                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                //                    self.marker1.position=self.position
               
                
            }else {
                if self.changeloactionflag==2{
                    self.lblreceivelocation.text="Outing, Burj Alfardan, lusail Marina, Doha"
                    self.lblreturnlocation.text=self.currentAddress
                    self.mapaddress1=self.currentAddress
                }else{
                    self.lblreturnlocation.text=self.currentAddress
                    self.mapaddress2=self.currentAddress
                }
                //                    self.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                //                    self.marker2.position=self.position
                //
                self.custlatt.append(self.latitude)
                self.custlong.append(self.longitude)
            }
      //  }
        
        
        
        
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        var target1 = CLLocationCoordinate2D()
        var target2 = CLLocationCoordinate2D()
        if reciveflag==0{
           
            showMarker(position: position)
                //         target1=CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                //         mapview.camera = GMSCameraPosition.camera(withTarget: target1, zoom: 17)
        }else if reciveflag==1{
           showMarker(position: position)
          
            //             marker1.position=position
            //            showMarker(position: marker1.position)
                 //        target1=CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                 //        mapview.camera = GMSCameraPosition.camera(withTarget: target1, zoom: 17)
            
        }else{
           
            showMarker2(position: position)
            //             marker2.position=position
            //            showMarker2(position: marker2.position)
            //            showMarker(position: marker1.position)
                     //   target2=CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                      //  mapview.camera = GMSCameraPosition.camera(withTarget: target2, zoom: 17)
            
        }
        viewcurrentlocation.isHidden=false
        searchController?.searchBar.isHidden=true
        if reciveflag==1{
            lblreceivelocation.text=currentAddress
            
        }else{
            lblreturnlocation.text=currentAddress
        }
        print("custlatt",custlatt)
//       for (i, element) in (["first","second"]).enumerated(){
//        if i < custlatt.count{
//                      let camera = GMSCameraPosition.camera(withLatitude:custlatt[i], longitude: custlong[i], zoom: 10.0)
//                      mapview.camera = camera
//                      markerDict[element] = GMSMarker()
//                      markerDict[element]?.position = CLLocationCoordinate2D(latitude:custlatt[i], longitude: custlong[i])
////                      markerDict[element]?.title = element
//                     // markerDict[element]?.snippet = element
//                      markerDict[element]?.map = mapview
//              }
//       }
    }
    
    
    @IBAction func btncontinueaction(_ sender: Any) {
        self.delegate.mapdatas(lat:latitude,long:longitude,mapaddress1:mapaddress1,mapadress2:mapaddress2,changelocationflag: changeloactionflag)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    @IBAction func btnrecivelocationaction(_ sender: Any) {
        
        reciveflag=1
        viewcurrentlocation.isHidden=true
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.countries = ["QA"]  //appropriate country code
        resultsViewController?.autocompleteFilter = filter
        if #available(iOS 13.0, *) {
            resultsViewController?.overrideUserInterfaceStyle = .light
        }
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.delegate = self
        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 350.0, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
      
        definesPresentationContext = true
    }
    
    
    @IBAction func btnreturnlocationaction(_ sender: Any) {
        reciveflag=2
        viewcurrentlocation.isHidden=true
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.countries = ["QA"]  //appropriate country code
        resultsViewController?.autocompleteFilter = filter
        if #available(iOS 13.0, *) {
            resultsViewController?.overrideUserInterfaceStyle = .light
        }
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.delegate = self
        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 350.0, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
    }
    
    
    
}








extension MapViewController: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapview.isMyLocationEnabled = true
   //     mapview.settings.myLocationButton = true
        
        
        
        
        
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        userlocation = locations.last!
        let target = CLLocationCoordinate2D(latitude: userlocation.coordinate.latitude, longitude: userlocation.coordinate.longitude)
        mapview.camera = GMSCameraPosition.camera(withTarget: target, zoom: 17)
        showMarker(position: mapview.camera.target)
        position = CLLocationCoordinate2D(latitude: userlocation.coordinate.latitude, longitude: userlocation.coordinate.longitude)
        showMarker(position: position)
        // 8
        
        locationManager.stopUpdatingLocation()
        
        
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        
        //        let marker = GMSMarker()
        marker1.position = position
        marker1.title = "\(currentAddress)"
        print("title",marker1.title)
        // marker.snippet = "San Francisco"
        marker1.map = mapview
        marker1.icon = UIImage(named:"Group 269-marker")
        
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(marker1.position)
        if marker2.position.latitude != 0{
            bounds = bounds.includingCoordinate(marker2.position)
            
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapview.animate(with: update)
    }
    func showMarker2(position: CLLocationCoordinate2D){
        //let marker = GMSMarker()
        marker2.position = position
        marker2.title = "\(currentAddress)"
        print("title",marker2.title)
        // marker.snippet = "San Francisco"
        marker2.map = mapview
        marker2.icon = UIImage(named:"Group 269-marker")
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(marker2.position)
        if marker1.position.latitude != 0{
            bounds = bounds.includingCoordinate(marker1.position)
            
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapview.animate(with: update)
    }
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
        
    }
    
    
}
extension MapViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
       
        placeaddress=place.formattedAddress!
        print("placeaddress",placeaddress)
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        lat=place.coordinate.latitude
        long=place.coordinate.longitude
        print("lat",lat)
        print("long",long)
        latitude=lat
        longitude=long
        
        //dismiss(animated: true, completion: nil)
        currentAddress=placeaddress
        searchController?.searchBar.text=placeaddress
        autosearchcompleted()
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController?.isActive = false
        viewcurrentlocation.isHidden=false
        searchController?.searchBar.isHidden=true
    }
}






