//
//  Carbookviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

public class Carbookviewmodel{
    var addon_cat_id=String()
    var addonbrandid=String()
    var carid=String()
    var starttime=String()
    var endtime=String()
    var startdate=String()
    var enddate=String()
    
func addon_category(completion : @escaping (Addonclass) -> ())  {
    

    let poststring="security_token=out564bkgtsernsko"
    
    var request = NSMutableURLRequest(url: APIEndPoint.addon_category.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Addonclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    
func addon_brand(completion : @escaping (Addonbrandclass) -> ())  {
    

    let poststring="addon_cat_id=\(addon_cat_id)&security_token=out564bkgtsernsko"
    
    var request = NSMutableURLRequest(url: APIEndPoint.addon_brand.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Addonbrandclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    
    
func gift(completion : @escaping (Giftclass) -> ())  {
    

    let poststring="addon_brand_id=\(addonbrandid)&addon_cat_id=\(addon_cat_id)&security_token=out564bkgtsernsko"
    
    var request = NSMutableURLRequest(url: APIEndPoint.gift.url as URL)
    request.httpMethod = "POST"
    request.httpBody = poststring.data(using: String.Encoding.utf8)
    let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
        
        //self.showIndicator(isHidden: true)
        if error != nil{
            print("error",error)
            return
        }
        let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        print("respnsedate,\(responsestring)")
        do {
            
           
                let decoder = JSONDecoder()
                let model = try decoder.decode(Giftclass.self, from:
                    data!) //Decode JSON Response Data
                print(model)
            completion(model)
            
            
        } catch let error as NSError {
        }
    }
    task.resume()
}
    
    
    func check_car(completion : @escaping (Carcheckclass) -> ())  {
        

        let poststring="car_id=\(carid)&startTime=\(starttime)&endTime=\(endtime)&startDate=\(startdate)&endDate=\(enddate)"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.check_car.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Carcheckclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
}
