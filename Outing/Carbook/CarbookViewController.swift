//
//  CarbookViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
//import GoogleMaps
//import GooglePlaces
import CoreData
import RxSwift
//import MobileCoreServices
import CoreLocation



class CarbookViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,mapdelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btngooglamaplink: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnsidemenu: UIButton!
    @IBOutlet weak var viewdoc2: UIView!
    @IBOutlet weak var viewdoc1: UIView!
    @IBOutlet weak var btnchoosefiledrvinglicense: UIButton!
    @IBOutlet weak var btnchoosefileqtrid: UIButton!
    @IBOutlet weak var lbldrivingliscnsedocmnt: UILabel!
    @IBOutlet weak var lblqtriddocumrnt: UILabel!
    @IBOutlet weak var btnclosedocumentupload: UIButton!
    @IBOutlet weak var viewdocumentupload: UIView!
    @IBOutlet weak var viewheightrorscrolling: NSLayoutConstraint!
    @IBOutlet weak var lbladdonsheading: UILabel!
    @IBOutlet weak var viewbookingdetailheight: NSLayoutConstraint!
    
    @IBOutlet weak var imageviewpickup: UIImageView!
    @IBOutlet weak var txtfldpickupreturn: UITextField!
    
    @IBOutlet weak var viewpickup: UIView!
    
    @IBOutlet weak var returnLocationHeight: NSLayoutConstraint!
    @IBOutlet weak var returnLocationContainer: UIView!
    @IBOutlet weak var addonviewheight: NSLayoutConstraint!
    @IBOutlet weak var viewaddon: UIView!
    @IBOutlet weak var txtfldreturn: UITextField!
    @IBOutlet weak var lblbrands: UILabel!
    @IBOutlet weak var txtfldreceive: UITextField!
    @IBOutlet weak var lblpriceofgiftheight: NSLayoutConstraint!
    @IBOutlet weak var lblgiftselectedheight: NSLayoutConstraint!
    @IBOutlet weak var lbljwelleryselectedheight: NSLayoutConstraint!
    @IBOutlet weak var btnaddtocart: UIButton!
    @IBOutlet weak var lbltotalprice: UILabel!
    @IBOutlet weak var lblpriceofgift: UILabel!
    @IBOutlet weak var lblgiftselected: UILabel!
    @IBOutlet weak var lbljwelleryselected: UILabel!
    @IBOutlet weak var lblreturnlocationselected: UILabel!
    @IBOutlet weak var lblreceivelocationselected: UILabel!
    @IBOutlet weak var lblpriceofcar: UILabel!
    @IBOutlet weak var lblreturntimeselected: UILabel!
    @IBOutlet weak var lblreceivetimeselected: UILabel!
    @IBOutlet weak var lblnoofdaysbooked: UILabel!
    @IBOutlet weak var lblreturndateselected: UILabel!
    @IBOutlet weak var lblreceivedateselected: UILabel!
    @IBOutlet weak var viewaddons: UIView!
    //@IBOutlet weak var mapview:GMSMapView!
    @IBOutlet weak var lblreturntime: UILabel!
    @IBOutlet weak var viewreceivetime: UIView!
    @IBOutlet weak var viewdatereturn: UIView!
    @IBOutlet weak var lblreturndate: UILabel!
    @IBOutlet weak var viewreturntime: UIView!
    @IBOutlet weak var lbldatereceive: UILabel!
    @IBOutlet weak var imageviewcalendar: UIImageView!
    @IBOutlet weak var viewlocation: UIView!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var segmentcontrol: UISegmentedControl!
    @IBOutlet weak var imageviewcar: UIImageView!
    @IBOutlet weak var viewdatetimereceive: UIView!
    @IBOutlet weak var lblcarprice: UILabel!
    @IBOutlet weak var lblcarname: UILabel!
    
    @IBOutlet weak var collectionviewaddons: UICollectionView!
    
    @IBOutlet weak var collectionviewgift: UICollectionView!
    @IBOutlet weak var collectionviewbrands: UICollectionView!
    
    @IBOutlet weak var lblreachthereformap: UILabel!
    
    @IBOutlet weak var viewbookingdetails: UIView!
    @IBOutlet weak var btncontinue: UIButton!
    @IBOutlet weak var lblcar: UILabel!
    @IBOutlet weak var viewaddonheight: NSLayoutConstraint!
    
    //   @IBOutlet weak var btnpickupforyatch: UIButton!
    @IBOutlet weak var viewyatchreceivelocation: UIView!
    var timeflag=Int()
    var carbookmodel=Carbookviewmodel()
    var addoncategory:[Addon_category]?=[]
    var addonbrands:[Addon_brands]?=[]
    var gift:[Gift]?=[]
    var imageurl=String()
    var selectedindex = -1
    var selectedindexaddonbrand = -1
    var selectedgiftindex = -1
    var imageurladdonbrand=String()
    var imageurlgift1=String()
    var carname=String()
    var carpricefromdetail=String()
    var carprice=String()
    var disablefromcardetails:[Disable]?=[]
    var datearray=[String]()
    var receivereturnflag=Int()
    var datearrayactuallyselected=[String]()
    var timeselected=String()
    var carimage=String()
    
    let locationManager = CLLocationManager()
    var latitude=CLLocationDegrees()
    var longitude=CLLocationDegrees()
    var priceofgiftselected=""
    var fmt = DateFormatter()
    var nameofjwellery=""
    var nameofbrand=""
    var priceofcarafterdateselected=""
    var datereceinveselected=String()
    var daterereceivereturn=String()
    var cartcarnamearray=[String]()
    var cartdatearry=[String]()
    var cartcarprice=[String]()
    var cartcarimagearry=[String]()
    var carid=String()
    
    var yatchdisablefromcardetails:[Disable_y]?=[]
    var yatchflag=Int()
    var yatchid=String()
    var yatchmodel=Yatchviewmodel()
    var timeselectedreceive="12:00 PM"
    var timeselectedreturn="12:00 PM"
    var loginmodel=Loginviewmodel()
    var doclist:[Doc_listdocument]?=[]
    var cardate=String()
    var cartdetail:[Cartdetail]=[]
    var userid=String()
    var imageToUpload = UIImage()
    var model = ProfileViewModel()
    let bag = DisposeBag()
    var idarray=[String]()
    var typearray=[String]()
    var typedoc_array=[String]()
    var numberofdaysbooked=String()
    var packageid="0"
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    //    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
    //
    //        let geocoder = GMSGeocoder()
    //        print("coordinate",coordinate)
    //        latitude=coordinate.latitude
    //        longitude=coordinate.longitude
    //
    //        print("latitude",latitude)
    //        print("longitude",longitude)
    //        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
    //            guard let address = response?.firstResult(), let lines = address.lines else {
    //                return
    //            }
    //
    //            // 3
    //            print("......",lines.joined(separator: "\n"))
    //
    //
    //            UIView.animate(withDuration: 0.25) {
    //                self.view.layoutIfNeeded()
    //            }
    //        }
    //
    //    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // btnback.layer.cornerRadius=25
        
        btnsidemenu.layer.masksToBounds=true
        
        btnsidemenu.layer.cornerRadius=btnsidemenu.frame.height/2
        if let image=UserDefaults.standard.value(forKey: "userimage"){
            let url = URL(string:image as! String)
            btnsidemenu.kf.setImage(with: url, for: .normal)
        }else{
            btnsidemenu.kf.setImage(with: URL(string:"https://www.outing.qa/user_image/user.png" as! String), for: .normal)
        }
        
        
        
        if yatchflag==1{
            viewlocation.isHidden=true
            viewpickup.isHidden=true
            viewyatchreceivelocation.isHidden=true
            segmentcontrol.isHidden=true
            //      btnpickupforyatch.isHidden=false
            lblreceivelocationselected.text="AlAsmakh Street,Mushaireb,Doha"
            //lblreachthereformap.isHidden=false
            btngooglamaplink.isHidden=false
            returnLocationHeight.constant = 0
            returnLocationContainer.isHidden = true
        }else{
            
            viewlocation.isHidden=false
            viewpickup.isHidden=true
            viewyatchreceivelocation.isHidden=true
            segmentcontrol.isHidden=false
            // btnpickupforyatch.isHidden=true
            // lblreachthereformap.isHidden=true
            btngooglamaplink.isHidden=true
            txtfldpickupreturn.text = "Return"
            
        }
        
        viewdoc1.layer.cornerRadius=10
        viewdoc2.layer.cornerRadius=10
        
        //locationupdate()
        btnchoosefileqtrid.layer.cornerRadius=10
        btnchoosefiledrvinglicense.layer.cornerRadius=10
        
        viewyatchreceivelocation.layer.cornerRadius=10
        // mapview.isHidden=true
        lbladdonsheading.isHidden=true
        // btnpickupforyatch.layer.cornerRadius=20
        viewaddonheight.constant=0
        viewheightrorscrolling.constant=1300
        viewbookingdetailheight.constant=0
        imageviewpickup.layer.cornerRadius=10
        viewlocation.layer.cornerRadius=10
        viewpickup.layer.cornerRadius=10
        viewdatetimereceive.layer.cornerRadius=10
        viewreceivetime.layer.cornerRadius=10
        viewdatereturn.layer.cornerRadius=10
        viewreturntime.layer.cornerRadius=10
        btnaddtocart.layer.cornerRadius=20
        btncontinue.layer.cornerRadius=20
        viewbookingdetails.layer.cornerRadius=10
        
        
        lblcar.text="hiiiiiiii"
        if yatchflag == 1{
            lblcarname.setHTMLFromString(text:self.carname,alignment: .center)
        }else{
            lblcarname.text=carname}
        if let number = Double(carpricefromdetail){
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
            if yatchflag == 1{
                let formattedNumber2 = numberFormatter.string(from: NSNumber(value: number*2))
                self.lblcarprice.text="QAR "+(formattedNumber2 ??  "")+"(2h)"
                lblpriceofcar.text="QAR "+formattedNumber
            }else{
                
                self.lblcarprice.text="QAR "+(formattedNumber )+" per day"
                lblpriceofcar.text="QAR "+formattedNumber
                
            }
            lblpriceofcar.text="QAR "+formattedNumber
            lbltotalprice.text="QAR "+formattedNumber
        }
        // lblcarprice.text="QAR "+carpricefromdetail+"/h"
        print("carpricefromdetail",carpricefromdetail)
        let url = URL(string:carimage)
        self.imageviewcar.kf.indicatorType = .activity
        self.imageviewcar.kf.setImage(with: url)
        self.imageviewcar.contentMode = .scaleAspectFit
        
        
        
        segmentcontrol.layer.cornerRadius = 20
        segmentcontrol.layer.masksToBounds = true
        // segmentcontrol.layer.borderColor = UIColor.darkGray.cgColor
        
        if segmentcontrol.selectedSegmentIndex==0{
            //            lblreceivelocationselected.text="International Airport,Doha"
            //        lblreturnlocationselected.text="Doha Airport"
        }else{
            lblreceivelocationselected.text="Outing"
            //            lblreturnlocationselected.text="Doha Airport"
        }
        lblbrands.isHidden=true
        addonviewheight.constant=0
        
        priceofcarafterdateselected=carpricefromdetail
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], for: .selected)
        // stylizeFonts()
    }
    
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //      return .lightContent
    //    }
    
    
    @IBAction func segmentcontolaction(_ sender: Any) {
        if segmentcontrol.selectedSegmentIndex==0{
            viewlocation.isHidden=false
            viewpickup.isHidden=true
            txtfldpickupreturn.text = "Return"
            txtfldreturn.text = ""
            txtfldreceive.text = ""
        }else{
            viewlocation.isHidden=true
            viewpickup.isHidden=false
            
            txtfldpickupreturn.text = ""
            txtfldreturn.text = "Return"
            txtfldreceive.text = "Receive"
        }
    }
    
    //    func locationupdate(){
    //
    //        locationManager.delegate = self
    //        locationManager.requestWhenInUseAuthorization()
    //
    //        mapview.delegate=self
    //        mapview.isHidden=false
    //
    //        //        mapview.camera=GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 9.582912, longitude: 76.547326), zoom: 15, bearing: 0, viewingAngle: 0)
    //
    //        mapview.delegate = self
    //        mapview.isMyLocationEnabled = true
    //        mapview.settings.zoomGestures   = true
    //
    //
    //
    //
    //    }
    //
    
    
    
    
    @IBAction func btngooglemapaction(_ sender: Any) {
//        let customURL="https://goo.gl/maps/XTUXK2kDbjHgHkhCA"
//        if UIApplication.shared.canOpenURL(NSURL(string: customURL) as! URL) {
//            UIApplication.shared.openURL(NSURL(string: customURL) as! URL)
//        }
//        else {
//            var alert = UIAlertController(title: "Error", message: "Google maps not installed", preferredStyle: UIAlertController.Style.alert)
//            var ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
//            alert.addAction(ok)
//            self.present(alert, animated:true, completion: nil)
//        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==collectionviewaddons{
            return addoncategory?.count ?? 0
        }else if collectionView==collectionviewbrands {
            return addonbrands?.count ?? 0
        }else{
            return gift?.count ?? 0
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView==collectionviewaddons{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AddonsCollectionViewCell",
                                                           for: indexPath) as! AddonsCollectionViewCell
            cell1.lbladdons.text=addoncategory?[indexPath.row].addon_cat_name
            let url = URL(string:imageurl+(addoncategory?[indexPath.row].addon_cat_image ?? ""))
            
            cell1.imageviewaddons.kf.indicatorType = .activity
            cell1.imageviewaddons.kf.setImage(with: url)
            cell1.imageviewaddons.contentMode = .scaleAspectFill
            if indexPath.row==selectedindex{
                // cell1.viewaddons.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell1.viewaddons.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell1.lbladdons.textColor = .white
            }else{
                cell1.viewaddons.backgroundColor = .darkGray
                cell1.lbladdons.textColor = .white
            }
            
            return cell1
        }else if collectionView==collectionviewbrands{
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandsCollectionViewCell",
                                                           for: indexPath) as! BrandsCollectionViewCell
            cell2.lblbrands.text=addonbrands?[indexPath.row].addon_brand_name
            let url = URL(string:imageurladdonbrand+(addonbrands?[indexPath.row].addon_brand_image ?? ""))
            
            cell2.imageviewbrands.kf.indicatorType = .activity
            cell2.imageviewbrands.kf.setImage(with: url)
            cell2.imageviewbrands.contentMode = .scaleAspectFit
            if indexPath.row==selectedindexaddonbrand{
                cell2.viewbrands.backgroundColor = UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell2.lblbrands.textColor = .white
            }else{
                cell2.viewbrands.backgroundColor = .darkGray
                cell2.lblbrands.textColor = .white
            }
            
            return cell2
        }else{
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell",
                                                           for: indexPath) as! GiftCollectionViewCell
            cell3.layer.cornerRadius=10
            cell3.lbltitle.text=gift?[indexPath.row].title
            cell3.lbldescrptn.text=gift?[indexPath.row].description
            if let number = Double(gift?[indexPath.row].base_price ?? ""){
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                cell3.lblprice.text="QAR "+formattedNumber
            }
            
            let url = URL(string:imageurlgift1+(gift?[indexPath.row].gift_images?[0].gift_image ?? ""))
            
            cell3.imageviewgift.kf.indicatorType = .activity
            cell3.imageviewgift.kf.setImage(with: url)
            cell3.imageviewgift.contentMode = .scaleAspectFill
            cell3.layer.cornerRadius=20
            cell3.btnbookgift.tag=indexPath.row
            cell3.btnbookgift.addTarget(self, action: #selector(bookgiftaction(sender:)), for: .touchUpInside)
            cell3.btnbookgift.isUserInteractionEnabled = false
            if indexPath.row==selectedgiftindex{
                cell3.lbltitle.textColor=UIColor.white
                cell3.lblprice.textColor=UIColor.white
                cell3.btnbookgift.setImage(UIImage(named:"tick"), for: .normal)
            }else{
                cell3.lbltitle.textColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell3.lblprice.textColor=UIColor(red: 136 / 255, green: 111 / 255, blue: 80 / 255, alpha: 1.0)
                cell3.btnbookgift.setImage(UIImage(named:"plus"), for: .normal)
            }
            return cell3
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        if collectionView==collectionviewaddons{
            return CGSize(width: 150, height: CGFloat(100))
        }else if collectionView==collectionviewbrands {
            return CGSize(width: 150, height: CGFloat(100))
        }else{
            return CGSize(width: 180, height: CGFloat(220))
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView==collectionviewaddons{
            selectedindex=indexPath.row
            selectedindexaddonbrand = -1
            carbookmodel.addon_cat_id=self.addoncategory?[indexPath.row].addon_cat_id ?? ""
            carbookmodel.addon_brand{ (model) in
                self.addonbranddata(data:model)
            }
            nameofjwellery=addoncategory?[indexPath.row].addon_cat_name ?? ""
            self.collectionviewaddons.reloadData()
        }else if collectionView==collectionviewbrands {
            selectedindexaddonbrand=indexPath.row
            if addonbrands?.count ?? 0>0{
                carbookmodel.addon_cat_id=self.addonbrands?[indexPath.row].addon_cat_id ?? ""
                carbookmodel.addonbrandid=self.addonbrands?[indexPath.row].addon_brand_id ?? ""
                
                
                carbookmodel.gift{ (model) in
                    self.giftdata(data:model)
                }
                nameofbrand=addonbrands?[indexPath.row].addon_brand_name ?? ""
                
                self.collectionviewbrands.reloadData()
            }
            
        }else{
            if selectedgiftindex != indexPath.row{
                selectedgiftindex=indexPath.row
                priceofgiftselected=self.gift?[indexPath.row].base_price ?? ""
                self.viewaddon.isHidden=false
                addonviewheight.constant=114
                // self.viewheightrorscrolling.constant=1300+795+684+114
                lblpriceofgift.text=priceofgiftselected
                lbljwelleryselected.text=nameofjwellery+" > "+nameofbrand
                lblgiftselected.text=gift?[indexPath.row].title
                carprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                let number = Double(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                lbltotalprice.text="QAR "+formattedNumber
            }else{
                selectedgiftindex = -1
                priceofgiftselected="0"
                self.viewaddon.isHidden=true
                addonviewheight.constant=0
                // self.viewheightrorscrolling.constant=1300+795+684+114
                lblpriceofgift.text=nil
                lbljwelleryselected.text=nil
                lblgiftselected.text=nil
                carprice=String(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                let number = Double(Int(priceofgiftselected)!+Int(priceofcarafterdateselected)!)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                lbltotalprice.text="QAR "+formattedNumber
            }
            
            self.collectionviewgift.reloadData()
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    @IBAction func btnsidemenuaction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc =   storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController
        
        let navController = UINavigationController(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationBar.isHidden=true
        if cart?.count ?? 0>0{
            vc?.cart=cart
            vc?.imageurlgift=imageurlgift
        }
        self.present(navController, animated:false, completion: nil)
    }
    
    @objc func bookgiftaction(sender:UIButton){
        
        
    }
    
    
    func mapdatas(lat: CLLocationDegrees, long: CLLocationDegrees, mapaddress1: String,mapadress2: String, changelocationflag: Int) {
        
        if changelocationflag==0{
            txtfldreceive.text=mapaddress1
            lblreceivelocationselected.text=mapaddress1
            
            txtfldreturn.text=mapadress2
            lblreturnlocationselected.text=mapadress2
        }else{
            txtfldpickupreturn.text=mapaddress1
            lblreturnlocationselected.text=mapaddress1
            lblreceivelocationselected.text="Outing"
            
        }
    }
    
    
    
    func addoncategorydata(data: Addonclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.addoncategory=data.data?.addon_category
                self.imageurl=data.data?.image_url ?? ""
                self.carbookmodel.addon_cat_id=self.addoncategory?[0].addon_cat_id ?? ""
                //                self.carbookmodel.addon_brand{ (model) in
                //                    self.addonbranddata(data:model)
                //                }
                self.collectionviewbrands.isHidden=true
                self.collectionviewgift.isHidden=true
                self.lblbrands.isHidden=true
                self.collectionviewaddons.reloadData()
               
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
            }
        }
    }
    
    
    func addonbranddata(data: Addonbrandclass) {
        print("data",data)
        addonbrands=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                // self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.addonbrands=data.data?.addon_brands
                self.imageurladdonbrand=data.data?.image_url ?? ""
                self.viewaddonheight.constant=460
                self.viewheightrorscrolling.constant=1300+795+460
                self.lblbrands.isHidden=false
                self.collectionviewgift.isHidden=true
                self.collectionviewbrands.isHidden=false
                self.collectionviewbrands.reloadData()
                self.collectionviewaddons.reloadData()
                self.carbookmodel.addon_cat_id = self.addonbrands?[0].addon_cat_id ?? ""
                self.carbookmodel.addonbrandid = self.addonbrands?[0].addon_brand_id ?? ""
                self.selectedgiftindex = -1
                self.priceofgiftselected="0"
                self.viewaddon.isHidden=true
                self.addonviewheight.constant=0
                // self.viewheightrorscrolling.constant=1300+795+684+114
                self.lblpriceofgift.text=nil
                self.lbljwelleryselected.text=nil
                self.lblgiftselected.text=nil
                self.carprice=String(Int(self.priceofgiftselected)!+Int(self.priceofcarafterdateselected)!)
                let number = Double(Int(self.priceofgiftselected)!+Int(self.priceofcarafterdateselected)!)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                self.lbltotalprice.text="QAR "+formattedNumber
                
                
                
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewaddonheight.constant=240
                self.viewheightrorscrolling.constant=1300+795+240
                self.lblbrands.isHidden=true
                self.collectionviewbrands.reloadData()
                self.collectionviewgift.isHidden=true
                self.collectionviewbrands.isHidden=true
            }
        }
    }
    func giftdata(data: Giftclass) {
        print("data",data)
        gift=[]
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                //self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
                self.gift=data.data?.gift
                self.imageurlgift1=data.data?.image_url ?? ""
                self.viewaddonheight.constant=684
                self.viewheightrorscrolling.constant=1300+795+684
                self.collectionviewgift.isHidden=false
                self.collectionviewgift.reloadData()
                
            }
        }else{
            DispatchQueue.main.async{
                self.viewaddonheight.constant=460
                self.viewheightrorscrolling.constant=1300+795+460
                self.collectionviewgift.isHidden=false
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.collectionviewgift.reloadData()
            }
        }
    }
    
    
    @IBAction func btnsearchaction(_ sender: Any) {
    }
    
    
    
    
    func afterdatetimeselected(){
        
        fmt.dateFormat="yyyy-MM-dd"
        let receivedate=fmt.date(from: datereceinveselected)
        let returndate=fmt.date(from: daterereceivereturn)
        daysBetweenDates(startDate: receivedate!, endDate: returndate!)
        
        
    }
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let daysBetween = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        print(daysBetween.day!)
        let day=daysBetween.day! + 1
        numberofdaysbooked=String(day)
        lblnoofdaysbooked.text=String(day)+" Day Booking"
        //                       let car_price=Int(carprice)
        //                   print("car_price",car_price)
        //                    let totalprice=day*car_price!
        //                priceofcarafterdateselected=String(totalprice)
        //                      lbltotalprice.text="QAR "+String(totalprice)
        //                carprice=String(totalprice)
        return daysBetween.day!
    }
    
    
    
    @IBAction func btnreceivedate(_ sender: Any) {
        receivereturnflag=0
        datearray.removeAll()
        print("datearray",datearray)
        var startDate = Date()
        var endDate=Date()
        let calendar = Calendar.current
        
        let fmt = DateFormatter()
        
        fmt.dateFormat = "yyyy-MM-dd"
        if yatchflag==1{
            for i in yatchdisablefromcardetails ?? []{
                startDate=fmt.date(from:i.pick_up_date ?? "")!
                endDate=fmt.date(from: i.drop_date ?? "")!
                if startDate==endDate{
                    let date=fmt.string(from: startDate)
                    print("date",date)
                    datearray.append(date)
                }else{
                    let date=fmt.string(from: startDate)
                    print("date",date)
                    datearray.append(date)
                    while startDate < endDate {
                        print(fmt.string(from: startDate))
                        
                        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
                        print("startDate",startDate)
                        
                        let date=fmt.string(from: startDate)
                        print("date",date)
                        datearray.append(date)
                        
                    }
                }
                
            }
            
            
            
        }else{
            if disablefromcardetails?.count ?? 0 > 0{
                for i in 0...(disablefromcardetails!.count - 1){
                    startDate=fmt.date(from: disablefromcardetails?[i].pick_up_date ?? "")!
                    endDate=fmt.date(from: disablefromcardetails?[i].drop_date ?? "")!
                    if startDate==endDate{
                        let date=fmt.string(from: startDate)
                        print("date",date)
                        datearray.append(date)
                    }else{
                        let date=fmt.string(from: startDate)
                        print("date",date)
                        datearray.append(date)
                        while startDate < endDate {
                            print(fmt.string(from: startDate))
                            
                            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
                            print("startDate",startDate)
                            
                            let date=fmt.string(from: startDate)
                            print("date",date)
                            datearray.append(date)
                            
                        }
                    }
                    
                    
                    
                }
            }
        }
        print("datearray",datearray)
        
        timeflag=0
        
        showCalendar()
        
        
    }
    
    
    
    func check_cardata(data: Carcheckclass) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                
                
                
                self.lbladdonsheading.isHidden=false
                self.collectionviewaddons.isHidden=false
                self.viewaddonheight.constant=240
                self.viewbookingdetailheight.constant=795
                self.viewheightrorscrolling.constant=1300+795+240
                self.lbladdonsheading.isHidden=false
                self.carbookmodel.addon_category{ (model) in
                    self.addoncategorydata(data:model)
                }
                
                let day=(data.data?.days as! NSNumber).stringValue
                
                self.lblnoofdaysbooked.text=day+" Day Booking"
                self.numberofdaysbooked=day
                
                let car_price=Int(self.carpricefromdetail)
                print("car_price",car_price)
                let totalprice=Int(day)!*car_price!
                self.priceofcarafterdateselected=String(totalprice)
                let number = Double(totalprice)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                self.lbltotalprice.text="QAR "+formattedNumber
                self.lblpriceofcar.text="QAR "+formattedNumber
                
                self.carprice=String(totalprice)
                self.scrollView.scrollToView(view: self.btncontinue, animated: true)
               // self.btncontinue.isUserInteractionEnabled = false
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewheightrorscrolling.constant=1300
                self.viewaddonheight.constant=0
                self.viewbookingdetailheight.constant=0
                self.collectionviewgift.isHidden=true
                self.collectionviewbrands.isHidden=true
                self.collectionviewaddons.isHidden=true
                self.lblbrands.isHidden=true
                self.lbladdonsheading.isHidden=true
                
                
                
            }
        }
    }
    
    
    
    
    
    func check_yatchdata(data: Yatchcheckclass) {
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                self.lbladdonsheading.isHidden=false
                self.collectionviewaddons.isHidden=false
                self.viewaddonheight.constant=240
                self.viewbookingdetailheight.constant=795
                self.lbladdonsheading.isHidden=false
                self.viewheightrorscrolling.constant=1300+795+240
                self.carbookmodel.addon_category{ (model) in
                    self.addoncategorydata(data:model)
                }
                let day=(data.data?.days as! NSNumber).stringValue
                
                // self.lblnoofdaysbooked.text=day+" Day Booking"
                let car_price=Int(self.carpricefromdetail)
                print("car_price",car_price)
                let totalprice=Int(day)!*car_price!
                self.priceofcarafterdateselected=String(totalprice)
                let number = Double(totalprice)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number)) ?? ""
                self.lbltotalprice.text="QAR "+formattedNumber
                self.lblpriceofcar.text="QAR "+formattedNumber
                self.carprice=String(totalprice)
                self.scrollView.scrollToView(view: self.btncontinue, animated: true)
               // self.btncontinue.isUserInteractionEnabled = false
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                self.viewheightrorscrolling.constant=1300
                self.viewaddonheight.constant=0
                self.viewbookingdetailheight.constant=0
                self.collectionviewgift.isHidden=true
                self.collectionviewbrands.isHidden=true
                self.collectionviewaddons.isHidden=true
                self.lblbrands.isHidden=true
                self.lbladdonsheading.isHidden=true
                
            }
        }
    }
    
    @IBAction func btnpickupreturnlocationaction(_ sender: Any) {
        let map = self.storyboard?.instantiateViewController (withIdentifier: "MapViewController") as! MapViewController
        map.changeloactionflag=2
        map.delegate=self
        
        
        self.navigationController?.pushViewController(map, animated: true)
        
        
    }
    
    @IBAction func btnreturnlocationaction(_ sender: Any) {
        
        let map = self.storyboard?.instantiateViewController (withIdentifier: "MapViewController") as! MapViewController
        map.changeloactionflag=0
        map.delegate=self
        self.navigationController?.pushViewController(map, animated: true)
        
    }
    
    @IBAction func btnreceivelocationaction(_ sender: Any) {
        let map = self.storyboard?.instantiateViewController (withIdentifier: "MapViewController") as! MapViewController
        map.changeloactionflag=0
        map.delegate=self
        self.navigationController?.pushViewController(map, animated: true)
        
        
        
        
        
    }
    @IBAction func btnreceivetime(_ sender: Any) {
        receivereturnflag=0
        timeflag=1
        showCalendar()
    }
    
    @IBAction func btnreturndate(_ sender: Any) {
        
        //        var startDate = Date()
        //         var endDate=Date()
        //         let calendar = Calendar.current
        //
        //         let formatter = DateFormatter()
        //
        //         formatter.dateFormat = "yyyy-MM-dd"
        //
        //         startDate=formatter.date(from:datearray[0])!
        //         print("startDate",startDate)
        //         endDate=formatter.date(from:datereceinveselected)!
        //         print("endDate",endDate)
        //         while startDate < endDate{
        //         let fromDate = calendar.date(byAdding: .day, value: 1, to: startDate)
        //            let date=formatter.string(from: fromDate!)
        //         datearray.append(date)
        //         }
        print("datearray",datearray)
        receivereturnflag=1
        timeflag=0
        showCalendar()
    }
    
    @IBAction func btnreturntime(_ sender: Any) {
        receivereturnflag=1
        timeflag=1
        showCalendar()
    }
    
    func showCalendar() {
        viewheightrorscrolling.constant=1300
        self.viewaddonheight.constant=0
        self.viewbookingdetailheight.constant=0
        self.addonviewheight.constant=0
        self.viewaddon.isHidden=true
        collectionviewgift.isHidden=true
        collectionviewbrands.isHidden=true
        collectionviewaddons.isHidden=true
        lblbrands.isHidden=true
        lbladdonsheading.isHidden=true
        selectedindex = -1
        selectedindexaddonbrand = -1
        selectedgiftindex = -1
        
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        if timeflag==1{
            selector.optionStyles.showTime(true)
            selector.optionStyles.showYear(false)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showDateMonth(false)
            
        }else{
            selector.optionStyles.showTime(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showMonth(true)
            selector.optionStyles.showDateMonth(true)
            selector.datearray=datearray
            
        }
        present(selector, animated: true, completion: nil)
    }
    //
    @IBAction func btnaddtocartaction(_ sender: Any) {
        //                if lbldatereceive.text==""||lblreturndate.text==""{
        //
        //                self.showToast(message: "Please specify return/receive dates to continue", font: .boldSystemFont(ofSize: 13), duration: 2)
        //               }else{
        
        if let user_id=UserDefaults.standard.value(forKey: "userid"){
            userid=UserDefaults.standard.value(forKey: "userid") as! String
//            loginmodel.document_list{ (model) in
//                self.document_listdata(data:model)
//            }
            
            
            cardate=lbldatereceive.text!+" - "+lblreturndate.text!
            saveData()
            let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            if self.cart?.count ?? 0>0{
                cart1.cart=self.cart
                cart1.imageurlgift=self.imageurlgift
            }
            self.navigationController?.pushViewController(cart1, animated: true)
            //          let cart = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            ////                  cart.cartname_array=cartcarnamearray
            ////                  cart.cartdate_array=cartdatearry
            ////                  cart.carprice_array=cartcarprice
            ////                  cart.carimage_array=cartcarimagearry
            //                 self.navigationController?.pushViewController(cart, animated: true)
            
        }else{
            let login = self.storyboard?.instantiateViewController (withIdentifier: "LoginViewController") as! LoginViewController
            login.cartflag=1
            if cart?.count ?? 0>0{
                login.cart=cart
                login.imageurlgift=imageurlgift
            }
            self.navigationController?.pushViewController(login, animated: true)
            
        }
        
        //         }
    }
    
    func document_listdata(data: Doclistclass) {
        
        let status=data.status
        if status==true{
            
            doclist=data.data?.doc_list
            if doclist?.count ?? 0 > 1 {
                for i in 0...(doclist?.count ?? 0)-1{
                    typedoc_array.append(doclist?[i].type ?? "")
                }
            }else{
                typedoc_array.append(doclist?[0].type ?? "")
            }
            
            DispatchQueue.main.async{
                if self.typedoc_array.contains("2"){
                    self.viewdoc1.isHidden=true
                }
                if self.typedoc_array.contains("3"){
                    self.viewdoc2.isHidden=true
                }
            }
            if typedoc_array.contains("2")  && typedoc_array.contains("3"){
                DispatchQueue.main.async{
                    self.viewdocumentupload.isHidden=true
                }
                
                DispatchQueue.main.async{
                    if self.idarray.count>0{
                        if self.typearray[0]=="1"{
                            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CardetailsViewController
                            
                            cardetail.carid=self.idarray[0]
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            cardetail.idarray=self.idarray
                            cardetail.typearray=self.typearray
                            if self.cart?.count ?? 0>0{
                                cardetail.cart=self.cart
                                cardetail.imageurlgift=self.imageurlgift
                            }
                            self.navigationController?.pushViewController(cardetail, animated: true)
                        }
                        else if self.typearray[0]=="2"{
                            let helicopter = self.storyboard?.instantiateViewController (withIdentifier: "HelicopterViewController") as! HelicopterViewController
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            helicopter.idarray=self.idarray
                            helicopter.typearray=self.typearray
                            
                            if self.cart?.count ?? 0>0{
                                helicopter.cart=self.cart
                                helicopter.imageurlgift=self.imageurlgift
                            }
                            self.navigationController?.pushViewController(helicopter, animated: true)
                        }else{
                            let cardetail = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CardetailsViewController
                            cardetail.yatchid=self.idarray[0]
                            cardetail.yatchflag=1
                            
                            self.idarray.remove(at: 0)
                            self.typearray.remove(at: 0)
                            cardetail.idarray=self.idarray
                            cardetail.typearray=self.typearray
                            
                            
                            if self.cart?.count ?? 0>0{
                                cardetail.cart=self.cart
                                cardetail.imageurlgift=self.imageurlgift
                            }
                            
                            self.navigationController?.pushViewController(cardetail, animated: true)
                        }
                    }else{
                        let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
                        if self.cart?.count ?? 0>0{
                            cart1.cart=self.cart
                            cart1.imageurlgift=self.imageurlgift
                        }
                        self.navigationController?.pushViewController(cart1, animated: true)
                    }
                    
                }
                
                
            }else{
                DispatchQueue.main.async{
                    self.viewdocumentupload.isHidden=false
                }
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
                
                self.viewdocumentupload.isHidden=false
            }
        }
    }
    
    func saveData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let manageContent = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Cartdetail", in: manageContent)!
        
        let users = NSManagedObject(entity: userEntity, insertInto: manageContent)
        
        users.setValue(carname, forKeyPath: "carname")
        users.setValue(carprice, forKeyPath: "carprice")
        users.setValue(carimage, forKeyPath: "carimage")
        users.setValue(cardate, forKeyPath: "cardate")
        users.setValue(userid, forKeyPath: "userid")
        
        users.setValue(datereceinveselected, forKey: "pick_date")
        users.setValue(daterereceivereturn, forKey: "drop_date")
        users.setValue(packageid, forKey: "package_id")
        if timeselectedreceive==""{
            users.setValue("12:00 PM", forKey: "pick_time")
        }else{
            users.setValue(timeselectedreceive, forKey: "pick_time")
        }
        if timeselectedreturn==""{
            users.setValue("12:00 PM", forKey: "drop_time")
        }else{
            users.setValue(timeselectedreturn, forKey: "drop_time")
        }
        
        users.setValue(numberofdaysbooked, forKey: "total_days")
        if segmentcontrol.selectedSegmentIndex == 0{
            users.setValue("1", forKey: "type")
            users.setValue(lblreceivelocationselected.text, forKey: "receive_loc")
            users.setValue(lblreturnlocationselected.text, forKey: "return_loc")
        }else{
            users.setValue("0", forKey: "type")
            users.setValue("Outing", forKey: "receive_loc")
            users.setValue(lblreturnlocationselected.text, forKey: "return_loc")
        }
        if yatchflag==0{
            users.setValue("1", forKey: "flag")
            users.setValue(carid, forKey: "car_id")
        }else{
            users.setValue("3", forKey: "flag")
            users.setValue(yatchid, forKey: "car_id")
        }
        if priceofgiftselected==""{
            users.setValue("", forKey: "addon_cat_id")
            users.setValue("", forKey: "addon_brand_id")
            users.setValue("", forKey: "gift_id")
        }else if selectedgiftindex != -1{
            users.setValue(self.addoncategory?[selectedindex].addon_cat_id, forKey: "addon_cat_id")
            users.setValue(self.addonbrands?[selectedindexaddonbrand].addon_brand_id, forKey: "addon_brand_id")
            users.setValue(self.gift?[selectedgiftindex].gift_id, forKey: "gift_id")
        }
        
        
        do{
            try manageContent.save()
            cartdetail.append(users as! Cartdetail)
        }catch let error as NSError {
            
            print("could not save . \(error), \(error.userInfo)")
        }
        
        
        
    }
    
    
    @IBAction func btncontinueaction(_ sender: Any) {
        
        if lbldatereceive.text=="RECEIVE DATE"||lblreturndate.text=="RETURN DATE"{
            
            self.showToast(message: "Please specify return/receive dates/location to continue", font: .boldSystemFont(ofSize: 13), duration: 2)
            
            
        }else if yatchflag==1{
            yatchmodel.starttime=timeselectedreceive
            yatchmodel.endtime=timeselectedreturn
            yatchmodel.startdate=datereceinveselected
            yatchmodel.enddate=daterereceivereturn
            yatchmodel.yatchid=yatchid
            yatchmodel.check_yacht{ (model) in
                self.check_yatchdata(data:model)
            }
            
        }else{
            if txtfldreceive.text==""||txtfldreturn.text==""||txtfldpickupreturn.text==""{
                // if yatchflag==0{
                
                self.showToast(message: "Please specify return/receive dates/location to continue", font: .boldSystemFont(ofSize: 13), duration: 2)
            }else{
                carbookmodel.starttime=timeselectedreceive
                carbookmodel.endtime=timeselectedreturn
                carbookmodel.startdate=datereceinveselected
                carbookmodel.enddate=daterereceivereturn
                carbookmodel.carid=carid
                carbookmodel.check_car{ (model) in
                    self.check_cardata(data:model)
                }
                
            }
            
        }
    }
    
    
    @IBAction func btnbackaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnchoosefileqtrdidaction(_ sender: Any) {
        // self.pickImage(type: .qatarId)
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //        importMenu.delegate = self
        //        importMenu.modalPresentationStyle = .formSheet
        //        importMenu.addOption(withTitle: "Image", image: nil, order: .first, handler: {
        self.pickImage(type: .qatarId)
        //            print("New Doc Requested") })
        //        self.present(importMenu, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func btnnchooselicensecation(_ sender: Any) {
        //        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //        importMenu.delegate = self
        //        importMenu.modalPresentationStyle = .formSheet
        //        importMenu.addOption(withTitle: "Image", image: nil, order: .first, handler: {
        self.pickImage(type: .license)
        //            print("New Doc Requested") })
        //        self.present(importMenu, animated: true, completion: nil)
        
        
    }
    
    
    
    func uploadUserData(documentType: String) {
        model.updateUserData(image: imageToUpload, type: documentType).subscribe(
            onNext:{ data in
                print("API success")
                self.loginmodel.document_list{ (model) in
                    self.document_listdata(data:model)
                }
            },onError:{ error in
                self.showToast(message: "Error in uploading please try again later", font: .boldSystemFont(ofSize: 13), duration: 2)
                print("error")
            }).disposed(by: bag)
    }
    
    
    func pickImage(type: DocumentType) {
        ImagePickerManager().pickImage(self) { image in
            // self.imageViewUser.image = image
            self.imageToUpload = image
            
            switch type {
            case .passport:
                self.uploadUserData(documentType: "1")
            case .license:
                self.uploadUserData(documentType: "3")
            case .qatarId:
                self.uploadUserData(documentType: "2")
            }
        }
    }
    
    
    @IBAction func btnclosedocaction(_ sender: Any) {
        viewdocumentupload.isHidden=true
    }
    
}

extension CarbookViewController: WWCalendarTimeSelectorProtocol{
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        print("disablefromcardetails",disablefromcardetails)
        let today_date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        format.timeZone = .current
        print("datearray",datearray)
        print("date",date)
        print("today_date",today_date)
        let selected=format.string(from: date)
        print("selected",selected)
        if datearray.contains(selected){
            return false
        }else{
            return true
        }
        
        //    if date.compare(today_date) == .orderedAscending{
        //        return false
        //    }else{
        //        return true
        //    }
        //return true
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected Date- \(date)")
        var notflag=0
        
        
        if timeflag==0{
            
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            format.timeZone = .current
            
            let selected=format.string(from: date)
            
            if datearray.contains(selected){
                self.showToast(message: "Not avilable at this dates", font: .boldSystemFont(ofSize: 13), duration: 2)
            }else{
                if receivereturnflag==0{
                    self.lblreturndate.text=""
                    
                    datereceinveselected=format.string(from: date)
                    let fmt = DateFormatter()
                    
                    fmt.dateFormat = "EE,MMM dd yyy"
                    
                    //                    self.lbldatereceive.text=selected
                    //                    self.lblreceivedateselected.text=selected
                    
                    print("datereceiveselected",datereceinveselected)
                    
                    
                    
                    self.lbldatereceive.text=fmt.string(from: date)
                    self.lblreceivedateselected.text=fmt.string(from: date)
                }else{
                    
                    var startDate = Date()
                    var endDate=Date()
                    let calendar = Calendar.current
                    
                    let fmt = DateFormatter()
                    
                    fmt.dateFormat = "yyyy-MM-dd"
                    
                    startDate=fmt.date(from: datereceinveselected) ?? Date()
                   // let result=date.compare(startDate)
//                    if result != .orderedDescending{
//                        self.showToast(message: "Please select a date as per receive date", font: .boldSystemFont(ofSize: 13), duration: 2)
//                    }else{
                        
                        
                        print("startDate",startDate)
                        endDate=fmt.date(from: selected)!
                        if startDate <= endDate {
                            print(fmt.string(from: startDate))
                           // startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
                            print("startDate",startDate)
                            let date_ad=fmt.string(from: startDate)
                            print("date",date)
                            if datearray.contains(date_ad){
                                self.showToast(message: "Not avilable at  \(date_ad)", font: .boldSystemFont(ofSize: 13), duration: 2)
                                notflag=1
                            }
                            else{
                                
                                daterereceivereturn=format.string(from: date)
                                if datereceinveselected==daterereceivereturn{
                                    notflag=0
                                    
                                }
                                let fmt = DateFormatter()
                                fmt.dateFormat = "EE,MMM dd yyy"
                                if notflag==1{
                                    self.lblreturndate.text=""
                                    self.lblreturndateselected.text=""
                                }else{
                                    self.lblreturndate.text=fmt.string(from: date)
                                    self.lblreturndateselected.text=fmt.string(from: date)
                                }
                                //
                                if lbldatereceive.text != "" && lblreturndate.text != ""{
                                    afterdatetimeselected()
                                }
                            }
                        }else{
                            self.showToast(message: "Please select a date as per receive date", font: .boldSystemFont(ofSize: 13), duration: 2)
                        }
                        
                        
                  //  }
                    
                }
                
                
            }
        }else{
            timeselected = date.stringFromFormat("h:mm a")
            let dateFormatter=DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let date = dateFormatter.date(from: timeselected)
            dateFormatter.dateFormat = "HH:mm"
            
            let Date24 = dateFormatter.string(from: date!)
            print("24 hour formatted Date:",Date24)
            if receivereturnflag==0{
                timeselectedreceive=timeselected
                
                lbltime.text=timeselected
                lblreceivetimeselected.text="Time: "+timeselected
                
            }else{
                timeselectedreturn=timeselected
                lblreturntime.text=timeselected
                lblreturntimeselected.text="Time: "+timeselected
            }
        }
        
        
        
    }
    
    
}
