//
//  GiftCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class GiftCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnbookgift: UIButton!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbldescrptn: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imageviewgift: UIImageView!
}
