//
//  BrandsCollectionViewCell.swift
//  Outing
//
//  Created by Arun Vijayan on 15/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit

class BrandsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewbrands: UIView!
    @IBOutlet weak var lblbrands: UILabel!
    @IBOutlet weak var imageviewbrands: UIImageView!
    override func awakeFromNib() {
    super.awakeFromNib()
     viewbrands.layer.cornerRadius=10
        
    }
}
