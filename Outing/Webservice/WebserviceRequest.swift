//
//  WebserviceRequest.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
import Alamofire

class WebserviceRequest {
    
    static let shared = WebserviceRequest()
    
    func updateProfileMultiPart(imgData:Data, parameter: [String: Any], handler: @escaping WebServiceCompletionHandler) {
        

        let url = Api.profile.upload
             
        // Request
        WebserviceManager.shared.requestMultipart(with: "image1", imageData: imgData, url: url, method: .post, parameter: parameter, header: nil, mappableOf: ProfileResponseModel.self, completionHandler: handler)
        
    }
    
    //******************************
    // MARK:- for  user profile
    //******************************
    func UserProfile(parameter: [String: Any], handler: @escaping WebServiceCompletionHandler) {
        
        
        let url = Api.profile.userDetails
        
        
        // Request
        WebserviceManager.shared.request(with: url, method: .post, parameter: parameter, header: nil, mappableOf: UserDetailsResponseModel.self , completionHandler: handler)
    }
    //user edit
    func updateProfileeditMultiPart(imgData:Data, parameter: [String: Any], handler: @escaping WebServiceCompletionHandler) {
        

         let url = Api.profile.profileedit
             
        // Request
        WebserviceManager.shared.requestMultipart(with: "user_image", imageData: imgData, url: url, method: .post, parameter: parameter, header: nil, mappableOf: Profileeditclass.self, completionHandler: handler)
        
    }
    
}
