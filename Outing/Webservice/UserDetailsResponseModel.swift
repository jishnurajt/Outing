//
//  UserDetailsResponseModel.swift
//  Outing
//
//  Created by Personal on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserDetailsResponseModel : Mappable {
    var status : Bool?
    var message : String?
    var data : UserDetailData?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }

}

struct User_documents : Mappable {
    var d_id : String?
    var user_id : String?
    var type : String?
    var document : String?
    var status : String?
    var created_at_date : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        d_id <- map["d_id"]
        user_id <- map["user_id"]
        type <- map["type"]
        document <- map["document"]
        status <- map["status"]
        created_at_date <- map["created_at_date"]
    }

}

struct UserDetails : Mappable {
    var user_id : String?
    var username : String?
    var email_id : String?
    var mobile : String?
    var dob : String?
    var address : String?
    var password : String?
    var last_login : String?
    var user_image : String?
    var device_id : String?
    var device_token : String?
    var latitude : String?
    var longitude : String?
    var source : String?
    var created_at_date : String?
    var status : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        user_id <- map["user_id"]
        username <- map["username"]
        email_id <- map["email_id"]
        mobile <- map["mobile"]
        dob <- map["dob"]
        address <- map["address"]
        password <- map["password"]
        last_login <- map["last_login"]
        user_image <- map["user_image"]
        device_id <- map["device_id"]
        device_token <- map["device_token"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        source <- map["source"]
        created_at_date <- map["created_at_date"]
        status <- map["status"]
    }

}

struct UserDetailData : Mappable {
    var details : UserDetails?
    var user_documents : [User_documents]?
    var doc_url : String?
    var user_image_url : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        details <- map["details"]
        user_documents <- map["user_documents"]
        doc_url <- map["doc_url"]
        user_image_url <- map["user_image_url"]
    }

}



