//
//  Api.swift
//  Outing
//
//  Created by Personal on 28/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation

struct Api {
    
    static let root = "https://www.outing.qa/api/Data/"
    
    struct profile {
        static let upload = root + "document_upload"
        static let userDetails = root + "user_details"
         static let profileedit = root + "profile_edit"
    }

}
