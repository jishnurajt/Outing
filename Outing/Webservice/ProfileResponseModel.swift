//
//  ProfileResponseModel.swift
//  Outing
//
//  Created by Personal on 29/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProfileResponseModel : Mappable {
    var status : Bool?
    var message : String?
    var data : ProfileData?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }

}


struct ProfileData : Mappable {
    var document : String?
    var type : String?
    var user_id : String?
    var created_at_date : String?
    var doc_list : [Doc_list]?
    var doc_url : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        document <- map["document"]
        type <- map["type"]
        user_id <- map["user_id"]
        created_at_date <- map["created_at_date"]
        doc_list <- map["doc_list"]
        doc_url <- map["doc_url"]
    }

}

struct Doc_list : Mappable {
    var d_id : String?
    var user_id : String?
    var type : String?
    var document : String?
    var status : String?
    var created_at_date : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        d_id <- map["d_id"]
        user_id <- map["user_id"]
        type <- map["type"]
        document <- map["document"]
        status <- map["status"]
        created_at_date <- map["created_at_date"]
    }

}







