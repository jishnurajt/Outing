//
//  Profileeditresponsemodel.swift
//  Outing
//
//  Created by Arun Vijayan on 01/10/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
import  ObjectMapper

struct ProfileeditData : Mappable {
    var user_id : String?
    var username : String?
    var email_id : String?
    var mobile : String?
    var dob : String?
    var password : String?
    var last_login : String?
    var user_image : String?
    var device_id : String?
    var device_token : String?
    var latitude : String?
    var longitude : String?
    var source : String?
    var created_at_date : String?
    var status : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        user_id <- map["user_id"]
        username <- map["username"]
        email_id <- map["email_id"]
        mobile <- map["mobile"]
        dob <- map["dob"]
        password <- map["password"]
        last_login <- map["last_login"]
        user_image <- map["user_image"]
        device_id <- map["device_id"]
        device_token <- map["device_token"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        source <- map["source"]
        created_at_date <- map["created_at_date"]
        status <- map["status"]
    }

}



struct Profileeditclass : Mappable {
    var status : Bool?
    var message : String?
    var data : ProfileeditData?
    var user_image_path : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
        user_image_path <- map["user_image_path"]
    }

}

