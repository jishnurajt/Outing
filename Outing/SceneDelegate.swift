//
//  SceneDelegate.swift
//  Outing
//
//  Created by Arun Vijayan on 05/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import LGSideMenuController

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

     var window:UIWindow?


   
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
//        if #available(iOS 13.0, *) {
            guard let _ = (scene as? UIWindowScene) else { return }
//        } else {
//            // Fallback on earlier versions
//        }
        
        
//        if let flag = UserDefaults.standard.value(forKey: "type") {
//
//        if flag as! String=="0"{
//        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//                            let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "SpashscreenvideoViewController") as! SpashscreenvideoViewController
//       //
//            let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//                self.window?.rootViewController=navigation
//            navigation.navigationBar.isHidden=true
//        }else if flag as! String=="1"{
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//            let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "CardetailsViewController") as! CardetailsViewController
//            rootviewcontroller.carid="8"
//                 let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//
//                     self.window?.rootViewController=navigation
//                 navigation.navigationBar.isHidden=true
//            }else if flag as! String=="2"{
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//            let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "HelicopterViewController") as! HelicopterViewController
//
//                 let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//                     self.window?.rootViewController=navigation
//                 navigation.navigationBar.isHidden=true
//        }else if flag as! String=="3"{
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//                       let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "CardetailsViewController") as! CardetailsViewController
//            rootviewcontroller.yatchid="6"
//                            let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//                                self.window?.rootViewController=navigation
//                            navigation.navigationBar.isHidden=true
//        }else if flag as! String=="4"{
//
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//            let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
//            //
//                 let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//                     self.window?.rootViewController=navigation
//                 navigation.navigationBar.isHidden=true
//
//        }else if flag as! String=="5"{
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//                       let rootviewcontroller = storyBoard.instantiateViewController(withIdentifier: "AddonViewController") as! AddonViewController
//                       //
//                            let navigation = UINavigationController.init(rootViewController: rootviewcontroller)
//                                self.window?.rootViewController=navigation
//                            navigation.navigationBar.isHidden=true
//
//        }
//
//
//    }
    }
   
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

   
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

   
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

   
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }



}
