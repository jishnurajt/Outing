//
//  LoginViewController.swift
//  Outing
//
//  Created by Arun Vijayan on 08/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import UIKit
import CoreData
//import MobileCoreServices
import RxSwift



extension UIViewController{

func showToast(message : String, font: UIFont , duration: Double) {
    var toastLabel=UILabel()
if UIDevice.current.userInterfaceIdiom == .pad{
    toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 65, y: self.view.frame.size.height-100, width: UIScreen.main.bounds.width*(3/4), height: 40))
}else{
   toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 85, y: self.view.frame.size.height-100, width: UIScreen.main.bounds.width-20, height: 40))
    }
    //toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.backgroundColor = UIColor.white
    toastLabel.textColor = UIColor.darkGray
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 5;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 7, delay: 2, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var btnchooselicense: UIButton!
    @IBOutlet weak var lbllicense: UILabel!
    @IBOutlet weak var viewdoc3: UIView!
    @IBOutlet weak var viewdoc2: UIView!
    @IBOutlet weak var btnchooseqtrid: UIButton!
    @IBOutlet weak var lblqtrid: UILabel!
    @IBOutlet weak var btnchoosepassport: UIButton!
    @IBOutlet weak var lblpassport: UILabel!
    @IBOutlet weak var viewdoc1: UIView!
    @IBOutlet weak var viewuploading: UIView!
    @IBOutlet weak var txtfldpassword: UITextField!
    @IBOutlet weak var btnGuest: UIButton!
    @IBOutlet weak var txtfldusername: UITextField!
    
    var loginmodel=Loginviewmodel()
    var cartflag=Int()
     var doclist:[Doc_listdocument]?=[]
    var typedoc_array=[String]()
    let bag=DisposeBag()
    var imageToUpload = UIImage()
    var model=ProfileViewModel()
    var fblink=String()
    var instagaramlink=String()
    var whatsappnumber=String()
    var contactnumber=String()
     var address=String()
    var cart:[Giftbook]?=[]
    var imageurlgift=String()
    
    @IBOutlet weak var btnlogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
   txtfldusername.placeholeder(name: "Username")
  // txtfldusername.setLeftPaddingPoints(30)
   txtfldpassword.placeholeder(name: "Password")
   //txtfldpassword.setLeftPaddingPoints(30)
        txtfldusername.setcornerradius1()
        txtfldpassword.setcornerradius1()
        viewdoc1.layer.cornerRadius=10
        viewdoc2.layer.cornerRadius=10
        viewdoc3.layer.cornerRadius=10
        btnchooseqtrid.layer.cornerRadius=10
        btnchoosepassport.layer.cornerRadius=10
        btnchooselicense.layer.cornerRadius=10
        btnlogin.layer.cornerRadius=25
      hideKeyboardWhenTappedAround()
        let height=UIScreen.main.bounds.height
        print("height",height)


        
//        let statusBarFrame = UIApplication.shared.statusBarFrame
//        let statusBarView = UIView(frame: statusBarFrame)
//        self.view.addSubview(statusBarView)
//        statusBarView.backgroundColor = .clear
    
        
      
       // deleteAllData("Cartdetail")
        if self.cartflag==1{

            self.btnGuest.isHidden = true
        }
        
    }
    
    
   
    func deleteAllData(_ entity:String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let managedContext = appDelegate.persistentContainer.viewContext
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    

    
    
    @IBAction func btnloginaction(_ sender: Any) {
        if (txtfldusername.text == ""){
            
            self.showToast(message: "Please enter your email", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }else if (txtfldpassword.text == ""){
            
            self.showToast(message: "Please enter password", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
//        }else if !isPasswordValid(txtfldpassword.text!){
//
//            self.showToast(message: "Invalid Password", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
//
//        }
        
//        else if(txtfldpassword.text!.count < 8){
//
//            self.showToast(message: "Please enter password with atleast 8 characters", font: UIFont.boldSystemFont(ofSize: 13), duration: 2)
//        }
            
            
        } else if !isValidEmail(testStr: txtfldusername.text!){
            
            self.showToast(message: "Please enter valid email id", font: UIFont.boldSystemFont(ofSize: 15), duration: 2)
            
        }else{
            loginmodel.email=txtfldusername.text ?? ""
            loginmodel.password=txtfldpassword.text ?? ""
            loginmodel.user_login{ (model) in
                self.userlogindata(data:model)
            }
        }
    
    }
        
        func isValidEmail(testStr:String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
//        func isPasswordValid(_ password : String) -> Bool{
//            let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^[A-Za-z\\d$@$#!%*?&]{1,}")
//            return passwordTest.evaluate(with: password)
//        }
    
    
    
    func userlogindata(data: Loginclass) {
        print("data",data)
        
        let status=data.status
        if status==true{
            DispatchQueue.main.async{
                UserDefaults.standard.set(data.data?.user_id, forKey: "userid")
                let image=(data.user_image_path ?? "")+(data.data?.user_image ?? "")
                UserDefaults.standard.set("https://www.outing.qa/user_image/user.png", forKey: "notimage")
                UserDefaults.standard.set(image, forKey: "userimage")
                self.showToast(message: data.message!, font: .boldSystemFont(ofSize: 13), duration: 2)
              if self.cartflag==1{

                self.navigationController?.popViewController(animated: true)
              }else{
//               let home = self.storyboard?.instantiateViewController (withIdentifier: "HomeViewController") as! HomeViewController
//               self.navigationController?.pushViewController(home, animated: true)
                    let home = self.storyboard?.instantiateViewController (withIdentifier: "SideMenuViewController") as! SideMenuViewController
                   
                    if self.cart?.count ?? 0>0{
                        home.cart=self.cart
                        home.imageurlgift=self.imageurlgift
                    }
                    self.navigationController?.pushViewController(home, animated: true)
                }
                
            }
        }else{
            DispatchQueue.main.async{
                self.showToast(message: data.message!, font:  .boldSystemFont(ofSize: 13), duration: 2)
               
            }
        }
    }
    
    
    func document_listdata(data: Doclistclass) {
     
     let status=data.status
     if status==true{
         
       doclist=data.data?.doc_list
    
      if doclist?.count ?? 0 > 1{
              for i in 0...(doclist?.count ?? 0)-1{
                  typedoc_array.append(doclist?[i].type ?? "")
              }
                 }else{
                     typedoc_array.append(doclist?[0].type ?? "")
                 }
        
      DispatchQueue.main.async{
      
          if self.typedoc_array.contains("2"){
              self.viewdoc2.isHidden=true
      }
          if self.typedoc_array.contains("3"){
              self.viewdoc3.isHidden=true
      }
          if self.typedoc_array.contains("1"){
              self.viewdoc1.isHidden=true
      }
      }
      if typedoc_array.contains("1") && typedoc_array.contains("2")  && typedoc_array.contains("3"){
           DispatchQueue.main.async{
        self.viewuploading.isHidden=true
            let cart1 = self.storyboard?.instantiateViewController (withIdentifier: "CartViewController") as! CartViewController
            
             if self.cart?.count ?? 0>0{
                 cart1.cart=self.cart
                 cart1.imageurlgift=self.imageurlgift
             }
             self.navigationController?.pushViewController(cart1, animated: true)
            
        }
      }else{
             self.viewuploading.isHidden=false
        }
    
        
     }else{
         DispatchQueue.main.async{
            self.viewuploading.isHidden=false
        }
        
        }
    }
    
    
    
    
   
        
    @IBAction func btnforgotpswrdaction(_ sender: Any) {
        let register = self.storyboard?.instantiateViewController (withIdentifier: "ForgotpasswordViewController") as! ForgotpasswordViewController
              if cart?.count ?? 0>0{
                register.cart=cart
                register.imageurlgift=imageurlgift
                }
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func btnregisteraction(_ sender: Any) {
        let register = self.storyboard?.instantiateViewController (withIdentifier: "RegisterViewController") as! RegisterViewController
        if cart?.count ?? 0>0{
        register.cart=cart
        register.imageurlgift=imageurlgift
        }
        register.cartflag=cartflag
                      self.navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func btncontinueasguestaction(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController (withIdentifier: "SideMenuViewController") as! SideMenuViewController
           if cart?.count ?? 0>0{
           home.cart=cart
           home.imageurlgift=imageurlgift
           }
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func uploadUserData(documentType: String) {
        model.updateUserData(image: imageToUpload, type: documentType).subscribe(
            onNext:{ data in
                print("API success")
                self.loginmodel.document_list{ (model) in
                    self.document_listdata(data:model)
                }
        },onError:{ error in
            self.showToast(message: "Error in uploading please try again later", font: .boldSystemFont(ofSize: 13), duration: 2)
            print("error")
        }).disposed(by: bag)
    }
    
    
    
    func pickImage(type: DocumentType) {
        ImagePickerManager().pickImage(self) { image in
            // self.imageViewUser.image = image
            self.imageToUpload = image
            
            switch type {
            case .passport:
                self.uploadUserData(documentType: "1")
            case .license:
                self.uploadUserData(documentType: "3")
            case .qatarId:
                self.uploadUserData(documentType: "2")
            }
        }
    }
    
    
    @IBAction func btnpasspotcation(_ sender: Any) {
        self.pickImage(type: .passport)
    }
    @IBAction func btnqtridaction(_ sender: Any) {
        self.pickImage(type: .qatarId)
    }
    
    @IBAction func btnlicensecation(_ sender: Any) {
         self.pickImage(type: .license)
    }
    
    @IBAction func btncloseviewaction(_ sender: Any) {
        self.viewuploading.isHidden=true
    }
    
}
