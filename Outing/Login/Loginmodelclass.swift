//
//  Loginmodelclass.swift
//  Outing
//
//  Created by Arun Vijayan on 14/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
struct Loginclass : Codable {
    let status : Bool?
    let message : String?
    let data : UserloginData?
    let user_image_path : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
        case user_image_path = "user_image_path"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(UserloginData.self, forKey: .data)
        user_image_path = try values.decodeIfPresent(String.self, forKey: .user_image_path)
    }

}
struct UserloginData : Codable {
    let user_id : String?
    let username : String?
    let email_id : String?
    let mobile : String?
    let dob : String?
    let password : String?
    let last_login : String?
    let user_image : String?
    let device_id : String?
    let device_token : String?
    let latitude : String?
    let longitude : String?
    let source : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case username = "username"
        case email_id = "email_id"
        case mobile = "mobile"
        case dob = "dob"
        case password = "password"
        case last_login = "last_login"
        case user_image = "user_image"
        case device_id = "device_id"
        case device_token = "device_token"
        case latitude = "latitude"
        case longitude = "longitude"
        case source = "source"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        last_login = try values.decodeIfPresent(String.self, forKey: .last_login)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
//document_list

struct doclistData : Codable {
    let doc_list : [Doc_listdocument]?
    let doc_url : String?

    enum CodingKeys: String, CodingKey {

        case doc_list = "doc_list"
        case doc_url = "doc_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        doc_list = try values.decodeIfPresent([Doc_listdocument].self, forKey: .doc_list)
        doc_url = try values.decodeIfPresent(String.self, forKey: .doc_url)
    }

}

struct Doc_listdocument : Codable {
    let d_id : String?
    let user_id : String?
    let type : String?
    let document : String?
    let status : String?
    let created_at_date : String?

    enum CodingKeys: String, CodingKey {

        case d_id = "d_id"
        case user_id = "user_id"
        case type = "type"
        case document = "document"
        case status = "status"
        case created_at_date = "created_at_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        d_id = try values.decodeIfPresent(String.self, forKey: .d_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        document = try values.decodeIfPresent(String.self, forKey: .document)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
    }

}



struct Doclistclass : Codable {
    let status : Bool?
    let message : String?
    let data : doclistData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(doclistData.self, forKey: .data)
    }

}
//contactus

struct Contactusclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
//delete_doc
struct Deletedocclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
//forgotpassword
struct Forgotpasswordclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

//createnewpassword



struct Createnewpasswrdclass : Codable {
    let data : createpasswordData?
    let status : Bool?
    let message : String?
    let user_image_path : String?

    enum CodingKeys: String, CodingKey {

        case data = "data"
        case status = "status"
        case message = "message"
        case user_image_path = "user_image_path"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(createpasswordData.self, forKey: .data)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        user_image_path = try values.decodeIfPresent(String.self, forKey: .user_image_path)
    }

}




struct createpasswordData : Codable {
    let user_id : String?
    let username : String?
    let email_id : String?
    let mobile : String?
    let dob : String?
    let password : String?
    let last_login : String?
    let user_image : String?
    let device_id : String?
    let device_token : String?
    let latitude : String?
    let longitude : String?
    let source : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case username = "username"
        case email_id = "email_id"
        case mobile = "mobile"
        case dob = "dob"
        case password = "password"
        case last_login = "last_login"
        case user_image = "user_image"
        case device_id = "device_id"
        case device_token = "device_token"
        case latitude = "latitude"
        case longitude = "longitude"
        case source = "source"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        last_login = try values.decodeIfPresent(String.self, forKey: .last_login)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
//writereview

struct Writereviewclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
struct Profile_editclass : Codable {
    let status : Bool?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

//social

struct SocialData : Codable {
    let details : [SocialDetails]?

    enum CodingKeys: String, CodingKey {

        case details = "details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        details = try values.decodeIfPresent([SocialDetails].self, forKey: .details)
    }

}




struct SocialDetails : Codable {
    let social_id : String?
    let fb : String?
    let instagram : String?
    let contact_number : String?
    let whatsapp : String?
    let address : String?
    let created_at_date : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case social_id = "social_id"
        case fb = "fb"
        case instagram = "instagram"
        case contact_number = "contact_number"
        case whatsapp = "whatsapp"
        case address = "address"
        case created_at_date = "created_at_date"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        social_id = try values.decodeIfPresent(String.self, forKey: .social_id)
        fb = try values.decodeIfPresent(String.self, forKey: .fb)
        instagram = try values.decodeIfPresent(String.self, forKey: .instagram)
        contact_number = try values.decodeIfPresent(String.self, forKey: .contact_number)
        whatsapp = try values.decodeIfPresent(String.self, forKey: .whatsapp)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        created_at_date = try values.decodeIfPresent(String.self, forKey: .created_at_date)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}


struct Socialclass : Codable {
    let status : Bool?
    let message : String?
    let data : SocialData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SocialData.self, forKey: .data)
    }

}
