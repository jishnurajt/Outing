//
//  Loginviewmodel.swift
//  Outing
//
//  Created by Arun Vijayan on 14/09/20.
//  Copyright © 2020 Arun Vijayan. All rights reserved.
//

import Foundation
public class Loginviewmodel{
    var password=String()
    var deviceid=1
    var devicetoken=1
    var email=String()
    var user_id=String()
    var name=String()
    var emailcontact=String()
    var phone=String()
    var message=String()
    var docid=String()
    var emailforpassword=String()
    var otpforpassword=String()
    var  passwordforcreatepassword=String()
    var bookingidforreview=String()
    var messagereview=String()
    var usernameedited=String()
    var dobedited=String()
    var addressedited=String()
    
    func user_login(completion : @escaping (Loginclass) -> ())  {
        

        let poststring="email_id=\(email)&password=\(password)&security_token=out564bkgtsernsko&device_id=\(deviceid)&device_token=\(devicetoken)"
        
        var request = NSMutableURLRequest(url: APIEndPoint.user_login.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Loginclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    func document_list(completion : @escaping (Doclistclass) -> ())  {
        if let userid=UserDefaults.standard.value(forKey: "userid"){
            user_id=UserDefaults.standard.value(forKey: "userid") as! String
        }

        let poststring="user_id=\(user_id)&security_token=out564bkgtsernsko"
        
        var request = NSMutableURLRequest(url: APIEndPoint.document_list.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Doclistclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
       
    
    func contact(completion : @escaping (Contactusclass) -> ())  {
           

           let poststring="mailid=\(emailcontact)&name=\(name)&security_token=out564bkgtsernsko&phonenumber=\(phone)&message=\(message)"
           
           var request = NSMutableURLRequest(url: APIEndPoint.contact.url as URL)
           request.httpMethod = "POST"
           request.httpBody = poststring.data(using: String.Encoding.utf8)
           let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
               print("poststring",poststring)
               //self.showIndicator(isHidden: true)
               if error != nil{
                   print("error",error)
                   return
               }
               let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
               
               print("respnsedate,\(responsestring)")
               do {
                   
                  
                       let decoder = JSONDecoder()
                       let model = try decoder.decode(Contactusclass.self, from:
                           data!) //Decode JSON Response Data
                       print(model)
                   completion(model)
                   
                   
               } catch let error as NSError {
               }
           }
           task.resume()
       }
      
    func delete_doc(completion : @escaping (Deletedocclass) -> ())  {
         

         let poststring="d_id=\(docid)"
         
         var request = NSMutableURLRequest(url: APIEndPoint.delete_doc.url as URL)
         request.httpMethod = "POST"
         request.httpBody = poststring.data(using: String.Encoding.utf8)
         let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
             print("poststring",poststring)
             //self.showIndicator(isHidden: true)
             if error != nil{
                 print("error",error)
                 return
             }
             let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
             
             print("respnsedate,\(responsestring)")
             do {
                 
                
                     let decoder = JSONDecoder()
                     let model = try decoder.decode(Deletedocclass.self, from:
                         data!) //Decode JSON Response Data
                     print(model)
                 completion(model)
                 
                 
             } catch let error as NSError {
             }
         }
         task.resume()
     }
    
    
    func forgot_password(completion : @escaping (Forgotpasswordclass) -> ())  {
        

        let poststring="email_id=\(emailforpassword)"
        
        var request = NSMutableURLRequest(url: APIEndPoint.forgot_password.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Forgotpasswordclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
      func create_new_password(completion : @escaping (Createnewpasswrdclass) -> ())  {
          

          let poststring="email_id=\(emailforpassword)&password=\(passwordforcreatepassword)&otp=\(otpforpassword)"
          
          var request = NSMutableURLRequest(url: APIEndPoint.create_new_password.url as URL)
          request.httpMethod = "POST"
          request.httpBody = poststring.data(using: String.Encoding.utf8)
          let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
              print("poststring",poststring)
              //self.showIndicator(isHidden: true)
              if error != nil{
                  print("error",error)
                  return
              }
              let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
              
              print("respnsedate,\(responsestring)")
              do {
                  
                 
                      let decoder = JSONDecoder()
                      let model = try decoder.decode(Createnewpasswrdclass.self, from:
                          data!) //Decode JSON Response Data
                      print(model)
                  completion(model)
                  
                  
              } catch let error as NSError {
              }
          }
          task.resume()
      }
    
      func add_review(completion : @escaping (Writereviewclass) -> ()){
          

          let poststring="user_id=\(user_id)&review=\(messagereview)&booking_id=\(bookingidforreview)&rating=2&security_token=out564bkgtsernsko"
          
          var request = NSMutableURLRequest(url: APIEndPoint.add_review.url as URL)
          request.httpMethod = "POST"
          request.httpBody = poststring.data(using: String.Encoding.utf8)
          let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
              print("poststring",poststring)
              //self.showIndicator(isHidden: true)
              if error != nil{
                  print("error",error)
                  return
              }
              let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
              
              print("respnsedate,\(responsestring)")
              do {
                  
                 
                      let decoder = JSONDecoder()
                      let model = try decoder.decode(Writereviewclass.self, from:
                          data!) //Decode JSON Response Data
                      print(model)
                  completion(model)
                  
                  
              } catch let error as NSError {
              }
          }
          task.resume()
      }
    
    func profile_edit(completion : @escaping (Profile_editclass) -> ()){
        if let userid=UserDefaults.standard.value(forKey: "userid"){
            user_id=UserDefaults.standard.value(forKey: "userid") as! String
        }
let poststring="user_id=\(user_id)&dob=\(dobedited)&username=\(usernameedited)&security_token=out564bkgtsernsko&address=\(addressedited)"
        print("poststring",poststring)
        var request = NSMutableURLRequest(url: APIEndPoint.profile_edit.url as URL)
        request.httpMethod = "POST"
        request.httpBody = poststring.data(using: String.Encoding.utf8)
        let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
            print("poststring",poststring)
            //self.showIndicator(isHidden: true)
            if error != nil{
                print("error",error)
                return
            }
            let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print("respnsedate,\(responsestring)")
            do {
                
               
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Profile_editclass.self, from:
                        data!) //Decode JSON Response Data
                    print(model)
                completion(model)
                
                
            } catch let error as NSError {
            }
        }
        task.resume()
    }
    
    func social(completion : @escaping (Socialclass) -> ()){
           
    let poststring="security_token=out564bkgtsernsko"
            print("poststring",poststring)
            var request = NSMutableURLRequest(url: APIEndPoint.social.url as URL)
            request.httpMethod = "POST"
            request.httpBody = poststring.data(using: String.Encoding.utf8)
            let task=URLSession.shared.dataTask(with: request as URLRequest){data,response,error in
                print("poststring",poststring)
                //self.showIndicator(isHidden: true)
                if error != nil{
                    print("error",error)
                    return
                }
                let responsestring=NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                print("respnsedate,\(responsestring)")
                do {
                    
                   
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(Socialclass.self, from:
                            data!) //Decode JSON Response Data
                        print(model)
                    completion(model)
                    
                    
                } catch let error as NSError {
                }
            }
            task.resume()
        }
    
    

}
